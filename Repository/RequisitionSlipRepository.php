<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\DemoBundle\TerminalbdDemoBundle;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlip;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Service\Image;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionSlipRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionSlip::class);
    }


    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {


            $data = $form['requisition_filter_form'];
            $branch = isset($data['branch'])? $data['branch'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $createdBy = isset($data['createdBy']) ? $data['createdBy'] : '';
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $branch = isset($data['branch']) ? $data['branch'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $priroty = isset($data['priority']) ? $data['priority'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $requisitionMode = !empty($data['requisitionMode'])? trim($data['requisitionMode']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';

            if (!empty($process) && $process == 'RUNNING') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['New', 'Checked']);
            }
            if (!empty($process) && $process == 'HISTORY') {
                $qb->andWhere('e.process IN (:processes)')->setParameter('processes', ['Approved']);
            }
            if (!empty($createdBy)) {
                $qb->andWhere('e.createdBy = :createdBy')->setParameter('createdBy', $createdBy);
            }
            if (!empty($requisitionNo)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if (!empty($requisitionMode)) {
                $qb->andWhere($qb->expr()->like("e.requisitionMode", "'%$requisitionMode%'"));
            }
            if(!empty($branch)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($unit)){
                $qb->andWhere('e.companyUnit = :unit')->setParameter('unit',$unit);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($priroty)) {
                $qb->andWhere('p.id = :priorityId')->setParameter('priorityId', $priroty);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.requisitionNo LIKE :searchTerm  OR b.name LIKE :searchTerm  OR t.name LIKE :searchTerm OR d.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }

        }
    }


    /**
     * @return Purchase[]
     */
    public function findBySearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo', 'e.created as created','e.updated as updated', 'e.processOrdering as ordering','e.expectedDate as expectedDate', 'e.subTotal as subTotal', 'e.total as total', 'e.process as process', 'e.waitingProcess as waitingProcess','e.requisitionMode as requisitionMode','e.path as path');
        $qb->addSelect('ben.name as benificiery');
        $qb->addSelect('v.companyName as vendor');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('d.name as department');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->leftJoin('e.benificiery','ben');
        $qb->leftJoin('e.vendor','v');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.processDepartment', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.isDelete != 1');
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "my-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess NOT IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "paymented"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Paymented'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function insertReverseRequsition(RequisitionSlip $requisition)
    {
        $em = $this->_em;
        $module = "requisition-slip";
        $entity = new RequisitionSlip();
        $entity->setConfig($requisition->getConfig());
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($requisition->getCreatedBy()->getTerminal(),$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
        }
        $entity->setFromReverse($requisition);
        $entity->setRequisitionMode($requisition->getRequisitionMode());
        $entity->setCreatedBy($requisition->getCreatedBy());
        $entity->setProcess("New");
        $entity->setContent($requisition->getContent());
        $entity->setWaitingProcess("New");
        $entity->setCompanyUnit($requisition->getCompanyUnit());
        if($requisition->getProcessDepartment()){
            $entity->setProcessDepartment($requisition->getProcessDepartment());
        }
        if($requisition->getDepartment()){
            $entity->setDepartment($requisition->getDepartment());
        }
        $em->persist($entity);
        $em->flush();
        if($requisition->getAdditionalItems()){

            /* @var $row JobRequisitionAdditionalItem  */

            foreach ($requisition->getAdditionalItems() as $row){
                $requsitionItem = new JobRequisitionAdditionalItem();
                $requsitionItem->setRequisitionSlip($entity);
                $requsitionItem->setName($row->getName());
                $requsitionItem->setUnit($row->getUnit());
                $requsitionItem->setDescription($row->getDescription());
                $requsitionItem->setPrice($row->getPrice());
                $requsitionItem->setQuantity($row->getQuantity());
                $requsitionItem->setSubTotal($requsitionItem->getPrice() * $row->getQuantity());
                $em->persist($requsitionItem);
                $em->flush();
            }
        }
        $requisition->setReverse($entity);
        $requisition->setProcess('Reversed');
        $requisition->setWaitingProcess('Reversed');
        $em->flush();
        return $entity;
    }

    public function getLastRequisitionSlip(RequisitionSlip $entity)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.benificiery','b');
        $qb->leftJoin('e.vendor','v');
        $qb->where('e.id != :id')->setParameter('id', $entity->getId());
        if($entity->getMode() == "vendor"){
            $qb->andWhere('v.id = :identifier')->setParameter('identifier', $entity->getVendor()->getId());
        }else{
            $qb->andWhere('b.id = :identifier')->setParameter('identifier', $entity->getBenificiery()->getId());
        }
        $qb->andWhere("e.waitingProcess = 'Approved'");
        $qb->setMaxResults(3);
        $qb->orderBy('e.id','DESC');
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function generateStoreRequisition(Requisition $requisition, $process = "")
    {
        $em = $this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' => $terminal, 'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row) {
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition' => $requisition, 'moduleProcessItem' => $row));
            if (empty($exist)) {
                $entity = new ProcurementProcess();
                $entity->setRequisition($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */

        if (!empty($requisition->getRequisitionItems())) {

            foreach ($requisition->getRequisitionItems() as $item) {

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function insertAttachmentFile(JobRequisition $requisition, $data, $files)
    {
        $em = $this->_em;

        define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
        define('DESTINATION', 'uploads/procurement/');
        define('RESIZEBY', 'w');
        define('FILESIZE', 10097152);
        define('RESIZETO', 520);
        define('QUALITY', 100);

        if (isset($files)) {
            $errors = array();
            foreach ($files['tmp_name'] as $key => $tmp_name) {
                if(!empty($tmp_name)){
                    $fileName = $requisition->getRequisitionNo() . '-' . time() . "-" . $_FILES['files']['name'][$key];
                    $fileType = $_FILES['files']['type'][$key];
                    $fileSize = $_FILES['files']['size'][$key];

                    if ($fileSize > FILESIZE) {
                        $errors[] = 'File size must be less than 2 MB';
                    }
                    if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        $image = new Image($_FILES['files']['tmp_name'][$key]);
                        $image->destination = DESTINATION . $fileName;
                        $image->constraint = RESIZEBY;
                        $image->size = RESIZETO;
                        $image->quality = QUALITY;
                        $image->render();
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } elseif (empty($errors) == true and $fileType = "application/pdf") {
                        $file_tmp = $_FILES['files']['tmp_name'][$key];
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        if (is_dir(DESTINATION . $fileName) == false) {
                            move_uploaded_file($file_tmp, DESTINATION . $fileName);
                        }
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } else {
                        return $errors;
                    }
                }
            }
        }
    }

    public function getDmsAttchmentFile(JobRequisition $entity)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(DmsFile::class,'e');
        $qb->select('e.id', 'e.created as created','e.name as name', 'e.fileName as fileName');
        $qb->where('e.sourceId = :sourceId')->setParameter('sourceId',"{$entity->getId()}");
        $qb->andWhere('e.bundle = :bundle')->setParameter('bundle',"Procurement");
        $qb->andWhere('e.module = :module')->setParameter('module',"Job-Requisition");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function itemDeletes($entity)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $delete = $qb->delete(RequisitionItem::class, 'e')->where('e.jobRequisition = ?1')->setParameter(1, $entity)->getQuery();
        if($delete){
            $delete->execute();
        }
    }


}
