<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Requisition::class);
    }


    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){
            $data = $form['requisition_filter_form'];
            $createdBy = isset($data['createdBy'])? $data['createdBy'] :'';
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $jobRequisition = !empty($data['jobRequisition'])? trim($data['jobRequisition']) :'';
            $budget = !empty($data['budget'])? trim($data['budget']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';
            $processOrdering = !empty($data['processOrdering'])? trim($data['processOrdering']) :'';
            $processPepartment = !empty($data['processPepartment'])? trim($data['processPepartment']) :'';
            $requisitionMode = !empty($data['requisitionMode'])? trim($data['requisitionMode']) :'';
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($jobRequisition)){
                $qb->andWhere($qb->expr()->like("jb.requisitionNo", "'%$jobRequisition%'"));
            }
            if(!empty($createdBy)){
                 $qb->andWhere('u.id = :userid')->setParameter('userid',$createdBy);
            }

            if(!empty($processPepartment)){
                $qb->andWhere('e.processOrdering >  1');
                $qb->andWhere('e.reportTo IS NOT NULL');
                $qb->andWhere('e.budgetApprove !=  1');
            }
            if (!empty($requisitionMode)) {
                $qb->andWhere($qb->expr()->like("e.requisitionMode", "'%$requisitionMode%'"));
            }

            if(!empty($department)){
                 $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
             if(!empty($unit)){
                 $qb->andWhere('e.companyUnit = :companyUnit')->setParameter('companyUnit',$unit);
            }
            if(!empty($priroty)){
                 $qb->andWhere('p.id = :priority')->setParameter('priroty',$priroty);
            }
            if(!empty($processOrdering)){
                 $qb->andWhere('e.processOrdering = 1');
            }
            if(!empty($requisitionType)){
                 $qb->andWhere('t.id = :requisitionType')->setParameter('requisitionType',$requisitionType);
            }
            if(!empty($budget) and $budget == "budget"){
                 $qb->andWhere('br.createdBy IS NOT NULL');
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($keyword)) {
                $qb->andWhere('e.requisitionNo LIKE :searchTerm OR jb.requisitionNo LIKE :searchTerm OR cu.name LIKE :searchTerm  OR t.name LIKE :searchTerm OR d.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }

        }
    }



    /**
     * @return Requisition[]
     */

    public function processModuleLists( $config, User $user, $data = "")
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'requisition');
        return $this->findRoleBaseSearchQuery( $config,$user,$data);

    }

    /**
     * @return Requisition[]
     */

    public function findBankBaseSearchQuery( $config, User $user, $data = "" ): array
    {
        $em = $this->_em;
        $terminal = $user->getTerminal()->getId();
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'store-requisition');
        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process', 'e.processOrdering as ordering','e.waitingProcess as waitingProcess');
        $qb->addSelect('mp.id as moduleId','mp.approveType as moduleApproveType');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('approve.id as approveId','approve.assignToRole as assignToRole','approve.assignToRoleName as roleToName');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->join('e.moduleProcess','mp');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.approveProcess', 'approve');
        $qb->leftJoin('approve.assignTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "role" and !empty($user->getApprovalRole())){
            $qb->andWhere('approve.assignToRole =:role')->setParameter('role',$user->getApprovalRole()->getRoleName());
        }elseif($data['mode'] == "approve" and $moduleProcess->getApproveType() == "user"){
            $qb->andWhere('approve.assignTo =:assignTo')->setParameter('assignTo',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */

    public function findUserBaseSearchQuery( $config, User $user, $data = "" ): array
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.processOrdering as ordering', 'e.process as process','e.requisitionMode as requisitionMode', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        }
        if(!empty($user->getProfile()) and $user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif(!empty($user->getProfile()) and $user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findRequisitionSearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
     //   $qb->andWhere('e.requisitionMode IN (:requisitionModes)')->setParameter('requisitionModes', array('CAPEX','OPEX','Expense','Cofidential'));
        if($data['mode'] == "in-progress" ) {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "my-progress"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "rejected"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Rejected','Reversed'));
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('In-progress','New','Reversed'));
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findRequisitionArchivedSearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
   //     $qb->andWhere('e.requisitionMode IN (:requisitionModes)')->setParameter('requisitionModes', array('CAPEX','OPEX','Expense'));
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findRequisitionInprogressSearchQuery( $config)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere("e.financialYear = '2024-2025'");
        $qb->andWhere('e.requisitionMode IN (:requisitionModes)')->setParameter('requisitionModes', array('OPEX','Expense'));
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('In-progress','New'));
        $result = $qb->getQuery()->getResult();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findBillEntrySearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.requisitionMode =:requisitionMode')->setParameter('requisitionMode', "Expense");
        $qb->andWhere('e.isDelete != 1');
        if($data['mode'] == "in-progress" ) {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "my-progress"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "rejected"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Rejected','Reversed'));
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('In-progress','New'));
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findConfidentialSearchQuery( $config,User $user, $data = "" )
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.requisitionMode =:requisitionMode')->setParameter('requisitionMode', "confidential");
        $qb->andWhere('e.isDelete != 1');
        if($data['mode'] == "in-progress" ) {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "my-progress"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "rejected"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Rejected','Reversed'));
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('In-progress','New'));
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }elseif($data['mode'] == "assaign"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere("brat.id != 'NULL'");
        }elseif($data['mode'] == "archived"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array('Approved','Closed'));
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findRequisitionBudgetProcessQuery( $config,User $user, $data = "" )
    {


        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $department = $user->getProfile()->getDepartment()->getId();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $company = isset($data['company']) ? $data['company']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.requisitionNo as fromReverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('brat.name as budgetUser');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('br.id IS NULL');
        if($data['mode'] == "in-progress" ) {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "my-progress"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',86);
        }
        if($company){
            $qb->andWhere('b.id =:company')->setParameter('company',$company);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function RequisitionBudgetCompanyBaseCount( $config,$financialYear)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as bucketCount','b.code as name','b.id as id');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.reportTo =:report')->setParameter('report',86);
        $qb->andWhere('e.financialYear =:financialYear')->setParameter('financialYear',$financialYear);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('br.id IS NULL');
        $qb->groupBy('b.id');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function RequisitionBudgetProcessCount( $config)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as bucketCount');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.budgetRequisition', 'br');
        $qb->leftJoin('br.createdBy', 'brat');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.reportTo =:report')->setParameter('report',86);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('br.id IS NULL');
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findPurchaseOrderSearchQuery( $config,User $user, $data = "" ): array
    {

        $group = $user->getTerminal()->getBusinessGroup()->getSlug();
        $report = isset($data[0]['reportTo']) ? $data[0]['reportTo']:'';
        $sort = isset($data['sort'])? $data['sort'] :'e.updated';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.updated as updated','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.isEmergency as isEmergency', 'e.isRevised as isRevised');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('reverse.id as reverseId','reverse.requisitionNo as reverseRequisitionNo');
        $qb->addSelect('d.name as department');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('jb.requisitionNo as jobRequisitionNo,jb.id as jobRequisition');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('SUM(ri.quantity) as quantity','SUM(ri.issueQuantity) as issueQuantity','SUM(ri.workorderQuantity) as workorderQuantity','SUM(ri.receiveQuantity) as receiveQuantity');
        $qb->leftJoin('e.jobRequisition','jb');
        $qb->leftJoin('e.requisitionItems','ri');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tender', 'tender');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete = 0');
        $qb->andWhere('e.waitingProcess =:wProcess')->setParameter('wProcess', "Approved");
        $qb->andWhere('e.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX','INV'));
        if($data['mode'] == "list") {
            $qb->andWhere("e.process !='Closed'");
        }elseif($data['mode'] == "pending") {
            $qb->andWhere("ri.issueQuantity = 0");
        }elseif($data['mode'] == "partial") {
            $qb->andWhere("ri.issueQuantity != 0");
            $qb->andWhere('ri.quantity > ri.issueQuantity');
            $qb->andWhere("e.process != 'Closed'");
        }elseif($data['mode'] == "archive") {
            $qb->andWhere("e.process = 'Closed'");
        }
        $qb->groupBy('e.id');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }


    /**
     * @return Requisition[]
     */
    public function findBySearchQuery( $config, User $user, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.processOrdering as ordering','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('rto.id as reportToId','rto.name as reportToName');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.createdBy','u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id = :initior')->setParameter('initior',$user->getId());
        $qb->andWhere('e.isDelete != 1');
        if($user->getProfile()->getBranch()){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($user->getProfile()->getDepartment()){
            $dId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :dId')->setParameter('dId',$dId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function findByApproveQuery( $config, User $user, $data = "" ): array
    {

        $workingArea = $user->getProfile()->getServiceMode()->getSlug();
        $roleId =  $user->getUserGroupRole()->getId();
        $process = $user->getUserGroupRole()->getProcess();

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.processOrdering as ordering','e.process as process','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('e.waitingProcess = :waitingProcess')->setParameter('waitingProcess',$process);
        $qb->andWhere('e.process != :process')->setParameter('process','Rejected');
        if($workingArea == "branch"){
            $branchId = $user->getProfile()->getBranch()->getId();
            $qb->andWhere('b.id = :branchId')->setParameter('branchId',$branchId);
        }elseif($workingArea == "department"){
            $branchId = $user->getProfile()->getDepartment()->getId();
            $qb->andWhere('d.id = :branchId')->setParameter('branchId',$branchId);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Purchase[]
     */
    public function findByStoreApproveQuery( $config, User $user, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total','e.process as process','e.processOrdering as ordering','e.filename as filename');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('d.name as department');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('u.name as createdBy');
        $qb->join('e.createdBy','u');
        $qb->leftJoin('e.branch','b');
        $qb->leftJoin('e.processDepartment','d');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('e.waitingProcess = :waitingProcess')->setParameter('waitingProcess','Approved');
        $qb->andWhere('t.slug = :type')->setParameter('type','store-requisition');
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function generateStoreRequisition(Requisition $requisition , $process = "")
    {
        $em =$this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' =>$terminal,'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row){
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition'=>$requisition,'moduleProcessItem'=>$row));
            if(empty($exist)){
                $entity = new ProcurementProcess();
                $entity->setRequisition($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }

    }

    public function requisitionItemHistory(Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function insertReverseRequsition(Requisition $requisition)
    {
        $em = $this->_em;
        $module = "purchase-requisition";
        $entity = new Requisition();
        $entity->setConfig($requisition->getConfig());
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($requisition->getCreatedBy()->getTerminal(),$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
        }
        $entity->setRequisitionMode($requisition->getRequisitionMode());
        if( $requisition->getJobRequisition()){
            $entity->setJobRequisition($requisition->getJobRequisition());
            $remaingAmount = ($requisition->getJobRequisition()->getTotal() - $requisition->getJobRequisition()->getRequisitionAmount());
            $entity->setJobApprovalAmount($remaingAmount);
        }
        $entity->setFromReverse($requisition);
        $entity->setBusinessGroup('garment');
        $entity->setCreatedBy($requisition->getCreatedBy());
        $entity->setProcess("New");
        $entity->setContent($requisition->getContent());
        $entity->setWaitingProcess("New");
        $entity->setCompanyUnit($requisition->getCompanyUnit());
        $entity->setSection($requisition->getSection());
        if($requisition->getProcessDepartment()){
            $entity->setProcessDepartment($requisition->getProcessDepartment());
        }
        if($requisition->getDepartment()){
            $entity->setDepartment($requisition->getDepartment());
        }
        if($requisition->getPriority()){
            $entity->setPriority($requisition->getPriority());
        }
        $em->persist($entity);
        $em->flush();
        if($entity->getCompanyUnit()){
            $em->getRepository(ComapnyRequisitionShare::class)->insertRequisitionShareSingle($entity);
        }
        if($requisition->getRequisitionItems()){

            /* @var $row RequisitionItem  */

            foreach ($requisition->getRequisitionItems() as $row){

                $requsitionItem = new RequisitionItem();
                $requsitionItem->setRequisition($entity);
                $requsitionItem->setBudgetHead($row->getBudgetHead());
                $requsitionItem->setCategory($row->getCategory());
                $requsitionItem->setSection($row->getSection());
                $requsitionItem->setUom($row->getUom());
                $requsitionItem->setItem($row->getItem());
                $requsitionItem->setStock($row->getStock());
                $requsitionItem->setStockBook($row->getStockBook());
                $requsitionItem->setQuantity($row->getQuantity());
                $requsitionItem->setApproveQuantity($row->getQuantity());
                $requsitionItem->setActualQuantity($row->getQuantity());
                $requsitionItem->setPrice($row->getPrice());
                $stockBook = $row->getStockBook();
                if($stockBook->getPrice()){
                    $requsitionItem->setPrice($stockBook->getPrice());
                }
                $requsitionItem->setCurrentPrice($stockBook->getCmp());
                if($stockBook->getCmp()){
                    $requsitionItem->setCmpDate($stockBook->getUpdated());
                }
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if($lastPurchase){
                    $requsitionItem->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $requsitionItem->setLastPurchasePrice($lastPurchase['price']);
                    $requsitionItem->setLastPurchaseDate($lastPurchase['updated']);
                }
                $requsitionItem->setSubTotal($requsitionItem->getPrice() * $row->getQuantity());
                $em->persist($requsitionItem);
                $em->flush();
            }
        }
        $requisition->setReverse($entity);
        $requisition->setProcess('Reversed');
        $requisition->setTotal(0);
        $requisition->setWaitingProcess('Reversed');
        $em->flush();
        return $entity;
    }

    public function insertRequsitionFromJobRequisition(JobRequisition $jobRequisition,$financial_year)
    {
        $em = $this->_em;
        $module = "purchase-requisition";
        $entity = new Requisition();
        $entity->setJobRequisition($jobRequisition);
        $entity->setConfig($jobRequisition->getConfig());
        $entity->setFinancialYear($financial_year);
        $moduleProcess = $em->getRepository(ModuleProcess::class)->existModuleProcess($jobRequisition->getCreatedBy()->getTerminal(),$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
        }
        $entity->setRequisitionMode($jobRequisition->getRequisitionMode());
        $remaingAmount = ($jobRequisition->getTotal() - $jobRequisition->getRequisitionAmount());
        $entity->setJobApprovalAmount($remaingAmount);
        $entity->setBusinessGroup('garment');
        $entity->setCreatedBy($jobRequisition->getCreatedBy());
        $entity->setRequisitionMode($jobRequisition->getRequisitionMode());
        $entity->setProcess("New");
        $entity->setContent($jobRequisition->getContent());
        $entity->setWaitingProcess("New");
        $entity->setDepartment($jobRequisition->getDepartment());
        $entity->setCompanyUnit($jobRequisition->getCompanyUnit());
        $entity->setProcessDepartment($jobRequisition->getProcessDepartment());
        $em->persist($entity);
        $em->flush();
        if($jobRequisition->getRequisitionItems()){

            /* @var $row RequisitionItem  */

            foreach ($jobRequisition->getRequisitionItems() as $row){

                $requsitionItem = new RequisitionItem();
                $requsitionItem->setRequisition($entity);
                if($row->getCategory()){
                    $requsitionItem->setCategory($row->getCategory());
                    if($row->getCategory()->getGeneralLedger()){
                        $requsitionItem->setBudgetHead($row->getCategory()->getGeneralLedger());
                    }
                }
                $requsitionItem->setItem($row->getItem());
                $requsitionItem->setStock($row->getStock());
                $requsitionItem->setStockBook($row->getStockBook());
                if($row->getBrand()){
                    $requsitionItem->setBrand($row->getBrand());
                }
                if($row->getSize()) {
                    $requsitionItem->setSize($row->getSize());
                }
                if($row->getColor()) {
                    $requsitionItem->setColor($row->getColor());
                }
                $requsitionItem->setQuantity($row->getQuantity());
                $requsitionItem->setApproveQuantity($row->getQuantity());
                $requsitionItem->setActualQuantity($row->getQuantity());
                $requsitionItem->setRemainigQuantity($row->getQuantity());
                $requsitionItem->setPrice($row->getPrice());
                $requsitionItem->setDescription($row->getDescription());
                $stockBook = $row->getStockBook();
                if($stockBook->getPrice()){
                    $requsitionItem->setPrice($stockBook->getPrice());
                }
                $requsitionItem->setCurrentPrice($stockBook->getCmp());
                if($stockBook->getCmp()){
                    $requsitionItem->setCmpDate($stockBook->getUpdated());
                }
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if($lastPurchase){
                    $requsitionItem->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $requsitionItem->setLastPurchasePrice($lastPurchase['price']);
                    $requsitionItem->setLastPurchaseDate($lastPurchase['updated']);
                }
                $requsitionItem->setSubTotal($requsitionItem->getPrice() * $row->getQuantity());
                $em->persist($requsitionItem);
                $em->flush();
                if ($entity->getRequisitionMode() != "CAPEX") {
                    $em->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity);
                }
            }

        }
        return $entity;
    }

    public function inserCanbanItem(Requisition $requisition, $data)
    {
        $em = $this->_em;
            foreach ($data['item'] as $row){

                /* @var $stock StockBook */

                $quantity = $data["quantity-{$row}"];
                $stock = $em->getRepository(StockBook::class)->find($row);
                $requsitionItem = new RequisitionItem();
                $requsitionItem->setRequisition($requisition);
                $requsitionItem->setStockBook($stock);
                $requsitionItem->setItem($stock->getItem());
                $requsitionItem->setStock($stock->getStock());
                $requsitionItem->setActualQuantity($quantity);
                $requsitionItem->setQuantity($quantity);
                $requsitionItem->setRemainigQuantity($quantity);
                $requsitionItem->setPrice($stock->getPrice());
                if($stock->getBrand()){
                    $requsitionItem->setBrand($stock->getBrand());
                }
                if($stock->getSize()){
                    $requsitionItem->setSize($stock->getSize());
                }
                if($stock->getColor()){
                    $requsitionItem->setColor($stock->getColor());
                }
                if($requsitionItem->getQuantity() > 0){
                    $requsitionItem->setSubTotal($stock->getPrice() * doubleval($requsitionItem->getQuantity()));
                }
                $em->persist($requsitionItem);
                $em->flush();

            }
        $em->flush();

    }

    public function insertAttachmentFile(Requisition $requisition, $data, $files)
    {
        $em = $this->_em;

        define('FILETYPE', array('jpg', 'png', 'jpeg', 'pdf'));
        define('DESTINATION', 'uploads/procurement/');
        define('RESIZEBY', 'w');
        define('FILESIZE', 10097152);
        define('RESIZETO', 520);
        define('QUALITY', 100);

        if (isset($files)) {
            $errors = array();
            foreach ($files['tmp_name'] as $key => $tmp_name) {

                if($tmp_name){

                    $fileName = $requisition->getRequisitionNo() . '-' . time() . "-" . $_FILES['files']['name'][$key];
                    $fileType = $_FILES['files']['type'][$key];
                    $fileSize = $_FILES['files']['size'][$key];

                    if ($fileSize > FILESIZE) {
                        $errors[] = 'File size must be less than 2 MB';
                    }
                    if (empty($errors) == true and in_array($fileType, FILETYPE)) {
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        $image = new Image($_FILES['files']['tmp_name'][$key]);
                        $image->destination = DESTINATION . $fileName;
                        $image->constraint = RESIZEBY;
                        $image->size = RESIZETO;
                        $image->quality = QUALITY;
                        $image->render();
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Job-Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } elseif (empty($errors) == true and $fileType = "application/pdf") {
                        $file_tmp = $_FILES['files']['tmp_name'][$key];
                        if (is_dir(DESTINATION) == false) {
                            mkdir(DESTINATION, 0777);        // Create directory if it does not exist
                        }
                        if (is_dir(DESTINATION . $fileName) == false) {
                            move_uploaded_file($file_tmp, DESTINATION . $fileName);
                        }
                        $entity = new DmsFile();
                        $entity->setSourceId($requisition->getId());
                        $entity->setBundle("Procurement");
                        $entity->setModule("Requisition");
                        if($data['labelName'][$key]){
                            $entity->setName($data['labelName'][$key]);
                        }
                        $entity->setFileName($fileName);
                        $em->persist($entity);
                        $em->flush();
                    } else {
                        return $errors;
                    }
                }

            }
        }
    }

    public function getDmsAttchmentFile(Requisition $entity)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->from(DmsFile::class,'e');

        $qb->select('e.id', 'e.created as created','e.updated as updated','e.name as name', 'e.fileName as fileName');
        $qb->where('e.sourceId = :sourceId')->setParameter('sourceId',"{$entity->getId()}");
        $qb->andWhere('e.bundle = :bundle')->setParameter('bundle',"Procurement");
        $qb->andWhere('e.module = :module')->setParameter('module',"purchase-requisition");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function requisitionBudgetGenerate(Requisition $entity)
    {

        $month = strtolower($entity->getCreated()->format('F'));
        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionBudgetItem::class,'e');
        $qb->join('e.glBudgetMonth','glbudgetmonth');
        $qb->join('glbudgetmonth.gl','gl');
        $qb->join('e.requisition','requisition');
        $qb->join('e.company','b');
        $qb->select('e.requisitionAmount as requisitionAmount','e.monthlyBudget as monthlyBudget','e.monthlyReminigBudget as monthlyReminigBudget','e.yearlyBudget as yearlyBudget','e.yearlyRemainigBudget as  yearlyRemainigBudget','e.yearlyExpenseBudget as yearlyExpenseBudget','e.yearlyHoldBudget as yearlyHoldBudget','e.lowStatus as lowStatus');
        $qb->addSelect('e.id as id','e.budgetAdjustmentAmount as adjustmentAmount','e.budgetAmendmentAmount as amendmentAmount','e.budgetTillAmount as tillAmount');
        $qb->addSelect('b.id as branchId','b.name as branchName');
        $qb->addSelect('glbudgetmonth.month as month');
        $qb->addSelect('gl.id as glId','gl.generalLedgerCode as glCode','gl.name as glName');
        $qb->where('e.requisition = :requisition')->setParameter('requisition',$entity->getId());
    //    $qb->andWhere('requisition.waitingProcess = :process')->setParameter('process','In-progress');
        $qb->groupBy('b.id','gl.id');
        $result = $qb->getQuery()->getArrayResult();
        $companyBudgets = array();
        $companies = array();
        if($result){
            foreach ($result as $id){
                $companyBudgets[$id['branchId']] = $result;
                $companies[$id['branchId']] = array('companyId'=> $id['branchId'],'comapnyName'=> $id['branchName']);
            }
        }
        $low = $this->checkBudgetLowStatus($entity);
        return array('lowStatus' => $low, 'companies' => $companies, 'monthlyBudget'=> $companyBudgets);
    }

    public function checkBudgetLowStatus($entity)
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionBudgetItem::class,'e');
        $qb->select('COUNT(e.id) as lowStatus');
        $qb->where('e.requisition = :requisition')->setParameter('requisition',$entity);
        $qb->andWhere('e.lowStatus = 1');
        $low = $qb->getQuery()->getSingleScalarResult();
        if($low > 0){
            return 1;
        }else{
            return 0;
        }
    }

    public function workorderwithBudgetExpense(TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->join('e.tenderWorkorder','workorder');
        $qb->join('e.gl','gl');
        $qb->join('e.requisition','requisition');
        $qb->select('sum(e.subTotal) workorderAmount','sum(e.vat) vat');
        $qb->addSelect('requisition.id as requisitionId');
        $qb->addSelect('gl.id as glId');
        $qb->where('workorder.requisition = :requisitionId')->setParameter('requisitionId',$workorder->getRequisition()->getId());
        $qb->andWhere("workorder.waitingProcess = 'Approved'");
        $qb->andWhere("requisition.requisitionMode != 'CAPEX'");
        $qb->groupBy('gl.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $row){
           $find = $em->getRepository(RequisitionBudgetItem::class)->findOneBy(array('requisition'=>$row['requisitionId'],'gl'=>$row['glId']));
           if($find){
               $find->setExpenseRequisition($row['workorderAmount']);
               $find->setHoldBudget($find->getRequisitionAmount() - $row['workorderAmount']);
               $find->setVatExpense($row['vat']);
               $em->flush();
           }
        }
        /* @var $item RequisitionBudgetItem */

        foreach ($workorder->getWorkOrderItems() as $workOrderItem) {

            if ($workOrderItem->getRequisition()->getRequisitionMode() != "CAPEX"){
                foreach ($workOrderItem->getRequisition()->getBudgetItems() as $item):
                    $glMonth = $item->getGlBudgetMonth();
                    $budget = $this->monthlyRequisitionSummary($glMonth);
                    $holdRemain = ((float)$budget['amount'] - (float)$budget['expense']);
                    $glMonth->setHoldAmount($holdRemain);
                    $glMonth->setExpense($budget['expense']);
                    $glMonth->setVatExpense($budget['vat']);
                    $em->flush();
                    $em->getRepository(BudgetMonth::class)->remainingBudget($glMonth);
                endforeach;
            }
        }
    }


    private function monthlyRequisitionSummary($glMonth)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionBudgetItem::class,'e');
        $qb->select('sum(e.requisitionAmount) as amount','sum(e.expenseRequisition) as expense','sum(e.vatExpense) as vat');
        $qb->where('e.glBudgetMonth = :id')->setParameter('id',$glMonth->getId());
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

    public function monthlyRequisitionExpenseProcess(Requisition $requisition){
        $em = $this->_em;

        /* @var $requisitionItem RequisitionItem */
        if($requisition->getRequisitionMode() == "Expense" ) {
            $requisition->setUpdated($requisition->getCreated());
            foreach ($requisition->getRequisitionItems() as $requisitionItem):
                $requisitionItem->setIssueQuantity($requisitionItem->getQuantity());
                $requisitionItem->setRemainigQuantity(0);
                $em->persist($requisitionItem);
                $em->flush();
            endforeach;
        }
        /* @var $item RequisitionBudgetItem */

        if($requisition->getBudgetItems()){
            foreach ($requisition->getBudgetItems() as $item):
                $item->setExpenseRequisition($item->getRequisitionAmount());
                $item->setHoldBudget(0);
                $em->persist($item);
                $em->flush();
                $em->getRepository(BudgetMonth::class)->remainingBudget($item->getGlBudgetMonth());
            endforeach;
            $this->monthlyRequisitionExpense($requisition);
        }
        $em->flush();

    }

    public function monthlyRequisitionExpense(Requisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionBudgetItem */
        foreach ($requisition->getBudgetItems() as $item):
            $glMonth = $item->getGlBudgetMonth();
            $budget = $this->monthlyRequisitionSummary($glMonth);
            $holdRemain = ((float)$budget['amount'] - (float) $budget['expense']);
            $glMonth->setHoldAmount($holdRemain);
            $glMonth->setExpense($budget['expense']);
            $em->flush();
            $em->getRepository(BudgetMonth::class)->remainingBudget($glMonth);

        endforeach;
    }

    public function getUserBaseRequisition($config,$users)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('e.waitingProcess IN(:waitingprocess)')->setParameter('waitingprocess',array('Approved','Closed'));
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }



    public function getUserBaseHODRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.waitingProcess =:approve')->setParameter('approve','In-progress');
        $qb->andWhere('e.processOrdering =1');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }

     public function getUserBaseInprogressRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.waitingProcess IN (:approve)')->setParameter('approve',array('In-progress'));
        $qb->andWhere('e.processOrdering =1');
        $qb->andWhere('e.isDelete != 1');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }

    public function getUserBaseProcessDepRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.waitingProcess =:approve')->setParameter('approve','In-progress');
        $qb->andWhere('e.processOrdering >  1');
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('e.reportTo IS NOT NULL');
        $qb->andWhere('e.budgetApprove !=  1');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;
    }


    public function getUserBaseBudgetRequisition($config,$users)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as count');
        $qb->addSelect('u.id as uid');
        $qb->join('e.createdBy','u');
        $qb->join('e.budgetRequisition', 'br');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->andWhere('e.waitingProcess =:approve')->setParameter('approve','In-progress');
        $qb->andWhere('e.isDelete != 1');
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        $arrs = array();
        foreach ($result as $row){
            $arrs[$row['uid']] = $row;
        }
        return $arrs;

    }

    public function pendingWorkorder($config , $users)
    {
        $qb = $this->createQueryBuilder('r');
        $qb->join('r.createdBy','u');
        $qb->select('COUNT(r.id) as count');
        $qb->addSelect('u.id as userId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX'));
        $qb->andWhere("r.process !='Closed'");
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->groupBy('userId');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['userId']] = $row;
        }
        return $data;
    }

    public function updateJobApproval(JobRequisition $jobRequisition)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('r');
        $qb->select('SUM(r.subTotal) as amount');
        $qb->where('r.jobRequisition = :config')->setParameter('config',"{$jobRequisition->getId()}");
        $qb->andWhere('r.isDelete != 1');
        $amount = $qb->getQuery()->getSingleScalarResult();
        $jobRequisition->setRequisitionAmount($amount);
        $em->persist($jobRequisition);
        $em->flush();
    }

    public function removeJobApproval(Requisition $requisition)
    {
        $em = $this->_em;
        $jobRequisition = $requisition->getJobRequisition();
        $qb = $this->createQueryBuilder('r');
        $qb->select('SUM(r.subTotal) as amount');
        $qb->where('r.jobRequisition = :config')->setParameter('config',"{$jobRequisition->getId()}");
        $qb->andWhere('r.isDelete != 1');
        $amount = $qb->getQuery()->getSingleScalarResult();
        if($amount > 0 ){
            $jobRequisition->setRequisitionAmount($amount);
        }else{
            $jobRequisition->setRequisitionAmount(0);
        }
        $em->persist($jobRequisition);
        $em->flush();
    }

    public function itemDeletes($entity)
    {
        $em = $this->_em;
        $id = $entity->getId();
        $qb = $em->createQueryBuilder();
        $delete = $qb->delete(RequisitionItem::class, 'e')->where('e.requisition = ?1')->setParameter(1, $id)->getQuery();
        if($delete){
            $delete->execute();
        }
    }

    public function updatePurchaseOrderItem(Requisition $requisition)
    {
        $em = $this->_em;
        foreach ($requisition->getRequisitionItems() as $item):
            /* @var $item RequisitionItem */
            $issueQty = $em->getRepository(TenderItemDetails::class)->countTenderQuantity($item);
            $item->setIssueQuantity($issueQty);
            $item->setRemainigQuantity($item->getQuantity() - $issueQty);
            $em->persist($item);
            $em->flush();
        endforeach;
    }

    public function pendingRequisition($config)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder('e');
        $qb->from(RequisitionItem::class,'ri');
        $qb->select('COUNT(e.requisitionNo) as total');
        $qb->join('ri.requisition','e');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete = 0');
        $qb->andWhere('ri.issueQuantity = 0');
        $qb->andWhere('e.waitingProcess =:wProcess')->setParameter('wProcess', "Approved");
        $qb->andWhere('e.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX'));
        $qb->groupBy('e.id');
        $count = $qb->getQuery()->getArrayResult();
        return COUNT($count);

    }

    public function partialRequisition($config)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('COUNT(e.id) as total');
        $qb->leftJoin('e.requisitionItems','ri');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete = 0');
        $qb->andWhere('e.waitingProcess =:wProcess')->setParameter('wProcess', "Approved");
        $qb->andWhere("e.process != 'Closed'");
        $qb->andWhere('e.requisitionMode IN (:mode)')->setParameter('mode', array('CAPEX','OPEX'));
        $qb->groupBy('e.id');
        $qb->andWhere('ri.issueQuantity != 0');
        $qb->andWhere('ri.quantity > ri.issueQuantity');
        $count = $qb->getQuery()->getArrayResult();
        return COUNT($count);

    }

    public function rejectFromBudget(BudgetRequisition $budgetRequisition){
        $entity = $budgetRequisition->getRequisition();
        $em = $this->_em;
        $entity->setProcess('Rejected');
        $entity->setWaitingProcess('Rejected');
        $entity->setReportTo(null);
        $entity->setComment($budgetRequisition->getComment());
        $entity->setSubTotal(0);
        $entity->setRejectedBy($budgetRequisition->getCreatedBy());
        $entity->setProcessOrdering(0);
        $em->persist($entity);
        $em->flush();
        $this->deleteBudgetItem($entity);
        if($entity->getJobRequisition()){
            $this->removeJobApproval($entity);
        }
    }
    public function deleteBudgetItem(Requisition $entity)
    {
        $em = $this->_em;
        $id = $entity->getId();
        $qb = $em->createQueryBuilder();
        $delete = $qb->delete(RequisitionBudgetItem::class, 'e')->where('e.requisition = ?1')->setParameter(1, $id)->getQuery();
        if($delete){
            $delete->execute();
        }
    }

    /**
     * @return Requisition[]
     */
    public function approveRequisitionBudgetChecked( $config,User $user )
    {

        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('e.reportTo = 86');
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array("In-progress"));
        $qb->andWhere('e.requisitionMode IN (:requisitionMode)')->setParameter('requisitionMode', array("Expense"));
        $datetime = new \DateTime("now");

        $startDate = $datetime->format('Y-m-01 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);

        $endDate = $datetime->format('Y-m-t 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);

        $result = $qb->getQuery()->getResult();
        foreach ($result as $row){
           $em->getRepository(BudgetRequisition::class)->insertExistingPurchaseRequisition($row,$user);
        }
    }


    public function approvedRequisitionMissingBudget($config,$financialYear)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.budgetRequisition','br');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.isDelete != 1');
        $qb->andWhere('br.id IS NULL');
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', array("Closed","Approved"));
        $qb->andWhere('e.requisitionMode IN (:requisitionMode)')->setParameter('requisitionMode', array("Expense","OPEX"));
        $qb->andWhere('br.requisition IS NULL');
        $datetime = new \DateTime("now");
        $startDate = $datetime->format('2023-01-01 00:00:00');
        $qb->andWhere("e.updated >= :startDate");
        $qb->setParameter('startDate', $startDate);

        $endDate = $datetime->format('Y-m-t 23:59:59');
        $qb->andWhere("e.updated <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getResult();
    //    dd($result);
        foreach ($result as $row){
            $em->getRepository(BudgetRequisition::class)->approvedPurchaseRequisitionProcess($row,$financialYear);
        }
    }
    
}
