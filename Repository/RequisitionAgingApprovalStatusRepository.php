<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionAgingApprovalStatusRepository extends EntityRepository
{
    public function getMonthWiseStatus($filterBy)
    {
        $startDate = isset($filterBy['startDate']) ? new \DateTime($filterBy['startDate']): date('Y-m-d');
        $endDate = isset($filterBy['endDate']) ? new \DateTime($filterBy['endDate']): date('Y-m-d');

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.approvedBy', 'approvedBy');
        $qb->join('e.requisition', 'requisition');
        $qb->select('e.days', 'approvedBy.id AS approvedById', 'approvedBy.name AS approvedByName');
        if (isset($filterBy['startDate'])){
            $qb->where('e.createdAt >= :startDate')->setParameter('startDate', $startDate);
        }
        if (isset($filterBy['enddate'])){
            $qb->andWhere('e.createdAt <= :endDate')->setParameter('endDate', $endDate);
        }


        $records = $qb->getQuery()->getArrayResult();

        $data = [];
        foreach ($records as $record) {
            $data[$record['approvedById']]['approvedBy'] = $record['approvedByName'];
            if ((int)$record['days'] >= 5){
                $data[$record['approvedById']]['requisition']['5'][] = $record;
            }else{
                $data[$record['approvedById']]['requisition'][$record['days']][] = $record;
            }
        }
        return $data;
    }
    public function getMonthWiseStatusDetails($approvedById, $filterBy, $day)
    {
        $startDate = isset($filterBy['startDate']) ? new \DateTime($filterBy['startDate']): date('Y-m-d');
        $endDate = isset($filterBy['endDate']) ? new \DateTime($filterBy['endDate']): date('Y-m-d');

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.approvedBy', 'approvedBy');
        $qb->join('e.requisition', 'requisition');
        $qb->select('requisition.requisitionNo', 'requisition.created');
        $qb->where('approvedBy.id =:approvedById')->setParameter('approvedById', $approvedById);
        if (isset($filterBy['startDate'])){
            $qb->andWhere('e.createdAt >= :startDate')->setParameter('startDate', $startDate);
        }
        if (isset($filterBy['enddate'])){
            $qb->andWhere('e.createdAt <= :endDate')->setParameter('endDate', $endDate);
        }
        if (null != $day){
            if ($day >= 5){
                $qb->andWhere('e.days >=:day')->setParameter('day', $day);
            }else{
                $qb->andWhere('e.days =:day')->setParameter('day', $day);

            }

        }

        return $qb->getQuery()->getArrayResult();
    }
}
