<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetGlYear;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetYear;
use Terminalbd\BudgetBundle\Entity\MonthCalendar;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlipItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionSlipItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionSlipItem::class);
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){
            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $generalLedger = isset($data['generalLedger'])? $data['generalLedger'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $heads = isset($data['head'])? $data['head'] :'';
            $glHead = isset($data['glHead'])? $data['glHead'] :'';
            $requisitionMode = isset($data['requisitionMode'])? $data['requisitionMode'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $itemName = !empty($data['itemName'])? trim($data['itemName']) :'';
            $createdBy = !empty($data['createdBy'])? trim($data['createdBy']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
             if(!empty($generalLedger)){
                 $qb->andWhere("e.budgetHead = :budgetHead")->setParameter('budgetHead', $generalLedger);
            }
            if (!empty($requisitionMode)) {
                $qb->andWhere($qb->expr()->like("r.requisitionMode", "'%$requisitionMode%'"));
            }
            if(!empty($itemName)){
                $qb->andWhere($qb->expr()->like("item.name", "'%$itemName%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere("r.createdBy = :createdBy")->setParameter('createdBy', $createdBy);
            }
            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($heads)) {
                $qb->andWhere("gl.id IN (:cids)")->setParameter('cids', $heads);
            }
            if(!empty($branch)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
            }
             if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($unit)){
                $qb->andWhere('unit.id = :cuunit')->setParameter('cuunit',$unit);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.updated >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }
            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.updated <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    protected function handleReportBetween($qb,$form)
    {
        if(isset($form)){
            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $section = isset($data['section'])? $data['section'] :'';
            $unit    = isset($data['unit'])? $data['unit'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $keyword            = !empty($data['keyword'])? trim($data['keyword']) :'';
            $item               = isset($data['item'])? $data['item'] :'';
            $itemCode           = isset($data['itemCode'])? $data['itemCode'] :'';
            $category           = isset($data['category'])? $data['category'] :'';
            $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            $department         = isset($data['department'])? $data['department'] :'';


            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("r.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }
            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("r.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($category)) {
                $qb->andWhere("cat.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($unit)) {
                $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
            }
            if (!empty($section)) {
                $qb->andWhere("sec.id = :sectionId")->setParameter('sectionId', $section);
            }
            if (!empty($department)) {
                $qb->andWhere("d.id = :did")->setParameter('did', $department);
            }
            if (!empty($item)) {
                $qb->andWhere("item.id = :pid")->setParameter('pid', $item);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }


        }
    }



    public function getItemWisePrReport($config,$data)
    {

        $config = $config->getId();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.comapnyRequisitionShares','cm');
        $qb->leftJoin('r.section','sec');
        $qb->leftJoin('r.createdBy','u');
        $qb->leftJoin('u.profile','profile');
        $qb->leftJoin('r.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->leftJoin('r.department','d');
        $qb->join('e.stockBook','sb');
        $qb->join('sb.item','item');
        $qb->leftJoin('item.unit','unit');
        $qb->leftJoin('sb.category','cat');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','e.subTotal as subTotal','e.remainigQuantity as remaining');
        $qb->addSelect('r.created as created','r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate','r.requisitionMode as requisitionMode');
        $qb->addSelect('b.code as company');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('cat.name as category');
        $qb->addSelect('d.name as department');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('unit.name as uom');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
     //   $this->handleReportBetween($qb,$data);
        $qb->orderBy('r.created','ASC');
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
      //  dd($result);
        return $result;

    }

    public function getItemSummary(Requisition $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.requisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $vat = $this->getCalculationVat($subTotal);
            $invoice->setVat($vat);
            $total = $vat + $subTotal;
            $invoice->setTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function getJobItemSummary(JobRequisition $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.jobRequisition','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $invoice->setAdditionalSubTotal($invoice->getAdditionalSubTotal());
            $vat = $this->getCalculationVat($subTotal);
            $invoice->setVat($vat);
            $total = ($invoice->getAdditionalSubTotal() + $subTotal + $vat);
            $invoice->setTotal($total);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setAdditionalSubTotal(0);
            $invoice->setVat(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function getCalculationVat($totalAmount)
    {
        $vat = ( ($totalAmount * (int)10)/100 );
        //$vat = ( ($totalAmount * (int)$sales->getRestaurantConfig()->getVatPercentage())/100 );
        return round($vat);
    }

    public function insertJobRequisitionItem(JobRequisition $requisition, $data)
    {


        $em = $this->_em;
        if(isset($data['item']) and $data['item'] > 0 and $data['quantity'] > 0) {

            /* @var $stock Stock */
            $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));

            /* @var $entity RequisitionItem */
            $entity = new RequisitionItem();
            $entity->setJobRequisition($requisition);
            if ($data['quantity']) {
                $entity->setQuantity(abs($data['quantity']));
                $entity->setApproveQuantity(abs($data['quantity']));
                $entity->setActualQuantity(abs($data['quantity']));
                $entity->setRemainigQuantity(abs($data['quantity']));
            }
            if ($data['brand']) {
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if ($data['size']) {
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if ($data['color']) {
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }
            if ($data['category']) {
                $category = $em->getRepository(Category::class)->find($data['category']);
                $entity->setCategory($category);
                if($requisition->getRequisitionMode() == "OPEX" and $category->getGeneralLedger()){
                    $entity->setBudgetHead($category->getGeneralLedger());
                }
            }
            if(isset($data['description']) and $data['description']){
                $entity->setDescription($data['description']);
            }
            $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $data);
            $exist = $this->findOneBy(array('jobRequisition'=>$requisition,'stockBook'=>$stockBook));
            if ($stockBook and empty($exist) and $stockBook->getPrice() > 0) {
                $entity->setStockIn($stockBook->getStockIn() - $stockBook->getStockOut());
                $entity->setStockBook($stockBook);
                $entity->setPrice($stockBook->getPrice());
                $entity->setCurrentPrice($stockBook->getCmp());
                $entity->setCmp($stockBook->getCmp());
                $entity->setCmpDate($stockBook->getUpdated());
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if ($lastPurchase) {
                    $entity->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $entity->setLastPurchasePrice($lastPurchase['price']);
                    $entity->setLastPurchaseDate($lastPurchase['updated']);
                }
                $entity->setStock($stock);
                $entity->setItem($stock->getItem());
                $entity->setName($stock->getItem()->getName());
                $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
            }

        }

    }

    public function insertGarmentRequisitionItem(Requisition $requisition, $data)
    {
        $em = $this->_em;
        if(isset($data['item']) and $data['item'] > 0 and $data['quantity'] > 0) {

            /* @var $stock Stock */

          //  dd($data);

            $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
            /* @var $entity RequisitionItem */

            $entity = new RequisitionItem();
            $entity->setRequisition($requisition);
            if (isset($data['created']) and $data['created']) {
                $created = new \DateTime($data['created']);
                $entity->setCreated($created);
            }
            if (isset($data['price']) and $data['price']) {
                $entity->setPrice(abs($data['price']));
            }
            if (isset($data['description']) and $data['description']) {
                $entity->setDescription($data['description']);
            }
            if (isset($data['remark']) and $data['remark']) {
                $entity->setRemark($data['remark']);
            }
            if (isset($data['uom']) and $data['uom']) {
                $entity->setUom($data['uom']);
            }
            if (isset($data['section']) and $data['section']) {
                $section = $em->getRepository(Setting::class)->find($data['section']);
                $entity->setSection($section);
            }
            if ($data['quantity']) {
                $entity->setQuantity(abs($data['quantity']));
                $entity->setApproveQuantity(abs($data['quantity']));
                $entity->setActualQuantity(abs($data['quantity']));
                $entity->setRemainigQuantity(abs($data['quantity']));
            }
            if (isset($data['brand']) and $data['brand']) {
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if (isset($data['size']) and $data['size']) {
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if (isset($data['color']) and $data['color']) {
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }

            /* @var $stockBook StockBook */

            $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $data);
            $exist = $this->findOneBy(array('requisition' => $requisition, 'stockBook' => $stockBook));

            if ($stockBook and empty($exist) and in_array($requisition->getRequisitionMode() , ["CAPEX","OPEX"])) {
                $entity->setStockBook($stockBook);
                if ($data['category']) {
                    $category = $em->getRepository(Category::class)->find($data['category']);
                    $entity->setCategory($category);
                    if (in_array($requisition->getRequisitionMode(), ['OPEX', 'Expense',"Confidential"]) and $category->getGeneralLedger()) {
                        $entity->setBudgetHead($category->getGeneralLedger());
                    }
                }
                $entity->setStockIn($stockBook->getStockIn() - $stockBook->getStockOut());
                $entity->setPrice($stockBook->getPrice());
                $entity->setCurrentPrice($stockBook->getCmp());
                $entity->setCmp($stockBook->getCmp());
                $entity->setCmpDate($stockBook->getUpdated());
                $lastPurchase = $em->getRepository(StockHistory::class)->getLastPurchaseItemInformation($stockBook->getId());
                if ($lastPurchase) {
                    $entity->setLastPurchaseQuantity($lastPurchase['purchaseQuantity']);
                    $entity->setLastPurchasePrice($lastPurchase['price']);
                    $entity->setLastPurchaseDate($lastPurchase['updated']);
                }
                $entity->setStock($stock);
                $entity->setItem($stock->getItem());
                $entity->setName($stock->getItem()->getName());
                $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
                /* This function use for specific requisition budget generate */
                if ($requisition->getRequisitionMode() != "CAPEX") {
                    $em->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition);
                }
            }elseif ($stockBook and in_array($requisition->getRequisitionMode() , ["Expense","Confidential"])) {
                $entity->setStockBook($stockBook);
                if ($data['category']) {
                    $category = $em->getRepository(Category::class)->find($data['category']);
                    $entity->setCategory($category);
                    if(in_array($requisition->getRequisitionMode(),['OPEX','Expense',"Confidential"]) and $category->getGeneralLedger()){
                        $entity->setBudgetHead($category->getGeneralLedger());
                    }
                }
                $entity->setStock($stock);
                $entity->setItem($stock->getItem());
                $entity->setName($stock->getItem()->getName());
                $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                $em->persist($entity);
                $em->flush();
                /* This function use for specific requisition budget generate */
                if ($requisition->getRequisitionMode() != "CAPEX") {
                    $em->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition);
                }
            }

        }
    }

    public function insertStoreItem(Requisition $requisition, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        if($stock){
            $lastReceiveItem = $this->getLastBranchItem($requisition,$stock);
            if($stock and $data['quantity'] > 0){
                $exist = $this->findOneBy(array('requisition'=> $requisition,'stock' => $stock));
                if(empty($exist)){
                    $entity = new RequisitionItem();
                    $entity->setRequisition($requisition);
                    $entity->setQuantity($data['quantity']);
                    $entity->setApproveQuantity($data['quantity']);
                    $entity->setActualQuantity($data['quantity']);
                    $entity->setRemainigQuantity($data['quantity']);
                    if($data['stockIn']){
                        $entity->setStockIn($data['stockIn']);
                    }
                    $entity->setDescription($data['description']);
                    $entity->setStock($stock);
                    $entity->setItem($stock->getItem());
                    $entity->setName($stock->getItem()->getName());
                    $entity->setPrice($stock->getPurchasePrice());
                    if(!empty($lastReceiveItem)){
                        $item = $this->find($lastReceiveItem);
                        $entity->setLastRequisitionItem($item);
                    }
                    $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                    $em->persist($entity);
                    $em->flush();
                }else{
                    $entity = $exist;
                    $entity->setQuantity($data['quantity']);
                    $entity->setApproveQuantity($data['quantity']);
                    $entity->setActualQuantity($data['quantity']);
                    $entity->setRemainigQuantity($data['quantity']);
                    $entity->setStockIn($data['stockIn']);
                    $entity->setDescription($data['description']);
                    $entity->setSubTotal($entity->getPrice() * $entity->getQuantity());
                    $em->persist($entity);
                    $em->flush();
                }

            }
        }
    }

    public function insertBankStoreItem(Requisition $requisition, RequisitionItem $entity)
    {
        $em = $this->_em;
        $lastReceiveItem = $this->getLastBranchItem($requisition,$entity->getStock());
        if(!empty($lastReceiveItem)){
            $item = $this->find($lastReceiveItem);
            $entity->setLastRequisitionItem($item);
            $em->flush();
        }
    }

    public function updateRequisitionItemQuantity($requisition, $items)
    {
        $em = $this->_em;
        foreach ($items as $itemId => $quantity){
            /* @var $entity RequisitionItem */
            $entity = $this->find($itemId);
            if($entity){
                $entity->setApproveQuantity($entity->getQuantity());
                $entity->setQuantity($quantity);
                $entity->setRemainigQuantity($entity->getQuantity());
                $entity->setSubTotal($quantity * $entity->getPrice());
                $em->persist($entity);
                $em->flush();
            }
        }

        $this->getItemSummary($requisition);
    }

    public function prOpenItem($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.comapnyRequisitionShares','cm');
        $qb->leftJoin('cm.branch','b');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('r.processDepartment','d');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','it');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.workorderQuantity as workorderQuantity','e.receiveQuantity as receiveQuantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('GROUP_CONCAT(b.name) as branches');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('it.name as uom');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("CAPEX","OPEX"));
        $qb->andWhere('e.isClose = 1');
        $qb->andHaving('remaining > 0');
        $qb->orderBy('e.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function requisitionItemInprogressReport($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.createdBy','u');
        $qb->leftJoin('e.section','s');
        $qb->leftJoin('r.reportTo','reportTo');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.processDepartment','d');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','it');
        $qb->leftJoin('e.budgetHead','gl');
        $qb->leftJoin('e.category','category');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.description as description','e.remark as remark','e.workorderQuantity as workorderQuantity','e.receiveQuantity as receiveQuantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate','r.waitingProcess as process');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('s.name as section');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('b.name as branches');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('it.name as uom');
        $qb->addSelect('u.name as createdBy');
        $qb->addSelect('reportTo.name as approvedBy');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',array("New","In-progress"));
        $qb->andWhere('r.isDelete != 1');
        $qb->orderBy('e.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery();
        return $result;
    }

    public function requisitionItemReport($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.processDepartment','d');
        $qb->leftJoin('r.priority','p');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('item.unit','itemUnit');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.actualQuantity as actualQuantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining', '(e.quantity * e.price) AS subTotal', 'e.created');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('b.name AS companyName');
        $qb->addSelect('itemUnit.name AS uom');
        $qb->addSelect('category.name AS categoryName');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("CAPEX","OPEX"));
        $qb->andWhere('r.isDelete != 1');
        $qb->orderBy('e.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery();
        return $result;
    }

    public function requisitionItemBudgetReport($config , $mode = "", $data = "")
    {
        $em = $this->_em;
        $glGroupHead = isset($data['requisition_filter_form']['glGroupHead']) ? $data['requisition_filter_form']['glGroupHead'] : '';
        $head = isset($data['requisition_filter_form']['head']) ? $data['requisition_filter_form']['head'] : '';
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->join('r.budgetRequisition','br');
        $qb->leftJoin('e.section','itemSec');
        $qb->leftJoin('r.section','sec');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->join('e.budgetHead','gl');
        $qb->leftJoin('gl.department','d');
        $qb->leftJoin('r.priority','p');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','itemUnit');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.actualQuantity as actualQuantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining', '(e.quantity * e.price) AS subTotal', 'e.updated as created', 'e.remark as remark', 'e.description as specification');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.created as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.generalLedgerCode  as glCode','gl.name  as glName');
        $qb->addSelect('b.name AS companyName');
        $qb->addSelect('itemUnit.name AS uom');
        $qb->addSelect('sec.name AS reqSection');
        $qb->addSelect('itemSec.name AS itemSection');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        $qb->andWhere('br.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        if($mode == "OPEX"){
            $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
        }elseif(in_array($mode ,array('Expense','Confidential'))){
            $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("Expense","Confidential"));
        }
        if($head){
            $qb->andWhere('e.budgetHead IN(:gls)')->setParameter('gls', $head);
        }
        if($glGroupHead){
            $qb->leftJoin('gl.parent', 'groupGl');
            $qb->leftJoin('groupGl.parent', 'subGroupHead');
            $qb->leftJoin('subGroupHead.parent', 'groupHead');
            $qb->andWhere("groupHead.id ={$glGroupHead}");
        }
        $qb->orderBy('r.created',"ASC");
        $this->handleReportBetween($qb,$data);
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery();
        return $result;
    }

    public function requisitionCapexItemBudgetReport($config , $mode = "", $data = "")
    {
        $em = $this->_em;
        $head = isset($data['requisition_filter_form']['head']) ? $data['requisition_filter_form']['head'] : '';
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->join('r.budgetRequisition','br');
        $qb->leftJoin('e.category','cat');
        $qb->leftJoin('e.section','itemSec');
        $qb->leftJoin('r.section','sec');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.processDepartment','d');
        $qb->leftJoin('r.priority','p');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('e.category','c');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','itemUnit');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.actualQuantity as actualQuantity','e.price as price','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining', '(e.quantity * e.price) AS subTotal', 'e.updated as created', 'e.remark as remark', 'e.description as specification');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.created as requisitionDate');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('p.name as priority');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('c.name  as glName');
        $qb->addSelect('b.name AS companyName');
        $qb->addSelect('itemUnit.name AS uom');
        $qb->addSelect('sec.name AS reqSection');
        $qb->addSelect('itemSec.name AS itemSection');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        $qb->andWhere('br.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("Capex"));
        $qb->orderBy('r.created',"ASC");
        $this->handleReportBetween($qb,$data);
        $qb->groupBy('id');
        $qb->orderBy('r.created','ASC');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery();
        return $result;
    }

    public function prBankOpenItem($config , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('e.batchItem','bi');
        $qb->leftJoin('bi.tenderBatch','tb');
        $qb->leftJoin('tb.assignTo','tbt');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.quantity as quantity','e.approveQuantity as aapquantity','e.remainigQuantity as remaining','e.price as price','e.isClose as close');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.quantity as approveQuantity');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('tbt.name as assignedTo');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('e.isClose = 1');
            $qb->andWhere('tb.id IS NOT NULL');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('tb.id IS NULL');
        }elseif( isset($data['mode']) and $data['mode'] == "archive"){
            $qb->andWhere('e.isClose != 1');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy('e.created',"DESC");
        $qb->groupBy('id');
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchItem($config , $user , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('tb.assignTo = :assignTo')->setParameter('assignTo',"{$user}");
        $qb->andWhere('e.remainigQuantity > 0');
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('bi.isClose = 1');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('bi.isClose IS NULL');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $qb->orderBy('r.created','DESC');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchAcknowledgeItem($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('tbDep.id =:dId')->setParameter('dId', $department->getId());
        if(isset($data['mode']) and $data['mode'] == "close"){
            $qb->andWhere('biApprovedBy.id is not NULL');
        }elseif(isset($data['mode']) and $data['mode'] == "in-progress"){
            $qb->andWhere('biApprovedBy.id is NULL');
        }
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $result;
    }

    public function bankBatchLssdAcknowledgeItem($config , $user , $data = "")
    {
        $department = $user->getProfile()->getDepartment();
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->leftJoin('tb.assignTo','tbassignTo');
        $qb->leftJoin('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('tbDep.name as assessmentDepartment');
        $qb->addSelect('tbassignTo.id as requisitionAssignToId','tbassignTo.name as requisitionAssignTo');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('tbDep.id != :dId')->setParameter('dId', $department->getId());
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function bankBatchAcknowledgeItemComment($config , $user , $data = "")
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->leftJoin('r.branch','b');
        $qb->join('e.batchItem','bi');
        $qb->leftJoin('bi.assignTo','biReportTo');
        $qb->leftJoin('bi.approvedBy','biApprovedBy');
        $qb->join('bi.tenderBatch','tb');
        $qb->join('tb.department','tbDep');
        $qb->join('e.item','item');
        $qb->select('e.id as id','e.issueQuantity as issueQuantity','e.remainigQuantity as remaining');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('d.name as department');
        $qb->addSelect('bi.id as batchItemId','bi.comment as comment','bi.quantity as quantity','bi.isClose as isClose');
        $qb->addSelect('biReportTo.id as assignToId','biReportTo.name as assignTo');
        $qb->addSelect('biApprovedBy.id as approvedById','biApprovedBy.name as approvedBy');
        $qb->addSelect('tb.id as batch','tb.content as content');
        $qb->addSelect('b.name as branch');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('biReportTo.id =:biReportToUser')->setParameter('biReportToUser', $user->getId());
        $this->handleSearchBetween($qb,$data);
        $qb->groupBy('id');
        $result = $qb->getQuery()->getArrayResult();
        $data = array();
        foreach ($result as $row)
        {
            $data[$row['itemId']][] = $row;
        }
        return $data;
    }

    public function prBankOpenBatchItem($config , $items)
    {

        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.requisitionType','rt');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('rt.slug IN (:type)')->setParameter('type',array("purchase-requisition","repair-requisition","replacement-requisition"));
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('e.isClose = 1');
        $qb->andWhere($qb->expr()->in("e.id", $items ));
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function updateStoreItem(RequisitionItem $item,$quantity,$stockIn)
    {
        $em = $this->_em;
        $item->setQuantity($quantity);
        $item->setApproveQuantity($item->getQuantity());
        $item->setRemainigQuantity($item->getQuantity());
        $item->setStockIn($stockIn);
        $item->setSubTotal($quantity * $item->getPrice());
        $em->persist($item);
        $em->flush();
    }

    public function updateApproveQuantity($data)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */
        foreach ($data['purchaseItem'] as $key => $value){
            $item = $this->find($value);
            $quantity = (integer)$data['quantity'][$value];
            $item->setActualQuantity($item->getQuantity());
            $item->setApproveQuantity($item->getQuantity());
            $item->setRemainigQuantity($quantity);
            $item->setQuantity($quantity);
            $item->setSubTotal($quantity * $item->getPrice());
            $em->persist($item);
            $em->flush();
        }
    }

    public function getLastBranchItem(Requisition $requisition,$stock)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->select('e.id as id');
        $qb->where('e.stock = :stock')->setParameter('stock',$stock->getId());
        if(!empty($requisition->getBranch())){
            $qb->andWhere('b.id = :id')->setParameter('id',"{$requisition->getBranch()->getId()}");
        }elseif(!empty($requisition->getDepartment())){
            $qb->andWhere('d.id = :id')->setParameter('id',"{$requisition->getDepartment()->getId()}");
        }
        $qb->setMaxResults(1);
        $qb->orderBy('e.id',"DESC");
        $result = $qb->getQuery()->getOneOrNullResult();
        if($result){
            $lastId = $result['id'];
        }else{
            $lastId = '';
        }
        return $lastId;
    }

    public function updateRequisitionItemForTender($ids)
    {
        $em = $this->_em;
        foreach ($ids as $pri):
            $issueQuantity  = $this->getTenderItemDetailsIssueQuantity($pri);
            $requisitionItem = $this->find($pri);
            $requisitionItem->setIssueQuantity($issueQuantity);
            $remain = ($requisitionItem->getQuantity() - $requisitionItem->getIssueQuantity());
            $requisitionItem->setRemainigQuantity($remain);
            $em->persist($requisitionItem);
            $em->flush();
        endforeach;

    }

    private function getTenderItemDetailsIssueQuantity($id){
        $em = $this->_em;
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderItemDetails::class,'e');
        $qb->select('SUM(e.issueQuantity) as issueQuantity');
        $qb->where('e.requisitionItem =:ids')->setParameter('ids',$id);
        $result = $qb->getQuery()->getOneOrNullResult();
        return ($result['issueQuantity'] > 0)? $result['issueQuantity']:0;

    }

    public function requisitionMonthlyBudgetReset($config , $data = "")
    {

        $em = $this->_em;
        $year = isset($data['requisition_filter_form']['year']) ? $data['requisition_filter_form']['year'] : '';
        $company = isset($data['requisition_filter_form']['company']) ? $data['requisition_filter_form']['company'] : '';
        $budgetYear = $em->getRepository(BudgetYear::class)->findOneBy(array('yearlyBudget'=>$year,'company'=>$company));

        if($budgetYear){
            $explode = explode("-",$year);
            $startYear = $explode[0];
            $endYear = $explode[1];
            
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.requisition','r');
            $qb->join('e.budgetHead','gl');
            $qb->leftJoin('r.companyUnit','unit');
            $qb->leftJoin('unit.parent','b');
            $qb->leftJoin('r.processDepartment','d');
            $qb->select('SUM(e.subTotal) AS subTotal');
            $qb->addSelect("MONTHNAME(r.created) as month");
            $qb->addSelect('gl.id  as glId');
            $qb->where('r.config = :config')->setParameter('config',"{$config}");
            // $qb->andWhere('gl.id = 162');
            $qb->andWhere('b.id = :company')->setParameter('company',"{$company}");
            $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['In-progress','New']);
            $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb->andWhere("r.created >= :startDate")->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb->andWhere("r.created <= :endDate")->setParameter('endDate', $endDate);
            $qb->groupBy('gl.id','month');
            $qb->orderBy('gl.id',"ASC");
            $holds = $qb->getQuery()->getArrayResult();

            $hold = array();
            foreach ($holds as $row){
                $monthHold = "{$row['glId']}-{$row['month']}";
                $hold[$monthHold] = $row;
            }

            $qb1 = $this->createQueryBuilder('e');
            $qb1->join('e.requisition','r');
            $qb1->join('r.budgetRequisition','br');
            $qb1->join('e.budgetHead','gl');
            $qb1->leftJoin('r.companyUnit','unit');
            $qb1->leftJoin('unit.parent','b');
            $qb1->leftJoin('r.processDepartment','d');
            $qb1->select('SUM(e.subTotal) AS subTotal');
            $qb1->addSelect("MONTHNAME(r.created) as month");
            $qb1->addSelect('gl.id  as glId');
            $qb1->where('r.config = :config')->setParameter('config',"{$config}");
            $qb1->andWhere('b.id = :company')->setParameter('company',"{$company}");
            $qb1->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved','Closed']);
            $qb1->andWhere('br.waitingProcess IN (:process)')->setParameter('process',['Approved','Closed']);
            $qb1->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
            //  $qb1->andWhere('gl.id = 162');
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb1->andWhere("r.created >= :startDate")->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb1->andWhere("r.created <= :endDate")->setParameter('endDate', $endDate);
            $qb1->groupBy('gl.id','month');
            $qb1->orderBy('gl.id',"ASC");
            $expenseResult = $qb1->getQuery()->getArrayResult();
            $expensees = [];
            foreach ($expenseResult as $exp) {
                $monthExpense = "{$exp['glId']}-{$exp['month']}";
                $expensees[$monthExpense] = $exp;
            }

            $budgetMonths = $em->getRepository(BudgetMonth::class)->findFinancialBudgetCompanyResetMonthly($budgetYear->getId());

            /* @var $budgetMonth BudgetMonth */

            $em = $this->_em;
            foreach ($budgetMonths as $budgetMonth){
                $monthCalendar = "{$budgetMonth->getGl()->getId()}-{$budgetMonth->getMonth()}";
                $holdAmount = (float) isset($hold[$monthCalendar]) ? $hold[$monthCalendar]['subTotal']:0;
                $expenseAmount = (float)isset($expensees[$monthCalendar]) ? $expensees[$monthCalendar]['subTotal']:0;
                $newHold = ($holdAmount - $expenseAmount);
                if($budgetMonth){
                    $em->getRepository(BudgetMonth::class)->MonthlyExpenseHoldReset($budgetMonth,$holdAmount,$expenseAmount);
                }
            }
        }

    }

    public function resetGLBudgetTillMonth(BudgetMonth $budgetMonth,$months)
    {

        $head = $budgetMonth->getGl()->getId();
        $year = $budgetMonth->getBudgetYear()->getYearlyBudget();
        $company = $budgetMonth->getCompany()->getId();
        $explode = explode("-",$year);
        $startYear = $explode[0];
        $endYear = $explode[1];
        $em = $this->_em;
        $budgetYear = $budgetMonth->getBudgetYear();
        if($budgetMonth->getBudgetYear()){
            $budgetMonths = $em->getRepository(BudgetMonth::class)->findFinancialBudgetMonthReset($budgetYear->getId(),$head);
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.requisition','r');
            $qb->join('e.budgetHead','gl');
            $qb->join('r.companyUnit','unit');
            $qb->leftJoin('unit.parent','b');
            $qb->leftJoin('r.processDepartment','d');
            $qb->select('SUM(e.subTotal) AS subTotal');
            $qb->addSelect("MONTHNAME(r.created) as month");
            $qb->addSelect('gl.id  as glId');
            $qb->where('b.id = :company')->setParameter('company',"{$company}");
            $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['In-progress','New']);
            $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
            $qb->andWhere("gl.id ={$head}");
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb->andWhere("r.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb->andWhere("r.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
            $qb->groupBy('month');
            $holds = $qb->getQuery()->getArrayResult();
            $hold = array();
            foreach ($holds as $row){
                $monthHold = "{$row['glId']}-{$row['month']}";
                $hold[$monthHold] = $row;
            }

            $qb1 = $this->createQueryBuilder('e');
            $qb1->join('e.requisition','r');
            $qb1->join('r.budgetRequisition','br');
            $qb1->join('e.budgetHead','gl');
            $qb1->join('r.companyUnit','unit');
            $qb1->leftJoin('unit.parent','b');
            $qb1->leftJoin('r.processDepartment','d');
            $qb1->select('SUM(e.subTotal) AS subTotal');
            $qb1->addSelect("MONTHNAME(r.created) as month");
            $qb1->addSelect('gl.id  as glId');
            $qb1->where('b.id = :company')->setParameter('company',"{$company}");
            $qb1->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
            $qb1->andWhere('br.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
            $qb1->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
            $qb1->andWhere("gl.id ={$head}");
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb1->andWhere("r.created >= :startDate");
            $qb1->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb1->andWhere("r.created <= :endDate");
            $qb1->setParameter('endDate', $endDate);
            $qb1->orderBy('gl.id',"ASC");
            $qb1->groupBy('month');
            $expense = $qb1->getQuery()->getArrayResult();

            $expensees = [];
            foreach ($expense as $exp) {
                $monthExpense = "{$exp['glId']}-{$exp['month']}";
                $expensees[$monthExpense] = $exp;
            }

            /* @var $budgetMonth BudgetMonth */

            foreach ($budgetMonths as $budgetMonth){
                $monthCalendar = "{$budgetMonth->getGl()->getId()}-{$budgetMonth->getMonth()}";
                $holdAmount = isset($hold[$monthCalendar]) ? $hold[$monthCalendar]['subTotal']:0;
                $expenseAmount = isset($expensees[$monthCalendar]) ? $expensees[$monthCalendar]['subTotal']:0;
                if($budgetMonth){
                    $em->getRepository(BudgetMonth::class)->MonthlyExpenseHoldReset($budgetMonth,$holdAmount,$expenseAmount);
                }
            }
        }

    }


    public function processRequisitionGLBudgetMonth($company, $year , $head)
    {
        $em = $this->_em;
        $explode = explode("-",$year);
        $startYear = $explode[0];
        $endYear = $explode[1];
        $budgetYear = $em->getRepository(BudgetYear::class)->findOneBy(array('yearlyBudget' => $year,'company' => $company));
        if($budgetYear){
            $budgetMonths = $em->getRepository(BudgetMonth::class)->findFinancialBudgetMonthReset($budgetYear->getId(),$head);
            $qb = $this->createQueryBuilder('e');
            $qb->join('e.requisition','r');
            $qb->join('e.budgetHead','gl');
            $qb->join('r.companyUnit','unit');
            $qb->leftJoin('unit.parent','b');
            $qb->leftJoin('r.processDepartment','d');
            $qb->select('SUM(e.subTotal) AS subTotal');
            $qb->addSelect("MONTHNAME(r.created) as month");
            $qb->addSelect('gl.id  as glId');
            $qb->where('b.id = :company')->setParameter('company',"{$company}");
            $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['In-progress','New']);
            $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense","Confidential"));
            $qb->andWhere("gl.id ={$head}");
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb->andWhere("r.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb->andWhere("r.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
            $qb->groupBy('month');
            $holds = $qb->getQuery()->getArrayResult();
            $hold = array();
            foreach ($holds as $row){
                $monthHold = "{$row['glId']}-{$row['month']}";
                $hold[$monthHold] = $row;
            }

            $qb1 = $this->createQueryBuilder('e');
            $qb1->join('e.requisition','r');
            $qb1->join('r.budgetRequisition','br');
            $qb1->join('e.budgetHead','gl');
            $qb1->join('r.companyUnit','unit');
            $qb1->leftJoin('unit.parent','b');
            $qb1->leftJoin('r.processDepartment','d');
            $qb1->select('SUM(e.subTotal) AS subTotal');
            $qb1->addSelect("MONTHNAME(r.created) as month");
            $qb1->addSelect('gl.id  as glId');
            $qb1->where('b.id = :company')->setParameter('company',"{$company}");
            $qb1->andWhere('r.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
            $qb1->andWhere('br.waitingProcess IN (:process)')->setParameter('process',['Approved', 'Closed']);
            $qb1->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("OPEX","Expense",'Confidential'));
            $qb1->andWhere("gl.id ={$head}");
            $datetime = new \DateTime("now");
            $startDate = $datetime->format("{$startYear}-07-01 00:00:00");
            $qb1->andWhere("r.created >= :startDate");
            $qb1->setParameter('startDate', $startDate);
            $endDate = $datetime->format("{$endYear}-06-30 23:59:59");
            $qb1->andWhere("r.created <= :endDate");
            $qb1->setParameter('endDate', $endDate);
            $qb1->orderBy('gl.id',"ASC");
            $qb1->groupBy('month');
            $expense = $qb1->getQuery()->getArrayResult();
            $expensees = [];
            foreach ($expense as $exp) {
                $monthExpense = "{$exp['glId']}-{$exp['month']}";
                $expensees[$monthExpense] = $exp;
            }
            
            foreach ($budgetMonths as $budgetMonth){
                $monthCalendar = "{$head}-{$budgetMonth->getMonth()}";
                $holdAmount = isset($hold[$monthCalendar]) ? $hold[$monthCalendar]['subTotal']:0;
                $expenseAmount = isset($expensees[$monthCalendar]) ? $expensees[$monthCalendar]['subTotal']:0;
                if($budgetMonth){
                    $em->getRepository(BudgetMonth::class)->MonthlyExpenseHoldReset($budgetMonth,$holdAmount,$expenseAmount);
                }
            }
        }
        
    }
}
