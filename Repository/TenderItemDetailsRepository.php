<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatchItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderItemDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderItemDetails::class);
    }

    public function insertTenderToPrItem(Tender $tender,Requisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */
        foreach ($requisition->getRequisitionItems() as $item){
            $find = $this->findOneBy(array('tender' => $tender,'requisitionItem'=> $item));
            if(empty($find) and $item and $item->getRemainigQuantity() > 0){
                $entity = new TenderItemDetails();
                $entity->setTender($tender);
                $entity->setRequisitionItem($item);
                $entity->setStockBook($item->getStockBook());
                $entity->setStock($item->getStock());
                $entity->setPoQuantity($item->getRemainigQuantity());
                $entity->setIssueQuantity($item->getRemainigQuantity());
                $entity->setRemainigQuantity($item->getRemainigQuantity());
                $entity->setPrice($item->getPrice());
                $entity->setSubTotal($item->getPrice() * $item->getRemainigQuantity());
                $em->persist($entity);
                $em->flush();
            }
        }
    }

    public function getRequisitionList(Tender $tender)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->select('r.id', 'r.requisitionNo');
        $qb->addSelect('d.id as departmentId', 'd.name as department');
        $qb->addSelect('b.id as branchId', 'b.name as branch');
        $qb->join('e.tender','t');
        $qb->join('e.requisitionItem','ri');
        $qb->join('ri.requisition','r');
        $qb->leftJoin('r.branch','b');
        $qb->leftJoin('r.department','d');
        $qb->where("t.id='{$tender->getId()}'");
        $qb->groupBy('r.id');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }



    public function updateTenderToPrItem(Tender $tender , TenderItem $tenderItem)
    {
        $em = $this->_em;
        /* @var $entity TenderItemDetails */
        $prItem = $tenderItem->getRequisitionItem();
        $entity = $em->getRepository(TenderItemDetails::class)->findOneBy(array('tender'=>$tender,'requisitionItem'=> $prItem));
        if($entity){
            $entity->setTenderItem($tenderItem);
            $em->persist($entity);
            $em->flush();
        }
    }

    public function insertTenderItem(Tender $tender , $data)
    {
        $em = $this->_em;
        foreach ($data['issue'] as $key => $item ){
            $pr = $data['prId'][$key];
            $price = $data['price'];
            $find = $this->findOneBy(array('tender'=> $tender,'requisitionItem'=> $pr));
            if(empty($find) and $item){
                $pr = $data['prId'][$key];
                $prId = $em->getRepository(RequisitionItem::class)->find($pr);
                $price = (isset($price[$data['prId'][$key]]) and $price[$data['prId'][$key]]) ? $price[$data['prId'][$key]]:$prId->getPrice();
                $batchItemId = (isset($data['batchItemId'][$key]) and $data['batchItemId'][$key]) ? $data['batchItemId'][$key]:'';
                $entity = new TenderItemDetails();
                $entity->setTender($tender);
                $entity->setRequisitionItem($prId);
                if($batchItemId){
                    $batchItem = $em->getRepository(TenderBatchItem::class)->find($data['batchItemId'][$key]);
                    $entity->setTenderBatchItem($batchItem);
                }
                $entity->setStockBook($prId->getStockBook());
                $entity->setStock($prId->getStock());
                $entity->setPoQuantity($item);
                $entity->setIssueQuantity($item);
                $entity->setRemainigQuantity($item);
                $entity->setPrice($price);
                $entity->setSubTotal($entity->getPrice() * $item);
                $itemName = $prId->getName();
                $entity->setItemName($itemName);
                $em->persist($entity);
                $em->flush();
                $this->updateRemainingQuantity($prId);
            }
        }
    }

    private function updateRemainingQuantity(RequisitionItem $prId)
    {
        $id = $prId->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.issueQuantity) as issueQuantity');
        $qb->where("e.requisitionItem = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        $remain = ($prId->getQuantity() - $result['issueQuantity']);
        $prId->setIssueQuantity($result['issueQuantity']);
        $prId->setRemainigQuantity($remain);
        $em->persist($prId);
        $em->flush();
    }

    public function updatePrItem(TenderItem $tenderItem , $ids)
    {
        $em = $this->_em;
        $data = explode(',',$ids);
        foreach ($data as $key => $item ){
            if($item){
                /* @var $entity TenderItemDetails */
                $entity = $em->getRepository(TenderItemDetails::class)->find($item);
                if($entity){
                    $entity->setTenderItem($tenderItem);
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function updateTenderItem(Tender $entity , $data)
    {
        $em = $this->_em;
        foreach ($data['itemId'] as $key => $item ){
            if($item){
                /* @var $entity TenderItem */
                $entity = $em->getRepository(TenderItem::class)->find($item);
                if($entity){
                    $entity->setIssueQuantity($data['issue'][$key]);
                    $em->persist($entity);
                    $em->flush();
                }
            }
        }
    }

    public function updateBankTenderPrItem(Tender $entity , $data)
    {
        $em = $this->_em;
        foreach ($data['itemId'] as $item ){
            if($item){
                /* @var $tenderItemDetails TenderItemDetails */
                $tenderItemDetails = $em->getRepository(TenderItemDetails::class)->find($item);
                if($tenderItemDetails){
                    $tenderItemDetails->setIssueQuantity($data['quantity'][$item]);
                    if($data['price'][$item]){
                        $tenderItemDetails->setPrice($data['price'][$item]);
                    }
                    $tenderItemDetails->setSubTotal($tenderItemDetails->getPrice() * $tenderItemDetails->getIssueQuantity());
                    $em->persist($tenderItemDetails);
                    $em->flush();
                    $this->getTenderItemSummary($tenderItemDetails->getTenderItem());
                }
            }
        }


    }

    public function getTenderItemSummary(TenderItem $tenderItem)
    {
        $id = $tenderItem->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal','SUM(e.issueQuantity) as issueQuantity');
        $qb->where("e.tenderItem = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $tenderItem->setSubTotal($subTotal);
            $tenderItem->setIssueQuantity($result['issueQuantity']);
            $tenderItem->setRemainigQuantity($result['issueQuantity']);
        }else{
            $tenderItem->setSubTotal(0);
            $tenderItem->setIssueQuantity(0);
            $tenderItem->setRemainigQuantity(0);
        }
        $em->persist($tenderItem);
        $em->flush();

    }

    public function countTenderQuantity(RequisitionItem $item)
    {
        $id = $item->getId();
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.issueQuantity) as quantity');
        $qb->join('e.requisitionItem','r');
        $qb->join('e.tender','t');
        $qb->where("r.id = '{$id}'");
        $qb->andWhere('t.waitingProcess =:process')->setParameter('process', "Approved");
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function bankBatchSelectedItem($bankBatchSelectedItem)
    {

        $items = array();
        foreach ($bankBatchSelectedItem as $entity) {
            foreach ($entity as $item){
                $items[] = $item['id'];
            }
        }
        if($items){
            $qb = $this->createQueryBuilder('e');
            $qb->select('ri.id as requisitionItemId','SUM(e.issueQuantity) as issueQuantity','SUM(e.poQuantity) as poQuantity');
            $qb->join('e.requisitionItem','ri');
            $qb->where($qb->expr()->in("ri.id", $items ));
            $qb->groupBy('ri.id');
            $result = $qb->getQuery()->getArrayResult();
            $selectedRows = array();
            foreach ($result as $row){
                $selectedRows[$row['requisitionItemId']] = $row;
            }
            return $selectedRows;
            dd($selectedRows);
        }
        return false;

    }

    public function bankPoExistingSelectedItem($bankBatchSelectedItem)
    {

        $items = array();
       // dd($bankBatchSelectedItem);
        foreach ($bankBatchSelectedItem as $entity) {
            foreach ($entity as $item){
                $items[] = $item['id'];
            }
        }
        if($items){
            $qb = $this->createQueryBuilder('e');
            $qb->select('ri.id as requisitionItemId','SUM(e.issueQuantity) as issueQuantity','SUM(e.poQuantity) as poQuantity');
            $qb->join('e.requisitionItem','ri');
            $qb->where($qb->expr()->in("ri.id", $items ));
            $qb->groupBy('ri.id');
            $result = $qb->getQuery()->getArrayResult();
            $selectedRows = array();
            foreach ($result as $row){
                $selectedRows[$row['requisitionItemId']] = $row;
            }
     //       dd($selectedRows);
            return $selectedRows;
        }
        return false;

    }

    public function bankTenderExistingSelectedItem(Tender $tender)
    {

        $items = array();
        foreach ($tender->getTenderItemDetails() as $entity) {
            $items[] = $entity->getRequisitionItem()->getId();
        }
        if($items){
            $qb = $this->createQueryBuilder('e');
            $qb->select('ri.id as requisitionItemId','SUM(e.issueQuantity) as issueQuantity','SUM(e.poQuantity) as poQuantity');
            $qb->join('e.requisitionItem','ri');
            $qb->where($qb->expr()->in("ri.id", $items ));
            $qb->groupBy('ri.id');
            $result = $qb->getQuery()->getArrayResult();
            $selectedRows = array();
            foreach ($result as $row){
                $selectedRows[$row['requisitionItemId']] = $row;
            }
            return $selectedRows;
        }
        return false;

    }

    public function getTenderRequisitionItem(TenderWorkorder $workorder){

        $selectedRows = array();
        if($workorder->getRequisition()){
            $requisition = $workorder->getRequisition()->getId();
            $tender = $workorder->getTenderComparative()->getTender();

            $qb = $this->createQueryBuilder('e');
            $qb->select('SUM(e.issueQuantity) as quantity','SUM(e.poQuantity) as poQuantity');
            $qb->addSelect('e.id as id');
            $qb->addSelect('s.id as stock');
            $qb->addSelect('ri.id as requisitionItem');
            $qb->join('e.requisitionItem','ri');
            $qb->join('ri.stock','s');
            $qb->join('ri.requisition','r');
            $qb->where('e.tender =:tender')->setParameter('tender', $tender);
            $qb->andWhere('r.id =:requisition')->setParameter('requisition', $requisition);
            $qb->andWhere('e.status !=1');
            $result = $qb->getQuery()->getArrayResult();
            foreach ($result as $row){
                $selectedRows[$row['id']] = $row;
            }
            return $selectedRows;
        }
        return $selectedRows;



    }

}


