<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementInvoiceConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderWorkorderItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TenderWorkorderItem::class);
    }

    public function findWorkorderItem($config , $data = "")
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.tenderWorkorder','w');
        $qb->leftJoin('e.requisitionItem','ri');
        $qb->leftJoin('ri.requisition','r');
        $qb->leftJoin('w.branch','cu');
        $qb->leftJoin('cu.parent','c');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','unit');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','e.grandTotal');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.updated as requisitionDate');
        $qb->addSelect('w.id as workorderId','w.invoice as workorderNo');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.name as uom');
        $qb->addSelect('cu.name as unitName');
        $qb->addSelect('c.name as companyName');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('gl.name  as glCode');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('w.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->orderBy('e.created',"DESC");
       // $this->handleSearchBetween($qb,$data);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function insertGarmentWorkorderItem(TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $workorder->getTenderComparative();
        $tenderVendor = $workorder->getTenderVendor();
        $remove = $qb->delete(TenderWorkorderItem::class, 'e')->where('e.tenderWorkorder = ?1')->setParameter(1, $workorder->getId())->getQuery();
        if($remove){
            $remove->execute();
        }
        $vendorItems = $em->getRepository(TenderComparativeItem::class)->findBy(array('tenderComparative'=>$cs,'tenderVendor'=> $tenderVendor));

        /* @var $vendorItem TenderComparativeItem */


        foreach ($vendorItems as $vendorItem):

            $em->getRepository(TenderItem::class)->updateTenderVendorRemainingItems($vendorItem->getTenderItem());
            $find = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'tenderComparativeItem' => $vendorItem));
            $vendorItem->getTenderItem()->getWoItemRemainingQuantity();
            if(empty($find)){
                if($vendorItem->getTenderItem()->getWoItemRemainingQuantity() > 0){
                    $workorderItem = new TenderWorkorderItem();
                    $workorderItem->setTenderWorkorder($workorder);
                    $workorderItem->setTenderComparativeItem($vendorItem);
                    if($vendorItem->getRequisitionItem()){
                        $workorderItem->setRequisitionItem($vendorItem->getRequisitionItem());
                        if($vendorItem->getRequisitionItem()->getBudgetHead()){
                            $workorderItem->setGl($vendorItem->getRequisitionItem()->getBudgetHead());
                        }
                    }
                    if($vendorItem->getTenderItem()){
                        $workorderItem->setTenderItem($vendorItem->getTenderItem());
                        if($vendorItem->getTenderItem()->getRequisitionItem()){
                            $workorderItem->setRequisitionItem($vendorItem->getTenderItem()->getRequisitionItem());
                        }
                        $workorderItem->setQuantity($vendorItem->getTenderItem()->getWoItemRemainingQuantity());
                        $workorderItem->setRemaining($vendorItem->getTenderItem()->getWoItemRemainingQuantity());
                        $workorderItem->setIssueQuantity($vendorItem->getTenderItem()->getWoItemRemainingQuantity());
                    }
                    $workorderItem->setUnitPrice($vendorItem->getRevisedUnitPrice());
                    $workorderItem->setPrice($vendorItem->getRevisedUnitPrice());
                    $workorderItem->setStock($vendorItem->getStock());

                    $workorderItem->setSubTotal($workorderItem->getQuantity() * $vendorItem->getRevisedUnitPrice());
                    $workorderItem->setTotal($workorderItem->getSubTotal());
                    $workorderItem->setGrandTotal($workorderItem->getSubTotal());
                    $workorderItem->setStockBook($vendorItem->getStockBook());
                    $workorderItem->setRequisition($cs->getTender()->getRequisition());
                    $em->persist($workorderItem);
                    $em->flush();
                }

            }
            $em->getRepository(TenderComparativeItem::class)->updateTenderVendorRemainingItems($vendorItem);
            $em->getRepository(TenderItem::class)->updateTenderVendorRemainingItems($vendorItem->getTenderItem());

        endforeach;
    }

    public function insertGarmentWorkorderPrItem(TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $workorder->getTenderComparative();
        $tenderVendor = $workorder->getTenderVendor();
        $remove = $qb->delete(TenderWorkorderItem::class, 'e')->where('e.tenderWorkorder = ?1')->setParameter(1, $workorder->getId())->getQuery();
        if($remove){
            $remove->execute();
        }
        $vendor = $workorder->getTenderVendor()->getId();
        $vendorItems = $em->getRepository(TenderComparativeItem::class)->getTenderVendorItems($vendor);

        /* @var $itemDetail TenderItemDetails */

        foreach ($cs->getTender()->getTenderItemDetails() as $itemDetail):

            $find = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'tenderItemDetail' => $itemDetail));
            if(empty($find)){

                $workorderItem = new TenderWorkorderItem();
                $workorderItem->setTenderWorkorder($workorder);
                $workorderItem->setTenderItemDetail($itemDetail);
                $workorderItem->setTenderItem($itemDetail->getTenderItem());
                if($vendorItems[$itemDetail->getStockBook()->getId()]){
                    $item = $em->getRepository(TenderComparativeItem::class)->find($itemDetail->getStockBook()->getId());
                    $workorderItem->setTenderComparativeItem($item);
                }
                $workorderItem->setUnitPrice($itemDetail->getPrice());
                $workorderItem->setPrice($workorderItem->getUnitPrice());
                $workorderItem->setQuantity($itemDetail->getIssueQuantity());
                $workorderItem->setRemaining($itemDetail->getIssueQuantity());
                $workorderItem->setStock($itemDetail->getStock());
                $workorderItem->setStockBook($itemDetail->getStockBook());
                if($itemDetail->getStockBook()->getItem()->getCategory()){
                    $workorderItem->setGl($itemDetail->getStockBook()->getItem()->getCategory()->getGeneralLedger());
                }
                $workorderItem->setSubTotal($itemDetail->getIssueQuantity() * $workorderItem->getUnitPrice());
                $workorderItem->setTotal($workorderItem->getSubTotal());
                $workorderItem->setGrandTotal($workorderItem->getSubTotal());
                $workorderItem->setVat(0);
                $workorderItem->setDiscount(0);
                $workorderItem->setVendorCommission(0);
                $workorderItem->setRequisition($itemDetail->getRequisitionItem()->getRequisition());
                $workorderItem->setRequisitionItem($itemDetail->getRequisitionItem());
                $em->persist($workorderItem);
                $em->flush();

            }else{

                /* @var $workorderItem TenderWorkorderItem */

                $workorderItem = $find;
                $workorderItem->setUnitPrice($vendorItems[$itemDetail->getStockBook()->getId()]->getPrice());
                $workorderItem->setPrice($workorderItem->getUnitPrice());
                $workorderItem->setQuantity($itemDetail->getRemainigQuantity());
                $workorderItem->setRemaining($itemDetail->getRemainigQuantity());
                $workorderItem->setStock($itemDetail->getStock());
                $workorderItem->setStockBook($itemDetail->getStockBook());
                if($itemDetail->getStockBook()->getItem()->getCategory()){
                    $workorderItem->setGl($itemDetail->getStockBook()->getItem()->getCategory()->getGeneralLedger());
                }
                $workorderItem->setSubTotal($itemDetail->getRemainigQuantity() * $workorderItem->getUnitPrice());
                $workorderItem->setTotal($workorderItem->getSubTotal());
                $workorderItem->setGrandTotal($workorderItem->getSubTotal());
                $workorderItem->setVat(0);
                $workorderItem->setDiscount(0);
                $workorderItem->setVendorCommission(0);
                $workorderItem->setRequisition($itemDetail->getRequisitionItem()->getRequisition());
                $workorderItem->setRequisitionItem($itemDetail->getRequisitionItem());
                $em->persist($workorderItem);
                $em->flush();
            }

        endforeach;
    }

    public function updateWorkOrderSummary(TenderWorkorder $workorder)
    {
        $id = $workorder->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal','SUM(e.grandTotal) as grandTotal','SUM(e.discount) as discount','(SUM(e.subTotal) - SUM(e.discount)) as total');
        $qb->where("e.tenderWorkorder = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $workorder->setSubTotal($subTotal);
            $amount = ($subTotal - $workorder->getAdditionalDiscount());
            $workorder->setNetTotal($total);
            $commission = 0;
            if($workorder->getEnlistedVendor() and $workorder->getEnlistedVendor()->getCommissionPercent()) {
                $workorder->setVendorCommissionPercent($workorder->getEnlistedVendor()->getCommissionPercent());
                $commission = ((floatval($workorder->getNetTotal()) * floatval($workorder->getEnlistedVendor()->getCommissionPercent())) / 100);
                $workorder->setVendorCommission($commission);
            }
            $total = ($workorder->getSubTotal() - $workorder->getDiscount());
            $grandTotal = (($workorder->getSubTotal() + $workorder->getVat() + $workorder->getTax() + $workorder->getAit() + $commission)-$workorder->getDiscount());
            $workorder->setTotal($total);
            $workorder->setGrandTotal($grandTotal);
        }else{
            $workorder->setSubTotal(0);
            $workorder->setTotal(0);
            $workorder->setVat(0);
            $workorder->setAdditionalDiscount(0);
            $workorder->setDiscount(0);
        }
        $em->persist($workorder);
        $em->flush();

    }

    public function updateGarmentsWorkOrderSummary(TenderWorkorder $workorder)
    {
        $id = $workorder->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal, SUM(e.total) as total, SUM(e.grandTotal) as grandTotal, SUM(e.discount) as discount, SUM(e.vat) as vat,SUM(e.vendorCommission) as commission, SUM(e.shippingCharge) as shippingCharge');
        $qb->where("e.tenderWorkorder = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['total'])){
            $total = $result['total'];
            $workorder->setSubTotal($result['subTotal']);
            $workorder->setTotal(floatval($result['grandTotal']));
            $workorder->setDiscount(floatval($result['discount']) + $workorder->getAdditionalDiscount());
            $workorder->setNetTotal($workorder->getTotal() - $workorder->getAdditionalDiscount());
            $workorder->setVat(floatval($result['vat']));
            if($workorder->getEnlistedVendor() and $workorder->getEnlistedVendor()->getCommissionPercent()) {
                $workorder->setVendorCommissionPercent($workorder->getEnlistedVendor()->getCommissionPercent());
                $commission = ((floatval($workorder->getNetTotal()) * floatval($workorder->getEnlistedVendor()->getCommissionPercent())) / 100);
                $workorder->setVendorCommission($commission);
            }else{
                $workorder->setVendorCommission(0);
            }
            $workorder->setShippingCharge(floatval($result['shippingCharge']) - $workorder->getAdditionalDiscount());
            $workorder->setGrandTotal((floatval($workorder->getNetTotal()) + $workorder->getVendorCommission()));
        }else{
            $workorder->setSubTotal(0);
            $workorder->setTotal(0);
            $workorder->setNetTotal(0);
            $workorder->setVat(0);
            $workorder->setAdditionalDiscount(0);
            $workorder->setDiscount(0);
            $workorder->setShippingCharge(0);
            $workorder->setGrandTotal(0);
        }
        $em->persist($workorder);
        $em->flush();

    }

    public function insertWorkOrderAmendmentItem(TenderWorkorder $workorder)
    {
        $amendment = $workorder->getAmendment();
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $workorder->getTenderComparative();
        $tenderVendor = $workorder->getTenderVendor();
        $remove = $qb->delete(TenderWorkorderItem::class, 'e')->where('e.tenderWorkorder = ?1')->setParameter(1, $workorder->getId())->getQuery();
        if($remove){
            $remove->execute();
        }

        /* @var $vendorItem TenderWorkorderItem */
        foreach ($amendment->getWorkOrderItems() as $vendorItem):

            $workorderItem = new TenderWorkorderItem();
            $workorderItem->setTenderWorkorder($workorder);
            $workorderItem->setTenderComparativeItem($vendorItem->getTenderComparativeItem());
            $workorderItem->setRequisitionItem($vendorItem->getRequisitionItem());
            if($vendorItem->getRequisitionItem()->getBudgetHead()){
                $workorderItem->setGl($vendorItem->getRequisitionItem()->getBudgetHead());
            }
            $workorderItem->setQuantity($vendorItem->getRemaining());
            $workorderItem->setRemaining($vendorItem->getRemaining());
            $workorderItem->setIssueQuantity($vendorItem->getRemaining());
            $workorderItem->setUnitPrice($vendorItem->getUnitPrice());
            $workorderItem->setPrice($vendorItem->getUnitPrice());
            $workorderItem->setStock($vendorItem->getStock());
            $workorderItem->setSubTotal($workorderItem->getQuantity() * $vendorItem->getUnitPrice());
            $workorderItem->setTotal($workorderItem->getSubTotal());
            $workorderItem->setGrandTotal($workorderItem->getSubTotal());
            $workorderItem->setStockBook($vendorItem->getStockBook());
            $workorderItem->setRequisition($workorder->getRequisition());
            $em->persist($workorderItem);
            $em->flush();

        endforeach;

    }

    public function insertBankWorkorderDirectItem(TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $workorder->getTenderComparative();
        $tenderVendor = $workorder->getTenderVendor();
        $remove = $qb->delete(TenderWorkorderItem::class, 'e')->where('e.tenderWorkorder = ?1')->setParameter(1, $workorder->getId())->getQuery();
        if($remove){
            $remove->execute();
        }
        $vendorItems = $em->getRepository(TenderComparativeItem::class)->findBy(array('tenderComparative'=>$cs,'tenderVendor'=> $tenderVendor));

        /* @var $vendorItem TenderComparativeItem */

        foreach ($vendorItems as $vendorItem):
            $find = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'tenderComparativeItem' => $vendorItem));
            if(empty($find)){
                $workorderItem = new TenderWorkorderItem();
                $workorderItem->setTenderWorkorder($workorder);
                $workorderItem->setTenderComparativeItem($vendorItem);
                $workorderItem->setUnitPrice($vendorItem->getRevisedUnitPrice());
                $workorderItem->setPrice($vendorItem->getRevisedUnitPrice());
                $workorderItem->setQuantity($vendorItem->getQuantity());
                $workorderItem->setRemaining($vendorItem->getQuantity());
                $workorderItem->setStock($vendorItem->getStock());
                $subTotral = ($workorderItem->getQuantity() * $workorderItem->getPrice());
                $workorderItem->setSubTotal($subTotral);
                $em->persist($workorderItem);
                $em->flush();
            }
        endforeach;
    }

    public function insertBankWorkorderRequisitionItem(TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $workorder->getTenderComparative();
        $tenderVendor = $workorder->getTenderVendor();
        $remove = $qb->delete(TenderWorkorderItem::class, 'e')->where('e.tenderWorkorder = ?1')->setParameter(1, $workorder->getId())->getQuery();
        if($remove){
            $remove->execute();
        }
        $requisitionItems = $em->getRepository(TenderItemDetails::class)->getTenderRequisitionItem($workorder);
        if($requisitionItems){
            $vendorItems = $em->getRepository(TenderComparativeItem::class)->getBankTenderVendorItems($workorder->getTenderVendor());
            foreach ($requisitionItems as $item):
                $find = $em->getRepository(TenderWorkorderItem::class)->findOneBy(array('tenderWorkorder' => $workorder,'requisitionItem' => $item['requisitionItem']));
                if(empty($find)){
                    if (isset($vendorItems[$item['stock']]) and !empty($vendorItems[$item['stock']])){
                        $workorderItem = new TenderWorkorderItem();
                        $workorderItem->setTenderWorkorder($workorder);
                        $requisitionItem = $em->getRepository(RequisitionItem::class)->find($item['requisitionItem']);
                        $stock = $em->getRepository(Stock::class)->find($item['stock']);
                        $workorderItem->setRequisitionItem($requisitionItem);
                        $workorderItem->setTenderComparativeItem( $vendorItems[$item['stock']]);
                        $workorderItem->setQuantity($item['quantity']);
                        $workorderItem->setRemaining($item['quantity']);
                        $workorderItem->setStock($stock);
                        $workorderItem->setUnitPrice($vendorItems[$item['stock']]->getRevisedUnitPrice());
                        $workorderItem->setPrice($vendorItems[$item['stock']]->getRevisedUnitPrice());
                        $subTotral = ($workorderItem->getQuantity() * $workorderItem->getPrice());
                        $workorderItem->setSubTotal($subTotral);
                        $em->persist($workorderItem);
                        $em->flush();
                    }
                }
            endforeach;
        }
    }
    
    public function updateTenderItemDetails(TenderWorkorder $workorder){

        $cs = $workorder->getTenderComparative()->getId();
        $id = $workorder->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.quantity) as quantity');
        $qb->addSelect('tenderItemDetail.id as id');
        $qb->join('e.tenderItemDetail','tenderItemDetail');
        $qb->join('e.tenderWorkorder','tw');
        $qb->join('tw.tenderComparative','cs');
        $qb->where("cs.id = '{$cs}'");
        $qb->andWhere("tw.process = 'Approved'");
        $qb->groupBy('tenderItemDetail.id');
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $row){

            $tenderItemDetail = $em->getRepository(TenderItemDetails::class)->find($row['id']);
            $issue = $tenderItemDetail->getIssueQuantity();
            $quantity = ($issue - $row['quantity']);
            $tenderItemDetail->setRemainigQuantity($quantity);
            $em->persist($tenderItemDetail);
            $em->flush();

        }


    }

    public function pendingDelivery($config,$users)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisitionItem','ri');
        $qb->join('ri.requisition','r');
        $qb->join('r.createdBy','u');
        $qb->select('COUNT(e.id) as count');
        $qb->where('r.config = :config')->setParameter('config', $config);
        $qb->andWhere('u.id IN(:users)')->setParameter('users',$users);
        $qb->groupBy('u.id');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function updateWearhouse(TenderWorkorder $workorder)
    {
        $wearhouse = $workorder->getWearhouse();
        $queryBuilder = $this->_em->createQueryBuilder();
        $query = $queryBuilder ->update(TenderWorkorderItem::class, 'e')
            ->set('e.wearhouse', ':wearhouse')
            ->setParameter('wearhouse', $wearhouse->getId())
            ->where('e.tenderWorkorder =:workorder')
            ->setParameter('workorder', $workorder->getId())
            ->getQuery();
        $query ->execute();
    }
    

    public function getItemWithPrice(TenderWorkorder $workorder)
    {
        $arries = array();
        foreach ($workorder->getWorkOrderItems() as $item){
            $arries[$item->getStock()->getId()]= $item->getPrice();
        }
        return $arries;
    }



}


