<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Report::class);
    }

    protected function handleReportBetween($qb,$form)
    {
        if(isset($form)){

            $data = $form['report_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $startUpdateDate = isset($data['startUpdateDate'])? $data['startUpdateDate'] :'';
            $endUpdateDate = isset($data['endUpdateDate'])? $data['endUpdateDate'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $section = isset($data['section'])? $data['section'] :'';
            $unit       = isset($data['unit'])? $data['unit'] :'';
            $wearhouse       = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $keyword            = !empty($data['keyword'])? trim($data['keyword']) :'';
            $item               = isset($data['item'])? $data['item'] :'';
            $itemCode           = isset($data['itemCode'])? $data['itemCode'] :'';
            $category           = isset($data['category'])? $data['category'] :'';
            $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            $department         = isset($data['department'])? $data['department'] :'';

            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($category)) {
                $qb->andWhere("cat.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($wearhouse)) {
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($unit)) {
                $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
            }

            if (!empty($section)) {
                $qb->andWhere("sec.id = :sectionId")->setParameter('sectionId', $section);
            }

            if (!empty($item)) {
                $qb->andWhere("item.id = :pid")->setParameter('pid', $item);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
            if (!empty($startUpdateDate) ) {
                $datetime = new \DateTime($startUpdateDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("r.updated >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endUpdateDate)) {
                $datetime = new \DateTime($endUpdateDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("r.updated <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }


        }
    }

    protected function handleSearchStockBetween($qb, $form)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $wearhouse              = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $company                = isset($data['company'])? $data['company'] :'';
            $itemMode               = isset($data['itemMode'])? $data['itemMode'] :'';
            if($company){
                $qb->join("store.parent",'c');
                $qb->andWhere("c.id = :company")->setParameter('company', $company);
            }

            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if($wearhouse){
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($itemMode)) {
                $qb->andWhere('item.itemMode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemMode.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
        }
    }

    protected function handleDateRangeBetween($qb, $form)
    {

        if (isset($form['report_filter_form'])) {
            $data = $form['report_filter_form'];
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.updated >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.updated <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }
        }
    }

    protected function handleSearchStockOpeningBetween($qb, $form)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $wearhouse              = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $company                = isset($data['company'])? $data['company'] :'';
            $itemMode               = isset($data['itemMode'])? $data['itemMode'] :'';
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';

            if($company){
                $qb->join("store.parent",'c');
                $qb->andWhere("c.id = :company")->setParameter('company', $company);
            }

            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if($wearhouse){
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($itemMode)) {
                $qb->andWhere('item.itemMode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemMode.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created < :startDate");
                $qb->setParameter('startDate', $startDate);
            }
        }
    }


    public function requisitionUrgentReport($config,$data)
    {
        $em = $this->_em;
        $company = (isset($data['report_filter_form']) and $data['report_filter_form']['company']) ? $data['report_filter_form']['company']:'';
        $qb = $em->createQueryBuilder();
        $qb ->from(Requisition::class,'e');
        $qb->select('COUNT(e.id) as total');
        $qb->addSelect('pd.id as departmentId','pd.name as department');
        $qb->addSelect('cu.id as unitId','cu.name as companyUnit');
        $qb->join('e.companyUnit','cu');
        $qb->join('cu.parent','c');
        $qb->join('e.processDepartment','pd');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if ($company) {
            $qb->andWhere('c.id = :cid')->setParameter('cid', $company);
        }
        $qb->andWhere("e.waitingProcess = 'Approved'");
        $qb->andWhere('e.requisitionMode IN (:requisitionMode)')->setParameter('requisitionMode',['CAPEX','OPEX']);
        $this->handleDateRangeBetween($qb,$data);
        $qb->groupBy("cu.id","pd.id");
        $result = $qb->getQuery()->getArrayResult();
        $departments = [];
        foreach ($result as $row)
        {
            $departments[$row['unitId']][] = $row;
        }
      
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb ->from(Requisition::class,'e');
        $qb->select('COUNT(e.id) as total');
        $qb->addSelect('p.id as priorityId','p.name as priority');
        $qb->addSelect('pd.id as departmentId','pd.name as department');
        $qb->addSelect('cu.id as unitId','cu.name as companyUnit');
        $qb->join('e.priority','p');
        $qb->join('e.companyUnit','cu');
        $qb->join('cu.parent','c');
        $qb->join('e.processDepartment','pd');
        $qb->where('e.config = :config')->setParameter('config',$config);
        if($company) {
            $qb->andWhere('c.id = :cid')->setParameter('cid', $company);
        }
        $qb->andWhere("e.waitingProcess = 'Approved'");
        $qb->andWhere('e.requisitionMode IN (:requisitionMode)')->setParameter('requisitionMode',['CAPEX','OPEX']);
        $qb->groupBy("p.id","cu.id","pd.id");
        $this->handleDateRangeBetween($qb,$data);
        $rows = $qb->getQuery()->getArrayResult();
        $priorities = [];
        foreach ($rows as $row)
        {
            $priority = "{$row['unitId']}-{$row['departmentId']}-{$row['priorityId']}";
            $priorities[$priority] = $row['total'];
        }
        $array = ['departments' => $departments, 'priorities' => $priorities];
        return $array;
    }

    /**
     * @return Requisition[]
     */
    public function companyWisePrReport( $config,$form = "" )
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(Requisition::class,'e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency');
        $qb->addSelect('b.name as company');
        $qb->addSelect('d.name as department');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.processDepartment','pd');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        if(!empty($department)){
            $qb->andWhere('d.id = :department')->setParameter('department',$department);
        }
        $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    /**
     * @return Requisition[]
     */
    public function departmentWisePrReport( $config,$form = "" )
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(Requisition::class,'e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency');
        $qb->addSelect('b.name as company');
        $qb->addSelect('d.name as department');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.processDepartment','pd');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        if(!empty($branch)){
            $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
        }
        $qb->andWhere('d.id = :department')->setParameter('department',$department);
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate");
        $qb->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate");
        $qb->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }

    public function stockBookItemReports( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['report_filter_form'])) {
            $form = $data['report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('sb.brand','b');
            $qb->leftJoin('sb.category','category');
            $qb->leftJoin('category.generalLedger','gl');
            $qb->leftJoin('sb.brand','brand');
            $qb->leftJoin('sb.size','size');
            $qb->leftJoin('sb.color','color');
            $qb->leftJoin('item.unit','unit');
            $qb->select("sb.id as stockId","sb.price as price","sb.cmp as cmp","sb.updated as updated");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("store.id as wearhouse","store.name as storeName");
            $qb->addSelect("item.id as itemId");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("category.name as categoryName");
            $qb->addSelect("size.name as sizeName");
            $qb->addSelect("color.name as colorName");
            $qb->addSelect("brand.name as brandName");
            $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockBetween($qb,$data);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }

        return array();

    }

    public function stockOpeningReport( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['procurement_report_filter_form'])) {
            $form = $data['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('e.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("(SUM(e.stockIn) - SUM(e.stockOut)) as remainingQuantity");
            $qb->addSelect("sb.id as stockId");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockOpeningBetween($qb,$data);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            $openines = array();
            foreach ($result as $row):
                $openines[$row['stockId']] = $row;
            endforeach;
            return $openines;
        }
        return array();

    }

    public function stockDateRangeReport( $config, $data ): array
    {
        $em = $this->_em;
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['procurement_report_filter_form'])) {
            $form = $data['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->select("sb.id as stockId");
            $qb->addSelect("SUM(e.stockIn) as stockIn","SUM(e.stockOut) as stockOut");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $this->handleSearchStockBetween($qb,$data);
            $this->handleDateRangeBetween($qb,$data);
            $qb->groupBy("sb.id");
            $result = $qb->getQuery()->getArrayResult();
            $entities = array();
            foreach ($result as $row):
                $entities[$row['stockId']] = $row;
            endforeach;
            return $entities;
        }
        return array();

    }

    public function stockBookCategoryConsumptionReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $category  = isset($data['category'])? $data['category'] :'';
            $qb = $em->createQueryBuilder();
            $qb->from(StockHistory::class,'e');
            $qb->join('e.wearhouse','store');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('sb.brand','b');
            $qb->leftJoin('sb.category','category');
            $qb->leftJoin('category.generalLedger','gl');
            $qb->leftJoin('item.unit','unit');
            $qb->select("sb.id as stockId","sb.price as price","sb.cmp as cmp","sb.updated as updated");
            $qb->addSelect("SUM(e.stockIn) as stockIn","SUM(e.stockOut) as stockOut");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("store.id as wearhouse","store.name as storeName");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("category.name as categoryName");
            $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
            $qb->where("e.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            $this->handleDateRangeBetween($qb,$form);
            $qb->groupBy("sb.id");
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function stockBookDepartmentConsumptionReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $qb = $em->createQueryBuilder();
            $qb->from(RequisitionOrderItem::class,'e');
            $qb->join('e.stockBook','sb');
            $qb->join('e.requisitionOrder','ro');
            $qb->join('ro.requisitionIssue','ri');
            $qb->join('ri.createdBy','u');
            $qb->join('ri.department','d');
            $qb->select("d.id as departmentId","d.name as department");
            $qb->addSelect("SUM(e.quantity * sb.price) as price");
            $qb->where("ro.config = :config")->setParameter('config', $config->getId());
            $this->handleDateRangeBetween($qb,$form);
            $qb->groupBy("d.id");
            $qb->orderBy("d.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function itemWisePoReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $qb = $em->createQueryBuilder();
            $qb->from(TenderWorkorderItem::class,'e');
            $qb->join('e.requisition','r');
            $qb->join('r.department','d');
            $qb->join('e.tenderWorkorder','tw');
            $qb->join('tw.tenderVendor','tv');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("e.unitPrice as unitPrice","e.price as price","e.quantity as quantity");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("tw.created as created","tw.invoice as invoice");
            $qb->addSelect("tv.name as vendor");
            $qb->addSelect("d.name as department");
            $qb->where("tw.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("tw.waitingProcess = :process")->setParameter('process',"Approved");
            $this->handleDateRangeBetween($qb,$form);
            $qb->orderBy("tw.created",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function vendorWisePoReports( $config, $form )
    {
        $em = $this->_em;
        if (isset($form['procurement_report_filter_form'])) {
            $data = $form['procurement_report_filter_form'];
            $vendor = isset($data['vendor'])? $data['vendor'] :'';
            $vendorName = $em->getRepository(Vendor::class)->find($vendor);
            $startDate              = isset($data['startDate'])? $data['startDate'] :'';
            $endDate                = isset($data['endDate'])? $data['endDate'] :'';
            $qb = $em->createQueryBuilder();
            $qb->from(TenderWorkorderItem::class,'e');
            $qb->join('e.requisition','r');
            $qb->join('r.department','d');
            $qb->join('e.tenderWorkorder','tw');
            $qb->join('tw.tenderVendor','tv');
            $qb->leftJoin('e.stockBook','sb');
            $qb->leftJoin('sb.item','item');
            $qb->leftJoin('item.unit','unit');
            $qb->select("e.unitPrice as unitPrice","e.price as price","e.quantity as quantity");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("tw.created as created","tw.invoice as invoice");
            $qb->addSelect("tv.name as vendor");
            $qb->addSelect("d.name as department");
            $qb->where("tw.config = :config")->setParameter('config', $config->getId());
            $qb->andWhere("tw.waitingProcess = :process")->setParameter('process',"Approved");
            $qb->andWhere("tv.name = :vendorId")->setParameter('vendorId',$vendorName->getName());
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
            $qb->orderBy("tw.created",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();
    }

    public function reportItemMonthlyConsumption($config)
    {

        if (isset($form['procurement_report_filter_form'])) {
            $formData = $form['procurement_report_filter_form'];
            $year = isset($formData['year']) ? $formData['year'] : '';
        }else{
            $compare = new \DateTime();
            $year =  $compare->format('Y');
        }
        $sql = "SELECT stock.id as stockId,item.name as itemName
                FROM procu_requisition_order_item as sales
                JOIN inv_stock_book as stock ON sales.stock_book_id = stock.id 
                JOIN  gmb_item as item ON stock.item_id = item.id
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY stock.id ORDER BY item.name ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $items =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (ro.updated) as month,SUM(sales.quantity) AS quantity,SUM(sales.quantity * sales.price) AS subTotal,stock.id as stockId,item.name as itemName
                FROM procu_requisition_order_item as sales
                JOIN inv_stock_book as stock ON sales.stock_book_id = stock.id 
                JOIN  gmb_item as item ON stock.item_id = item.id
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY month,stock.id ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['stockId']}";
            $entities[$key] = $row;
        }
        $data = array('items'=>$items,'entities'=>$entities);
        return $data;
    }

    public function reportDepartmentMonthlyConsumption($config,$form)
    {
        if (isset($form['procurement_report_filter_form'])) {
            $formData = $form['procurement_report_filter_form'];
            $year = isset($formData['year']) ? $formData['year'] : '';
        }else{
            $compare = new \DateTime();
            $year =  $compare->format('Y');
        }
        $sql = "SELECT d.id as departmentId , d.name as department
                FROM procu_requisition_order_item as sales
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                JOIN procu_requisition_issue as ri ON ro.requisition_issue_id = ri.id
                JOIN core_setting as d ON ri.department_id = d.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY d.id ORDER BY d.name ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $departments =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (ro.updated) as month,SUM(sales.quantity * sales.price) AS amount,s.id as departmentId
                FROM procu_requisition_order_item as sales
                JOIN procu_requisition_order as ro ON sales.requisition_order_id = ro.id
                JOIN procu_requisition_issue as ri ON ro.requisition_issue_id = ri.id
                JOIN core_setting as s ON ri.department_id = s.id
                WHERE ro.config_id = :config AND ro.waiting_process = :process  AND YEAR(sales.updated) =:year
                GROUP BY month,s.id ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['departmentId']}";
            $entities[$key] = $row;
        }
        $data = array('items' => $departments,'entities'=>$entities);
        return $data;
    }

    public function reportPoSummary($config,$form)
    {

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT COUNT(wo.id) AS quantity,SUM(wo.total) AS price ,r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY r.requisition_mode";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        return $result;
    }

    public function reportPoSummaryMonthly($config,$form)
    {
        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY r.requisition_mode";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $modes =  $stmt->fetchAll();

        $compare = new \DateTime();
        $year =  $compare->format('Y');
        $year = isset($data['year'])? $data['year'] :$year;
        $sql = "SELECT MONTH (wo.updated) as month,COUNT(wo.id) AS quantity,SUM(wo.total) AS price ,r.requisition_mode as mode
                FROM procu_tender_workorder as wo
                JOIN procu_tender_workorder_item as woi ON wo.id = woi.tender_workorder_id
                JOIN procu_requisition as r ON woi.requisition_id = r.id
                WHERE wo.config_id = :config AND wo.waiting_process = :process  AND YEAR(wo.updated) =:year
                GROUP BY month,r.requisition_mode ORDER BY month ASC";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('process', 'Approved');
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $result =  $stmt->fetchAll();
        $entities = array();
        foreach ($result as $row){
            $key = "{$row['month']}-{$row['mode']}";
            $entities[$key] = $row;
        }
        $data = array('items' => $modes,'entities'=>$entities);
        return $data;
    }

    public function getSupplierSummaryReport($config,$form)
    {
        $em = $this->_em;

        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';
        $company = isset($data['company']) ? $data['company']:'';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->select('SUM(e.total) as amount','COUNT(e.id) as totalCount');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if ($company) {
            $qb->leftJoin('e.requisition','r');
            $qb->leftJoin('r.companyUnit','cu');
            $qb->leftJoin('cu.parent','c');
            $qb->andWhere('c.id = :cid')->setParameter('cid', $company);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $workOrderAmount = $qb->getQuery()->getOneOrNullResult();

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->join('e.tenderVendor','tv');
        $qb->select('SUM(e.total) as amount','COUNT(e.id) as totalCount');
        $qb->addSelect('tv.name as company');
        $qb->groupBy('tv.name');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if ($company) {
            $qb->leftJoin('e.requisition','r');
            $qb->leftJoin('r.companyUnit','cu');
            $qb->leftJoin('cu.parent','c');
            $qb->andWhere('c.id = :cid')->setParameter('cid', $company);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $qb->orderBy('company','ASC');
        $result = $qb->getQuery()->getArrayResult();
        
        $array = ['workOrderAmount'=>$workOrderAmount,'suppliers' => $result ];

        return $array;
    }


    public function getUnitSummaryReport($config,$form)
    {
        $em = $this->_em;
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->select('SUM(e.total) as amount','COUNT(e.id) as totalCount');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $workOrderAmount = $qb->getQuery()->getOneOrNullResult();

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->join('e.requisition','r');
        $qb->join('r.companyUnit','wh');
        $qb->join('wh.parent','b');
        $qb->select('SUM(e.total) as amount','COUNT(e.id) as totalCount');
        $qb->addSelect('wh.code as unit');
        $qb->addSelect('b.code as company','b.id as companyId');
        $qb->groupBy('wh.id');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $qb->orderBy('unit','ASC');
        $result = $qb->getQuery()->getArrayResult();
        $unites = [];
        foreach ($result as $row)
        {
            $unites[$row['companyId']][] = $row;
        }
        $array = ['workOrderAmount'=>$workOrderAmount,'unites' => $unites ];

        return $array;
    }

    public function workorderItemReport($config,$form)
    {
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        $company = isset($data['company']) ? $data['company'] : '';
        $invoice = isset($data['invoice']) ? $data['invoice'] : '';
        $unit = isset($data['companyUnit']) ? $data['companyUnit'] : '';
        $vendor = isset($data['vendor']) ? $data['vendor'] : '';
        $paymentMode = isset($data['paymentMode']) ? $data['paymentMode'] : '';
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class, 'wi');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created','e.updated as updated', 'e.process as process', 'e.sendToVendor as sendToVendor');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('wi.price as price','wi.quantity as quantity','wi.vat as vat','wi.discount as discount','wi.grandTotal as grandTotal');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionNo');
        $qb->addSelect('ri.description as description');
        $qb->addSelect('c.name as category');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','tv.name as enlistedVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('cm.code as unit');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sb.id as stockBookId','item.name as itemName','brand.name as brandName','size.name as sizeName','color.name as colorName','itemUnit.name as uom');
        $qb->join('wi.tenderWorkorder', 'e');
        $qb->join('wi.stockBook', 'sb');
        $qb->join('sb.item', 'item');
        $qb->join('item.unit', 'itemUnit');
        $qb->leftJoin('sb.brand', 'brand');
        $qb->leftJoin('sb.size', 'size');
        $qb->leftJoin('sb.color', 'color');
        $qb->leftJoin('wi.requisition', 'r');
        $qb->leftJoin('r.processDepartment', 'pd');
        $qb->leftJoin('wi.requisitionItem', 'ri');
        $qb->leftJoin('ri.category', 'c');
        $qb->leftJoin('r.companyUnit', 'cm');
        $qb->leftJoin('cm.parent', 'com');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.enlistedVendor', 'ev');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($invoice)) {
            $qb->andWhere('e.invoice LIKE :searchTerm OR r.requisitionNo LIKE :searchTerm');
            $qb->setParameter('searchTerm', '%'.trim($invoice).'%');
        }
        if (!empty($unit)) {
            $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
        }
        if(!empty($company)){
            $qb->andWhere('com.id = :branch')->setParameter('branch',$company);
        }
        if (!empty($vendor)) {
            $qb->join('tv.vendor','v');
            $qb->andWhere('v.id = :tenderVendor')->setParameter('tenderVendor', $vendor);

        }
        if (!empty($paymentMode)) {
            $qb->andWhere("e.paymentMode = :paymentMode")->setParameter('paymentMode', $paymentMode);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function jobapprovalToWorkorder($config,$form)
    {
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        $invoice = isset($data['invoice']) ? $data['invoice'] : '';
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->select('e.id', 'e.invoice as workorder','e.created as created','e.updated as updated');
        $qb->addSelect('r.id as requisition','r.created as rCreated','r.updated as rUpdate','r.requisitionNo as requisitionNo');
        $qb->addSelect('jr.id as jrId','jr.created as jrCreated','jr.updated as jrUpdated','jr.requisitionNo as jobApprovalNo');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('r.jobRequisition', 'jr');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if (!empty($invoice)) {
            $qb->andWhere('e.invoice LIKE :searchTerm OR r.requisitionNo LIKE :searchTerm');
            $qb->setParameter('searchTerm', '%'.trim($invoice).'%');
        }

        if (!empty($unit)) {
            $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
        }
        if (!empty($vendor)) {
            $qb->join('tv.vendor','v');
            $qb->andWhere('v.id = :tenderVendor')->setParameter('tenderVendor', $vendor);

        }
        if (!empty($paymentMode)) {
            $qb->andWhere("e.paymentMode = :paymentMode")->setParameter('paymentMode', $paymentMode);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $qb->orderBy('e.created','DESC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function requisitionTimeline($config,$form)
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(Requisition::class,'e');
        $qb->select('e.id','e.requisitionNo as requisitionNo','e.created as created','e.expectedDate as expectedDate','e.subTotal as subTotal','e.total as total', 'e.process as process','e.processOrdering as ordering', 'e.waitingProcess as waitingProcess', 'e.requisitionMode as requisitionMode', 'e.isEmergency as isEmergency');
        $qb->addSelect('user.name as username');
        $qb->addSelect('b.name as company');
        $qb->addSelect('d.name as department');
        $qb->addSelect('pd.name as processDepartment');
        $qb->addSelect('sec.name as section');
        $qb->addSelect('t.name as requisitionType');
        $qb->addSelect('p.name as priority');
        $qb->leftJoin('e.createdBy','user');
        $qb->leftJoin('e.section','sec');
        $qb->leftJoin('e.department','d');
        $qb->leftJoin('e.processDepartment','pd');
        $qb->leftJoin('e.requisitionType','t');
        $qb->leftJoin('e.priority','p');
        $qb->leftJoin('e.companyUnit','cu');
        $qb->leftJoin('cu.parent','b');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approved");
        if(!empty($department)){
            $qb->andWhere('d.id = :department')->setParameter('department',$department);
        }
        if(!empty($branch)){
             $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
        }
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate")->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate")->setParameter('endDate', $endDate);
        $result = $qb->getQuery()->getArrayResult();
        $timelineAssignUser = $this->getTimelineAssignUser($config,$form);
//        dd($timelineAssignUser['d0d59987-8304-11ec-9133-00155d0afb03']);
        return  $returnArray = array('requisitions'=>$result,'timelineAssignUser'=>$timelineAssignUser);
    }

    public function getTimelineAssignUser($config,$form)
    {
        $data = $form['report_filter_form'];
        $startDate = isset($data['startDate'])? $data['startDate'] :'';
        $endDate = isset($data['endDate'])? $data['endDate'] :'';
        $branch = isset($data['company'])? $data['company'] :'';
        $department = isset($data['department'])? $data['department'] :'';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(ProcurementProcess::class,'e');
        $qb->select('e.id','e.entityId as entityId','e.created as created','e.updated as updated');
        $qb->addSelect('assignTo.name as name');
        $qb->addSelect('assignTo.id as assignId');
        $qb->leftJoin('e.assignTo','assignTo');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $datetime = new \DateTime($startDate);
        $startDate = $datetime->format('Y-m-d 00:00:00');
        $qb->andWhere("e.created >= :startDate")->setParameter('startDate', $startDate);
        $datetime = new \DateTime($endDate);
        $endDate = $datetime->format('Y-m-d 23:59:59');
        $qb->andWhere("e.created <= :endDate")->setParameter('endDate', $endDate);
        $results = $qb->getQuery()->getArrayResult();
        $assings = array();
        foreach ($results as $result){
            $assings[$result['entityId']][]= $result;
        }
        return $assings;

    }

    public function woinhouse($config,$form)
    {

        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        $invoice = isset($data['invoice']) ? $data['invoice'] : '';
        $company = isset($data['company']) ? $data['company'] : '';
        $unit = isset($data['companyUnit']) ? $data['companyUnit'] : '';
        $store = isset($data['wearhouse']) ? $data['wearhouse'] : '';
        $vendor = isset($data['vendor']) ? $data['vendor'] : '';
        $paymentMode = isset($data['paymentMode']) ? $data['paymentMode'] : '';
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';

        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderReceive::class, 'e');
        $qb->select('e.id', 'e.invoice as grn','e.total as total','e.challanDate as challanDate','e.challanNo as challanNo','e.gatePassNo as gatePassNo','e.receiveDate as receiveDate','e.created as created','e.updated as updated');
        $qb->addSelect('w.id', 'w.invoice as workorder', 'w.updated as workorderDate', 'w.sendToVendor as sendToVendor');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionNo');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','tv.name as enlistedVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('cm.code as unit');
        $qb->addSelect('st.name as storeName');
        $qb->leftJoin('e.workorder', 'w');
        $qb->leftJoin('w.shipTo', 'st');
        $qb->leftJoin('w.requisition', 'r');
        $qb->leftJoin('r.companyUnit', 'cm');
        $qb->leftJoin('cm.parent', 'com');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('w.tenderVendor', 'tv');
        $qb->leftJoin('w.enlistedVendor', 'ev');
        $qb->leftJoin('w.paymentMode', 'pm');
        $qb->leftJoin('w.deliveryMode', 'dm');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if($company){
            $qb->andWhere('com.id = :branch')->setParameter('branch',$company);
        }
        if (!empty($invoice)) {
            $qb->andWhere('e.invoice LIKE :searchTerm OR r.requisitionNo LIKE :searchTerm');
            $qb->setParameter('searchTerm', '%'.trim($invoice).'%');
        }
        if (!empty($unit)) {
            $qb->andWhere("cu.id = :unitId")->setParameter('unitId', $unit);
        }
        if (!empty($store)) {
            $qb->andWhere("st.id = :storeId")->setParameter('storeId', $store);
        }
        if (!empty($vendor)) {
            $qb->join('tv.vendor','v');
            $qb->andWhere('v.id = :tenderVendor')->setParameter('tenderVendor', $vendor);

        }
        if (!empty($paymentMode)) {
            $qb->andWhere("e.paymentMode = :paymentMode")->setParameter('paymentMode', $paymentMode);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function workorderReport($config,$filter)
    {
        $em = $this->_em;
        $data = $filter['report_filter_form'];
        $startDate = isset($data['startDate']) ? $data['startDate'] : '';
        $endDate = isset($data['endDate']) ? $data['endDate'] : '';
        $company = isset($data['company']) ? $data['company'] : '';
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorder::class, 'e');
        $qb->select('e.id', 'e.invoice as invoice','e.total as total','e.recurring as recurring','e.workorderDate as workorderDate','e.validateDate as validateDate','e.created as created','e.updated as updated', 'e.process as process','e.sendToVendor as sendToVendor', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('t.id as tenderComparative','t.invoice as tenderComparativeInvoice');
        $qb->addSelect('tv.id as tenderVendorId','tv.name as tenderVendor');
        $qb->addSelect('ev.id as enlistedVendorId','ev.name as enlistedVendor');
        $qb->addSelect('pm.id as paymentModeId','pm.name as paymentMode');
        $qb->addSelect('dm.id as deliveryModeId','dm.name as deliveryMode');
        $qb->addSelect('b.code as unit');
        $qb->addSelect('ten.id as tenderId','ten.invoice as tenderInvoice');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo');
        $qb->addSelect('SUM(wi.quantity)  as quantity','SUM(wi.issueQuantity)  as issueQuantity');
        $qb->leftJoin('e.tenderComparative', 't');
        $qb->leftJoin('t.tender', 'ten');
        $qb->leftJoin('ten.requisition', 'r');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.branch', 'b');
        $qb->leftJoin('b.parent', 'com');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.tenderVendor', 'tv');
        $qb->leftJoin('e.enlistedVendor', 'ev');
        $qb->leftJoin('e.paymentMode', 'pm');
        $qb->leftJoin('e.deliveryMode', 'dm');
        $qb->leftJoin('e.workOrderItems','wi');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        if($company){
            $qb->andWhere('com.id = :branch')->setParameter('branch',$company);
        }
        if (!empty($startDate)) {
            $datetime = new \DateTime($startDate);
            $startDate = $datetime->format('Y-m-d 00:00:00');
            $qb->andWhere("e.created >= :startDate");
            $qb->setParameter('startDate', $startDate);
        }
        if (!empty($endDate)) {
            $datetime = new \DateTime($endDate);
            $endDate = $datetime->format('Y-m-d 23:59:59');
            $qb->andWhere("e.created <= :endDate");
            $qb->setParameter('endDate', $endDate);
        }
        $qb->groupBy('e.id');
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function requisitionItemStatus($config , $data = "")
    {

        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionItem::class,'e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.createdBy','cb');
        $qb->leftJoin('r.jobRequisition','jr');
        $qb->leftJoin('r.section','s');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.department','d');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.unit','it');
        $qb->leftJoin('e.category','cat');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->leftJoin('e.workorderItems','wi');
        $qb->leftJoin('wi.receiveItems','ri');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','e.description as description','(e.quantity -  COALESCE(e.issueQuantity,0)) as remaining');
        $qb->addSelect('SUM(wi.quantity) as workorderQuantity','AVG(wi.price) as workorderPrice','SUM(wi.discount) as discount');
        $qb->addSelect('SUM(ri.quantity) as receiveQuantity');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.created as requisitionCreatedDate','r.updated as requisitionDate');
        $qb->addSelect('jr.id as jobRequisitionId','jr.requisitionNo as jobRequisitionNo');
        $qb->addSelect('cb.name as createdBy');
        $qb->addSelect('s.name as section');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('b.name as company');
        $qb->addSelect('cat.name as category');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('unit.code as unitName');
        $qb->addSelect('d.name as department');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('it.name as uom');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $qb->andWhere('r.requisitionMode IN (:type)')->setParameter('type',array("CAPEX","OPEX","INV"));
        $this->handleReportBetween($qb,$data);
        $qb->groupBy('e.id');
        $qb->orderBy('e.created',"DESC");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function stockReceiveItem($config , $data = "")
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderWorkorderReceiveItem::class,'e');
        $qb->leftJoin('e.workorderReceive','r');
        $qb->leftJoin('e.tenderWorkorderItem','wi');
        $qb->leftJoin('wi.requisition','pr');
        $qb->leftJoin('wi.tenderWorkorder','w');
        $qb->leftJoin('wi.wearhouse','store');
        $qb->leftJoin('w.branch','cu');
        $qb->leftJoin('w.tenderVendor','tv');
        $qb->leftJoin('r.createdBy','gcb');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('item.category','cat');
        $qb->leftJoin('item.unit','it');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->select('e.id as id','e.quantity as quantity','e.price as price','e.casNo as casNo','e.casType as casType','e.msds as msds','e.expireDate as expireDate');
        $qb->addSelect('r.created as created','r.receiveDate as receiveDate','r.invoice as grn','r.challanDate as challanDate','r.challanNo as challanNo','r.gatePassNo as gatePassNo');
        $qb->addSelect('wi.quantity as wiQuantity','wi.price as wiPrice','wi.receive as wiReceive','wi.subTotal as wiSubTotal');
        $qb->addSelect('w.invoice as invoice','w.updated as workorderDate');
        $qb->addSelect('cu.code as unit');
        $qb->addSelect('gcb.name as grnCreatdBy');
        $qb->addSelect('store.code as wearhouse');
        $qb->addSelect('tv.name as vendor');
        $qb->addSelect('sb.id as sbId');
        $qb->addSelect('cat.name as category');
        $qb->addSelect('brand.name as brandName');
        $qb->addSelect('size.name as sizeName');
        $qb->addSelect('color.name as colorName');
        $qb->addSelect('item.name as itemName','item.id as itemId');
        $qb->addSelect('it.name as uom');
        $qb->addSelect('pr.requisitionNo as requisitionNo');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess = :process')->setParameter('process',"Approved");
        $this->handleReportBetween($qb,$data);
        $qb->orderBy('e.created',"DESC");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }


    public function kanbanDashboard( $config, $form ): array
    {
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : "";
        $wearhouse = isset($data['wearhouse']) ? $data['wearhouse'] : '';
        $item = isset($data['item']) ? $data['item'] : '';
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from(TenderWorkorderItem::class,'e');
        $qb->join('e.tenderWorkorder','wo');
        $qb->leftJoin('e.stockBook','sb');
        $qb->leftJoin('sb.item','item');
        $qb->leftJoin('sb.brand','b');
        $qb->leftJoin('sb.brand','brand');
        $qb->leftJoin('sb.size','size');
        $qb->leftJoin('sb.color','color');
        $qb->leftJoin('item.unit','unit');
        $qb->select("sb.id as stockId","sb.price as price","sb.cmp as cmp","sb.updated as updated");
        $qb->addSelect("item.itemCode as itemCode","item.name as name");
        $qb->addSelect("SUM(e.quantity) as purchaseQuantity","SUM(e.receive) as receiveQuantity");
        $qb->addSelect("item.id as itemId");
        $qb->addSelect("unit.name as uom");
        $qb->addSelect("size.name as sizeName");
        $qb->addSelect("color.name as colorName");
        $qb->addSelect("brand.name as brandName");
        $qb->where("wo.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere('wo.waitingProcess = :process')->setParameter('process',"Approved");
        if($item){
            $qb->andWhere('item.id = :itemId')->setParameter('itemId',$item);
        }
        $qb->groupBy("sb.id");
        if($wearhouse){
            $qb->join('wo.wearhouse','wh');
            $qb->andWhere('wh.id = :whId')->setParameter('whId',$wearhouse);
        }
        $qb->orderBy("item.name",'ASC');
        $result = $qb->getQuery()->getArrayResult();

        $qb = $em->createQueryBuilder();
        $qb->from(StockHistory::class,'e');
        $qb->leftJoin('e.stockBook','sb');
        $qb->select("sb.id as stockId");
        $qb->addSelect("SUM(e.stockIn) as stockIn","SUM(e.stockOut) as stockOut");
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->groupBy("sb.id");
        if($item){
            $qb->leftJoin('sb.item','item');
            $qb->andWhere('item.id = :itemId')->setParameter('itemId',$item);
        }
        $qb->groupBy("sb.id");
        if($wearhouse){
            $qb->join('e.wearhouse','wh');
            $qb->andWhere('wh.id = :whId')->setParameter('whId',$wearhouse);
        }
        $closing = $qb->getQuery()->getArrayResult();
        $entities = array();
        foreach ($closing as $row):
            $entities[$row['stockId']] = $row;
        endforeach;
        $array = array('receive'=>$result,'closing'=>$entities);
        return $array;



    }


}
