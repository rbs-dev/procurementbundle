<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Budget;
use App\Entity\Domain\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetAmendment;
use Terminalbd\BudgetBundle\Entity\BudgetGlYear;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetRequisitionAmendment;
use Terminalbd\BudgetBundle\Repository\BudgetMonthRepository;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionBudgetItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionBudgetItem::class);
    }

    public function insertBudgetItem(Requisition $requisition,$budgetYear = "2024-2025", $process ="")
    {
        $em = $this->_em;
        $month = strtolower($requisition->getCreated()->format('F'));
        $currentMonth = strtolower($requisition->getCreated()->format('d-m-Y'));
        $budgetTillMonth = $this->getFirstToCurrentMonthsFinancialYear($currentMonth);
        $budgetTillMonth = $this->getCurrentToFirstMonthsFinancialYear();
        if($requisition->getBillMonth() == "Previous" and $month == "june" and in_array($requisition->getRequisitionMode() , ["Expense",'Confidential'])){
            $budgetTillMonth = $this->getCurrentToFirstMonthsFinancialYear();
        }else{
            $budgetTillMonth = $this->getFirstToCurrentMonthsFinancialYear($currentMonth);
        }
        $year = ($requisition->getFinancialYear()) ? $requisition->getFinancialYear() : $budgetYear;
        $budgetadjustmentMonths = $this->getCurrentToLastMonthsFinancialYear();

        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionItem::class,'e');
        $qb->join('e.item','item');
        $qb->join('e.budgetHead','gl');
        $qb->join('e.requisition','requisition');
        $qb->select('sum(e.subTotal) as requisitionAmount');
        $qb->addSelect('gl.id as glId','gl.name as glName');
        $qb->where('requisition.id = :requisition')->setParameter('requisition',$requisition->getId());
        $qb->groupBy('gl.id');
        $result = $qb->getQuery()->getArrayResult();
        $requisitionId = $requisition->getId();

        if(empty($result)){
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->delete('TerminalbdProcurementBundle:RequisitionBudgetItem', 'e');
            $qb->where('e.requisition = :requisition')->setParameter('requisition',$requisitionId);
            $qb->getQuery()->execute();
        }elseif(!empty($result)){
            if($requisition->getBudgetItems() and $requisition->getCompanyUnit()){
                $guids =  array();
                foreach ($result as $res){
                    $guids[]  = $res['glId'];
                }
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->delete('TerminalbdProcurementBundle:RequisitionBudgetItem', 'e');
                $qb->where('e.requisition = :requisition')->setParameter('requisition',$requisitionId);
                $qb->andWhere('e.gl NOT  IN (:guids)')->setParameter(':guids', $guids);
                $qb->getQuery()->execute();
            }
            foreach ($result as $row) {

                /* @var $budgetMonth BudgetMonth */

                if($requisition->getCompanyUnit()){
                    $company = $requisition->getCompanyUnit()->getParent();
                    $budgetMonth = $em->getRepository(BudgetMonth::class)->findOneBy(array('company' => $company, 'glYear' => $year, 'month' => $month, 'gl' => $row['glId']));
                    if ($budgetMonth) {
                        $budgetReset = $em->getRepository(RequisitionItem::class)->resetGLBudgetTillMonth($budgetMonth,$budgetTillMonth);
                        $yralyBudget = $em->getRepository(BudgetMonth::class)->glSpecificYearlyBudget($budgetMonth);
                        $monthlyBudget = $em->getRepository(BudgetMonth::class)->glSpecificMonthlyBudget($budgetMonth);
                        $tillBudget = $em->getRepository(BudgetMonth::class)->glMonthTillAmount($budgetMonth, $budgetTillMonth);
                        $tillAmount = $tillBudget['tillBudget'];
                        $adjustmebtMinus = $tillBudget['adjustmebtMinus'];
                        // $adjustmentMinus = $em->getRepository(BudgetMonth::class)->glMonthAdjustAmountMinus($budgetMonth, $budgetadjustmentMonths);
                        // $adjustAmount = $em->getRepository(BudgetMonth::class)->glMonthTillAdjustAmount($budgetMonth, $budgetadjustmentMonths);
                        $exist = $this->findOneBy(array('requisition' => $requisition, 'glBudgetMonth' => $budgetMonth));
                        $tillMonthAmount = (($tillAmount['amount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $forcastTillMonthAmount = (($tillAmount['forcastAmount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $actualTillMonthAmount = (($tillAmount['actualAmount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $tillMonthExpenseAmount = ($tillAmount['expense'] + $tillAmount['hold'] - $row['requisitionAmount']);
                        if (empty($exist)) {
                            $entity = new RequisitionBudgetItem();
                            $entity->setRequisition($requisition);
                            $entity->setGl($budgetMonth->getGl());
                            $entity->setRequisitionAmount($row['requisitionAmount']);
                            $entity->setGlBudgetMonth($budgetMonth);
                            $entity->setMonthlyBudget($budgetMonth->getAmount() + $budgetMonth->getAmendment() + $budgetMonth->getAdjustmentPlus());
                            $entity->setBudgetTillAmount($tillMonthAmount);
                            $entity->setBudgetTillForcastAmount($forcastTillMonthAmount);
                            $entity->setBudgetTillActualAmount($actualTillMonthAmount);
                            $entity->setBudgetTillExpense($tillMonthExpenseAmount);
                            $entity->setBudgetTillAdditional($tillAmount['amendment']);
                           // $entity->setHoldBudget($tillAmount['hold']);
                            $entity->setHoldBudget($tillAmount['hold'] - $row['requisitionAmount']);
                            $entity->setExpenseBudget($budgetMonth->getExpense());
                            $entity->setBudgetAdditional($budgetMonth->getAmendment());
                            $entity->setYearlyBudget($yralyBudget['amount']);
                            $currentRemaining = round($entity->getBudgetTillAmount() - ($entity->getBudgetTillExpense() + $row['requisitionAmount']));
                            if ($currentRemaining >= 0) {
                                $entity->setLowStatus(false);
                            } else {
                                $entity->setLowStatus(true);
                            }
                        }else{
                            $entity = $exist;
                            $entity->setRequisitionAmount($row['requisitionAmount']);
                            $entity->setGlBudgetMonth($budgetMonth);
                            $entity->setMonthlyBudget($budgetMonth->getAmount() + $budgetMonth->getAmendment() + $budgetMonth->getAdjustmentPlus() - $budgetMonth->getAdjustment());
                            $entity->setBudgetTillAmount($tillMonthAmount);
                            $entity->setBudgetTillForcastAmount($forcastTillMonthAmount);
                            $entity->setBudgetTillActualAmount($actualTillMonthAmount);
                            $entity->setBudgetTillExpense($tillMonthExpenseAmount);
                            $entity->setBudgetTillAdditional($tillAmount['amendment']);
                            $entity->setHoldBudget($tillAmount['hold'] - $row['requisitionAmount']);
                            //$entity->setHoldBudget($tillAmount['hold']);
                            $entity->setExpenseBudget($budgetMonth->getExpense());
                            $entity->setBudgetAdditional($budgetMonth->getAmendment());
                            $entity->setYearlyBudget($yralyBudget['amount']);
                            $currentRemaining = round($entity->getBudgetTillAmount() - ($entity->getBudgetTillExpense()+$row['requisitionAmount']));
                            if ($currentRemaining >= 0) {
                                $entity->setLowStatus(false);
                            } else {
                                $entity->setLowStatus(true);
                            }
                        }
                        $entity->setCompany($company);
                        $em->persist($entity);
                        $em->flush();

                    }
                }
            }
        }
    }

    public function deleteRequisitionItemBudgetHead(Requisition $requisition,$year,$gl){
        $em = $this->_em;
        $company = $requisition->getCompanyUnit()->getParent()->getId();
        $em->getRepository(RequisitionItem::class)->processRequisitionGLBudgetMonth($company, $year,$gl);
        $this->insertBudgetItem($requisition,$year);
    }

    public function deleteRequisitionBudgetHead(Requisition $requisition,$year){
        $em = $this->_em;
        $company = $requisition->getCompanyUnit()->getParent()->getId();
        if($requisition->getRequisitionItems()){
            $company = $requisition->getCompanyUnit()->getParent()->getId();
            $qb = $this->_em->createQueryBuilder();
            $qb->from(RequisitionItem::class,'e');
            $qb->join('e.item','item');
            $qb->join('e.budgetHead','gl');
            $qb->join('e.requisition','requisition');
            $qb->select('gl.id as glId');
            $qb->where('requisition.id = :requisition')->setParameter('requisition',$requisition->getId());
            $qb->groupBy('gl.id');
            $result = $qb->getQuery()->getArrayResult();
            if(!empty($result)) {
                foreach ($result as $row) {
                    $em->getRepository(RequisitionItem::class)->processRequisitionGLBudgetMonth($company, $year,$row['glId']);
                }
            }
            $qb1 = $this->getEntityManager()->createQueryBuilder()
                ->delete('TerminalbdProcurementBundle:RequisitionBudgetItem', 'e')
                ->where('e.requisition = :requisition')->setParameter('requisition',$requisition->getId());
            $qb1->getQuery()->execute();
        }
    }

    public function getFirstToCurrentMonthsFinancialYear($tillMonth) {


        $year = date('Y',strtotime($tillMonth));
        $month = date('m',strtotime($tillMonth));
        if($month < 7 ){
            $year = $year-1;
        }
        $start_date = date('Y-m-d',strtotime(($year)."-07-01"));
        $end_date = $tillMonth;
        $start    = (new \DateTime($start_date))->modify('first day of this month');
        $end    = (new \DateTime($end_date))->modify('last day of this month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);
        $returnArray=array();
        foreach ($period as $dt) {
            $returnArray[]=$dt->format("F");
        }
        return $returnArray;
    }

    function getCurrentToFirstMonthsFinancialYear() {

        $year = date('Y');
        $month = date('m');
        if($month < 7 ){
            $year = $year-1;
        }
        $start    = (new \DateTime('now'))->modify('first day of this month');
        $end_date = date('Y-m-d',strtotime(($year+1).'-06-30'));
        $end      = (new \DateTime($end_date))->modify('last day of this month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);
        $returnArray=array();
        foreach ($period as $dt) {
            $returnArray[]=$dt->format("F");
        }
        return $returnArray;
    }

    function getCurrentToLastMonthsFinancialYear() {
        $year = date('Y');
        $month = date('m');
        if($month < 7 ){
            $year = $year - 1;
        }
        $start    = (new \DateTime('now'))->modify('first day of this month');
        $end_date = date('Y-m-d',strtotime(($year+1).'-06-30'));
        $end      = (new \DateTime($end_date))->modify('last day of this month');
        $interval = \DateInterval::createFromDateString('1 month');
        $period   = new \DatePeriod($start, $interval, $end);
        $returnArray=array();
        foreach ($period as $dt) {
            $returnArray[]=$dt->format("F");
        }
        return $returnArray;
    }

    public function insertAmendmentBudgetItem(BudgetRequisitionAmendment $requisitionAmendment)
    {
        /* @var $item BudgetAmendment */

        $em = $this->_em;
        if($requisitionAmendment->getBudgetAmendments()){
            foreach ($requisitionAmendment->getBudgetAmendments() as $item){
                $id = $item->getRequisitionBudgetId();
                $find = $em->getRepository(RequisitionBudgetItem::class)->findOneBy(['id' => $id]);
                if($find){
                    $find->setBudgetAmendmentAmount($item->getAmount());
                    $find->setBudgetAmendment($item);
                    $em->persist($find);
                    $em->flush();
                }

            }
        }

    }

    public function requisitionBudgetUpdateHold(Requisition $entity)
    {
        $em = $this->_em;
        /* @var $budgetItem RequisitionBudgetItem */
        foreach ($entity->getBudgetItems() as $budgetItem){
            $budgetMonth = $budgetItem->getGlBudgetMonth();
            $holdAmount =  $budgetMonth->getHoldAmount();
            $curHold =  $holdAmount + $budgetItem->getRequisitionAmount();
            $budgetMonth->setHoldAmount($curHold);
            $em->persist($budgetMonth);
            $em->flush();
        }
    }

    public function requisitionBudgetGenerate(Requisition $entity)
    {
        $month = strtolower($entity->getCreated()->format('F'));
        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionItem::class,'e');
        $qb->join('e.item','item');
        $qb->join('item.category','category');
        $qb->join('category.generalLedger','gl');
        $qb->join('e.requisition','requisition');
        $qb->join('requisition.comapnyRequisitionShares','share');
        $qb->join('share.branch','b');
        $qb->select('sum(e.subTotal) requisitionAmount');
        $qb->addSelect('b.id as branchId','b.name as branchName');
        $qb->addSelect('gl.id as id','gl.name as glName');
        $qb->addSelect('share.branchShare as branchShare');
        $qb->addSelect('((sum(e.subTotal)* share.branchShare)/100) as percentAmount');
        $qb->where('e.requisition = :requisition')->setParameter('requisition',$entity->getId());
        $qb->andWhere('requisition.waitingProcess = :process')->setParameter('process','In-progress');
        $qb->andWhere('share.branchShare > 0');
        $qb->groupBy('gl.id','share.id');
        $result = $qb->getQuery()->getArrayResult();
        $ids = array();
        $branchShares = array();

        foreach ($result as $id){
            $ids[] = $id['id'];
            $branchShares[] = $result;
        }

        $qb1 = $this->_em->createQueryBuilder();
        $qb1->from(BudgetMonth::class,'e');
        $qb1->join('e.gl','gl');
        $qb1->join('e.budgetYear','budgetYear');
        $qb1->join('budgetYear.company','branch');
        $qb1->select("e.amount as monthAmount",'e.amount','e.remaining');
        $qb1->addSelect('gl.id as glId','gl.generalLedgerCode as glCode','gl.name as glName');
        $qb1->where('e.gl IN (:ids)')->setParameter('ids',$ids);
        $qb1->andWhere('e.month = :month')->setParameter('month',$month);
        $qb->groupBy('gl.id','branch.id');
        $result1 = $qb1->getQuery()->getArrayResult();
        $monthlyBudget = array();
        foreach ($result1 as $b){
            $monthlyBudget[$b['glId']] = $b;
        }
        return array('branchShares' => $branchShares, 'puchaseBudgetLedger' => $result, 'monthlyBudget'=> $monthlyBudget);
    }

    public function requisitionItemExpense(BudgetMonth $budgetMonth)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition', 'mp');
        $qb->select('SUM(e.requisitionAmount) AS amount');
        $qb->where('e.glBudgetMonth = :gl')->setParameter('gl', $budgetMonth->getId());
        $qb->andWhere('mp.waitingProcess NOT IN (:process)')->setParameter('process', ['Reversed','Rejected']);
        $qb->andWhere('mp.isDelete = 0 ');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['amount'];
    }

    public function requisitionItemHoldAmount(BudgetMonth $budgetMonth)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition', 'mp');
        $qb->select('SUM(e.requisitionAmount) AS amount');
        $qb->where('e.glBudgetMonth = :gl')->setParameter('gl', $budgetMonth->getId());
        $qb->andWhere('mp.waitingProcess NOT IN (:process)')->setParameter('process', ['Reversed','Rejected']);
        $qb->andWhere('mp.isDelete = 0 ');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['amount'];
    }

    public function requisitionBudgetMonthExpense(BudgetMonth $budgetMonth)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition', 'mp');
        $qb->join('mp.budgetRequisition', 'br');
        $qb->select('SUM(e.requisitionAmount) AS amount');
        $qb->where('e.glBudgetMonth = :gl')->setParameter('gl', $budgetMonth->getId());
        $qb->andWhere('mp.waitingProcess IN  (:process)')->setParameter('process', ['Approved','Closed']);
        $qb->andWhere('br.waitingProcess IN (:process)')->setParameter('process', ['Approved']);
        $qb->andWhere('mp.isDelete = 0 ');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['amount'];
    }

    public function getGlToalHoldAmount(BudgetGlYear $glYear)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.requisition', 'mp');
        $qb->join('e.glBudgetMonth', 'glm');
        $qb->select('SUM(e.requisitionAmount) AS amount');
        $qb->where('glm.budgetGlYear = :gl')->setParameter('gl', $glYear->getId());
        $qb->andWhere('mp.waitingProcess NOT IN (:process)')->setParameter('process', ['Reversed','Rejected']);
        $qb->andWhere('mp.isDelete = 0');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['amount'];
    }

    public function closeRequisitionItem(Requisition $requisition)
    {
        $em = $this->_em;
        if($requisition->getBudgetItems()){
            /* @var $item RequisitionBudgetItem */
            foreach ($requisition->getBudgetItems() as $item){
                $item->setRequisitionAmount($item->getExpenseRequisition());
                $item->setIsClosed(1);
                $item->setHoldBudget(0);
                $em->persist($item);
                $em->flush();
                $em->getRepository(BudgetMonth::class)->remainingBudget($item->getGlBudgetMonth());
            }
        }
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['requisition_filter_form'])){
            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $endDate = isset($data['endDate'])? $data['endDate'] :'';
            $generalLedger = isset($data['generalLedger'])? $data['generalLedger'] :'';
            $branch = isset($data['branch'])? $data['branch'] :'';
            $company = isset($data['company'])? $data['company'] :'';
            $unit = isset($data['unit'])? $data['unit'] :'';
            $department = isset($data['department'])? $data['department'] :'';
            $heads = isset($data['head'])? $data['head'] :'';
            $glHead = isset($data['glHead'])? $data['glHead'] :'';
            $requisitionMode = isset($data['requisitionMode'])? $data['requisitionMode'] :'';
            $priroty = isset($data['priroty'])? $data['priroty'] :'';
            $process = isset($data['process'])? $data['process'] :'';
            $requisitionType = isset($data['requisitionType'])? $data['requisitionType'] :'';
            $requisitionNo = !empty($data['requisitionNo'])? trim($data['requisitionNo']) :'';
            $itemName = !empty($data['itemName'])? trim($data['itemName']) :'';
            $createdBy = !empty($data['createdBy'])? trim($data['createdBy']) :'';
            $keyword = !empty($data['keyword'])? trim($data['keyword']) :'';
            if(!empty($requisitionNo)){
                $qb->andWhere($qb->expr()->like("r.requisitionNo", "'%$requisitionNo%'"));
            }
            if(!empty($generalLedger)){
                $qb->andWhere("e.gl = :budgetHead")->setParameter('budgetHead', $generalLedger);
            }
            if (!empty($requisitionMode)) {
                $qb->andWhere($qb->expr()->like("r.requisitionMode", "'%$requisitionMode%'"));
            }
            if(!empty($itemName)){
                $qb->andWhere($qb->expr()->like("item.name", "'%$itemName%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere("r.createdBy = :createdBy")->setParameter('createdBy', $createdBy);
            }
            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($heads)) {
                $qb->andWhere("gl.id IN (:cids)")->setParameter('cids', $heads);
            }
            if(!empty($branch)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$branch);
            }
            if(!empty($company)){
                $qb->andWhere('b.id = :branch')->setParameter('branch',$company);
            }
            if(!empty($unit)){
                $qb->andWhere('unit.id = :cuunit')->setParameter('cuunit',$unit);
            }
            if(!empty($department)){
                $qb->andWhere('d.id = :department')->setParameter('department',$department);
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("r.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }
            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("r.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function requisitionItemInprogressReport($config , $data = "")
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'ASC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.createdBy','u');
        $qb->leftJoin('r.reportTo','reportTo');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.processDepartment','d');
        $qb->leftJoin('e.glBudgetMonth','glMonth');
        $qb->leftJoin('glMonth.gl','gl');
        $qb->select('e.id as id','e.requisitionAmount as amount');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.created as requisitionDate','r.waitingProcess as process');
        $qb->addSelect('gl.generalLedgerCode  as glCode','gl.name  as glName');
        $qb->addSelect('glMonth.month as month');
        $qb->addSelect('b.name as branches');
        $qb->addSelect('d.name as department');
        $qb->addSelect('u.name as createdBy');
        $qb->addSelect('reportTo.name as approvedBy');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',array("New","In-progress"));
        $qb->orderBy('r.created',"DESC");
        $this->handleSearchBetween($qb,$data);
        if(!empty($data['items'])){
            $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
        }
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function requisitionRejects($config , $items)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.requisition','r');
        $qb->leftJoin('r.createdBy','u');
        $qb->leftJoin('r.reportTo','reportTo');
        $qb->leftJoin('r.companyUnit','unit');
        $qb->leftJoin('unit.parent','b');
        $qb->leftJoin('r.processDepartment','d');
        $qb->select('r.id as id','r.subTotal as amount','r.created as created');
        $qb->addSelect('r.id as requisitionId','r.requisitionNo as requisitionNo','r.created as requisitionDate','r.waitingProcess as process');
        $qb->addSelect('b.name as branches');
        $qb->addSelect('d.name as department');
        $qb->addSelect('u.name as createdBy');
        $qb->addSelect('reportTo.name as approvedBy');
        $qb->where('r.config = :config')->setParameter('config',"{$config}");
        $qb->andWhere('r.waitingProcess IN (:process)')->setParameter('process',array("New","In-progress"));
        $qb->andWhere($qb->expr()->in("e.id", $items ));
        $qb->groupBy('r.id');
        $qb->orderBy('e.created',"DESC");
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function resetRequisitionBudgetItem(Requisition $requisition , $budgetYear = "2024-2025", $process ="")
    {
        $em = $this->_em;
        $month = strtolower($requisition->getCreated()->format('F'));
        $currentMonth = strtolower($requisition->getCreated()->format('d-m-Y'));
        $budgetTillMonth = $this->getFirstToCurrentMonthsFinancialYear($currentMonth);
        $budgetTillMonth = $this->getCurrentToFirstMonthsFinancialYear();
        if($requisition->getBillMonth() == "Previous" and $month == "june" and in_array($requisition->getRequisitionMode() , ["Expense",'Confidential'])){
            $budgetTillMonth = $this->getCurrentToFirstMonthsFinancialYear();
        }else{
            $budgetTillMonth = $this->getFirstToCurrentMonthsFinancialYear($currentMonth);
        }
        $year = ($requisition->getFinancialYear()) ? $requisition->getFinancialYear() : $budgetYear;
        $budgetadjustmentMonths = $this->getCurrentToLastMonthsFinancialYear();

        $qb = $this->_em->createQueryBuilder();
        $qb->from(RequisitionItem::class,'e');
        $qb->join('e.item','item');
        $qb->join('e.budgetHead','gl');
        $qb->join('e.requisition','requisition');
        $qb->select('sum(e.subTotal) as requisitionAmount');
        $qb->addSelect('gl.id as glId','gl.name as glName');
        $qb->where('requisition.id = :requisition')->setParameter('requisition',$requisition->getId());
        $qb->groupBy('gl.id');
        $result = $qb->getQuery()->getArrayResult();
        $requisitionId = $requisition->getId();

        if(empty($result)){
            $qb = $this->getEntityManager()->createQueryBuilder();
            $qb->delete('TerminalbdProcurementBundle:RequisitionBudgetItem', 'e');
            $qb->where('e.requisition = :requisition')->setParameter('requisition',$requisitionId);
            $qb->getQuery()->execute();
        }elseif(!empty($result)){
            if($requisition->getBudgetItems() and $requisition->getCompanyUnit()){
                $guids =  array();
                foreach ($result as $res){
                    $guids[]  = $res['glId'];
                }
                $qb = $this->getEntityManager()->createQueryBuilder();
                $qb->delete('TerminalbdProcurementBundle:RequisitionBudgetItem', 'e');
                $qb->where('e.requisition = :requisition')->setParameter('requisition',$requisitionId);
                $qb->andWhere('e.gl NOT  IN (:guids)')->setParameter(':guids', $guids);
                $qb->getQuery()->execute();
            }
            foreach ($result as $row) {

                /* @var $budgetMonth BudgetMonth */

                if($requisition->getCompanyUnit()){
                    $company = $requisition->getCompanyUnit()->getParent();
                    $budgetMonth = $em->getRepository(BudgetMonth::class)->findOneBy(array('company' => $company, 'glYear' => $year, 'month' => $month, 'gl' => $row['glId']));
                    if ($budgetMonth) {
                        $budgetReset = $em->getRepository(RequisitionItem::class)->resetGLBudgetTillMonth($budgetMonth,$budgetTillMonth);
                        $yralyBudget = $em->getRepository(BudgetMonth::class)->glSpecificYearlyBudget($budgetMonth);
                        $monthlyBudget = $em->getRepository(BudgetMonth::class)->glSpecificMonthlyBudget($budgetMonth);
                        $tillBudget = $em->getRepository(BudgetMonth::class)->glMonthTillAmount($budgetMonth, $budgetTillMonth);
                        $tillAmount = $tillBudget['tillBudget'];
                        $adjustmebtMinus = $tillBudget['adjustmebtMinus'];
                        // $adjustmentMinus = $em->getRepository(BudgetMonth::class)->glMonthAdjustAmountMinus($budgetMonth, $budgetadjustmentMonths);
                        // $adjustAmount = $em->getRepository(BudgetMonth::class)->glMonthTillAdjustAmount($budgetMonth, $budgetadjustmentMonths);
                        $exist = $this->findOneBy(array('requisition' => $requisition, 'glBudgetMonth' => $budgetMonth));
                        $tillMonthAmount = (($tillAmount['amount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $forcastTillMonthAmount = (($tillAmount['forcastAmount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $actualTillMonthAmount = (($tillAmount['actualAmount'] + $tillAmount['amendment'] + $tillAmount['adjustmentPlus']) - $tillAmount['adjustmentMinus']);
                        $tillMonthExpenseAmount = ($tillAmount['expense'] + $tillAmount['hold'] - $row['requisitionAmount']);
                        if (empty($exist)) {
                            $entity = new RequisitionBudgetItem();
                            $entity->setRequisition($requisition);
                            $entity->setGl($budgetMonth->getGl());
                            $entity->setRequisitionAmount($row['requisitionAmount']);
                            $entity->setGlBudgetMonth($budgetMonth);
                            $entity->setMonthlyBudget($budgetMonth->getAmount() + $budgetMonth->getAmendment() + $budgetMonth->getAdjustmentPlus());
                            $entity->setBudgetTillAmount($tillMonthAmount);
                            $entity->setBudgetTillForcastAmount($forcastTillMonthAmount);
                            $entity->setBudgetTillActualAmount($actualTillMonthAmount);
                            $entity->setBudgetTillExpense($tillMonthExpenseAmount);
                            $entity->setBudgetTillAdditional($tillAmount['amendment']);
                            // $entity->setHoldBudget($tillAmount['hold']);
                            $entity->setHoldBudget($tillAmount['hold'] - $row['requisitionAmount']);
                            $entity->setExpenseBudget($budgetMonth->getExpense());
                            $entity->setBudgetAdditional($budgetMonth->getAmendment());
                            $entity->setYearlyBudget($yralyBudget['amount']);
                            $currentRemaining = round($entity->getBudgetTillAmount() - ($entity->getBudgetTillExpense() + $row['requisitionAmount']));
                            if ($currentRemaining >= 0) {
                                $entity->setLowStatus(false);
                            } else {
                                $entity->setLowStatus(true);
                            }
                        }else{
                            $entity = $exist;
                            $entity->setRequisitionAmount($row['requisitionAmount']);
                            $entity->setGlBudgetMonth($budgetMonth);
                            $entity->setMonthlyBudget($budgetMonth->getAmount() + $budgetMonth->getAmendment() + $budgetMonth->getAdjustmentPlus() - $budgetMonth->getAdjustment());
                            $entity->setBudgetTillAmount($tillMonthAmount);
                            $entity->setBudgetTillForcastAmount($forcastTillMonthAmount);
                            $entity->setBudgetTillActualAmount($actualTillMonthAmount);
                            $entity->setBudgetTillExpense($tillMonthExpenseAmount);
                            $entity->setBudgetTillAdditional($tillAmount['amendment']);
                            $entity->setHoldBudget($tillAmount['hold'] - $row['requisitionAmount']);
                            //$entity->setHoldBudget($tillAmount['hold']);
                            $entity->setExpenseBudget($budgetMonth->getExpense());
                            $entity->setBudgetAdditional($budgetMonth->getAmendment());
                            $entity->setYearlyBudget($yralyBudget['amount']);
                            $currentRemaining = round($entity->getBudgetTillAmount() - ($entity->getBudgetTillExpense()+$row['requisitionAmount']));
                            if ($currentRemaining >= 0) {
                                $entity->setLowStatus(false);
                            } else {
                                $entity->setLowStatus(true);
                            }
                        }
                        $entity->setCompany($company);
                        $em->persist($entity);
                        $em->flush();

                    }
                }
            }
        }
    }

}
