<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Application\Procurement;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderMemoComment;
use Terminalbd\ProcurementBundle\Entity\TenderPreparetion;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */

class TenderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tender::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $unit = isset($data['unit']) ? $data['unit'] : '';
            $vendor = isset($data['vendor']) ? $data['vendor'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? trim($data['invoice']): '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';
            $requisitionNo = !empty($data['requisitionNo']) ? $data['requisitionNo'] : '';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }

            if (!empty($invoice)) {
                $qb->andWhere('e.invoice LIKE :searchTerm OR r.requisitionNo LIKE :searchTerm OR tc.invoice LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($invoice).'%');
            }

            if (!empty($requisitionNo)) {
                $qb->andWhere('r.requisitionNo LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($requisitionNo).'%');
            }

            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($vendor)) {
                $qb->andWhere('v.id = :vendorId')->setParameter('vendorId', $vendor);
            }
            if (!empty($unit)) {
                $qb->andWhere('unit.id = :cunit')->setParameter('cunit', $unit);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function findBankMemoLists($config)
    {
        
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.subject as subject', 'e.created as created');
        $qb->join('e.tenderMemo','tm');
     //   $qb->leftJoin('e.meetingComments','tmc');
        $qb->where('e.config = :config')->setParameter('config', $config);
        //$qb->andWhere('e.tenderCommittee IS NULL');
        $qb->andWhere('e.processMode = :processMode')->setParameter('processMode', 'tender');
        $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved"]);
        $qb->andWhere('tm.id IS NOT NULL');
     //   $qb->andWhere("tmc.process != 'Closed'");
        $qb->andWhere('tm.waitingProcess IN (:tmProcess)')->setParameter('tmProcess', ["Approved"]);
        $qb->groupBy('e.id');
        $qb->orderBy('e.updated','DESC');
        $result = $qb->getQuery()->getArrayResult();
        return $result;

    }



    public function findTenderBankSearchQuery($config, User $user,  $processMode = "tender", $data = [])
    {
        $department = $user->getProfile()->getDepartment()->getId();
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess','e.processMode as processMode');
        $qb->addSelect('d.name as department','d.id as departmentId');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('ato.name as approveTo','u.id as approveId');
        $qb->addSelect('tac.id as tenderCommittee','tac.expectedDate as expectedDate');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.approveTo', 'ato');
        $qb->leftJoin('e.tenderCommittee', 'tac');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.processMode = :processMode')->setParameter('processMode', $processMode);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Rejected","Closed","Approved"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function findTenderBankRepeatOrderSearchQuery($config, User $user,  $processMode = "tender", $data = [])
    {
        $department = $user->getProfile()->getDepartment()->getId();
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess','e.processMode as processMode');
        $qb->addSelect('d.name as department','d.id as departmentId');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('ato.name as approveTo','u.id as approveId');
        $qb->addSelect('tac.id as tenderCommittee','tac.expectedDate as expectedDate');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->addSelect('wo.id as workOrder','wo.invoice as workOrderInvoice');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.approveTo', 'ato');
        $qb->leftJoin('e.tenderCommittee', 'tac');
        $qb->leftJoin('e.workOrder', 'wo');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.processMode = :processMode')->setParameter('processMode', $processMode);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Rejected","Closed","Approved"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }


    public function findBankSearchQuery($config, User $user, $data = [])
    {
        $department = $user->getProfile()->getDepartment()->getId();
        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess');
        $qb->addSelect('d.name as department','d.id as departmentId');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('ato.name as approveTo','u.id as approveId');
        $qb->addSelect('tac.id as tenderCommittee','tac.expectedDate as expectedDate');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.department', 'd');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.approveTo', 'ato');
        $qb->leftJoin('e.tenderCommittee', 'tac');
        $qb->where('e.config = :config')->setParameter('config', $config);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
            $qb->andWhere('e.approveTo IS NOT NULL');
        }elseif($data['mode'] == "floating"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Floating");
            $qb->andWhere('e.approveTo IS NOT NULL');
        }elseif($data['mode'] == "cs-sheet"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "CS-sheet");
            $qb->andWhere('e.approveTo IS NOT NULL');
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "Approve");
            $qb->andWhere('e.approveTo IS NULL');
            $qb->andWhere('d.id =:processDepartment')->setParameter('processDepartment', $department);
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Rejected","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function findSearchQuery($config, User $user, $processMode, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess', 'e.processMode as processMode');
        $qb->addSelect('d.name as department');
        $qb->addSelect('unit.code as companyUnit');
        $qb->addSelect('v.companyName as vendor');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionInvoice');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('r.processDepartment', 'd');
        $qb->leftJoin('r.companyUnit', 'unit');
        $qb->leftJoin('e.directVendor', 'v');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.processMode = :processMode')->setParameter('processMode', $processMode);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }


    public function updateOpenItem(Tender $tender)
    {
        $em = $this->_em;
        /* @var $item TenderItem */
        $items = $em->getRepository(TenderItemDetails::class)->findBy(['tender'=>$tender]);
        foreach ($items as $item){
            $requisitionItem = $item->getRequisitionItem();
            $issue = $em->getRepository(TenderItemDetails::class)->countTenderQuantity($requisitionItem);
            $requisitionItem->getQuantity();
            $requisitionItem->setIssueQuantity($issue);
            $remin = ($requisitionItem->getQuantity() - $issue);
            $requisitionItem->setRemainigQuantity($remin);
            $em->persist($requisitionItem);
            $em->flush();
        }
    }

    public function getTenderRequisitions($tender,TenderWorkorder $workorder)
    {

        $em = $this->_em;
        $req ="";
        $tc = $workorder->getTenderComparative();
        if($workorder->getRequisition()){
            $req = $workorder->getRequisition()->getId();
        }
        $qb = $em->createQueryBuilder();
        $qb->from(TenderWorkorder::class,'wo');
        $qb->select('requisition.id as id');
        $qb->join('wo.requisition','requisition');
        $qb->where('wo.tenderComparative = :config')->setParameter('config', $tc->getId());
        if($workorder->getRequisition()) {
            $qb->andWhere("requisition.id != '{$req}'");
        }
        $result = $qb->getQuery()->getArrayResult();
        $requisitionIds = array();
        foreach ($result as $row){
            $requisitionIds[] = $row['id'];
        }
        $qb = $this->createQueryBuilder('tender');
        $qb->select('requisition.id as id','requisition.requisitionNo as requisitionNo','department.name as departmentName','branch.name as branchName');
        $qb->join('tender.tenderItemDetails','e');
        $qb->join('e.requisitionItem','requisitionItem');
        $qb->join('requisitionItem.requisition','requisition');
        $qb->leftJoin('requisition.branch','branch');
        $qb->leftJoin('requisition.department','department');
        $qb->where('tender.id = :config')->setParameter('config', $tender);
        if($requisitionIds){
            $qb->andWhere('requisition.id NOT IN (:ids)')->setParameter('ids',$requisitionIds);
        }
        $qb->groupBy('requisition.id');
        $result = $qb->getQuery()->getArrayResult();
        $array = array();
        foreach ($result as $row){
            if($row['branchName']){
                $value = "{$row['branchName']} - {$row['requisitionNo']}";
            }else{
                $value = "{$row['branchName']} - {$row['requisitionNo']}";
            }
            $array[$value] =  $row['id'];
        }
        return $array;
    }


    public function repeatApproved(Tender $tender)
    {
        $em = $this->_em;
        $tender->setProcess('Approved');
        $tender->setWaitingProcess('Approved');
        $tender->setReportTo(null);
        $em->flush();
        $entityId =  $tender->getId();
        $qb = $em->createQueryBuilder();
        $ProcurementProcess = $qb->delete(ProcurementProcess::class, 'e')->where('e.entityId = ?1')->setParameter(1, $entityId)->getQuery();
        if ($ProcurementProcess) {
            $ProcurementProcess->execute();
        }
    }

    public function getRequisitionFromTender($requisitions)
    {
        $ids = array();
        foreach ($requisitions as $requisition){
            $ids[]= $requisition['id'];
        }
        $qb = $this->_em->createQueryBuilder();
        $qb->from(TenderItemDetails::class,'e');
        $qb->select('r.id','r.requisitionNo as requisitionNo','tender.id as tenderId','tender.invoice as tenderInvoice');
        $qb->join('e.requisitionItem','ri');
        $qb->join('ri.requisition','r');
        $qb->leftJoin('e.tender', 'tender');
        $qb->andWhere('r.id IN (:ids)')->setParameter('ids',$ids);
        $qb->groupBy('r.id');
        $qb->groupBy('tender.id');
        $result = $qb->getQuery()->getArrayResult();
        $tenders = array();
        foreach ($result as $tender):
            $tenders[$tender['requisitionNo']][] = $tender;
        endforeach;
        return $tenders;
    }

    public function resetPurchaseWorder(Tender $tender)
    {
        $entityId = $tender->getId();
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $cs = $qb->delete(TenderComparative::class, 'e')->where('e.tender = ?1')->setParameter(1, $entityId)->getQuery();
        if ($cs) {
            $cs->execute();
        }
        $TenderItem = $qb->delete(TenderItem::class, 'e')->where('e.tender = ?1')->setParameter(1, $entityId)->getQuery();
        if ($TenderItem) {
            $TenderItem->execute();
        }
        if($tender->getRequisition()){
            $em->getRepository(Requisition::class)->updatePurchaseOrderItem($tender->getRequisition());
        }
        $tender = $qb->delete(Tender::class, 'e')->where('e.id = ?1')->setParameter(1, $entityId)->getQuery();
        if ($tender) {
            $tender->execute();
        }

    }

    public function vendorDeletes($entity)
    {
        $em = $this->_em;
        $id = $entity->getId();
        $qb = $em->createQueryBuilder();
        $delete = $qb->delete(TenderVendor::class, 'e')->where('e.tender = ?1')->setParameter(1, $id)->getQuery();
        if($delete){
            $delete->execute();
        }
    }
    public function directWorkOrderCount($config, User $user, $processMode, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.invoice as invoice','e.created as created', 'e.process as process', 'e.waitingProcess as waitingProcess', 'e.processMode as processMode');
        $qb->addSelect('d.name as department');
        $qb->addSelect('unit.code as companyUnit');
        $qb->addSelect('v.companyName as vendor');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('r.id as requisition','r.requisitionNo as requisitionInvoice');
        $qb->addSelect('tc.id as tenderComparative','tc.invoice as tenderComparativeInvoice','tc.process as comparativeProcess');
        $qb->leftJoin('e.tenderComparative', 'tc');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.requisition', 'r');
        $qb->leftJoin('r.processDepartment', 'd');
        $qb->leftJoin('r.companyUnit', 'unit');
        $qb->leftJoin('e.directVendor', 'v');
        $qb->where('e.config = :config')->setParameter('config', $config);
        $qb->andWhere('e.processMode = :processMode')->setParameter('processMode', $processMode);
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess =:process')->setParameter('process', "In-progress");
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "list" and !empty($user->getProfile()->getDepartment())){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

}


