<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\BudgetBundle\Entity\BudgetGlAdditional;
use Terminalbd\DemoBundle\TerminalbdDemoBundle;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\UtilizationLetter;
use Terminalbd\ProcurementBundle\Service\Image;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class UtilizationLetterRepository extends ServiceEntityRepository
{


    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UtilizationLetter::class);
    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['requisition_filter_form'])) {

            $data = $form['requisition_filter_form'];
            $startDate = isset($data['startDate']) ? $data['startDate'] : '';
            $endDate = isset($data['endDate']) ? $data['endDate'] : '';
            $department = isset($data['department']) ? $data['department'] : '';
            $process = isset($data['process']) ? $data['process'] : '';
            $invoice = !empty($data['invoice']) ? $data['invoice'] : '';
            $tenderVendor = !empty($data['vendor']) ? $data['vendor'] : '';
            $branch = !empty($data['branch']) ? $data['branch'] : '';
            $createdBy = !empty($data['createdBy']) ? $data['createdBy'] : '';
            $generalLedger = isset($data['generalLedger'])? $data['generalLedger'] :'';

            if (!empty($process)) {
                $qb->andWhere('e.process = :process')->setParameter('process',$process);
            }
            if (!empty($invoice)) {
                $qb->andWhere($qb->expr()->like("e.requisitionNo", "'%$invoice%'"));
            }
            if (!empty($createdBy)) {
                $qb->andWhere('u.id = :user')->setParameter('user', $createdBy);
            }
            if (!empty($department)) {
                $qb->andWhere('d.id = :department')->setParameter('department', $department);
            }
            if (!empty($branch)) {
                $qb->andWhere('e.company = :branch')->setParameter('branch', $branch);
            }
            if(!empty($generalLedger)){
                $qb->andWhere("bgl.id = :budgetHead")->setParameter('budgetHead', $generalLedger);
            }
            if (!empty($tenderVendor)) {
                $qb->join('tv.vendor','v');
                $qb->andWhere('v.id = :tenderVendor')->setParameter('tenderVendor', $tenderVendor);
            }
            if (!empty($startDate)) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }

        }
    }

    public function findBySearchQuery($config, User $user, $data = [])
    {

        $sort = isset($data['sort']) ? $data['sort'] : 'e.created';
        $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id', 'e.requisitionNo as invoice','e.totalApprovalAmount as total','e.processAmount as processAmount','e.created as created','e.updated as updated', 'e.process as process', 'e.waitingProcess as waitingProcess','e.processOrdering as ordering','e.path as path');
        $qb->addSelect('rto.id as reportTo','rto.name as reportToName');
        $qb->addSelect('ato.id as attentionTo','ato.name as attentionToName');
        $qb->addSelect('c.id as companyId','c.name as company');
        $qb->addSelect('u.name as createdBy','u.id as userId');
        $qb->addSelect('cb.name as checkedBy','cb.id as checkedId');
        $qb->addSelect('d.name as department','d.id as departmentId');
        $qb->addSelect('reverse.id as reverseId','reverse.invoiceNo as reverseInvoiceNo');
        $qb->addSelect('fromReverse.id as fromReverseId','fromReverse.invoiceNo as fromReverseinvoice');
        $qb->addSelect('bgl.name as budgetGl');
        $qb->leftJoin('e.attentionTo', 'ato');
        $qb->leftJoin('ato.profile', 'profile');
        $qb->leftJoin('e.processDepartment', 'd');
        $qb->leftJoin('e.budgetGl', 'bg');
        $qb->leftJoin('bg.gl', 'bgl');
        $qb->leftJoin('e.reportTo', 'rto');
        $qb->leftJoin('e.createdBy', 'u');
        $qb->leftJoin('e.checkedBy', 'cb');
        $qb->leftJoin('e.company', 'c');
        $qb->leftJoin('e.reverse','reverse');
        $qb->leftJoin('e.fromReverse','fromReverse');
        $qb->where("e.isDelete != 1");
        if($data['mode'] == "in-progress") {
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ['Verify','Verified','In-progress']);
        }elseif($data['mode'] == "approve"){
            $qb->andWhere('e.reportTo =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "verify"){
            $qb->andWhere('e.process IN (:process)')->setParameter('process', ['Verify']);
            $qb->andWhere('e.waitingProcess =:process1')->setParameter('process1', "In-progress");
            $qb->andWhere('e.checkedBy =:report')->setParameter('report',"{$user->getId()}");
        }elseif($data['mode'] == "my-progress"){
            $qb->andWhere('e.attentionTo =:report')->setParameter('report',"{$user->getId()}");
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ['Verify','Verified','In-progress']);
        }elseif($data['mode'] == "list"){
            $qb->andWhere('u.id = :initior')->setParameter('initior', $user->getId());
            $qb->andWhere('e.waitingProcess NOT IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }elseif($data['mode'] == "archive"){
            $qb->andWhere('e.waitingProcess IN (:process)')->setParameter('process', ["Approved","Closed"]);
        }
        $this->handleSearchBetween($qb, $data);
        $qb->orderBy("{$sort}", $direction);
        $result = $qb->getQuery();
        return $result;

    }

    public function updateAdditionalBudget(BudgetGlAdditional $additional)
    {
        $em = $this->_em;
        $qb = $em->createQueryBuilder();
        $qb->from(BudgetGlAdditional::class,'e');
        $qb->select( 'SUM(e.amount) as amount');
        $qb->where('e.utilizationLetter = :initior')->setParameter('initior', $additional->getUtilizationLetter()->getId());
        $amount = $qb->getQuery()->getSingleScalarResult();
        $additional->getUtilizationLetter()->setProcessAmount($amount);
        $em->flush();
    }

    public function insertReverseRequsition(UtilizationLetter $letter,User $user){

        $em = $this->_em;
        $entity = new UtilizationLetter();
        $entity->setFromReverse($letter);
        $entity->getCreatedBy($user);
        $entity->setModuleProcess($letter->getModuleProcess());
        $module = "utilization-letter";
        $entity->setModule($module);
        $entity->setWaitingProcess("In-progress");
        $entity->setProcess("New");
        $entity->setCreatedBy($user);
        $entity->setConfig($letter->getConfig());
        $entity->setCondition($letter->getCondition());
        $entity->setContent($letter->getContent());
        $entity->setCompany($letter->getCompany());
        $entity->setAttentionTo($letter->getAttentionTo());
        $entity->setTitle($letter->getTitle());
        $entity->setReasons($letter->getReasons());
        $entity->setTimeline($letter->getTimeline());
        $entity->setRecoveryPlan($letter->getRecoveryPlan());
        $entity->setCheckedBy($letter->getCheckedBy());
        $entity->setApprovedBy($letter->getApprovedBy());
        $entity->setFinancialYear($letter->getFinancialYear());
        $entity->setBillMonth($letter->getBillMonth());
        $entity->setBillMonthName($letter->getCreated()->format('F'));
        if ($user->getProfile() and $user->getProfile()->getDepartment()) {
            $entity->setDepartment($user->getProfile()->getDepartment());
        }
        $entity->setBudgetGl($letter->getBudgetGl());
        $entity->setYearlyQuantity($letter->getYearlyQuantity());
        $entity->setYearlyAmount($letter->getYearlyAmount());
        $entity->setYearlyYtdAmount($letter->getYearlyYtdAmount());
        $entity->setExpenseYtdAmount($letter->getExpenseYtdAmount());
        $entity->setYtdQuantity($letter->getYtdQuantity());
        $entity->setRequiredApprovalQuantity($letter->getRequiredApprovalQuantity());
        $entity->setRequiredApprovalAmount($letter->getRequiredApprovalAmount());
        $entity->setRequiredApprovalQuantityUpto($letter->getRequiredApprovalQuantityUpto());
        $em->persist($entity);
        $em->flush();
        return $entity;

    }

}
