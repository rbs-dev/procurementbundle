<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class RequisitionItemHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RequisitionItemHistory::class);
    }

    public function requisitionItemHistory(User $user,Requisition $requisition)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($requisition->getRequisitionItems())){

            foreach ($requisition->getRequisitionItems() as $item){

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $entity->setCreatedBy($user);
                $em->persist($entity);
                $em->flush();

            }
        }
    }

    public function jobrequisitionItemHistory(User $user ,JobRequisition $requisition)
    {
        $em = $this->_em;
        /* @var $item RequisitionItem */

        if (!empty($requisition->getRequisitionItems())) {

            foreach ($requisition->getRequisitionItems() as $item) {

                $entity = new RequisitionItemHistory();
                $entity->setRequisitionItem($item);
                $entity->setActualQuantity($item->getActualQuantity());
                $entity->setQuantity($item->getQuantity());
                $entity->setCreatedBy($user);
                $em->persist($entity);
                $em->flush();

            }
        }
    }

}
