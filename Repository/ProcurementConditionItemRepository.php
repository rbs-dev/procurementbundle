<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;


/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class ProcurementConditionItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProcurementConditionItem::class);
    }

    public function insertConditionItem(ProcurementCondition $condition, $data)
    {

        $em = $this->_em;
        if ($condition and isset($data['metaValue']) and !empty($data['metaValue'])) {
            $i = 0;
            if(isset($data['metaKey']) OR isset($data['metaValue']) ){
                foreach ($data['metaKey'] as $value) {
                    $metaId = isset($data['metaId'][$i]) ? $data['metaId'][$i] : '' ;
                    if(!empty($metaId)){
                        $itemKeyValue = $this->findOneBy(array('condition' => $condition,'id' => $metaId));
                        if($itemKeyValue){
                            $this->updateMetaAttribute($itemKeyValue,$data['metaKey'][$i],$data['metaValue'][$i]);
                        }
                    }elseif(($data['metaKey'][$i] != "" and empty($metaId))){
                        $entity = new ProcurementConditionItem();
                        $entity->setConfig($condition->getConfig());
                        $entity->setMetaKey($data['metaKey'][$i]);
                        $entity->setMetaValue($data['metaValue'][$i]);
                        $entity->setCondition($condition);
                        $em->persist($entity);
                        $em->flush($entity);
                    }
                    $i++;
                }
            }

            $em->flush();

        }

    }

    public function updateMetaAttribute(ProcurementConditionItem $itemKeyValue , $key , $value ='')
    {
        $em = $this->_em;
        $itemKeyValue->setMetaKey($key);
        $itemKeyValue->setMetaValue($value);
        $em->flush();
    }

}
