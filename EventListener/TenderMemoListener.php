<?php

namespace Terminalbd\ProcurementBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderBatch;
use Terminalbd\ProcurementBundle\Entity\TenderMemo;

class TenderMemoListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $this->createCode($args);
    }

    public function createCode(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof TenderMemo) {
            $datetime = new \DateTime("now");
            $lastCode = $this->getLastCode($args, $datetime, $entity);
            $entity->setCode($lastCode+1);
            if($entity->getModule() == 'purchase-memo'){
                $entity->setInvoice(sprintf("%s%s%s","PCM-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            }elseif($entity->getModule() == 'management-memo'){
                $entity->setInvoice(sprintf("%s%s%s","MCM-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            }elseif($entity->getModule() == 'floating-memo'){
                $entity->setInvoice(sprintf("%s%s%s","TF-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            }elseif($entity->getModule() == 'board-memo'){
                $entity->setInvoice(sprintf("%s%s%s","BAM-", $datetime->format('my'), str_pad($entity->getCode(),4, '0', STR_PAD_LEFT)));
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     * @param $datetime
     * @param $entity
     * @return int|mixed
     */
    public function getLastCode(LifecycleEventArgs $args, $datetime, $entity)
    {
        $today_startdatetime = $datetime->format('Y-m-01 00:00:00');
        $today_enddatetime = $datetime->format('Y-m-t 23:59:59');
        $entityManager = $args->getEntityManager();
        $qb = $entityManager->getRepository(TenderMemo::class)->createQueryBuilder('s');
        $qb
            ->select('MAX(s.code)')
            ->where('s.config = :config')
            ->andWhere('s.module = :module')
            ->andWhere('s.updated >= :today_startdatetime')
            ->andWhere('s.updated <= :today_enddatetime')
            ->setParameter('config', $entity->getConfig())
            ->setParameter('module', $entity->getModule())
            ->setParameter('today_startdatetime', $today_startdatetime)
            ->setParameter('today_enddatetime', $today_enddatetime);
        $lastCode = $qb->getQuery()->getSingleScalarResult();
        if (empty($lastCode)) {
            return 0;
        }
        return $lastCode;
    }
}