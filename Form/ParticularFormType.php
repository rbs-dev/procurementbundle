<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ParticularType;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ParticularFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
            ])
            ->add('config', HiddenType::class, [
                'attr' => ['autofocus' => true,'value' => $config],
                'required' => true
            ])
             ->add('slug', TextType::class, [
                'attr' => ['autofocus' => true],
                'required' => true,
            ])
            ->add('code', TextType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'label.name',
                 'required' => false,

            ])

            ->add('description', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea'],
                 'required' => false,

            ])

            ->add('particularType', EntityType::class, [
                'class' => ParticularType::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a setting type',
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "info",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Particular::class,
            'config' => Procurement::class,
        ]);
    }
}
