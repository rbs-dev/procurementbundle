<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class GarmentRequisitionFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $mode  = $options['data']->getRequisitionMode();
        $departs =  $options['data']->getCreatedBy()->getProfile()->getProcessDepartment();
        $depArrs = [];
        foreach ($departs as $depart){
            $depArrs[] =  $depart->getId();
        }
        $deps  = $depArrs;
        $curMonth = Date("F");
        $prevMonth = Date("F", strtotime("first day of previous month"));
        if($options['data']->getBillMonth() == "Previous"){
            $lastMonth = Date("Y-m-d", strtotime("first day of previous month"));
            $prevMonth = Date("F", strtotime("first day of previous month"));
            $lastMonthDay = Date("Y-m-t", strtotime("first day of previous month"));
        }
        if(in_array($mode,['CAPEX','OPEX','INV'])) {
            $builder
                ->add('content', TextareaType::class, [
                    'attr' => ['autofocus' => true, 'class' => 'input editor'],
                    'required' => false,
                ])
                ->add('isEmergency', ChoiceType::class, [
                    'choices' => [
                        'Regular' => true,
                        'Emergency' => false,
                    ],
                    'expanded' => true,
                    'attr' => [
                        'class' => 'form-check-inline input'
                    ]
                ])
                ->add('companyUnit', EntityType::class, [
                    'class' => Branch::class,
                    'required' => true,
                    'group_by' => 'parent.name',
                    'choice_translation_domain' => true,
                    'query_builder' => function (EntityRepository $er) use ($terminal) {
                        return $er->createQueryBuilder('e')
                            ->join('e.parent', 'p')
                            ->where("e.branchType ='branch-unit'")
                            ->andWhere('e.isDelete IS NULL')
                            ->andWhere('p.status=1')
                            ->andWhere('e.status=1')
                            ->andWhere("e.terminal ={$terminal}")
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr' => ['class' => 'select2 input'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a company unit',
                ])
                ->add('expectedDate', DateType::class, [
                    'widget' => 'single_text',
                    'html5' => true,
                    'required' => false,
                    'attr' => ['class' => 'input', 'min' => date('Y-m-d'), 'placeholder' => "Expected Date"],
                ])
                ->add('priority', EntityType::class, [
                    'class' => Particular::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use ($config) {
                        return $er->createQueryBuilder('e')
                            ->join('e.particularType', 'type')
                            ->where('e.status =1')
                            ->andWhere("type.slug ='priority'")
                            ->andWhere("e.config ='{$config}'")
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr' => ['class' => 'select2 input'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a requisition priority',
                ])
                ->add('section', EntityType::class, [
                    'class' => Setting::class,
                    'required' => false,
                    'query_builder' => function (EntityRepository $er) use ($terminal) {
                        return $er->createQueryBuilder('e')
                            ->join('e.settingType', 'type')
                            ->where('e.status =1')
                            ->andWhere("type.slug ='section'")
                            ->andWhere("e.terminal ='{$terminal}'")
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr' => ['class' => 'select2 input'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a requisition section',
                ])
                ->add('processDepartment', EntityType::class, [
                    'class' => Setting::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use($terminal,$deps) {
                        return $er->createQueryBuilder('e')
                            ->join('e.settingType','type')
                            ->where('e.status =1')
                            ->andWhere("type.slug ='department'")
                            ->andWhere("e.terminal ='{$terminal}'")
                            ->andWhere('e.id in (:deps)')->setParameter('deps',$deps)
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'select2 input processDepartment'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a department name',
                ])
            ;
            }
            if(in_array($mode,['Expense','Confidential'])) {
                $builder
                    ->add('content', TextareaType::class, [
                        'attr' => ['autofocus' => true, 'class' => 'input editor'],
                        'required' => false,
                    ])
                    ->add('companyUnit', EntityType::class, [
                        'class' => Branch::class,
                        'required' => true,
                        'group_by' => 'parent.name',
                        'choice_translation_domain' => true,
                        'query_builder' => function (EntityRepository $er) use ($terminal) {
                            return $er->createQueryBuilder('e')
                                ->join('e.parent', 'p')
                                ->where("e.branchType ='branch-unit'")
                                ->andWhere('e.isDelete IS NULL')
                                ->andWhere('p.status=1')
                                ->andWhere('e.status=1')
                                ->andWhere("e.terminal ={$terminal}")
                                ->orderBy('e.name', 'ASC');
                        },
                        'attr' => ['class' => 'select2 input'],
                        'choice_label' => 'name',
                        'placeholder' => 'Choose a company unit',
                    ])
                    ->add('processDepartment', EntityType::class, [
                        'class' => Setting::class,
                        'required' => true,
                        'query_builder' => function (EntityRepository $er) use($terminal,$deps) {
                            return $er->createQueryBuilder('e')
                                ->join('e.settingType','type')
                                ->where('e.status =1')
                                ->andWhere("type.slug ='department'")
                                ->andWhere("e.terminal ='{$terminal}'")
                               // ->andWhere('e.id in (:deps)')->setParameter('deps',$deps)
                                ->orderBy('e.name', 'ASC');
                        },
                        'attr'=>['class'=>'select2 input processDepartment'],
                        'choice_label' => 'name',
                        'placeholder' => 'Choose a department name',
                    ])
                    ->add('expenseBillType', EntityType::class, [
                    'class' => Particular::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use($config) {
                        return $er->createQueryBuilder('e')
                            ->join('e.particularType','type')
                            ->where('e.status =1')
                            ->andWhere("type.slug ='bill-entry'")
                            ->andWhere("e.config ='{$config}'")
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'select2 input'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a expense bill type',
                    ])
                    ->add('billMonth', ChoiceType::class, [
                    'attr' => ['class'=>'billMonth'],
                    'choices'  => [
                            "Current - {$curMonth}" => 'Current',
                            "Previous - {$prevMonth}" => 'Previous'
                        ],
                    ]);
                    if($options['data']->getBillMonth() == "Previous"){
                         $builder ->add('created', DateType::class, [
                             'widget' => 'single_text',
                             'html5' => true,
                             'required' => false,
                             'attr' => ['class' => 'input', 'min' => $lastMonth, 'max' => $lastMonthDay, 'placeholder' => "Expected Date"],
                         ]);
                    }

            }

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Requisition::class,
            'config' => Procurement::class,
        ]);
    }
}
