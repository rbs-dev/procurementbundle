<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderCsWorkorderFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor ajaxUpdate','rows' => 4],
                'required' => true,
                'help' => "",
            ])
            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition col-md-12'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template name',
            ])
            ->add('shipTo', EntityType::class, [
                'class' => Branch::class,
                'required' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='sub-branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'ajaxUpdate'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a shipment to',
            ])
            ->add('paymentMode', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='payment-mode'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'ajaxUpdate'],
                'choice_label' => 'name',
            ])
            ->add('directVendor', EntityType::class, [
                'class' => Vendor::class,
                'multiple' => false,
                'required' => true,
                'group_by'  => 'vendorType.name',
                'choice_label'  => 'companyName',
                'attr'=>['class'=>'select2'],
                'help' => "",
                'placeholder' => 'Choose a vendor name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where('e.terminal = :terminal')->setParameter('terminal', $terminal)
                        ->andWhere('e.status = 1')
                        ->orderBy('e.name', 'ASC');
                },
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tender::class,
            'terminal' => Terminal::class,
            'config' => Procurement::class,
        ]);
    }
}
