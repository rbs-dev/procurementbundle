<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetGlYear;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\UtilizationLetter;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UtilizationLetterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $curMonth = Date("F");
        $prevMonth = Date("F", strtotime("first day of previous month"));
        if($options['data']->getBillMonth() == "Previous"){
            $lastMonth = Date("Y-m-d", strtotime("first day of previous month"));
            $prevMonth = Date("F", strtotime("first day of previous month"));
            $lastMonthDay = Date("Y-m-t", strtotime("first day of previous month"));
        }

        $builder

            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template name',
            ])

            ->add('billMonth', ChoiceType::class, [
                'attr' => ['class'=>'billMonth'],
                'choices'  => [
                    "Current - {$curMonth}" => 'Current',
                    "Previous - {$prevMonth}" => 'Previous'
                ],
            ])

            ->add('company', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='branch'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company name',
            ])

            ->add('processDepartment', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 processDepartment input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department',
            ])


            ->add('attentionTo', EntityType::class, [
                'class' => User::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.enabled =1')
                        ->andWhere("e.userGroup ='employee'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 col-md-12 input','data-trigger' => "focus"],
                'choice_label' => 'nameWithEmployeeId',
                'placeholder' => 'Choose a created by',
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor','rows'=> 8],
                 'required' => false,
            ])

             ->add('title', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor','rows'=> 8,'data-trigger' => "focus"],
                 'required' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UtilizationLetter::class,
            'config' => Procurement::class,
        ]);
    }
}
