<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Core\Employee;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlip;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionSlipFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {

        $terminal =  $options['data']->getCreatedBy()->getTerminal()->getId();
        $mode =  $options['data']->getMode();
        $builder

            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a Format',
            ])
            ->add('companyUnit', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where("e.branchType ='branch-unit'")
                        ->andWhere('e.isDelete IS NULL')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company unit',
            ])

            ->add('processDepartment', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input processDepartment'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department name',
            ])

            ->add('targetDays', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'input'],
                 'required' => false,
            ])
             ->add('description', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor input','rows'=> 8],
                 'required' => false,
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'editor input','rows'=> 8],
                 'required' => false,
            ])
            ->add('requisitionMode', ChoiceType::class, [
                'expanded'=>true,
                'required' => true,
                'attr' => ['class'=>'input'],
                'choices'  => [
                    'AAP' => 'AAP',
                    'OAR' => 'OAR',
                ],
            ])
            ->add('expectedDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => true,
                'attr' => ['class' => 'input', 'min' => date('Y-m-d'),'placeholder'=>"Expected Date",'data-trigger' => "focus"],
            ])

            ->add('file', FileType::class, [
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '10240k',
                        'maxSizeMessage' => 'Please upload a valid valid document',
                    ])
                ],
                'attr'=>['class'=>''],
            ]);

            if($mode == "vendor"){

                $builder ->add('vendor', EntityType::class, [
                    'class' => Vendor::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('e')
                            ->where('e.status =1')
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'select2  vendor'],
                    'choice_label' => 'companyName',
                    'placeholder' => 'Choose a vendor/supplier name',
                ]);

            }else{

                $builder ->add('benificiery', EntityType::class, [
                    'class' => Employee::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er){
                        return $er->createQueryBuilder('e')
                            ->where('e.status =1')
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'select2 input'],
                    'choice_label' => 'nameWithEmployeeId',
                    'placeholder' => 'Choose a Employee ID & Name',
                ]);
            }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RequisitionSlip::class,
        ]);
    }
}
