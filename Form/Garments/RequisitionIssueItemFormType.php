<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\GenericMaster;
use App\Entity\Core\Setting;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionIssueItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $issue =  $options['issue'];
        $wearhouse =  ($options['issue']->getWearhouse()) ? $options['issue']->getWearhouse()->getId():'';
        $builder
            
            ->add('issueUser', ChoiceType::class, [
                'choices'  => [],
                'mapped' => false,
                'attr'=>['class'=>'autocomplete2Employee'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a Employee name & ID',
            ])

            ->add('line', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='line'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 reset-select'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a line',
            ])

            ->add('section', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='section'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 reset-select'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a section',
            ])

            ->add('issueType', ChoiceType::class, [
                'multiple' => false,
                'required' => true,
                'expanded' => true,
                'placeholder' => 'Choose an option',
                'choices' => [
                    'New' => 'New',
                    'Replace' => 'Replace'
                ],
                'data' => 'New',
                'attr' => [
                    'class' => 'form-check-inline'
                ]
            ])

            ->add('machineType', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='machine-type'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a machine type',
            ])

            ->add('machineNo', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='machine'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a machine no',
            ])

            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-4 number number-input','placeholder'=>"Quantity"],
                'required' => true,
            ])
            ->add('description', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-12','placeholder'=>"Description"],
                'required' => false,
            ])
            ->add('returnQuantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-4 number-input number','placeholder'=>"Return Qty"],
                'required' => false,
            ]);

        $builder->add('item', EntityType::class, array(
            'required' => true,
            'mapped' => false,
            'expanded' => false,
            'multiple' => false,
            'placeholder' => 'Choose a Item Name',
            'class' => StockBook::class,
            'choice_label' => 'stockBookName',
            'attr' => array('class' => 'col-md-12 itemAttribute select2 select2BookItem'),
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('e')
                    ->join("e.stock",'s')
                    ->where("s.status = -1")
                    ->orderBy('s.name', 'ASC');
            },
        ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => RequisitionIssueItem::class,
            'config' => GenericMaster::class,
            'issue' => RequisitionIssue::class,
        ]);
    }
}
