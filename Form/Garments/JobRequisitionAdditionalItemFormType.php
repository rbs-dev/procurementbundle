<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\SecurityBillingBundle\Entity\Particular;
use Terminalbd\SecurityBillingBundle\Entity\ParticularType;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class JobRequisitionAdditionalItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder
            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-12','placeholder'=>"Enter item name"],
                'required' => true,
            ])
            ->add('unit', EntityType::class, [
                'class' => ItemUnit::class,
                'multiple' => false,
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2 col-md-6'],
                'placeholder' => 'Item unit',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->where('e.status=1')
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('description', TextType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-10','placeholder'=>"Enter Description"],
                'required' => false,
            ])
            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-6 number','placeholder'=>"Quantity"],
                'required' => false,
            ])
            ->add('price', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'col-md-6 number','placeholder'=>"Price"],
                'required' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => JobRequisitionAdditionalItem::class,
            'config' => GenericMaster::class,
        ]);
    }
}
