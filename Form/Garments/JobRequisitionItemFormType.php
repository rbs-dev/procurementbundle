<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\GenericMaster;
use App\Entity\Core\Setting;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\SecurityBillingBundle\Entity\Particular;
use Terminalbd\SecurityBillingBundle\Entity\ParticularType;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class JobRequisitionItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config = $options['config']->getId();
        $terminal = $options['config']->getTerminal();
        $mode   = $options['mode'];
        $department   =  $options['entity']->getProcessDepartment() ? $options['entity']->getProcessDepartment()->getId():'';
        if($mode == 'Expense'){
            $builder
                ->add('description', TextType::class, [
                    'attr' => ['autofocus' => true,'class' => 'col-md-12','placeholder'=>"Specification"],
                    'required' => false,
                ])
                ->add('remark', TextType::class, [
                    'attr' => ['autofocus' => true,'class' => 'col-md-12','placeholder'=>"Remark"],
                    'required' => false,
                ])
                ->add('uom', TextType::class, [
                    'attr' => ['autofocus' => true,'class' => 'col-md-12','list'=>'units','placeholder'=>"Unit of Mesaurement"],
                    'required' => false,
                ])
                ->add('price', NumberType::class, [
                    'attr' => ['autofocus' => true,'class' => 'col-md-12 number','placeholder'=>"Price"],
                    'required' => true,
                ])
                ->add('created', DateType::class, [
                    'widget' => 'single_text',
                    'html5' => true,
                    'required' => true,
                    'attr' => ['class' => 'input col-md-12','placeholder'=>"Date",'data-trigger' => "focus"],
                ])
                ->add('section', EntityType::class, [
                    'class' => Setting::class,
                    'required' => true,
                    'query_builder' => function (EntityRepository $er) use($terminal) {
                        return $er->createQueryBuilder('e')
                            ->join('e.settingType','type')
                            ->where('e.status =1')
                            ->andWhere("type.slug ='section'")
                            ->andWhere("e.terminal ='{$terminal}'")
                            ->orderBy('e.name', 'ASC');
                    },
                    'attr'=>['class'=>'select2 col-md-12 input'],
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a requisition section',
                ])
                ->add('quantity', NumberType::class, [
                    'attr' => ['autofocus' => true, 'class' => 'col-md-12 number', 'placeholder' => "QTY"],
                    'required' => true,
                    'data' => 1
                ]);

        }else {

            $builder

                ->add('size', EntityType::class, array(
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'class' => ItemSize::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a Size/Parts id',
                    'attr' => array('class' => 'size select2'),
                    'query_builder' => function (EntityRepository $er) use ($config) {
                        return $er->createQueryBuilder('e')
                            ->where("e.status =-1")
                            ->orderBy('e.name', 'ASC');
                    },
                ))
                ->add('color', EntityType::class, array(
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'placeholder' => 'Choose a Color/GG',
                    'class' => ItemColor::class,
                    'choice_label' => 'name',
                    'attr' => array('class' => 'color col-md-6 select2'),
                    'query_builder' => function (EntityRepository $er) use ($config) {
                        return $er->createQueryBuilder('e')
                            ->where("e.status =-1")
                            ->orderBy('e.name', 'ASC');
                    },
                ))
                ->add('brand', EntityType::class, array(
                    'required' => false,
                    'expanded' => false,
                    'multiple' => false,
                    'class' => ItemBrand::class,
                    'choice_label' => 'name',
                    'placeholder' => 'Choose a brand',
                    'attr' => array('class' => 'brand select2'),
                    'query_builder' => function (EntityRepository $er) use ($config) {
                        return $er->createQueryBuilder('e')
                            ->where("e.status =-1")
                            ->orderBy('e.name', 'ASC');
                    },
                ))
                ->add('description', TextType::class, [
                    'attr' => ['autofocus' => true,'class' => 'col-md-12','placeholder'=>"Description"],
                    'required' => false,
                ])
                ->add('quantity', NumberType::class, [
                    'attr' => ['autofocus' => true, 'class' => 'col-md-12 number', 'placeholder' => "Quantity"],
                    'required' => true,
                ]);
        }
        if($mode == "CAPEX") {
            $builder->add('item', EntityType::class, array(
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Choose a Item Name',
                'class' => Item::class,
                'choice_label' => 'skuName',
                'attr' => array('class' => 'itemAttribute col-md-12 select2Item'),
                'query_builder' => function (EntityRepository $er) use ($config) {
                    return $er->createQueryBuilder('e')
                        ->where("e.status =-1")
                        ->andWhere("e.isDelete =0")
                        ->orderBy('e.name', 'ASC');
                },
            ));
            $builder->add('category', EntityType::class, array(
                'required' => false,
                'expanded' => false,
                'multiple' => false,
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => 'Select Charge Line',
                'attr' => array('class' => 'category select2'),
                'query_builder' => function (EntityRepository $er) use ($config) {
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.itemMode = 'CAPEX'")
                        ->orderBy('e.name', 'ASC');
                },
            ));
        }else{
            $builder->add('item', EntityType::class, array(
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => 'Choose a Item Name',
                'class' => Item::class,
                'choice_label' => 'skuName',
                'attr' => array('class' => 'itemAttribute col-md-12 select2Item'),
                'query_builder' => function (EntityRepository $er) use ($config) {
                    return $er->createQueryBuilder('e')
                        ->where("e.status =-1")
                        ->andWhere("e.isDelete =0")
                        ->orderBy('e.name', 'ASC');
                },
            ));
            $builder->add('category', EntityType::class, array(
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'class' => Category::class,
                'choice_label' => 'name',
                'placeholder' => 'Select Charge Line',
                'attr' => array('class' => 'category select2'),
                'query_builder' => function (EntityRepository $er) use ($department) {
                    $qb =  $er->createQueryBuilder('e');
                    $qb->where("e.status =-1");
                    if($department){
                        $qb->join("e.generalLedger",'gl');
                        $qb->where("gl.department = $department");
                        $qb->andWhere("e.status =1");
                        $qb->andWhere("e.itemMode = 'OPEX'");
                    }else{
                        $qb->where("e.status =-1");
                    }
                    $qb ->orderBy('e.name', 'ASC');
                    return $qb;
                },
            ));
            /*$builder->add('item', ChoiceType::class, [
                'choices' => [],
                'required' => true,
                'attr' => ['class' => 'select2GenericItemSearch itemAttribute','data-id'=> $entity,'placeholder'=>'Please select item name']
            ]);*/
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'config' => GenericMaster::class,
            'mode' => '',
            'entity' => JobRequisition::class,
        ]);
    }
}
