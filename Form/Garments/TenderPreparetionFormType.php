<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Branch;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderPreparetionFormType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $config =  $options['config']->getId();
        $builder
            ->add('subject', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea ajaxUpdate','rows' => 3],
                'required' => true,
                'help' => "",
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea editor ajaxUpdate','rows' => 8],
                'required' => true,
                'help' => "",
            ])
            ->add('condition', EntityType::class, [
                'class' => ProcurementCondition::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'condition select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a template',
            ])
            ->add('deliveryDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => true,
                'attr' => ['class' => 'ajaxUpdate','min' => date('Y-m-d'),'placeholder'=>"Validate Date"],
            ])
            ->add('tenderReplyDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => true,
                'required' => false,
                'attr' => ['class' => 'ajaxUpdate','min' => date('Y-m-d'),'placeholder'=>"Validate Date"],
            ])
            ->add('shipTo', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='sub-branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'ajaxUpdate'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a wear house name',
            ])
            ->add('paymentMode', EntityType::class, [
                'class' => Particular::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($config) {
                    return $er->createQueryBuilder('e')
                        ->join('e.particularType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='delivery-mode'")
                        ->andWhere("e.config ='{$config}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'ajaxUpdate'],
                'choice_label' => 'name',
            ])
            ->add('vendors', EntityType::class, [
                'class' => Vendor::class,
                'multiple' => true,
                'group_by'  => 'vendorType.name',
                'choice_label'  => 'companyName',
                'attr'=>['class'=>'ghhh'],
                'help' => "",
                'placeholder' => 'Choose a item name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($terminal){
                    return $er->createQueryBuilder('e')
                        ->where('e.terminal = :terminal')->setParameter('terminal', $terminal)
                        ->andWhere('e.status = 1')
                        ->orderBy('e.name', 'ASC');
                },
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Tender::class,
            'terminal' => Terminal::class,
            'config' => Procurement::class,
        ]);
    }
}
