<?php

namespace Terminalbd\ProcurementBundle\Form\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetGlYear;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\UtilizationLetter;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;

/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UtilizationLetterBudgetFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $department = ($options['data']->getProcessDepartment()) ? $options['data']->getProcessDepartment()->getId():'';
        $company = ($options['data']->getCompany()) ? $options['data']->getCompany()->getId():'';
        $financialYear = ($options['data']->getFinancialYear()) ? $options['data']->getFinancialYear():'';
        $builder
            ->add('budgetGl', EntityType::class, [
                'class' => BudgetGlYear::class,
                'multiple' => false,
                'required' => false,
             //   'group_by'  => 'gl.parent.name',
                //  'choice_translation_domain' => true,
                'choice_label'  => 'gl.nameWithCode',
                'attr'=>['class'=>'select2 budget-input'],
                'placeholder' => 'Choose a ledeger name',
                'query_builder' => function(EntityRepository $er) use($financialYear,$department,$company){
                    return $er->createQueryBuilder('e')
                        ->join('e.gl','gl')
                        ->join('e.budgetYear','bgy')
                        ->where("bgy.yearlyBudget = '{$financialYear}'")
                        ->andWhere("gl.department = '{$department}'")
                        ->andWhere("bgy.company = '{$company}'")
                        ->orderBy('gl.name', 'ASC');
                },
            ])
            ->add('yearlyQuantity', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                 'required' => true,
            ])
            ->add('ytdQuantity', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                'required' => true,
            ])
            ->add('expenseYtdQuantity', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                 'required' => true,
            ])
            ->add('requiredApprovalQuantityUpto', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                'required' => true,
            ])
            ->add('yearlyYtdAmount', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number red-bg','data-trigger' => "focus"],
                 'required' => true,
            ])
            ->add('yearlyAmount', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                'required' => true,
            ])
            ->add('expenseYtdAmount', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number','data-trigger' => "focus"],
                 'required' => true,
            ])
            ->add('requiredApprovalAmount', TextType::class, [
                'attr' => ['autofocus' => true,'class'=>'budget-input number red-bg','data-trigger' => "focus"],
                'required' => true,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UtilizationLetter::class,
            'config' => Procurement::class,
        ]);
    }
}
