<?php

namespace Terminalbd\ProcurementBundle\Form;

use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\ImportFile;
use Terminalbd\ProcurementBundle\Entity\ExpenseImportFile;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Requisition;


class ExpenseImportFileFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal = $options['terminal'];
        $builder

            ->add('requisitionMode', ChoiceType::class, [
                'required' => true,
                'attr'=>['class'=>'requisitionMode'],
                'choices'  => [
                    'Expense' => 'Expense',
                    'Confidential' => 'Confidential',
                ],
            ])
            ->add('processDepartment', EntityType::class, [
                'class' => Setting::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal){
                    return $er->createQueryBuilder('e')
                        ->join('e.settingType','type')
                        ->where('e.status =1')
                        ->andWhere("type.slug ='department'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input processDepartment'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a department name',
            ])
            ->add('companyUnit', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('e')
                        ->join('e.parent','p')
                        ->where("e.branchType ='branch-unit'")
                        ->andWhere('e.isDelete IS NULL')
                        ->andWhere('p.status=1')
                        ->andWhere('e.status=1')
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2 input'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a company unit',
            ])
            ->add('file', FileType::class, [
                'required' => true,
                'help' => 'Accepts only excel file.',
                'attr'=>['class'=>'custom-file-input'],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Requisition::class,
            'terminal' => 'terminal',
        ]);
    }
}
