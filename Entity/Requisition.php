<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionRepository")
 * @ORM\Table(name="procu_requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class Requisition
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var RequisitionItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionItem", mappedBy="requisition")
     * @ORM\OrderBy({"ordering" = "ASC"})
     */
    private $requisitionItems;


    /**
     * @var BudgetRequisition
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\BudgetBundle\Entity\BudgetRequisition", mappedBy="requisition")
     */
    private $budgetRequisition;

    /**
     * @var Tender
     *
     * @ORM\OneToMany(targetEntity="Tender", mappedBy="requisition")
     */
    private $tender;


    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var RequisitionBudgetItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem", mappedBy="requisition")
     */
    private $budgetItems;

    /**
     * @var RequisitionOrder
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionOrder", mappedBy="requisition")
     */
    private $requisitionOrders;

    /**
     * @var ComapnyRequisitionShare
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare", mappedBy="requisition")
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $comapnyRequisitionShares;


    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $requisitionType;


    /**
     * @var JobRequisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\JobRequisition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $jobRequisition;


    /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $priority;


     /**
     * @var Particular
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Particular")
     */
    private $expenseBillType;


     /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $section;


    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $branch;

    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $company;


    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $companyUnit;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $department;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $processDepartment;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $createdBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $reportTo;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $transferTo;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $checkedBy;


    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $approvedBy;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $rejectedBy;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $deletedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10M")
     */
    protected $file;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vat=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $total=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $jobApprovalAmount=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $billMonth="Current";

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $financialYear;


    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $reverse;


     /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fromReverse;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionMode='OPEX';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="requisition";


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $businessGroup;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $filename;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $revisedContent;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $deleteContent;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

    /**
     * @var Date
     *
     * @ORM\Column(name="expectedDate", type="date", nullable=true)
     */
    private $expectedDate;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRevised = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $budgetApprove = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isEmergency = true;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $requisitionToBudget = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Branch
     */
    public function getBranch()
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return RequisitionItem
     */
    public function getRequisitionItems()
    {
        return $this->requisitionItems;
    }

    /**
     * @return Particular
     */
    public function getRequisitionType()
    {
        return $this->requisitionType;
    }

    /**
     * @param Particular $requisitionType
     */
    public function setRequisitionType(Particular $requisitionType)
    {
        $this->requisitionType = $requisitionType;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy(User $checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy(User $approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal(float $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total)
    {
        $this->total = $total;
    }

    /**
     * @return Date
     */
    public function getExpectedDate()
    {
        return $this->expectedDate;
    }

    /**
     * @param Date $expectedDate
     */
    public function setExpectedDate($expectedDate)
    {
        $this->expectedDate = $expectedDate;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment(Setting $department)
    {
        $this->department = $department;
    }

    /**
     * @return Particular
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param Particular $priority
     */
    public function setPriority(Particular $priority)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getRequisitionNo()
    {
        return $this->requisitionNo;
    }

    /**
     * @param string $requisitionNo
     */
    public function setRequisitionNo(string $requisitionNo)
    {
        $this->requisitionNo = $requisitionNo;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat(float $vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo): void
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return mixed
     */
    public function getTransferTo()
    {
        return $this->transferTo;
    }

    /**
     * @param mixed $transferTo
     */
    public function setTransferTo($transferTo): void
    {
        $this->transferTo = $transferTo;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }


    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess(string $waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }


    /**
     * @return JobRequisition
     */
    public function getJobRequisition()
    {
        return $this->jobRequisition;
    }

    /**
     * @param JobRequisition $jobRequisition
     */
    public function setJobRequisition($jobRequisition)
    {
        $this->jobRequisition = $jobRequisition;
    }

    /**
     * @return string
     */
    public function getBusinessGroup()
    {
        return $this->businessGroup;
    }

    /**
     * @param string $businessGroup
     */
    public function setBusinessGroup(string $businessGroup)
    {
        $this->businessGroup = $businessGroup;
    }

    /**
     * @return int
     */
    public function getProcessOrdering(): int
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering): void
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return ComapnyRequisitionShare
     */
    public function getComapnyRequisitionShares()
    {
        return $this->comapnyRequisitionShares;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return RequisitionBudgetItem
     */
    public function getBudgetItems()
    {
        return $this->budgetItems;
    }

    /**
     * @return bool
     */
    public function isRequisitionToBudget()
    {
        return $this->requisitionToBudget;
    }

    /**
     * @param bool $requisitionToBudget
     */
    public function setRequisitionToBudget($requisitionToBudget)
    {
        $this->requisitionToBudget = $requisitionToBudget;
    }

  

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess): void
    {
        $this->moduleProcess = $moduleProcess;
    }



    /**
     * @return bool
     */
    public function isEmergency()
    {
        return $this->isEmergency;
    }

    /**
     * @param bool $isEmergency
     */
    public function setIsEmergency(bool $isEmergency)
    {
        $this->isEmergency = $isEmergency;
    }

    /**
     * @return Setting
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param Setting $section
     */
    public function setSection($section)
    {
        $this->section = $section;
    }

    /**
     * @return Setting
     */
    public function getProcessDepartment()
    {
        return $this->processDepartment;
    }

    /**
     * @param Setting $processDepartment
     */
    public function setProcessDepartment($processDepartment)
    {
        $this->processDepartment = $processDepartment;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return bool
     */
    public function isBudgetApprove()
    {
        return $this->budgetApprove;
    }

    /**
     * @param bool $budgetApprove
     */
    public function setBudgetApprove($budgetApprove)
    {
        $this->budgetApprove = $budgetApprove;
    }

    /**
     * @return Requisition
     */
    public function getReverse()
    {
        return $this->reverse;
    }

    /**
     * @param Requisition $reverse
     */
    public function setReverse($reverse)
    {
        $this->reverse = $reverse;
    }


    /**
     * @return BudgetRequisition
     */
    public function getBudgetRequisition()
    {
        return $this->budgetRequisition;
    }

    /**
     * @return string
     */
    public function getRequisitionMode()
    {
        return $this->requisitionMode;
    }

    /**
     * @param string $requisitionMode
     */
    public function setRequisitionMode($requisitionMode)
    {
        $this->requisitionMode = $requisitionMode;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @return Branch
     */
    public function getCompanyUnit()
    {
        return $this->companyUnit;
    }

    /**
     * @param Branch $companyUnit
     */
    public function setCompanyUnit($companyUnit)
    {
        $this->companyUnit = $companyUnit;
    }

    /**
     * @return bool
     */
    public function isRevised()
    {
        return $this->isRevised;
    }

    /**
     * @param bool $isRevised
     */
    public function setIsRevised(bool $isRevised)
    {
        $this->isRevised = $isRevised;
    }

    /**
     * @return string
     */
    public function getRevisedContent()
    {
        return $this->revisedContent;
    }

    /**
     * @param string $revisedContent
     */
    public function setRevisedContent(string $revisedContent)
    {
        $this->revisedContent = $revisedContent;
    }

    /**
     * @return Branch
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Branch $company
     */
    public function setCompany(Branch $company)
    {
        $this->company = $company;
    }

    /**
     * @return RequisitionOrder
     */
    public function getRequisitionOrders()
    {
        return $this->requisitionOrders;
    }

    /**
     * @return Particular
     */
    public function getExpenseBillType()
    {
        return $this->expenseBillType;
    }

    /**
     * @param Particular $expenseBillType
     */
    public function setExpenseBillType($expenseBillType)
    {
        $this->expenseBillType = $expenseBillType;
    }

    /**
     * @return Requisition
     */
    public function getFromReverse()
    {
        return $this->fromReverse;
    }

    /**
     * @param Requisition $fromReverse
     */
    public function setFromReverse($fromReverse)
    {
        $this->fromReverse = $fromReverse;
    }

    /**
     * @return float
     */
    public function getJobApprovalAmount()
    {
        return $this->jobApprovalAmount;
    }

    /**
     * @param float $jobApprovalAmount
     */
    public function setJobApprovalAmount($jobApprovalAmount)
    {
        $this->jobApprovalAmount = $jobApprovalAmount;
    }

    /**
     * @return string
     */
    public function getBillMonth()
    {
        return $this->billMonth;
    }

    /**
     * @param string $billMonth
     */
    public function setBillMonth(string $billMonth)
    {
        $this->billMonth = $billMonth;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return string
     */
    public function getDeleteContent()
    {
        return $this->deleteContent;
    }

    /**
     * @param string $deleteContent
     */
    public function setDeleteContent(string $deleteContent)
    {
        $this->deleteContent = $deleteContent;
    }

    /**
     * @return User
     */
    public function getRejectedBy()
    {
        return $this->rejectedBy;
    }

    /**
     * @param User $rejectedBy
     */
    public function setRejectedBy(User $rejectedBy)
    {
        $this->rejectedBy = $rejectedBy;
    }

    /**
     * @return User
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * @param User $deletedBy
     */
    public function setDeletedBy(User $deletedBy)
    {
        $this->deletedBy = $deletedBy;
    }

    /**
     * @return string
     */
    public function getFinancialYear()
    {
        return $this->financialYear;
    }

    /**
     * @param string $financialYear
     */
    public function setFinancialYear(string $financialYear)
    {
        $this->financialYear = $financialYear;
    }

    
    /**
     * Sets file.
     *
     * @param Requisition $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return Requisition
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }


    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


}
