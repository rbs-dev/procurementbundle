<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\SecurityBilling;
use App\Entity\Core\Customer;
use App\Entity\Domain\Branch;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\BudgetBundle\Entity\BudgetAmendment;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\Head;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionBudgetItemRepository")
 * @ORM\Table(name="procu_requisitionitem_budget_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionBudgetItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Requisition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Requisition", inversedBy="budgetItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     * @ORM\OrderBy({"id" = "DESC"})
     */
    private $requisition;

     /**
     * @var BudgetMonth
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\BudgetBundle\Entity\BudgetMonth")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $glBudgetMonth;

     /**
     * @var Head
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\BudgetBundle\Entity\Head")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $gl;


    /**
     * @var BudgetAmendment
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\BudgetBundle\Entity\BudgetAmendment", inversedBy="requisitionBudgetItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $budgetAmendment;

    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $company;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $month;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $requisitionAmount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $monthlyBudget=0;
    
    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetTillAmount = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetTillForcastAmount = 0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetTillActualAmount = 0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetTillExpense = 0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetTillAdditional = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetAdjustmentAmount = 0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetAmendmentAmount = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetAdjustment = 0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetAdjustmentPlus = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $budgetAdditional = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $monthlyReminigBudget=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyBudget=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyRemainigBudget=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyExpenseBudget=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyHoldBudget=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $expenseBudget=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $expenseRequisition=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $vatExpense=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $holdBudget=0;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment=0;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $lowStatus = false;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isAdjustment = false;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isClosed = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return BudgetMonth
     */
    public function getGlBudgetMonth()
    {
        return $this->glBudgetMonth;
    }

    /**
     * @param BudgetMonth $glBudgetMonth
     */
    public function setGlBudgetMonth($glBudgetMonth)
    {
        $this->glBudgetMonth = $glBudgetMonth;
    }

    /**
     * @return Branch
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Branch $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return int
     */
    public function getGlBudgetMonthId()
    {
        return $this->glBudgetMonthId;
    }

    /**
     * @param int $glBudgetMonthId
     */
    public function setGlBudgetMonthId($glBudgetMonthId)
    {
        $this->glBudgetMonthId = $glBudgetMonthId;
    }

    /**
     * @return float
     */
    public function getRequisitionAmount()
    {
        return $this->requisitionAmount;
    }

    /**
     * @param float $requisitionAmount
     */
    public function setRequisitionAmount($requisitionAmount)
    {
        $this->requisitionAmount = $requisitionAmount;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return float
     */
    public function getMonthlyBudget()
    {
        return $this->monthlyBudget;
    }

    /**
     * @param float $monthlyBudget
     */
    public function setMonthlyBudget($monthlyBudget)
    {
        $this->monthlyBudget = $monthlyBudget;
    }

    /**
     * @return float
     */
    public function getYearlyBudget()
    {
        return $this->yearlyBudget;
    }

    /**
     * @param float $yearlyBudget
     */
    public function setYearlyBudget($yearlyBudget)
    {
        $this->yearlyBudget = $yearlyBudget;
    }

    /**
     * @return float
     */
    public function getExpenseBudget()
    {
        return $this->expenseBudget;
    }

    /**
     * @param float $expenseBudget
     */
    public function setExpenseBudget($expenseBudget)
    {
        $this->expenseBudget = $expenseBudget;
    }

    /**
     * @return Requisition
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param Requisition $requisition
     */
    public function setRequisition(Requisition $requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return float
     */
    public function getMonthlyReminigBudget()
    {
        return $this->monthlyReminigBudget;
    }

    /**
     * @param float $monthlyReminigBudget
     */
    public function setMonthlyReminigBudget($monthlyReminigBudget)
    {
        $this->monthlyReminigBudget = $monthlyReminigBudget;
    }

    /**
     * @return float
     */
    public function getYearlyRemainigBudget()
    {
        return $this->yearlyRemainigBudget;
    }

    /**
     * @param float $yearlyRemainigBudget
     */
    public function setYearlyRemainigBudget($yearlyRemainigBudget)
    {
        $this->yearlyRemainigBudget = $yearlyRemainigBudget;
    }

    /**
     * @return bool
     */
    public function isLowStatus()
    {
        return $this->lowStatus;
    }

    /**
     * @param bool $lowStatus
     */
    public function setLowStatus(bool $lowStatus)
    {
        $this->lowStatus = $lowStatus;
    }

    /**
     * @return float
     */
    public function getBudgetTillAmount()
    {
        return $this->budgetTillAmount;
    }

    /**
     * @param float $budgetTillAmount
     */
    public function setBudgetTillAmount( $budgetTillAmount)
    {
        $this->budgetTillAmount = $budgetTillAmount;
    }

    /**
     * @return float
     */
    public function getBudgetAdjustmentAmount()
    {
        return $this->budgetAdjustmentAmount;
    }

    /**
     * @param float $budgetAdjustmentAmount
     */
    public function setBudgetAdjustmentAmount($budgetAdjustmentAmount)
    {
        $this->budgetAdjustmentAmount = $budgetAdjustmentAmount;
    }

    /**
     * @return string
     */
    public function getMonth(): string
    {
        return $this->month;
    }

    /**
     * @param string $month
     */
    public function setMonth(string $month)
    {
        $this->month = $month;
    }

    /**
     * @return float
     */
    public function getBudgetAmendmentAmount()
    {
        return $this->budgetAmendmentAmount;
    }

    /**
     * @param float $budgetAmendmentAmount
     */
    public function setBudgetAmendmentAmount($budgetAmendmentAmount)
    {
        $this->budgetAmendmentAmount = $budgetAmendmentAmount;
    }

    /**
     * @return BudgetAmendment
     */
    public function getBudgetAmendment()
    {
        return $this->budgetAmendment;
    }

    /**
     * @param BudgetAmendment $budgetAmendment
     */
    public function setBudgetAmendment($budgetAmendment)
    {
        $this->budgetAmendment = $budgetAmendment;
    }

    /**
     * @return float
     */
    public function getYearlyExpenseBudget()
    {
        return $this->yearlyExpenseBudget;
    }

    /**
     * @param float $yearlyExpenseBudget
     */
    public function setYearlyExpenseBudget($yearlyExpenseBudget)
    {
        $this->yearlyExpenseBudget = $yearlyExpenseBudget;
    }

    /**
     * @return float
     */
    public function getYearlyHoldBudget()
    {
        return $this->yearlyHoldBudget;
    }

    /**
     * @param float $yearlyHoldBudget
     */
    public function setYearlyHoldBudget($yearlyHoldBudget)
    {
        $this->yearlyHoldBudget = $yearlyHoldBudget;
    }

    /**
     * @return float
     */
    public function getHoldBudget()
    {
        return $this->holdBudget;
    }

    /**
     * @param float $holdBudget
     */
    public function setHoldBudget($holdBudget)
    {
        $this->holdBudget = $holdBudget;
    }

    /**
     * @return float
     */
    public function getExpenseRequisition()
    {
        return $this->expenseRequisition;
    }

    /**
     * @param float $expenseRequisition
     */
    public function setExpenseRequisition($expenseRequisition)
    {
        $this->expenseRequisition = $expenseRequisition;
    }

    /**
     * @return Head
     */
    public function getGl()
    {
        return $this->gl;
    }

    /**
     * @param Head $gl
     */
    public function setGl($gl)
    {
        $this->gl = $gl;
    }

    /**
     * @return float
     */
    public function getVatExpense()
    {
        return $this->vatExpense;
    }

    /**
     * @param float $vatExpense
     */
    public function setVatExpense($vatExpense)
    {
        $this->vatExpense = $vatExpense;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isAdjustment()
    {
        return $this->isAdjustment;
    }

    /**
     * @param bool $isAdjustment
     */
    public function setIsAdjustment($isAdjustment)
    {
        $this->isAdjustment = $isAdjustment;
    }

    /**
     * @return float
     */
    public function getBudgetAdjustment()
    {
        return $this->budgetAdjustment;
    }

    /**
     * @param float $budgetAdjustment
     */
    public function setBudgetAdjustment($budgetAdjustment)
    {
        $this->budgetAdjustment = $budgetAdjustment;
    }

    /**
     * @return float
     */
    public function getBudgetAdditional()
    {
        return $this->budgetAdditional;
    }

    /**
     * @param float $budgetAdditional
     */
    public function setBudgetAdditional($budgetAdditional)
    {
        $this->budgetAdditional = $budgetAdditional;
    }

    /**
     * @return float
     */
    public function getBudgetAdjustmentPlus()
    {
        return $this->budgetAdjustmentPlus;
    }

    /**
     * @param float $budgetAdjustmentPlus
     */
    public function setBudgetAdjustmentPlus($budgetAdjustmentPlus)
    {
        $this->budgetAdjustmentPlus = $budgetAdjustmentPlus;
    }

    /**
     * @return bool
     */
    public function isClosed()
    {
        return $this->isClosed;
    }

    /**
     * @param bool $isClosed
     */
    public function setIsClosed($isClosed)
    {
        $this->isClosed = $isClosed;
    }

    /**
     * @return float
     */
    public function getBudgetTillExpense()
    {
        return $this->budgetTillExpense;
    }

    /**
     * @param float $budgetTillExpense
     */
    public function setBudgetTillExpense(float $budgetTillExpense)
    {
        $this->budgetTillExpense = $budgetTillExpense;
    }

    /**
     * @return float
     */
    public function getBudgetTillAdditional()
    {
        return $this->budgetTillAdditional;
    }

    /**
     * @param float $budgetTillAdditional
     */
    public function setBudgetTillAdditional(float $budgetTillAdditional)
    {
        $this->budgetTillAdditional = $budgetTillAdditional;
    }

    /**
     * @return float
     */
    public function getBudgetTillForcastAmount()
    {
        return $this->budgetTillForcastAmount;
    }

    /**
     * @param float $budgetTillForcastAmount
     */
    public function setBudgetTillForcastAmount($budgetTillForcastAmount)
    {
        $this->budgetTillForcastAmount = $budgetTillForcastAmount;
    }

    /**
     * @return float
     */
    public function getBudgetTillActualAmount()
    {
        return $this->budgetTillActualAmount;
    }

    /**
     * @param float $budgetTillActualAmount
     */
    public function setBudgetTillActualAmount($budgetTillActualAmount)
    {
        $this->budgetTillActualAmount = $budgetTillActualAmount;
    }




    

}
