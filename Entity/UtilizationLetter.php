<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\BudgetBundle\Entity\BudgetGlYear;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\UtilizationLetterRepository")
 * @ORM\Table(name="procu_utilization_letter")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UtilizationLetter
{

    const CHECKED_BY = 86;
    const APPROVED_BY = 130;

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

    /**
     * @var ProcurementCondition
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $condition;


    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;

    /**
     * @var BudgetGlYear
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\BudgetBundle\Entity\BudgetGlYear")
     */
    private $budgetGl;

    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $company;


    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $department;


    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $processDepartment;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $createdBy;


    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $checkedBy;


    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;
    

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $attentionTo;


     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;


     /**
     * @var User
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $rejectedBy;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyQuantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $ytdQuantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $expenseYtdQuantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $requiredApprovalQuantity=0;

      /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $requiredApprovalQuantityUpto=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyAmount=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $yearlyYtdAmount=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $expenseYtdAmount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $requiredApprovalAmount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $requiredApprovalAmountUpto=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $totalApprovalAmount=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $processAmount=0;


    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $code=0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $invoiceNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionNo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';


    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $reasons;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $recoveryPlan;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $timeline;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $futureActionPlan;


    /**
     * @var UtilizationLetter
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\UtilizationLetter")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $reverse;

    /**
     * @var UtilizationLetter
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\UtilizationLetter")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fromReverse;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $billMonth = "Current";

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $billMonthName = "";

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $financialYear;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\File(maxSize="10M")
     */
    protected $file;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $content;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;

    
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $deleteContent;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = false;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig(Procurement $config)
    {
        $this->config = $config;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess($moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

   
    /**
     * @return BudgetGlYear
     */
    public function getBudgetGl()
    {
        return $this->budgetGl;
    }

    /**
     * @param BudgetGlYear $budgetGl
     */
    public function setBudgetGl($budgetGl)
    {
        $this->budgetGl = $budgetGl;
    }

    /**
     * @return Branch
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Branch $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Setting
     */
    public function getProcessDepartment()
    {
        return $this->processDepartment;
    }

    /**
     * @param Setting $processDepartment
     */
    public function setProcessDepartment(Setting $processDepartment)
    {
        $this->processDepartment = $processDepartment;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * @return User
     */
    public function getRejectedBy()
    {
        return $this->rejectedBy;
    }

    /**
     * @param User $rejectedBy
     */
    public function setRejectedBy(User $rejectedBy)
    {
        $this->rejectedBy = $rejectedBy;
    }



    /**
     * @return float
     */
    public function getRequisitionAmount()
    {
        return $this->requisitionAmount;
    }

    /**
     * @param float $requisitionAmount
     */
    public function setRequisitionAmount( $requisitionAmount)
    {
        $this->requisitionAmount = $requisitionAmount;
    }


    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat( $vat)
    {
        $this->vat = $vat;
    }


    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getRequisitionNo()
    {
        return $this->requisitionNo;
    }

    /**
     * @param string $requisitionNo
     */
    public function setRequisitionNo($requisitionNo)
    {
        $this->requisitionNo = $requisitionNo;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @param string $project
     */
    public function setProject(string $project)
    {
        $this->project = $project;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess(string $waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename(string $filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getDeleteContent()
    {
        return $this->deleteContent;
    }

    /**
     * @param string $deleteContent
     */
    public function setDeleteContent(string $deleteContent)
    {
        $this->deleteContent = $deleteContent;
    }

    /**
     * @return bool
     */
    public function isDelete(): bool
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete(bool $isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return mixed
     */
    public function getAttention()
    {
        return $this->attention;
    }

    /**
     * @param mixed $attention
     */
    public function setAttention($attention)
    {
        $this->attention = $attention;
    }

    /**
     * @return float
     */
    public function getYearlyQuantity()
    {
        return $this->yearlyQuantity;
    }

    /**
     * @param float $yearlyQuantity
     */
    public function setYearlyQuantity( $yearlyQuantity)
    {
        $this->yearlyQuantity = $yearlyQuantity;
    }

    /**
     * @return float
     */
    public function getYtdQuantity()
    {
        return $this->ytdQuantity;
    }

    /**
     * @param float $ytdQuantity
     */
    public function setYtdQuantity( $ytdQuantity)
    {
        $this->ytdQuantity = $ytdQuantity;
    }

    /**
     * @return float
     */
    public function getExpenseYtdQuantity()
    {
        return $this->expenseYtdQuantity;
    }

    /**
     * @param float $expenseYtdQuantity
     */
    public function setExpenseYtdQuantity( $expenseYtdQuantity)
    {
        $this->expenseYtdQuantity = $expenseYtdQuantity;
    }

    /**
     * @return float
     */
    public function getRequiredApprovalQuantityUpto()
    {
        return $this->requiredApprovalQuantityUpto;
    }

    /**
     * @param float $requiredApprovalQuantityUpto
     */
    public function setRequiredApprovalQuantityUpto( $requiredApprovalQuantityUpto)
    {
        $this->requiredApprovalQuantityUpto = $requiredApprovalQuantityUpto;
    }

    /**
     * @return float
     */
    public function getExpenseYtdAmount()
    {
        return $this->expenseYtdAmount;
    }

    /**
     * @param float $expenseYtdAmount
     */
    public function setExpenseYtdAmount( $expenseYtdAmount)
    {
        $this->expenseYtdAmount = $expenseYtdAmount;
    }

    /**
     * @return float
     */
    public function getRequiredApprovalAmount()
    {
        return $this->requiredApprovalAmount;
    }

    /**
     * @param float $requiredApprovalAmount
     */
    public function setRequiredApprovalAmount( $requiredApprovalAmount)
    {
        $this->requiredApprovalAmount = $requiredApprovalAmount;
    }

    /**
     * @return float
     */
    public function getRequiredApprovalAmountUpto()
    {
        return $this->requiredApprovalAmountUpto;
    }

    /**
     * @param float $requiredApprovalAmountUpto
     */
    public function setRequiredApprovalAmountUpto( $requiredApprovalAmountUpto)
    {
        $this->requiredApprovalAmountUpto = $requiredApprovalAmountUpto;
    }

    /**
     * @return float
     */
    public function getTotalApprovalAmount()
    {
        return $this->totalApprovalAmount;
    }

    /**
     * @param float $totalApprovalAmount
     */
    public function setTotalApprovalAmount( $totalApprovalAmount)
    {
        $this->totalApprovalAmount = $totalApprovalAmount;
    }

    /**
     * @return User
     */
    public function getCheckedBy()
    {
        return $this->checkedBy;
    }

    /**
     * @param User $checkedBy
     */
    public function setCheckedBy(User $checkedBy)
    {
        $this->checkedBy = $checkedBy;
    }

    /**
     * @return User
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param User $approvedBy
     */
    public function setApprovedBy(User $approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }


    /**
     * @return string
     */
    public function getInvoiceNo()
    {
        return $this->invoiceNo;
    }

    /**
     * @param string $invoiceNo
     */
    public function setInvoiceNo(string $invoiceNo)
    {
        $this->invoiceNo = $invoiceNo;
    }

    /**
     * @return string
     */
    public function getReasons()
    {
        return $this->reasons;
    }

    /**
     * @param string $reasons
     */
    public function setReasons(string $reasons)
    {
        $this->reasons = $reasons;
    }

    /**
     * @return string
     */
    public function getRecoveryPlan()
    {
        return $this->recoveryPlan;
    }

    /**
     * @param string $recoveryPlan
     */
    public function setRecoveryPlan(string $recoveryPlan)
    {
        $this->recoveryPlan = $recoveryPlan;
    }

    /**
     * @return string
     */
    public function getTimeline()
    {
        return $this->timeline;
    }

    /**
     * @param string $timeline
     */
    public function setTimeline(string $timeline)
    {
        $this->timeline = $timeline;
    }

    /**
     * @return mixed
     */
    public function getAttentionTo()
    {
        return $this->attentionTo;
    }

    /**
     * @param mixed $attentionTo
     */
    public function setAttentionTo($attentionTo)
    {
        $this->attentionTo = $attentionTo;
    }

    /**
     * @return UtilizationLetter
     */
    public function getReverse()
    {
        return $this->reverse;
    }

    /**
     * @param UtilizationLetter $reverse
     */
    public function setReverse( $reverse)
    {
        $this->reverse = $reverse;
    }

    /**
     * @return UtilizationLetter
     */
    public function getFromReverse()
    {
        return $this->fromReverse;
    }

    /**
     * @param UtilizationLetter $fromReverse
     */
    public function setFromReverse($fromReverse)
    {
        $this->fromReverse = $fromReverse;
    }

    /**
     * @return float
     */
    public function getRequiredApprovalQuantity()
    {
        return $this->requiredApprovalQuantity;
    }

    /**
     * @param float $requiredApprovalQuantity
     */
    public function setRequiredApprovalQuantity( $requiredApprovalQuantity)
    {
        $this->requiredApprovalQuantity = $requiredApprovalQuantity;
    }

    /**
     * @return float
     */
    public function getYearlyAmount()
    {
        return $this->yearlyAmount;
    }

    /**
     * @param float $yearlyAmount
     */
    public function setYearlyAmount( $yearlyAmount)
    {
        $this->yearlyAmount = $yearlyAmount;
    }

    /**
     * @return float
     */
    public function getYearlyYtdAmount()
    {
        return $this->yearlyYtdAmount;
    }

    /**
     * @param float $yearlyYtdAmount
     */
    public function setYearlyYtdAmount( $yearlyYtdAmount)
    {
        $this->yearlyYtdAmount = $yearlyYtdAmount;
    }

    /**
     * @return string
     */
    public function getBillMonth()
    {
        return $this->billMonth;
    }

    /**
     * @param string $billMonth
     */
    public function setBillMonth(string $billMonth)
    {
        $this->billMonth = $billMonth;
    }

    /**
     * @return string
     */
    public function getFinancialYear()
    {
        return $this->financialYear;
    }

    /**
     * @param string $financialYear
     */
    public function setFinancialYear(string $financialYear)
    {
        $this->financialYear = $financialYear;
    }

    /**
     * @return string
     */
    public function getBillMonthName()
    {
        return $this->billMonthName;
    }

    /**
     * @param string $billMonthName
     */
    public function setBillMonthName($billMonthName)
    {
        $this->billMonthName = $billMonthName;
    }

    /**
     * @return float
     */
    public function getProcessAmount()
    {
        return $this->processAmount;
    }

    /**
     * @param float $processAmount
     */
    public function setProcessAmount($processAmount)
    {
        $this->processAmount = $processAmount;
    }

    /**
     * @return string
     */
    public function getFutureActionPlan()
    {
        return $this->futureActionPlan;
    }

    /**
     * @param string $futureActionPlan
     */
    public function setFutureActionPlan($futureActionPlan)
    {
        $this->futureActionPlan = $futureActionPlan;
    }

    /**
     * Sets file.
     *
     * @param UtilizationLetter $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UtilizationLetter
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }


    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/utilization/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }




}
