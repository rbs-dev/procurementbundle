<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderConditionItemRepository")
 * @ORM\Table(name="procu_tender_condition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderConditionItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var ProcurementCondition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $template;

    /**
     * @var TenderWorkorder
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $workorder;



    /**
     * @var Tender
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Tender", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tender;

    /**
     * @var TenderCommittee
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderCommittee", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderCommittee;

    /**
     * @var TenderMemo
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderMemo", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tenderMemo;

     /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $metaId;


     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $metaKey;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $metaValue;

    /**
     * @var boolean
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }


    /**
     * @return TenderWorkorder
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @param TenderWorkorder $workorder
     */
    public function setWorkorder($workorder)
    {
        $this->workorder = $workorder;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey( $metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue( $metaValue)
    {
        $this->metaValue = $metaValue;
    }

    /**
     * @return Tender
     */
    public function getTender()
    {
        return $this->tender;
    }

    /**
     * @param Tender $tender
     */
    public function setTender($tender)
    {
        $this->tender = $tender;
    }

    /**
     * @return int
     */
    public function getMetaId()
    {
        return $this->metaId;
    }

    /**
     * @param int $metaId
     */
    public function setMetaId( $metaId)
    {
        $this->metaId = $metaId;
    }

    /**
     * @return TenderMemo
     */
    public function getTenderMemo()
    {
        return $this->tenderMemo;
    }

    /**
     * @param TenderMemo $tenderMemo
     */
    public function setTenderMemo($tenderMemo)
    {
        $this->tenderMemo = $tenderMemo;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return ProcurementCondition
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param ProcurementCondition $template
     */
    public function setTemplate($template)
    {
        $this->template = $template;
    }

    /**
     * @return TenderCommittee
     */
    public function getTenderCommittee()
    {
        return $this->tenderCommittee;
    }

    /**
     * @param TenderCommittee $tenderCommittee
     */
    public function setTenderCommittee($tenderCommittee)
    {
        $this->tenderCommittee = $tenderCommittee;
    }



}
