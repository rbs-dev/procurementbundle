<?php

namespace Terminalbd\ProcurementBundle\Entity;
use App\Entity\Application\Procurement;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\ProcurementConditionItemRepository")
 * @ORM\Table(name="procu_condition_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class ProcurementConditionItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;

    /**
     * @var TenderWorkorder
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $workorder;

    /**
     * @var ProcurementCondition
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $condition;

    /**
     * @var Tender
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\Tender", inversedBy="conditionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $tender;

    /**
     * @var TenderConditionItem
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderConditionItem")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $conditionItem;


    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @Gedmo\Slug(fields={"name"})
     * @Doctrine\ORM\Mapping\Column(length=255,unique=false)
     */
    private $slug;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $description;

     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $metaKey;

     /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $metaValue;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }



    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }


    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription( $description)
    {
        $this->description = $description;
    }


    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return string
     */
    public function getMetaKey()
    {
        return $this->metaKey;
    }

    /**
     * @param string $metaKey
     */
    public function setMetaKey( $metaKey)
    {
        $this->metaKey = $metaKey;
    }

    /**
     * @return string
     */
    public function getMetaValue()
    {
        return $this->metaValue;
    }

    /**
     * @param string $metaValue
     */
    public function setMetaValue( $metaValue)
    {
        $this->metaValue = $metaValue;
    }

}
