<?php

namespace Terminalbd\ProcurementBundle\Entity;

use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\Domain\Vendor;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\StockBook;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveItemRepository")
 * @ORM\Table(name="procu_tender_workorder_receive_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderReceiveItem
{

    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    protected $id;

    /**
     * @var TenderWorkorderReceive
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive", inversedBy="receiveItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $workorderReceive;

    /**
     * @var TenderWorkorderItem
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem", inversedBy="receiveItems")
     * @ORM\JoinColumn(onDelete="SET NULL")
     **/
    private  $tenderWorkorderItem;

     /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item")
     **/
    private  $item;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     */
    private $section;

    /**
     * @var ItemBrand
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     */
    private $brand;


    /**
     * @var ItemSize
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     */
    private $size;


    /**
     * @var ItemColor
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     */
    private $color;

     /**
     * @var StockBook
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     **/
    private  $stockBook;


    /**
     * @var Stock
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    private  $stock;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $returnQuantity=0;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $unitPrice=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $chemicalNo;


     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $casNo;


     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $casType;


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $msds = false;

      /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $status = true;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expireDate;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return TenderWorkorderReceive
     */
    public function getWorkorderReceive()
    {
        return $this->workorderReceive;
    }

    /**
     * @param TenderWorkorderReceive $workorderReceive
     */
    public function setWorkorderReceive($workorderReceive)
    {
        $this->workorderReceive = $workorderReceive;
    }

    /**
     * @return TenderWorkorderItem
     */
    public function getTenderWorkorderItem()
    {
        return $this->tenderWorkorderItem;
    }

    /**
     * @param TenderWorkorderItem $tenderWorkorderItem
     */
    public function setTenderWorkorderItem($tenderWorkorderItem)
    {
        $this->tenderWorkorderItem = $tenderWorkorderItem;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getUnitPrice()
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     */
    public function setUnitPrice($unitPrice)
    {
        $this->unitPrice = $unitPrice;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus( $status)
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getReturnQuantity()
    {
        return $this->returnQuantity;
    }

    /**
     * @param float $returnQuantity
     */
    public function setReturnQuantity($returnQuantity)
    {
        $this->returnQuantity = $returnQuantity;
    }

    /**
     * @return string
     */
    public function getChemicalNo()
    {
        return $this->chemicalNo;
    }

    /**
     * @param string $chemicalNo
     */
    public function setChemicalNo($chemicalNo)
    {
        $this->chemicalNo = $chemicalNo;
    }

    /**
     * @return string
     */
    public function getCasNo()
    {
        return $this->casNo;
    }

    /**
     * @param string $casNo
     */
    public function setCasNo($casNo)
    {
        $this->casNo = $casNo;
    }

    /**
     * @return string
     */
    public function getCasType()
    {
        return $this->casType;
    }

    /**
     * @param string $casType
     */
    public function setCasType($casType)
    {
        $this->casType = $casType;
    }

    /**
     * @return bool
     */
    public function isMsds()
    {
        return $this->msds;
    }

    /**
     * @param bool $msds
     */
    public function setMsds($msds)
    {
        $this->msds = $msds;
    }

    /**
     * @return \DateTime
     */
    public function getExpireDate()
    {
        return $this->expireDate;
    }

    /**
     * @param \DateTime $expireDate
     */
    public function setExpireDate($expireDate)
    {
        $this->expireDate = $expireDate;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Setting
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param Setting $section
     */
    public function setSection($section): void
    {
        $this->section = $section;
    }

    /**
     * @return ItemBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param ItemBrand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return ItemSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ItemSize $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return ItemColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param ItemColor $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }








}
