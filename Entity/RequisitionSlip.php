<?php

namespace Terminalbd\ProcurementBundle\Entity;


use App\Entity\Admin\Bank;
use App\Entity\Application\Procurement;
use App\Entity\Core\Employee;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Date;
use Terminalbd\ProcurementBundle\Form\FundForcastFormType;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;

/**
 * RequisitionSlip
 *
 * @ORM\Table(name="procu_requisition_slip")
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionSlipRepository")
 */
class RequisitionSlip
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Procurement
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Procurement")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


    /**
     * @var ProcurementCondition
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementCondition")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $condition;

    /**
     * @var ProcurementProcess
     *
     * @ORM\OneToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\ProcurementProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approveProcess_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * })
     */
    private $approveProcess;


    /**
     * @var JobRequisitionAdditionalItem
     *
     * @ORM\OneToMany(targetEntity="Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem", mappedBy="requisitionSlip")
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $additionalItems;

    /**
     * @var RequisitionSlip
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionSlip")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $reverse;

    /**
     * @var RequisitionSlip
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionSlip")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $fromReverse;



    /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $company;


     /**
     * @var Branch
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     */
    private $companyUnit;

    /**
     * @var integer
     * @ORM\Column(type="integer",nullable=true)
     */
    private $processOrdering = 0;

    /**
     * @var ModuleProcess
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\ModuleProcess")
     */
    private $moduleProcess;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $department;

    /**
     * @var Setting
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Setting")
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $processDepartment;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $process='New';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionMode='AAP';

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $mode='benificiery';


    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isDelete = 0;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $module ="requisition-slip";
    

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $waitingProcess='New';

    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float",nullable=true)
     */
    private $total = 0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;

    /**
     * @var integer
     *
     * @ORM\Column(name="code", type="integer",  nullable=true)
     */
    private $code;

     /**
     * @var integer
     *
     * @ORM\Column(type="integer",  nullable=true)
     */
    private $targetDays = 0;

     /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean",  nullable=true)
     */
    private $status;


     /**
     * @var boolean
     *
     * @ORM\Column(name="migrated", type="boolean",  nullable=true)
     */
    private  $migrated= 0;

    /**
     * @var Employee
     * @ORM\ManyToOne(targetEntity="App\Entity\Core\Employee")
     **/
    private  $benificiery;


    /**
     * @var Vendor
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Vendor")
     **/
    private  $vendor;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $preparedBy;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $deletedBy;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $deleteContent;

     /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $rejectedBy;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $approvedBy;

    /**
     * @var Date
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $paymentDate;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $description;


    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $content;

     /**
     * @var string
     *
     * @ORM\Column(type="text",nullable = true)
     */
    private $comment;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $reportTo;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $requisitionNo;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $path;


    /**
     * @Assert\File(maxSize="8388608")
     */
    protected $file;

    /**
     * @var Date
     *
     * @ORM\Column(name="expectedDate", type="date", nullable=true)
     */
    private $expectedDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="generatedDate", type="datetime", nullable=true)
     */
    private $generatedDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return ProcurementProcess
     */
    public function getApproveProcess()
    {
        return $this->approveProcess;
    }

    /**
     * @param ProcurementProcess $approveProcess
     */
    public function setApproveProcess($approveProcess)
    {
        $this->approveProcess = $approveProcess;
    }

    /**
     * @return AccountBank
     */
    public function getBankAccount()
    {
        return $this->bankAccount;
    }

    /**
     * @param AccountBank $bankAccount
     */
    public function setBankAccount($bankAccount)
    {
        $this->bankAccount = $bankAccount;
    }

    /**
     * @return Branch
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Branch $company
     */
    public function setCompany(Branch $company)
    {
        $this->company = $company;
    }

    /**
     * @return Particular
     */
    public function getOperationMode()
    {
        return $this->operationMode;
    }

    /**
     * @param Particular $operationMode
     */
    public function setOperationMode(Particular $operationMode)
    {
        $this->operationMode = $operationMode;
    }


    /**
     * @return int
     */
    public function getProcessOrdering()
    {
        return $this->processOrdering;
    }

    /**
     * @param int $processOrdering
     */
    public function setProcessOrdering(int $processOrdering)
    {
        $this->processOrdering = $processOrdering;
    }

    /**
     * @return ModuleProcess
     */
    public function getModuleProcess()
    {
        return $this->moduleProcess;
    }

    /**
     * @param ModuleProcess $moduleProcess
     */
    public function setModuleProcess(ModuleProcess $moduleProcess)
    {
        $this->moduleProcess = $moduleProcess;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess($process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getWaitingProcess()
    {
        return $this->waitingProcess;
    }

    /**
     * @param string $waitingProcess
     */
    public function setWaitingProcess($waitingProcess)
    {
        $this->waitingProcess = $waitingProcess;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * @param float $balance
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @param string $paymentMethod
     */
    public function setPaymentMethod($paymentMethod)
    {
        $this->paymentMethod = $paymentMethod;
    }

    /**
     * @return string
     */
    public function getMemoNo()
    {
        return $this->memoNo;
    }

    /**
     * @param string $memoNo
     */
    public function setMemoNo($memoNo)
    {
        $this->memoNo = $memoNo;
    }

    /**
     * @return string
     */
    public function getPaymentRefNo()
    {
        return $this->paymentRefNo;
    }

    /**
     * @param string $paymentRefNo
     */
    public function setPaymentRefNo($paymentRefNo)
    {
        $this->paymentRefNo = $paymentRefNo;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return mixed
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * @param mixed $approvedBy
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getReportTo()
    {
        return $this->reportTo;
    }

    /**
     * @param mixed $reportTo
     */
    public function setReportTo($reportTo)
    {
        $this->reportTo = $reportTo;
    }

    /**
     * Sets file.
     *
     * @param AccountExpense $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return AccountExpense
     */
    public function getFile()
    {
        return $this->file;
    }


    public function getAbsolutePath()
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        if(!file_exists( $this->getUploadDir())){
            mkdir( $this->getUploadDir(), 0777, true);
        }
        return __DIR__ . '/../../../../public/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/procurement/requisition-slip/';
    }

    public function removeUpload()
    {
        if ($file = $this->getAbsolutePath()) {
            unlink($file);
            $this->path = null ;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }
        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $filename = date('YmdHmi') . "_" . $this->getFile()->getClientOriginalName();
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $filename
        );

        // set the path property to the filename where you've saved the file
        $this->path = $filename ;

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getGeneratedDate()
    {
        return $this->generatedDate;
    }

    /**
     * @param \DateTime $generatedDate
     */
    public function setGeneratedDate($generatedDate)
    {
        $this->generatedDate = $generatedDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getPreparedBy()
    {
        return $this->preparedBy;
    }

    /**
     * @param mixed $preparedBy
     */
    public function setPreparedBy($preparedBy)
    {
        $this->preparedBy = $preparedBy;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }


    /**
     * @return bool
     */
    public function isUrgent()
    {
        return $this->urgent;
    }

    /**
     * @param bool $urgent
     */
    public function setUrgent(bool $urgent)
    {
        $this->urgent = $urgent;
    }

    /**
     * @return bool
     */
    public function isPostDated()
    {
        return $this->postDated;
    }

    /**
     * @param bool $postDated
     */
    public function setPostDated(bool $postDated)
    {
        $this->postDated = $postDated;
    }

    /**
     * @return Branch
     */
    public function getCompanyUnit()
    {
        return $this->companyUnit;
    }

    /**
     * @param Branch $companyUnit
     */
    public function setCompanyUnit(Branch $companyUnit)
    {
        $this->companyUnit = $companyUnit;
    }


    /**
     * @return ProcurementCondition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * @param ProcurementCondition $condition
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
    }

    /**
     * @return Setting
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param Setting $department
     */
    public function setDepartment($department)
    {
        $this->department = $department;
    }

    /**
     * @return Setting
     */
    public function getProcessDepartment()
    {
        return $this->processDepartment;
    }

    /**
     * @param Setting $processDepartment
     */
    public function setProcessDepartment($processDepartment)
    {
        $this->processDepartment = $processDepartment;
    }

    /**
     * @return Procurement
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Procurement $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return RequisitionSlip
     */
    public function getReverse()
    {
        return $this->reverse;
    }

    /**
     * @param RequisitionSlip $reverse
     */
    public function setReverse($reverse)
    {
        $this->reverse = $reverse;
    }

    /**
     * @return RequisitionSlip
     */
    public function getFromReverse()
    {
        return $this->fromReverse;
    }

    /**
     * @param RequisitionSlip $fromReverse
     */
    public function setFromReverse(RequisitionSlip $fromReverse)
    {
        $this->fromReverse = $fromReverse;
    }

    /**
     * @return string
     */
    public function getRequisitionNo()
    {
        return $this->requisitionNo;
    }

    /**
     * @param string $requisitionNo
     */
    public function setRequisitionNo($requisitionNo)
    {
        $this->requisitionNo = $requisitionNo;
    }

    /**
     * @return Date
     */
    public function getExpectedDate()
    {
        return $this->expectedDate;
    }

    /**
     * @param Date $expectedDate
     */
    public function setExpectedDate($expectedDate)
    {
        $this->expectedDate = $expectedDate;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return string
     */
    public function getRequisitionMode()
    {
        return $this->requisitionMode;
    }

    /**
     * @param string $requisitionMode
     */
    public function setRequisitionMode($requisitionMode)
    {
        $this->requisitionMode = $requisitionMode;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return JobRequisitionAdditionalItem
     */
    public function getAdditionalItems()
    {
        return $this->additionalItems;
    }

    /**
     * @return Employee
     */
    public function getBenificiery()
    {
        return $this->benificiery;
    }

    /**
     * @param Employee $benificiery
     */
    public function setBenificiery($benificiery)
    {
        $this->benificiery = $benificiery;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getRejectedBy()
    {
        return $this->rejectedBy;
    }

    /**
     * @param mixed $rejectedBy
     */
    public function setRejectedBy($rejectedBy)
    {
        $this->rejectedBy = $rejectedBy;
    }

    /**
     * @return int
     */
    public function getTargetDays()
    {
        return $this->targetDays;
    }

    /**
     * @param int $targetDays
     */
    public function setTargetDays($targetDays)
    {
        $this->targetDays = $targetDays;
    }

    /**
     * @return bool
     */
    public function isMigrated()
    {
        return $this->migrated;
    }

    /**
     * @param bool $migrated
     */
    public function setMigrated($migrated)
    {
        $this->migrated = $migrated;
    }


    /**
     * @return mixed
     */
    public function getDeletedBy()
    {
        return $this->deletedBy;
    }

    /**
     * @param mixed $deletedBy
     */
    public function setDeletedBy($deletedBy)
    {
        $this->deletedBy = $deletedBy;
    }

    /**
     * @return string
     */
    public function getDeleteContent()
    {
        return $this->deleteContent;
    }

    /**
     * @param string $deleteContent
     */
    public function setDeleteContent($deleteContent)
    {
        $this->deleteContent = $deleteContent;
    }

    /**
     * @return Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendor($vendor)
    {
        $this->vendor = $vendor;
    }

    /**
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param string $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return Date
     */
    public function getPaymentDate()
    {
        return $this->paymentDate;
    }

    /**
     * @param Date $paymentDate
     */
    public function setPaymentDate( $paymentDate)
    {
        $this->paymentDate = $paymentDate;
    }



}

