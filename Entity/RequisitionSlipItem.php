<?php

namespace Terminalbd\ProcurementBundle\Entity;


use App\Entity\Admin\Bank;
use App\Entity\Core\Employee;
use App\Entity\Domain\Branch;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;

/**
 * RequisitionSlipItem
 *
 * @ORM\Table(name="procu_requisition_slip_item")
 * @ORM\Entity(repositoryClass="Terminalbd\ProcurementBundle\Repository\RequisitionSlipItemRepository")
 */
class RequisitionSlipItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var RequisitionSlip
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\RequisitionSlip" , inversedBy="requisitionItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $requisition;


     /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
     private $name;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $remark;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $advanceMode ="claimable";


    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $amount = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float",nullable=true)
     */
    private $interest = 0;


     /**
     * @var int
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $ordering = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $totalDay = 0;


     /**
     * @var int
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $targetDay = 0;


     /**
     * @var int
     *
     * @ORM\Column(type="integer",nullable=true)
     */
    private $overdueDay = 0;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $advanceReceiveDate;


    /**
     * @var \DateTime
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $committedSattleDate;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return RequisitionSlip
     */
    public function getRequisition()
    {
        return $this->requisition;
    }

    /**
     * @param RequisitionSlip $requisition
     */
    public function setRequisition($requisition)
    {
        $this->requisition = $requisition;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * @param int $ordering
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getRemark()
    {
        return $this->remark;
    }

    /**
     * @param string $remark
     */
    public function setRemark( $remark)
    {
        $this->remark = $remark;
    }

    /**
     * @return string
     */
    public function getAdvanceMode()
    {
        return $this->advanceMode;
    }

    /**
     * @param string $advanceMode
     */
    public function setAdvanceMode($advanceMode)
    {
        $this->advanceMode = $advanceMode;
    }

    /**
     * @return float
     */
    public function getInterest()
    {
        return $this->interest;
    }

    /**
     * @param float $interest
     */
    public function setInterest( $interest)
    {
        $this->interest = $interest;
    }

    /**
     * @return int
     */
    public function getTotalDay()
    {
        return $this->totalDay;
    }

    /**
     * @param int $totalDay
     */
    public function setTotalDay($totalDay)
    {
        $this->totalDay = $totalDay;
    }

    /**
     * @return int
     */
    public function getTargetDay()
    {
        return $this->targetDay;
    }

    /**
     * @param int $targetDay
     */
    public function setTargetDay($targetDay)
    {
        $this->targetDay = $targetDay;
    }

    /**
     * @return int
     */
    public function getOverdueDay()
    {
        return $this->overdueDay;
    }

    /**
     * @param int $overdueDay
     */
    public function setOverdueDay(int $overdueDay)
    {
        $this->overdueDay = $overdueDay;
    }

    /**
     * @return \DateTime
     */
    public function getAdvanceReceiveDate()
    {
        return $this->advanceReceiveDate;
    }

    /**
     * @param \DateTime $advanceReceiveDate
     */
    public function setAdvanceReceiveDate( $advanceReceiveDate)
    {
        $this->advanceReceiveDate = $advanceReceiveDate;
    }

    /**
     * @return \DateTime
     */
    public function getCommittedSattleDate()
    {
        return $this->committedSattleDate;
    }

    /**
     * @param \DateTime $committedSattleDate
     */
    public function setCommittedSattleDate($committedSattleDate)
    {
        $this->committedSattleDate = $committedSattleDate;
    }
    
    
  

}

