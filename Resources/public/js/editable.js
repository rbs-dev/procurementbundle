var EditableInit = function () {

    var initEditables = function () {
        //global settings
        $.fn.editable.defaults.inputclass = 'select2';
        $.fn.editable.defaults.url = '/post';
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-mini green-bg white-font editable-submit"><i class="feather icon-save"></i></button>';
        $.fn.editableform.buttons += '<button type="button" class="btn btn-mini red-bg white-font editable-cancel"><i class="feather icon-trash-2"></i></button>';
    }
    $(".editable").editable(initEditables());
    $(document).on("click", ".editable-submit", function() {
        setTimeout(pageReload, 1000);
    });
    function pageReload() {
        /*location.reload();*/
    }
}

var EditableInitReload = function () {

    var initEditables = function () {
        //global settings
        $.fn.editable.defaults.inputclass = 'select2';
        $.fn.editable.defaults.url = '/post';
        $.fn.editableform.buttons = '<button type="submit" class="btn btn-mini green-bg white-font editable-submit"><i class="feather icon-save"></i></button>';
        $.fn.editableform.buttons += '<button type="button" class="btn btn-mini red-bg white-font editable-cancel"><i class="feather icon-trash-2"></i></button>';
    }
    $(".editable").editable(initEditables());
    $(document).on("click", ".editable-submit", function() {
        setTimeout(pageReload, 1000);
    });
    function pageReload() {
        location.reload();
    }
}