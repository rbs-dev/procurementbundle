$(document).ready(function () {
    var table = $('#datatable').DataTable( {
        "ajax": {
            "type"   : "POST",
            "processing": true,
            "serverSide": true,
            "url": Routing.generate('procure_particular_data_table')
        },
        "initComplete": function() {
            $('.status').bootstrapToggle()
        },
        columnDefs: [{orderable: false,targets:4}],
       // pageLength: 25,
        scrollY:'100vh',
        scrollCollapse: true,
        "paging": false,
        order: [ 0, 'asc' ]
    });

});

