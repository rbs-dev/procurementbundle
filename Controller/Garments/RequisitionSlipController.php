<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\CommentCreatedEvent;
use App\Event\EmailEvent;
use App\EventListener\EmailListener;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\EasyMailer;
use App\Service\FileUploader;
use App\Service\FormValidationManager;
use App\Service\GoogleMailer;
use App\Service\PHPMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\DmsBundle\Entity\ProcurementFile;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\DmsBundle\Repository\ProcurementFileRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\BudgetRequisition;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlip;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlipAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\RequisitionDistrubutionCompany;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\UtilizationLetter;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionAdditionalItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionSlipAdditionalItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionSlipFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionSlipItemFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\BudgetRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionSlipRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/payment/requisition-slip")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionSlipController extends AbstractController
{
    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_slip")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request,TranslatorInterface $translator, RequisitionSlipRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        } 
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/requisition-slip/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_requisition_slip_archived")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator, RequisitionSlipRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = "archived";
        $pageMode = array('mode' => $mode);
        $data = array_merge($data,$pageMode);
        $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/requisition-slip/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_requisition_slip_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = new RequisitionSlip();
        $entity->setMode($_REQUEST['mode']);
        $entity->setConfig($config);
        $module = "requisition-slip";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and $user->getProfile() and $user->getProfile()->getDepartment()) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_requisition_slip_edit', array('id' => $entity->getId()));
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_slip_edit")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PAYMENT_REQUISITION_SLIP_ADMIN')")
     */
    public function edit(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator,UserRepository $userRepository, ProcurementRepository $procurementRepository, RequisitionSlipRepository $repository, StockRepository $stockRepository, InventoryRepository $inventoryRepository , ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository,GenericMasterRepository $genericMasterRepository, ApprovalUserRepository $approvalUserRepository , ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $inventory = $inventoryRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity RequisitionSlip */

        $entity = $repository->findOneBy(array('config' => $config,'createdBy' => $this->getUser(),'id' => $id));
        $form = $this->createForm(RequisitionSlipFormType::class , $entity);
        $form->handleRequest($request);
        $additionalCount = $this->getDoctrine()->getRepository(JobRequisitionAdditionalItem::class)->count(array('requisitionSlip' => $id));
        $formValidation = new FormValidationManager();
        $errors = $formValidation->getErrorsFromForm($form);
        if (($form->isSubmitted() && $form->isValid() and $additionalCount > 0)) {
            $data = $request->request->all();
            $entity->getCompany($entity->getCompanyUnit()->getParent());
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $processRepository->requisitionSlipAssignHod($entity);
            $assignUsers = $approvalUserRepository->getApprovalAssignUserRequisitionSlip($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $processRepository->approvalAssign($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
           /* if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_slip')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }*/
            return $this->redirectToRoute('procure_requisition_slip',['mode'=>'list']);
        }
        $itemAdditionalForm = $this->createForm(RequisitionSlipItemFormType::class, new JobRequisitionAdditionalItem());
        return $this->render('@TerminalbdProcurement/garments/requisition-slip/edit.html.twig', [
            'entity'                => $entity,
            'form'                  => $form->createView(),
            'itemAdditionalForm'    => $itemAdditionalForm->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/admin-edit", methods={"GET", "POST"}, name="procure_requisition_slip_admin_edit")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PAYMENT_REQUISITION_SLIP_ADMIN')")
     */
    public function adminEdit(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator,UserRepository $userRepository, ProcurementRepository $procurementRepository, RequisitionSlipRepository $repository, StockRepository $stockRepository, InventoryRepository $inventoryRepository , ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository,GenericMasterRepository $genericMasterRepository, ApprovalUserRepository $approvalUserRepository , ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $inventory = $inventoryRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity RequisitionSlip */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(RequisitionSlipFormType::class , $entity);
        $form->handleRequest($request);
        $additionalCount = $this->getDoctrine()->getRepository(JobRequisitionAdditionalItem::class)->count(array('requisitionSlip' => $id));
        if (($form->isSubmitted() && $form->isValid() and $additionalCount > 0)) {
            $data = $request->request->all();
            $entity->getCompany($entity->getCompanyUnit()->getParent());
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $processRepository->requisitionSlipAssignHod($entity);
            $assignUsers = $approvalUserRepository->getApprovalAssignUserRequisitionSlip($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $processRepository->approvalAssign($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_slip',['mode'=>'list']);
        }
        $itemAdditionalForm = $this->createForm(RequisitionSlipItemFormType::class, new JobRequisitionAdditionalItem());
        return $this->render('@TerminalbdProcurement/garments/requisition-slip/edit.html.twig', [
            'entity'                => $entity,
            'form'                  => $form->createView(),
            'itemAdditionalForm'    => $itemAdditionalForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_requisition_slip_ajax_update")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PAYMENT_REQUISITION_SLIP_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $entity = $this->getDoctrine()->getRepository(RequisitionSlip::class)->find($id);
        $form = $this->createForm(RequisitionSlipFormType::class , $entity);
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
          //  $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/reverse", methods={"GET"}, name="procure_requisition_slip_garment_reverse" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReverse(RequisitionSlip $entity, RequisitionSlipRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $requisition = $requisitionRepository->insertReverseRequsition($entity);
        return $this->redirectToRoute('procure_requisition_slip_edit',array('id'=> $requisition->getId()));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_slip_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, EventDispatcherInterface $eventDispatcher , RequisitionSlipRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessForGenericComment($this->getUser(),$entity,$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                /*if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_slip')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }*/
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_requisition_slip',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/requisition-slip/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_slip_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionSlipRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-slip/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="requisition_slip_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, RequisitionSlip $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /* @var $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setContent($entity->getBody());
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_slip_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id, RequisitionSlipRepository $repository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/garments/requisition-slip/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview',
            )
        );

    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/claim-preview", methods={"GET"}, name="procure_requisition_slip_claim" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function claimPreview($id, RequisitionSlipRepository $repository): Response
    {
        $entity = $repository->find($id);
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        $previous = $repository->getLastRequisitionSlip($entity);
        return $this->render(
            '@TerminalbdProcurement/garments/requisition-slip/claim-form.html.twig', array(
                'entity' => $entity,
                'previous' => $previous,
                'approvals' => $approvals,
            )
        );

    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/generated", methods={"GET"}, name="requisition_slip_payment" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PAYMENT_ROLE_PAYMENT_OPERATION')")
     */
    public function requisitionGenerated($id, RequisitionSlipRepository $repository, RequisitionRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = $repository->find($id);
        $entity->setApprovedBy($this->getUser());
        $date = new \DateTime();
        $entity->setPaymentDate($date);
        $entity->setWaitingProcess('Paymented');
        $entity->setProcess('Paymented');
        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute('procure_requisition_slip',['mode'=>'archive']);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_slip_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(RequisitionSlip::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_slip_delete")
     * @Security("is_granted('ROLE_PAYMENT_REQUISITION_SLIP') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id): Response
    {

        $em = $this->getDoctrine()->getManager();
         /* @var $entity RequisitionSlip */
        $entity = $this->getDoctrine()->getRepository(RequisitionSlip::class)->findOneBy(array('id' => "{$id}"));
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        $html = $this->renderView('@TerminalbdProcurement/garments/requisition-slip/show.html.twig',array(
            'entity' => $entity,
            'approvals' => $approvals,
            'mode' => "print"
        ));
        $entity->setDeleteContent($html);
        $entity->setIsDelete(1);
        $entity->setDeletedBy($this->getUser());
        $em->flush();
        $this->getDoctrine()->getRepository(RequisitionSlip::class)->itemDeletes($entity->getId());
        $response = 'valid';
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/requisition-slip-reset", methods={"GET"}, name="procure_requisition_slip_reset" , options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReset(Request $request ,RequisitionSlip $entity, ProcurementProcessRepository $processRepository,ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $entity->setWaitingProcess("In-progress");
        $entity->setProcess("In-progress");
        $entity->setProcessOrdering(1);
        $em->flush();
        $processRepository->requisitionSlipAssignHod($entity);
        $assignUsers = $approvalUserRepository->getApprovalAssignUserRequisitionSlip($terminal,$entity);
        $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        $processRepository->approvalAssign($entity);
     //   $this->getDoctrine()->getRepository(ProcurementProcess::class)->revisedRequisitionSlipProcess($entity);
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_requisition_slip_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request,$id)
    {

        $entity =  $this->getDoctrine()->getRepository(RequisitionSlip::class)->find($id);
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/requisition-slip/{$entity->getPath()}";

        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }


}
