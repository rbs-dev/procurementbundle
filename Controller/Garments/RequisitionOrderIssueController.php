<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Domain\EmailTemplate;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Entity\WearHouse;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionOrderFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Security("is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
 * @Route("/procure/garments/order-issue")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionOrderIssueController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_garments_order_issue")
     * @Security("is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionOrderRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/order-issue/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/requisition-issued", methods={"GET", "POST"}, name="procure_requisition_garments_requisition_issued")
     * @Security("is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function issued(Request $request, RequisitionIssueRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode'=> $mode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }
        if(isset($data['wearhouse']) and $data['wearhouse']){
            $company = array('wearhouse'=> $data['wearhouse']);
            $data = array_merge($data,$company);
        }else{
            $company = array('wearhouse'=> '');
            $data = array_merge($data,$company);
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionIssuedSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionIssuedSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $companyBaseRequisition = $search = $repository->RequisitionIssueCompanyBaseCount($config);
        return $this->render('@TerminalbdProcurement/garments/order-issue/requsition-issued.html.twig',
            [
                'pagination' => $pagination,
                'companyBaseRequisitions' => $companyBaseRequisition,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/generated", methods={"GET"}, name="procure_requisition_garments_order_issue_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionGenerated($id, RequisitionIssueRepository $issueRepository, RequisitionOrderRepository $requisitionRepository, ProcurementProcessRepository $processRepository,ApprovalUserRepository $approvalUserRepository): Response
    {
        $entity = $issueRepository->find($id);
        $terminal = $this->getUser()->getTerminal()->getId();
        $user = $this->getUser();
        $newEntity = $requisitionRepository->insertOrderFromRequisitionIssue($user,$entity);
    //    $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$newEntity);
    //    $processRepository->insertProcurementProcessAssign($newEntity,$newEntity->getModule(),$assignUsers);
        return $this->redirectToRoute('procure_requisition_garments_order_issue_edit',array('id' => $newEntity->getId()));
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_garments_order_issue_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator, UserRepository $userRepository,ApprovalUserRepository $approvalUserRepository , ProcurementRepository $procurementRepository, RequisitionOrderRepository $repository,ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity RequisitionOrder */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(ApproveFormType::class , null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $repository->insertOrderIssueItem($entity,$data);
           // $processRepository->approvalAssign($entity);
            $entity->setWaitingProcess('Approved');
            $entity->setProcess('Approved');
            $this->getDoctrine()->getManager()->flush();
            $issue = $entity->getRequisitionIssue();
            $issue->setWaitingProcess('Closed');
            $em->flush();
            $this->getDoctrine()->getRepository(StockBook::class)->stockOutRequisitionOrderStockQuantity($entity);
            $this->getDoctrine()->getRepository(StockWearhouse::class)->orderIssueWearhouse($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_garments_requisition_issued');
        }
        $wearhouseStocks = $this->getDoctrine()->getRepository(StockWearhouse::class)->findStockItems($entity);
        return $this->render('@TerminalbdProcurement/garments/order-issue/edit.html.twig', [
            'entity'            => $entity,
            'wearhouseStocks'   => $wearhouseStocks,
            'form'              => $form->createView(),
        ]);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_garments_order_issue_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionOrderRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/order-issue/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_garments_order_issue_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request ,EventDispatcherInterface $eventDispatcher , RequisitionOrder $entity, RequisitionOrderRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, RequisitionIssueRepository $requisitionIssueRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_garments_order_issue')}?requisition_filter_form[requisitionNo]={$entity->getOrderNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $issue = $entity->getRequisitionIssue();
                $issue->setWaitingProcess('Closed');
                $em->flush();
                $this->getDoctrine()->getRepository(StockBook::class)->stockOutRequisitionOrderStockQuantity($entity);
                $this->getDoctrine()->getRepository(StockWearhouse::class)->orderIssueWearhouse($entity);
            }
            return $this->redirectToRoute('procure_requisition_garments_order_issue',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/order-issue/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/acceptence", methods={"GET","POST"}, name="procure_requisition_garments_order_issue_acceptence" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function acceptence(Request $request ,EventDispatcherInterface $eventDispatcher , RequisitionOrder $entity, RequisitionOrderRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, RequisitionIssueRepository $requisitionIssueRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $entity->setAcceptence($comment);
            $entity->setWaitingProcess('Closed');
            $entity->setProcess('Closed');
            $em->flush();
            return $this->redirectToRoute('procure_requisition_garments_order_issue',['mode'=>'list']);
        }
        return $this->render('@TerminalbdProcurement/garments/order-issue/acceptence.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/update-item", methods={"GET","POST"}, name="procure_requisition_garments_order_issue_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request,RequisitionOrderItem $item ): Response
    {

        /* @var $item  RequisitionOrderItem */

        $data = $request->request->all();
        $item->setQuantity($data['quantity']);
        $this->getDoctrine()->getManager()->flush();
        return  new Response('success');

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_garments_order_issue_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_garments_order_issue_delete")
     * @Security("is_granted('ROLE_ST') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, ProcurementRepository $procurementRepository, RequisitionOrderRepository $repository): Response
    {

        /* @var $config  GenericMaster */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_garments_order_issue_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(RequisitionOrder $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/order-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/order-issue/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/order-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }



}
