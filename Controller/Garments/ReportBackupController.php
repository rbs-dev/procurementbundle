<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\BranchRepository;
use Mpdf\Tag\Option;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\RequisitionAgingApprovalStatus;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\Garments\ReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionAgingApprovalStatusRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;


/**
 * @Route("/procure/report/")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class ReportBackupController extends AbstractController
{

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("item-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="item_wise_pr_report")
     */
    public function itemWiseReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $filterBy = $_REQUEST;
        $filepath = $this->get('kernel')->getProjectDir();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
            $filterBy = $request->query->get('filterBy');
            $entities = $repository->getItemWisePrReport($config,$data);
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $repository->getItemWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-item-wise-excel.html.twig', ['entities' => $entities]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-item-wise-pdf.html.twig',[
                'entities' => $entities,
                'company' => '',
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/report-item-wise.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("company-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="company_pr_report")
     */
    public function companyPrReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->companyWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
            if ($searchForm['company']) {
                $caption = $this->getDoctrine()->getRepository(Branch::class)->find($searchForm['company']);
                $caption = "Department Name: {$caption->getName()}";
            }
        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/company-pr-item-excel.html.twig', [
                'entities' => $entities,
                'searchForm' => $searchForm,
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $filepath = $this->get('kernel')->getProjectDir();
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/company-pr-item-pdf.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/company-pr-item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => $company,
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("department-wise-pr-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="department_pr_report")
     */
    public function departmentPrReport(Request $request, $mode, ProcurementRepository $procurementRepository, RequisitionItemRepository $repository)
    {
        $entities = [];
        $data = [];
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $getData = $searchForm->getData();
        $company = (isset($getData['company']) and $getData['company']) ? $getData['company']:'' ;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if(in_array($mode,['pdf','excel'])){

            $caption = '';
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->departmentWisePrReport($config,$data);
            $searchForm = $data['report_filter_form'];
            if ($searchForm['department']) {
                $caption = $this->getDoctrine()->getRepository(Setting::class)->find($searchForm['department']);
                $caption = "Department Name: {$caption->getName()}";
            }
        }
        if ($mode == 'excel'){

            $fileName = $request->get('_route').'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/department-pr-item-excel.html.twig', [
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();

        }elseif ($mode == 'pdf'){
            $filepath = $this->get('kernel')->getProjectDir();
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/department-pr-item-pdf.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/department-pr-item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => $company,
            'filterBy' => $data,
        ]);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("work-order-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="workorder_report")
     */
    public function workOrderReport(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filepath = $this->get('kernel')->getProjectDir();
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->workorderReport($config, $data);
            $searchForm = $data['report_filter_form'];

        }
        if ($mode == 'excel'){

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/workorder/excel.html.twig', [
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
//                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
//                $mpdf->SetHTMLFooter('<div style="background-color:#ff0000; height: 20px"><img width="100%" src="' . $imageFooterPath .'"></div>');
            }

            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/workorder/pdf.html.twig',[
                'entities' => $entities,
                'company' => '',
                'headerImage' => $terminal->getCoreDomain()->getWebHeaderPath(),
                'footerImage' => $terminal->getCoreDomain()->getWebFooterPath(),
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/garments/report/workorder/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'company' => '',
            'entities' => $entities,
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("category-wise-consumption-report/{mode}", name="category_wise_consumption_report", defaults={"mode"=null})
     */
    public function categoryWiseConsumptionReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $entities = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookCategoryConsumptionReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
            if ($searchForm['category']) {
                $caption = $this->getDoctrine()->getRepository(Category::class)->find($searchForm['category']);
                $caption = "Category Name: {$caption->getName()}";
            }
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-category-wise-consumption-pdf.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-category-wise-consumption-excel.html.twig',[
                'entities' => $entities,
                'caption' => $caption,
                'searchForm' => $searchForm,
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/report-category-wise-consumption.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @Route("purchase-order-summary-report/{mode}", name="purchase_order_summary_report", defaults={"mode" = null})
     */
    public function purchaseOrderSummaryReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-purchase-order-summary-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-purchase-order-summary-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/purchase-order/report-purchase-order-summary.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("item-wise-po-report/{mode}", name="item_wise_po_report", defaults={"mode" = null})
     */
    public function itemWisePurchaseOrderReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->itemWisePoReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];

        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-item-wise-purchase-order-pdf.html.twig',[
                'entities'=>$entities,
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-item-wise-purchase-order-excel.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/purchase-order/report-item-wise-purchase-order.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("vendor-wise-po-report/{mode}", name="vendor_wise_po_report", defaults={"mode" = null})
     */
    public function vendorWisePurchaseReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {

        $data = [];
        $caption ="";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->vendorWisePoReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-vendor-wise-purchase-order-pdf.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/purchase-order/report-vendor-wise-purchase-order-excel.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/purchase-order/report-vendor-wise-purchase-order.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @Route("department-wise-consumption-report/{mode}", name="department_wise_consumption_report", defaults={"mode" = null})
     */
    public function departmentWiseConsumptionReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookDepartmentConsumptionReports($config, $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-department-wise-consumption-pdf.html.twig',[
                'entities'=>$entities,
                'searchForm' => $searchForm,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-department-wise-consumption-excel.html.twig',[
                'entities' => $entities,
                'searchForm' => $searchForm,
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/report-department-wise-consumption.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report/{mode}", name="stock_report", defaults={"mode" = null})
     */
    public function stockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/stock/report-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report-company/{mode}", name="stock_company_report", defaults={"mode" = null})
     */
    public function companyStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookItemReports($config, $data);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningReport($config, $data);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangeReport($config, $data);
        }
        return $this->render('@TerminalbdProcurement/garments/report/stock/report-company-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("report-item-monthly-consumption/{mode}", name="report_item_monthly_consumption", defaults={"mode" = null})
     */
    public function reportItemMonthlyConsumption(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportItemMonthlyConsumption($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/consumption/report-item-pdf.html.twig',['entities' => $entities,'searchForm' =>$searchForm]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/consumption/report-stock-excel.html.twig',['entities' => $entities,'opening'=>$openingEntities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/consumption/report-item.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("report-department-monthly-consumption/{mode}", name="report_department_monthly_consumption", defaults={"mode" = null})
     */
    public function reportDepartmentMonthlyConsumption(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportDepartmentMonthlyConsumption($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/consumption/report-department-pdf.html.twig',['entities' => $entities,'searchForm' =>$searchForm,'caption' => $caption]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/consumption/report-stock-excel.html.twig',['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/consumption/report-department.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report-wearhouse/{mode}", name="stock_wearhouse_report", defaults={"mode" = null})
     */
    public function wearhouseStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/stock/report-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report-category/{mode}", name="stock_category_report", defaults={"mode" = null})
     */
    public function categoryStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/stock/report-stock-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/stock/report-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("contoso-expenses-report/{mode}", name="contoso_expenses_report", defaults={"mode" = null})
     */
    public function contosoExpensesReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];

        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
            $filterBy = ['data' => true];
            $entities = ['data' => true];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);


            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/expense/report-contoso-expense-pdf.html.twig');
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){

            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/expense/report-contoso-expense-excel.html.twig');

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/expense/report-contoso-expense.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }


    /**
     * Status a Branch entity.
     *
     * @Route("/category-item", methods={"GET","POST"}, name="stock_report_item_select" , options={"expose"=true})
     */
    public function subBranch(Request $request): Response
    {
        $id = $_REQUEST['id'];
        /* @var $entity Category */
        $entity = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $selectItem = "";
        $selectItem .= "<option value=''>--- Choose a item name ---</option>";
        if($entity){
            foreach ($entity->getItems() as $item){
                $selectItem .= "<option value='{$item->getId()}'>{$item->getName()}</option>";
            }
        }
        return new Response($selectItem);
    }

    /**
     * @Route("month-wise-requsition-aging-approval-status/{mode}", name="report_requsition_aging_approval_status", defaults={"mode" = null})
     */
    public function monthWiseRequsitionAgingApprovalStatus(Request $request, $mode, ProcurementRepository $procurementRepository)
    {

        $records = [];
        $filterBy = [];
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filepath = $this->get('kernel')->getProjectDir();

        if ($searchForm->isSubmitted()){
            $filterBy = $_REQUEST['report_filter_form'];
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
        }
        if ($mode == 'excel'){

            $filterBy = $request->query->get('filterBy');
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status-excel.html.twig', [
                'records' => $records
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $filterBy = $request->query->get('filterBy');

            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status-pdf.html.twig',[
                'records' => $records,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }
        return $this->render('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status.html.twig',[
            'searchForm' => $searchForm->createView(),
            'records' => $records,
            'filterBy' => $filterBy,
        ]);
    }

    /**
     * @Route("month-wise-requsition-aging-approval-status-details/{mode}", name="report_requsition_aging_approval_status_details", defaults={"mode" = null})
     */
    public function monthWiseRequsitionAgingApprovalStatusDetails(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $data = $request->query->all();

        if (!isset($data['day'])){
            $data['day'] = null;
        }
        $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatusDetails($data['approvedById'], $data['filterBy'], $data['day']);

        if ($mode == 'excel'){

            $filterBy = $request->query->get('filterBy');
            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);

            $fileName = 'report-item-wise'.'_'.time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status-excel.html.twig', [
                'records' => $records
            ]);

            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");

            echo $html;
            die();

        }elseif ($mode == 'pdf'){

            $filterBy = $request->query->get('filterBy');

            $records = $this->getDoctrine()->getRepository(RequisitionAgingApprovalStatus::class)->getMonthWiseStatus($filterBy);
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebHeaderPath()){
                $imageHeaderPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebHeaderPath()}";
                $mpdf->SetHTMLHeader('<div style="padding-bottom: -50px"><img width="100%" src="'. $imageHeaderPath .'"></div>');
            }
            if($terminal->getCoreDomain() and $terminal->getCoreDomain()->getWebFooterPath()){
                $imageFooterPath = "{$filepath}/public/{$terminal->getCoreDomain()->getWebFooterPath()}";
                $mpdf->SetHTMLFooter('<div style="background-color:red; height: 20px"><img width="100%" src="'. $imageFooterPath .'"></div>');
            }
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status-pdf.html.twig',[
                'records' => $records,
            ]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }

        return $this->render('@TerminalbdProcurement/garments/report/requisition/report-aging-approval-status-details.html.twig',[
            'records' => $records,
        ]);

    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("report-po-summary/{mode}", name="report_po_summary", defaults={"mode" = null})
     */
    public function reportPoSummary(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportPoSummary($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/po-order/pdf.html.twig',['entities' => $entities,'searchForm' =>$searchForm,'caption' => $caption]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/po-order/excel.html.twig',['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/po-order/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("report-po-monthly-summary/{mode}", name="report_po_monthly_summary", defaults={"mode" = null})
     */
    public function reportPoMonthlySummary(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $caption = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class, null, array('terminal' => $terminal, 'config' => $config, 'generic' => $generic, 'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()) {
            $data = $_REQUEST;
        }
        if (in_array($mode,['pdf','excel'])){
            $data = $request->query->get('filterBy');
            $entities = $this->getDoctrine()->getRepository(Report::class)->reportPoSummaryMonthly($config->getId(), $data);
            $searchForm = $data['procurement_report_filter_form'];
        }
        if ($mode == 'pdf'){
            $mpdfOptions = [
                'tempDir' => $filepath. '/public/temp',
                'format' => 'A4',
                'orientation' => 'L',
                'setAutoTopMargin' => 'pad',
            ];
            $mpdf = new \Mpdf\Mpdf($mpdfOptions);
            $mpdf->AddPage('P','','','','',15,15, 15,30,10,-10);
            $fileName = $request->get('_route') . '-' . time() . '.pdf';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/po-order-monthly/pdf.html.twig',['entities' => $entities,'searchForm' =>$searchForm,'caption' => $caption]);
            $mpdf->WriteHTML($html);
            $mpdf->Output($fileName, 'I');
            exit();
        }elseif ($mode == 'excel'){
            $fileName = $request->get('_route') . '-' . time().'.xls';
            $html = $this->renderView('@TerminalbdProcurement/garments/report/po-order-monthly/excel.html.twig',['entities' => $entities]);
            header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachment; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/report/po-order-monthly/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $data
        ]);
    }


}