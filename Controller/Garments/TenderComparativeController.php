<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItemAttribute;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderComparativeFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;


/**
 * @Route("/procure/garments/comparative")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderComparativeController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_garments_comparative")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderComparativeRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/tender-comparative/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/create-vendor", methods={"GET","POST"}, name="procure_tender_garments_comparative_create_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function createVendor(Request $request , Tender $tender, TenderRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = new TenderVendor();
        $form = $this->createForm(TenderVendorFormType::class, $entity,array('terminal' => $terminal));
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $attachFile = $form->get('file')->getData();
            if ($attachFile and $entity->getPath()) {
                $entity->removeUpload();
            }
            $entity->setTender($tender);
            if(!empty($entity->getVendor()) and empty($entity->getName())){
                $entity->setName($entity->getVendor()->getName());
            }
            if(!empty($entity->getVendor()) and empty($entity->getAddress())){
                $entity->setAddress($entity->getVendor()->getAddress());
            }
            if(!empty($entity->getVendor()) and empty($entity->getPhone())){
                $entity->setPhone($entity->getVendor()->getPhone());
            }
            $entity->setStatus(1);
            $entity->setTender($tender);
            $entity->upload();
            $em->persist($entity);
            $em->flush();
        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->redirectToRoute('procure_tender_garments_comparative_cs_new',array('id' => $tender->getId()));
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/cs-new", methods={"GET","POST"}, name="procure_tender_garments_comparative_cs_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csNew(Request $request, Tender $tender,ProcurementProcessRepository $processRepository, TranslatorInterface $translator, TenderVendorRepository $tenderVendorRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = new TenderComparative();
        $form = $this->createForm(TenderCsFormType::class, $entity);
        $form->handleRequest($request);
        $data = $request->request->all();
        $vendorCheck = (isset($data['vendorCheck']) and !empty($data['vendorCheck'])) ? $data['vendorCheck']:'';
        if ($form->isSubmitted() && $form->isValid() && !empty($vendorCheck) && empty($tender->getTenderComparative())) {
            $em = $this->getDoctrine()->getManager();
            $module = "tender-comparative";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
                $entity->setModule($module);
            }
            $entity->setCreatedBy($this->getUser());
            $entity->setConfig($tender->getConfig());
            $entity->setTender($tender);
            $entity->setBusinessGroup("garment");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $files = !empty($_FILES['vendorFiles']) ?  $_FILES['vendorFiles'] : "";
            if(!empty($files)){
                $tenderVendorRepository->vendorAttachmentFile($tender,$data,$files);
            }
            $tenderVendorRepository->csVendorSelection($data);
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $this->getDoctrine()->getRepository(TenderComparative::class)->csItemGenerate($tender,$entity);
           // $this->getDoctrine()->getRepository(TenderComparative::class)->csGroupItemGenerate($tender,$entity);
            return $this->redirectToRoute('procure_tender_garments_comparative_generate',array('id' => $entity->getId()));
        }elseif(!empty($tender->getTenderComparative())){
            return $this->redirectToRoute('procure_tender_garments_comparative_generate',array('id' => $tender->getTenderComparative()->getId()));
        }elseif(!empty($vendorCheck) && empty($tender->getTenderComparative())){
            $message = 'Tender Comparative Statement already created';
            $this->addFlash('notice', $message);
        }
        $vendorForm = $this->createForm(TenderVendorFormType::class, new TenderVendor(),array('terminal' => $terminal));
        return $this->render('@TerminalbdProcurement/garments/tender-comparative/cs-new.html.twig', [
            'tender' => $tender,
            'entity' => $entity,
            'form' => $form->createView(),
            'vendorForm' => $vendorForm->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     * @Route("/{id}/generate", methods={"GET","POST"}, name="procure_tender_garments_comparative_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csTender(Request $request , TenderComparative $entity, EventDispatcherInterface $eventDispatcher , TenderComparativeRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $form = $this->createForm(TenderComparativeFormType::class , $entity);
        $form -> handleRequest($request);
      //  $this->getDoctrine()->getRepository(TenderComparative::class)->csGroupStockBookItemGenerate($tender,$entity);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $processRepository->approvalAssign($entity);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_comparative')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $data = $request->request->all();
            $em->persist($entity);
            $em->flush();
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            }
            return $this->redirectToRoute('procure_tender_garments_comparative');
        }
        $attchments = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getDmsAttchmentFile($entity->getId(),'tender-comparative');

        /* Purchase Requisition Item base CS */
        $repository->csItemGenerate($tender,$entity);
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);

        /* Purchase Requisition Item base CS */
        /*  $csGroupStockBookItemGenerate = $this->getDoctrine()->getRepository(TenderComparative::class)->csGroupStockBookItemGenerate($tender,$entity);
        $comparativeStockBookItems = $repository->comparativeStockBookItems($entity);
        $comparativeStockBookItemAttribites = $this->getDoctrine()->getRepository(TenderComparative::class)->comparativeStockBookItemAttribites($entity);*/


        return $this->render('@TerminalbdProcurement/garments/tender-comparative/comparetive-statement.html.twig', [

            'tender'                                => $tender,
            'entity'                                => $entity,
          //  'csGroupStockBookItemGenerate'          => $csGroupStockBookItemGenerate,
            'form'                                  => $form->createView(),
            'comparativeItems'                      => $comparativeItems,
            'comparativeItemAttribites'             => $comparativeItemAttribites,
          //  'comparativeStockBookItems'             => $comparativeStockBookItems,
          //  'comparativeStockBookItemAttribites'    => $comparativeStockBookItemAttribites,
            'attchments'                            => $attchments

        ]);
    }

   /**
     * Show a Setting entity.
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_tender_garments_comparative_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , TranslatorInterface $translator, EventDispatcherInterface $eventDispatcher , TenderComparative $entity, TenderComparativeRepository $repository, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if (isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            } else {
                $count = $processRepository->count(array('entityId' => $entity->getId(), 'module' => $entity->getModule(), 'close' => 1));
                $ordering = ($count == 0) ? 1 : $count + 1;
                $processRepository->approvalAssign($entity, $ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_comparative')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_tender_garments_comparative',['mode'=>'approve']);
        }
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/garments/tender-comparative/process.html.twig', [
            'tender'                    => $tender,
            'entity'                    => $entity,
            'form'                      => $form->createView(),
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'attchments'                => ''
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/cs-vendor", methods={"GET"}, name="procure_tender_garments_comparative_cs_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csVendor(TenderComparative $entity, TenderComparativeRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/garments/tender-comparative/show.html.twig', [
            'tender' => $tender,
            'entity' => $entity,
            'comparativeItems' => $comparativeItems,
            'comparativeItemAttribites' => $comparativeItemAttribites,
            'attchments' => ''
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-update", methods={"GET","POST"}, name="procure_tender_garments_comparative_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderComparativeItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setUnitPrice($data['unitPrice']);
        $attribute->setPrice($data['unitPrice']);
        $attribute->setRevisedUnitPrice($data['unitPrice']);
        $attribute->setQuantity($data['quantity']);
        $attribute->setSubTotal($attribute->getQuantity() * $attribute->getUnitPrice());
        $attribute->setRevisedSubTotal($attribute->getQuantity() * $attribute->getUnitPrice());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderComparativeItem::class)->updateSummary($attribute->getTenderVendor());
        return new Response('success');
    }


     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/attribute-update", methods={"GET","POST"}, name="procure_tender_garments_comparative_attribute_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function attributeUpdate(Request $request, TenderComparativeItemAttribute $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setMetaValue($data['description']);
        $em->flush();
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_garments_comparative_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Request $request , TenderComparative $entity, TenderComparativeRepository $repository, ProcurementProcessRepository $procurementRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        return $this->render('@TerminalbdProcurement/garments/tender-comparative/show.html.twig', [
            'tender'                    => $tender,
            'entity'                    => $entity,
            'comparativeItems'          => $comparativeItems,
            'comparativeItemAttribites' => '',
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_garments_comparative_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderComparative $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/delete-vendor", methods={"GET"}, name="procure_tender_garments_comparative_vendor_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function deleteVendor(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_particular_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, TenderComparativeRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/cs-closed", methods={"GET"}, name="procure_tender_garments_comparative_closed" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function csClosed(TenderComparative $tenderComparative): Response
    {
        $em =$this->getDoctrine()->getManager();
        $tenderComparative->setWaitingProcess('Closed');
        $tenderComparative->setProcess('Closed');
        $em->persist($tenderComparative);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_garments_comparative_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderComparative $entity,TenderComparativeRepository $repository, ProcurementProcessRepository $procurementRepository): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $terminal = $this->getUser()->getTerminal();
        $tender = $entity->getTender();
        $comparativeItems = $repository->comparativeItems($entity);
        $comparativeItemAttribites = $repository->comparativeItemAttribites($entity);
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/tender-comparative/preview.html.twig', array(
                    'tender'                    => $tender,
                    'entity'                    => $entity,
                    'approvals'                    => $approvals,
                    'comparativeItems'          => $comparativeItems,
                    'comparativeItemAttribites' => $comparativeItemAttribites,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/tender-comparative/pdf.html.twig',array(
                'tender'                    => $tender,
                'entity'                    => $entity,
                'approvals'                    => $approvals,
                'comparativeItems'          => $comparativeItems,
                'comparativeItemAttribites' => $comparativeItemAttribites,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/tender-comparative/preview.html.twig', array(
                    'tender'                    => $tender,
                    'entity'                    => $entity,
                    'approvals'                    => $approvals,
                    'comparativeItems'          => $comparativeItems,
                    'comparativeItemAttribites' => $comparativeItemAttribites,
                    'mode' => 'preview'
                )
            );
        }

    }



    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_tender_garments_comparative_vendor_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request, TenderVendor $entity)
    {

        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/{$entity->getPath()}";
        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}
