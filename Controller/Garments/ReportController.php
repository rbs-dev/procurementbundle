<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\BranchRepository;
use Mpdf\Tag\Option;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Dompdf;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\Report;
use Terminalbd\ProcurementBundle\Entity\RequisitionAgingApprovalStatus;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\Garments\ReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionAgingApprovalStatusRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;


/**
 * @Route("/procure/report/")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class ReportController extends AbstractController
{

    /**
     * @Route("report-dashboard", name="procure_report_dashboard")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index(Request $request) {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        return $this->render('@TerminalbdProcurement/garments/report/dashboard.html.twig', [
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @return Response
     * @Route("report-dashboard-ajax-process", methods={"GET", "POST"}, name="procurement_report_dashboard_ajax_process",options={"expose"=true})
     */
    public function reportDashboardAjaxProcess(Request $request)
    {
        $entities = [];
        $filterBy = [];
        $html = "";
        ini_set("pcre.backtrack_limit", "5000000");
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        $form = $request->request->all();

        if ($form){
            $filepath = $this->get('kernel')->getProjectDir();
            if($form['reportMode'] == 'req-item-flow'){
                $html = $this->itemPurchaseStatus($request,$form);
            }elseif($form['reportMode'] == 'requisition-urgent'){
                $html = $this->requisitionUrgent($request,$form);
            }elseif($form['reportMode'] == 'supplier-financial'){
                $html = $this->supplierFinancial($request,$form);
            }elseif($form['reportMode'] == 'factory-wo'){
                $html = $this->unitFinancial($request,$form);
            }elseif($form['reportMode'] == 'workorder'){
                $html = $this->workorder($request,$form);
            }elseif($form['reportMode'] == 'wo-inhouse'){
                $html = $this->woinhouse($request,$form);
            }elseif($form['reportMode'] == 'jobapproval-to-workorder'){
                $html = $this->jobapprovalToWorkorder($request,$form);
            }elseif($form['reportMode'] == 'requisition-timeline'){
                $html = $this->requisitionTimeline($request,$form);
            }elseif($form['reportMode'] == 'workorder-item'){
                $html = $this->workorderItem($request,$form);
            }
        }
        return new Response($html);
    }

    /**
     * @Route("report-item-status", name="requisition_item_status_report", options={"expose"=true})
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function itemStatus(Request $request) {
        $html = "";
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        $form = $request->request->all();
        if ($searchForm->isSubmitted()){
            if ($form['report_filter_form']['endUpdateDate'] and $form['report_filter_form']['endUpdateDate']) {
                $data = $form['report_filter_form'];
                if (isset($data['company']) and $data['company']) {
                    $company = $this->getDoctrine()->getRepository(Branch::class)->find($data['company']);
                }
                $html = $this->itemProcumentPurchaseStatus($request, $form);
            }
        }
        return $this->render('@TerminalbdProcurement/garments/report/procurement/item-status/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'html' =>$html,
        ]);
    }

    /**
     * @return Response
     * @Route("report-procurement-ajax-process", methods={"GET", "POST"}, name="procurement_report_procurement_ajax_process",options={"expose"=true})
     */
    public function itemProcumentPurchaseStatus(Request $request)
    {
        /* @var $config  Procurement */
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        $form = $request->request->all();
        if($form['report_filter_form']['startUpdateDate'] and $form['report_filter_form']['endUpdateDate']) {
            $data = $form['report_filter_form'];
            $pagination = $this->getDoctrine()->getRepository(Report::class)->requisitionItemStatus($config->getId(), $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/item-status/data.html.twig', [
                'pagination' => $pagination,
            ]);
            return new Response($records);
        }
        return new Response("No Records found, this report need to (Star & End Date)");
    }

    public function itemPurchaseStatus(Request $request, $form)
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $data = $form['report_filter_form'];
            $pagination = $this->getDoctrine()->getRepository(Report::class)->requisitionItemStatus($config->getId(), $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/item-status/data.html.twig', [
                'pagination' => $pagination,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }

    public function requisitionUrgent(Request $request, $form){

        /* @var $config  Procurement */
        $company = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        if(isset($data['company']) and $data['company']){
            $branch = $this->getDoctrine()->getRepository(Branch::class)->find($data['company']);
            $company = $branch->getName();
        }
        $entities = $this->getDoctrine()->getRepository(Report::class)->requisitionUrgentReport($config->getId(),$form);
        $priorities = $this->getDoctrine()->getRepository(Particular::class)->getChildRecords($config,'priority');
        $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/requisition-urgent/data.html.twig',[
            'company' => $company,
            'entities' => $entities,
            'priorities' => $priorities,
            'searchForm' => $data,
        ]);
        return $records;


    }

    public function supplierFinancial(Request $request, $form){

        /* @var $config  Procurement */
        $company = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        if(isset($data['company']) and $data['company']){
            $branch = $this->getDoctrine()->getRepository(Branch::class)->find($data['company']);
            $company = $branch->getName();
        }
        $entities = $this->getDoctrine()->getRepository(Report::class)->getSupplierSummaryReport($config, $form);
        return $this->renderView('@TerminalbdProcurement/garments/report/procurement/supplier-financial/data.html.twig', [
            'company' => $company,
            'entities' => $entities,
            'searchForm' => $data,
        ]);

    }

    public function unitFinancial(Request $request, $form){

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $data = isset($form['report_filter_form']) ? $form['report_filter_form'] : '';
        if(isset($data['company']) and $data['company']){
            $company = $this->getDoctrine()->getRepository(Branch::class)->find($data['company']);
        }
        $entities = $this->getDoctrine()->getRepository(Report::class)->getUnitSummaryReport($config, $form);
        return $this->renderView('@TerminalbdProcurement/garments/report/procurement/unit-financial/data.html.twig', [
            'company' => '',
            'entities' => $entities,
        ]);
        return "No Records found, this report need to (Star & End Date)";
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("wo-report", defaults={"mode"=null}, methods={"GET"}, name="procure_wo_report")
     */
    function woReport(Request $request) {
        $html = "";
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        $form = $request->request->all();
        if ($searchForm->isSubmitted()) {
            $html = $this->workorder($request, $form);
        }
        return $this->render('@TerminalbdProcurement/garments/report/procurement/workorder/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'html' =>$html,
        ]);
    }

    public function workorder(Request $request, $form){

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $data = $form['report_filter_form'];
            $entities = $this->getDoctrine()->getRepository(Report::class)->workorderReport($config, $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/workorder/data.html.twig', [
                'entities' => $entities,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }

    public function workorderItem(Request $request, $form)
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $data = $form['report_filter_form'];
            $pagination = $this->getDoctrine()->getRepository(Report::class)->workorderItemReport($config->getId(), $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/workorder-item/data.html.twig', [
                'pagination' => $pagination,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("wo-inhouse-report", defaults={"mode"=null}, methods={"GET"}, name="procure_wo_inhouse_report")
     */
    function woinhouseReport(Request $request) {
        $html = "";
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        $form = $request->request->all();
        if ($searchForm->isSubmitted()) {
            $html = $this->woinhouse($request, $form);
        }
        return $this->render('@TerminalbdProcurement/garments/report/procurement/wo-inhouse/index.html.twig', [
            'searchForm' => $searchForm->createView(),
            'html' =>$html,
        ]);
    }

    public function woinhouse(Request $request, $form){

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $pagination = $this->getDoctrine()->getRepository(Report::class)->woinhouse($config->getId(),$form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/wo-inhouse/data.html.twig', [
                'entities' => $pagination,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }

    public function jobapprovalToWorkorder(Request $request, $form){

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $data = $form['report_filter_form'];
            $entities = $this->getDoctrine()->getRepository(Report::class)->jobapprovalToWorkorder($config, $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/jobapproval-workorder/data.html.twig', [
                'entities' => $entities,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }

    public function requisitionTimeline(Request $request, $form){

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        if($form['report_filter_form']['startDate'] and $form['report_filter_form']['startDate']) {
            $data = $form['report_filter_form'];
            $entities = $this->getDoctrine()->getRepository(Report::class)->requisitionTimeline($config, $form);
            $records = $this->renderView('@TerminalbdProcurement/garments/report/procurement/requisition-approve-timeline/data.html.twig', [
                'entities' => $entities,
            ]);
            return $records;
        }
        return "No Records found, this report need to (Star & End Date)";
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("wo-jobapproval-report", defaults={"mode"=null}, methods={"GET"}, name="procure_wo_jobapproval_report")
     */
    public function woJobapprovalReport(Request $request,  ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $html = "";
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $form = $_REQUEST;
        if ($searchForm->isSubmitted()){
            $data = $_REQUEST;
            $html = $this->jobapprovalToWorkorder($request,$form);
        }
        return $this->render('@TerminalbdProcurement/garments/report/procurement/jobapproval-workorder/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'html' => $html
        ]);
    }

     /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("requisition-urgent-report", defaults={"mode"=null}, methods={"GET"}, name="requisition_urgent_report")
     */
    public function requisitionUrgentReport(Request $request,  ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $html = "";
        $caption = '';
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $form = $_REQUEST;
        $html = $this->requisitionUrgent($request,$form);
        return $this->render('@TerminalbdProcurement/garments/report/procurement/requisition-urgent/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'html' => $html
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("supplier-financial-report/{mode}", defaults={"mode"=null}, methods={"GET"}, name="supplier_financial_report")
     */
    public function supplierFinancialReport(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filterBy = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
        $form = $_REQUEST;
        $html = $this->supplierFinancial($request,$form);
        return $this->render('@TerminalbdProcurement/garments/report/procurement/supplier-financial/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'html' => $html
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("unit-financial-report", defaults={"mode"=null}, methods={"GET"}, name="unit_financial_report")
     */
    public function unitFinancialReport(Request $request, $mode, ProcurementRepository $procurementRepository)
    {
        $entities = [];
        $data = [];
        $html = "";
        $terminal = $this->getUser()->getTerminal();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $filterBy = $searchForm->getData();
        $filepath = $this->get('kernel')->getProjectDir();
        $form = $_REQUEST;
        $html = $this->unitFinancial($request,$form);
        return $this->render('@TerminalbdProcurement/garments/report/procurement/unit-financial/index.html.twig',[
            'searchForm' => $searchForm->createView(),
            'html' => $html
        ]);
    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report/{mode}", name="stock_report", defaults={"mode" = null})
     */
    public function stockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $filterBy = [];
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ProcurementReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $entities = [];
        if ($searchForm->isSubmitted()){
//            $filterBy = $searchForm->getData();
        }
        return $this->render('@TerminalbdProcurement/garments/report/store/report-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'filterBy' => $filterBy,
            'entities' => $entities,
        ]);
    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-inventory-report/{mode}", name="stock_inventory_report", defaults={"mode" = null})
     */
    public function stockInventoryReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $entities = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $form = $_REQUEST;
        if ($searchForm->isSubmitted()) {
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockReceiveItem($config->getId(), $form);
        }
        return $this->render('@TerminalbdProcurement/garments/report/store/report-inventory-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'filterBy' => $form
        ]);
    }

    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report-company/{mode}", name="stock_company_report", defaults={"mode" = null})
     */
    public function companyStockReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $entities = "";
        $openingEntities = "";
        $rangEntities = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $form = $_REQUEST;
        if ($searchForm->isSubmitted()) {
            $entities = $this->getDoctrine()->getRepository(Report::class)->stockBookItemReports($config, $form);
            $openingEntities = $this->getDoctrine()->getRepository(Report::class)->stockOpeningReport($config, $form);
            $rangEntities = $this->getDoctrine()->getRepository(Report::class)->stockDateRangeReport($config, $form);
        }
        return $this->render('@TerminalbdProcurement/garments/report/store/report-company-stock.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'openingEntities' => $openingEntities,
            'rangEntities' => $rangEntities,
            'filterBy' => $form
        ]);
    }


    /**
     * @param Request $request
     * @param ProcurementRepository $procurementRepository
     * @param $mode
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Mpdf\MpdfException
     * @Route("stock-report-kanban-dashboard/{mode}", name="stock_kanban_dashboard_report", defaults={"mode" = null})
     */
    public function kanbanDashboardReport(Request $request, ProcurementRepository $procurementRepository, $mode)
    {
        $data = [];
        $entities = "";
        $openingEntities = "";
        $rangEntities = "";
        $filepath = $this->get('kernel')->getProjectDir();
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm->handleRequest($request);
        $form = $_REQUEST;
        $entities = $this->getDoctrine()->getRepository(Report::class)->kanbanDashboard($config, $form);
        return $this->render('@TerminalbdProcurement/garments/report/store/kanban.html.twig',[
            'searchForm' => $searchForm->createView(),
            'entities' => $entities,
            'filterBy' => $form
        ]);
    }

}