<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderWorkorderFormAmendmentType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\TenderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderWorkorderFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository;


/**
 * @Route("/procure/garments/work-order")
 * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_TENDER') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderAmendmentController extends AbstractController
{

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/amendment-process", methods={"GET", "POST"}, name="procure_tender_garments_workorder_amendment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function amendment(Request $request, EventDispatcherInterface $eventDispatcher , TenderWorkorder $workorder ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new TenderWorkorder();
        $em = $this->getDoctrine()->getManager();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'work-order');
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $comparative = $workorder->getTenderComparative();
        $entity->setConfig($config);
        $entity->setAmendment($workorder);
        $entity->setTenderComparative($workorder->getTenderComparative());
        $entity->setModule("work-order");
        $entity->setBusinessGroup("garment");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $date = new \DateTime();
        $date1 = new \DateTime();
        $date2 = new \DateTime();
        $date1->add(new \DateInterval('P15D'));
        $date2->add(new \DateInterval('P30D'));
        $deliveryDate = $date1->format('Y-m-d');
        $validateDate = $date2->format('Y-m-d');
        $entity->setWorkorderMode('PO-01');
        $entity->setWorkorderDate($date);
        $entity->setDeliveryDate($date1);
        $entity->setValidateDate($date2);
        $entity->setPaymentMode($comparative->getTender()->getPaymentMode());
        if($comparative->getTender()->getRequisition()){
            $entity->setRequisition($comparative->getTender()->getRequisition());
        }
        $entity->setBranch($comparative->getTender()->getRequisition()->getCompanyUnit());
        $entity->setShipTo($comparative->getTender()->getShipTo());
        $entity->setWearhouse($comparative->getTender()->getShipTo());
        $user = $this->getUser();
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->insertWorkOrderAmendmentItem($entity);
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($entity);
        return $this->redirectToRoute('procure_tender_garments_workorder_amendment_edit',array('id' => $entity->getId()));
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-amendment-update", methods={"GET", "POST"}, name="procure_tender_garments_workorder_ajax_update_amendment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function ajaxUpdate(Request $request, ProcurementRepository $procurementRepository, TenderWorkorder $entity): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $data = $request->request->all();
        $form = $this->createForm(TenderWorkorderFormAmendmentType::class , $entity,array('config' => $config,'tenderComparative'=>$tenderComparative));
        $form->handleRequest($request);
        $entity->setAdditionalDiscount($data['additionalDiscount']);
        $this->getDoctrine()->getManager()->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($entity);
        $reponse = $this->returnResultData($entity->getId());
        return new Response(json_encode($reponse));

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/amendment-edit", methods={"GET", "POST"}, name="procure_tender_garments_workorder_amendment_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function amendmentEdit(Request $request, EventDispatcherInterface $eventDispatcher , TenderWorkorder $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $data = $request->request->all();
        $form = $this->createForm(TenderWorkorderFormAmendmentType::class , $entity,array('config' => $config,'tenderComparative' => $tenderComparative));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setWaitingProcess('In-progress');
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $this->getDoctrine()->getRepository(TenderVendor::class)->amendmentInsertVendor($entity);
            $processRepository->approvalAssign($entity);
            if($entity->getWorkorderMode() == "PO-01" and $entity->getWearhouse()){
                $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWearhouse($entity);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateTenderItemDetails($entity);
                $this->getDoctrine()->getRepository(StockBook::class)->updateItemPrice($entity);
            }
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertWorkorderConditionItem($entity,$data);
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_workorder')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_garments_workorder');
        }
        $vendorItems = "";
        return $this->render('@TerminalbdProcurement/garments/workorder/amendment.html.twig', [
            'entity'            => $entity,
            'vendorItems'       => $vendorItems,
            'form'              => $form->createView()
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/vendor-amendment", methods={"GET","POST"}, name="procure_tender_garments_workorder_vendor_amendment" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function vendorUpdate(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(Vendor::class)->find($v);
        $workorder->setVendor($vendor);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($workorder);
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{invoice}/{id}/item-update-amendment", methods={"GET","POST"}, name="procure_tender_garments_workorder_item_update_amendment" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdateAmendment(Request $request, TenderWorkorder $invoice , TenderWorkorderItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $discount = floatval($data['discount']) ;
        $vat = floatval($data['vat']);
        $description = isset($data['description']) ? $data['description']:'';
        $attribute->setDiscount($discount);
        $attribute->setVendorCommissionPercent($invoice->getVendorCommissionPercent());
        $attribute->setVat($vat);
        $attribute->setDescription($description);
        $attribute->setTotal($attribute->getSubTotal() - $attribute->getDiscount());
        /* if($invoice->getEnlistedVendor() and $attribute->getVendorCommissionPercent()) {
             $commission = ((floatval($attribute->getTotal()) * floatval($attribute->getVendorCommissionPercent())) / 100);
             $attribute->setVendorCommission($commission);
         }*/
        $attribute->setGrandTotal($attribute->getTotal() + $attribute->getVat() + $attribute->getShippingCharge());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($invoice);
        $entity = $this->getDoctrine()->getRepository(TenderWorkorder::class)->find($invoice);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/workorder/item-amendment.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }



}
