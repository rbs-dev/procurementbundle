<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\Budget;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use App\Entity\Application\Procurement;
use App\Entity\Core\PercentLabel;
use App\Entity\Core\Profile;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\EmailEvent;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\GoogleMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetMonthlyAdjusment;
use Terminalbd\BudgetBundle\Entity\BudgetPurchaseRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisitionAmendment;
use Terminalbd\BudgetBundle\Repository\BudgetRequisitionAmendmentRepository;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Form\InventoryFilterFormType;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\InventoryBundle\Repository\StockWearhouseRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\GarmentRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\PurchaseRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\GarmentsRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\SecurityBillingBundle\Entity\Invoice;
use Terminalbd\SecurityBillingBundle\Repository\CompanyInvoiceTemplateRepository;
use Terminalbd\SecurityBillingBundle\Repository\InvoiceParticularRepository;


/**
 * @Route("/procure/garments/purchase-requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseRequisitionController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_garment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $tenders = $this->getDoctrine()->getRepository(Tender::class)->getRequisitionFromTender($pagination);
        $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorderRequisition($pagination);

        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'tenders' => $tenders,
                'workOrders' => $workOrders,
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * @Route("/bill-entry", methods={"GET", "POST"}, name="procure_billentry_garment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function indexBillEntry(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBillEntrySearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBillEntrySearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/billentry.html.twig',
            [
                'pagination' => $pagination,
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/confidential-entry", methods={"GET", "POST"}, name="procure_confidential_garment")
     * @Security("is_granted('ROLE_BUDGET') or is_granted('ROLE_DOMAIN')")
     */
    public function indexConfidentialEntry(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findConfidentialSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findConfidentialSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $tenders = $this->getDoctrine()->getRepository(Tender::class)->getRequisitionFromTender($pagination);
        $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorderRequisition($pagination);

        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/confidential.html.twig',
            [
                'pagination' => $pagination,
                'tenders' => $tenders,
                'workOrders' => $workOrders,
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }



    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_requisition_garment_archived")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = !empty($data['mode']) ? $data['mode'] : "archived";
        $pageMode = array('mode' => "archived");
        $data = array_merge($data,$pageMode);
        $search = $repository->findRequisitionArchivedSearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorderRequisition($pagination);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'workOrders' => $workOrders,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/in-progress-requisition-item", methods={"GET", "POST"}, name="procure_inprogress_requisition_item")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function progressRequisitionItem(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'particularRepo' => $particularRepository))->add('Excel', SubmitType::class);
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = array('items'=> '');
        $data = array_merge($data,$mode);
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->requisitionItemInprogressReport($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->requisitionItemInprogressReport($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        if($searchForm->get('Excel')->isClicked()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->requisitionItemInprogressReport($config->getId(),$data);
            $html = $this->renderView('@TerminalbdProcurement/garments/purchase-requisition/excel-pending-item-report.html.twig',[
                'pagination' => $search->getArrayResult()
            ]);
            $spreadsheet = new Spreadsheet();
            $reader = new Html();
            $spreadsheet = $reader->loadFromString($html);
            ob_end_clean();
            $fileName = $request->get('_route') . '_' . time() . '.xlsx';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=$fileName");
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet,'Xlsx');
            $writer = new Xlsx($spreadsheet);
            exit($writer->save('php://output'));
        }
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/inprogress-requisition-item.html.twig', [
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView(),
        ]);
    }

     /**
     * @Route("/in-progress-requisition-budget-item", methods={"GET", "POST"}, name="procure_inprogress_requisition_budget_item")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function progressRequisitionBudgetItem(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $items = explode(',',$request->cookies->get('itemIds'));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = array('items'=> '');
        $data = array_merge($data,$mode);
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->requisitionItemInprogressReport($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->requisitionItemInprogressReport($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/inprogress-budget-item.html.twig', [
            'pagination' => $pagination,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'searchForm' => $searchForm->createView(),
        ]);
    }

     /**
     * @Route("/in-progress-requisition-reject", methods={"GET", "POST"}, name="procure_requisition_garment_reject")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function progressRequisitionReject(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $items = explode(',',$request->cookies->get('itemIds'));
        $pagination = $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->requisitionRejects($config->getId(),$items);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            foreach ($data['item'] as $row){
                $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($row);
                if($entity){
                    /* @var $entity Requisition */
                    $entity->setProcess('Rejected');
                    $entity->setWaitingProcess('Rejected');
                    $entity->setReportTo(null);
                    $entity->setComment($comment);
                    $entity->setSubTotal(0);
                    $entity->setRejectedBy($this->getUser());
                    $entity->setProcessOrdering(0);
                    $em->persist($entity);
                    $em->flush();
                    if($entity->getJobRequisition()){
                        $this->getDoctrine()->getRepository(Requisition::class)->removeJobApproval($entity);
                    }
                    if($entity->getRequisitionMode() != "CAPEX"){
                        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->deleteRequisitionBudgetHead($entity,$entity->getFinancialYear());
                    }
                }

            }
            unset($_COOKIE['itemIds']);
            setcookie('itemIds', '', time() - 3600, '/');
            $message = $translator->trans('data.reject_successfully');
            $this->addFlash('notice', $message);
            return $this->redirectToRoute('procure_inprogress_requisition_budget_item');
        }
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/requisition-reject.html.twig', [
            'pagination' => $pagination,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/budget-process", methods={"GET", "POST"}, name="procure_requisition_garment_budget_process")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function processBudget(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "approve";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "approve";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "approve";
        }
        $department = $user->getProfile()->getDepartment()->getId();
        $budgetUsers = $this->getDoctrine()->getRepository(Profile::class)->getDepartmentUsers($user->getId(),$department);
        $search = $repository->findRequisitionBudgetProcessQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/budget-process.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'selected' => explode(',', $request->cookies->get('itemIds', '')),
                'budgetUsers' => $budgetUsers,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * @Route("/requisition-item-report", methods={"GET", "POST"}, name="procure_requisition_item_report_garment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionItemReport(Request $request, ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = !empty($data['mode']) ? $data['mode'] : "archived";
        $pageMode = array('mode' => "archived");
        $data = array_merge($data,$pageMode);
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->requisitionItemReport($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->requisitionItemReport($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        if($request->query->has('excel')){
            $html = $this->renderView('@TerminalbdProcurement/garments/purchase-requisition/excel-workorder-item-report.html.twig',[
                'pagination' => $search->getArrayResult()
            ]);
            $fileName = $request->get('_route') . '_' . time() . '.xls';
            header("Content-Type: application/vnd.ms-excel; charset=utf-8");
            header("Content-Disposition: attachement; filename=$fileName");
            echo $html;
            die();
        }
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/workorder-item-report.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/new", methods={"GET","POST"}, name="procure_requisition_garment_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator ,ProcurementRepository $procurementRepository,  ProcurementProcessRepository $processRepository,ApprovalUserRepository $approvalUserRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new Requisition();
        $entity->setConfig($config);
        $module = "purchase-requisition";
        $user = $this->getUser();
        $em =  $this->getDoctrine()->getManager();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and $user->getProfile() and $user->getProfile()->getDepartment()) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            if(isset($_REQUEST['requisitionMode'])){
                $mode = $_REQUEST['requisitionMode'];
                $entity->setRequisitionMode($mode);
            }
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            if($this->getUser()->getProfile()->getDepartment()){
                $entity->setProcessDepartment($this->getUser()->getProfile()->getDepartment());
            }
            $entity->setCreatedBy($user);
            $entity->setFinancialYear($_ENV['FINANCIAL_YEAR']);
            if($entity->getRequisitionMode() == 'Expense') {
                $entity->setBillMonth("Previous");
                $lastmonth = date("Y-m-d H:i:s",strtotime("last day of previous month"));
                $date = new \DateTime($lastmonth);
                $entity->setCreated($date);
                if ($entity->getCreated()->format('F') == "June") {
                    $entity->setFinancialYear($_ENV['OLD_FINANCIAL_YEAR']);
                }
            }
            $entity->setCreatedBy($user);
            if ($user->getProfile() and $user->getProfile()->getDepartment()) {
                $entity->setDepartment($user->getProfile()->getDepartment());
                $entity->setProcessDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
           // $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
          //  $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_requisition_garment_edit', array('id' => $entity->getId()));
        }
        $requisitionShares = '';
        $branches = $em->getRepository(Branch::class)->findBy(array('terminal' => $terminal,'branchType' => 'branch','status'=>1), array('name'=>'ASC'));
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/new.html.twig', [
            'entity'                => $entity,
            'items'                => '',
            'branches'              => $branches,
            'requisitionShares'     => $requisitionShares,
            'form'                  => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_garment_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id, EventDispatcherInterface $eventDispatcher , TranslatorInterface $translator, UserRepository $userRepository,ApprovalUserRepository $approvalUserRepository , ProcurementRepository $procurementRepository, RequisitionRepository $repository,GenericMasterRepository $genericMasterRepository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository,ProcurementProcessRepository $processRepository, StockRepository $stockRepository,BudgetRequisitionAmendmentRepository $requisitionAmendmentRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity Requisition */

        $em =  $this->getDoctrine()->getManager();
        $entity = $repository->findOneBy(array('config' => $config ,'id' => $id,'createdBy' => $this->getUser()));
        if (!$entity || !in_array($entity->getWaitingProcess(),['New','In-progress']) ) {
            $message = "Error message: You are not authorized to access this requisition";
            $this->addFlash('notice', $message);
            return $this->redirectToRoute('procure_requisition_garment');
        }
        $form = $this->createForm(GarmentRequisitionFormType::class , $entity,array('config'=>$config));
        $count = $this->getDoctrine()->getRepository(RequisitionItem::class)->count(array('requisition' => $id));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid() && $count > 0 AND in_array($entity->getWaitingProcess(),['New','In-progress'])) {
            if($entity->getJobRequisition() and $entity->getSubTotal() > $entity->getJobApprovalAmount()){
                $message = "This requisition is not valid becasue job approval amount is low {$entity->getJobApprovalAmount()}";
                $this->addFlash('notice', $message);
                return $this->redirectToRoute('procure_requisition_garment_edit', array('id' => $entity->getId()));
            }
            $data = $request->request->all();
            $files = $_FILES['files'];
            $requisitionToBudget = $_REQUEST['requisitionToBudget'];
            $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity, $_ENV['FINANCIAL_YEAR']);
            //  $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($entity);
            $low = $this->getDoctrine()->getRepository(Requisition::class)->checkBudgetLowStatus($entity->getId());
            if($low === 1 and $entity->getRequisitionMode() != "CAPEX"){
                //  $requisitionAmendmentRepository->insertRequisition($this->getUser() , $entity);
                $entity->setReportTo(NULL);
                $entity->setProcessOrdering(0);
                $entity->setProcess("Budget-Cross");
                $em->flush();
                if(in_array($entity->getRequisitionMode(),array('Expense'))) {
                    return $this->redirectToRoute('procure_billentry_garment', array('mode' => 'list'));
                }elseif(in_array($entity->getRequisitionMode(),array('Confidential'))) {
                    return $this->redirectToRoute('procure_confidential_garment', array('mode' => 'list'));
                }else{
                    return $this->redirectToRoute('procure_requisition_garment', array('mode' => 'list'));
                }
            }
            $entity->setWaitingProcess('In-progress');
            $em->flush();
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            //  $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->requisitionAssignHod($entity);
            if(in_array($entity->getRequisitionMode(),array('CAPEX','OPEX','Expense','Confidential','INV'))) {
                $assignUsers = $approvalUserRepository->getApprovalAssignMatrixUser($terminal, $entity);
                $processRepository->insertProcurementRequisitionProcessAssign($entity, $entity->getModule(), $assignUsers);
            }
            if(empty($entity->getCreatedBy()->getReportTo())){
                $processRepository->approvalAssign($entity);
            }
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_garment')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            if($entity->getJobRequisition()){
                $this->getDoctrine()->getRepository(Requisition::class)->updateJobApproval($entity->getJobRequisition());
            }
            // $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            if(in_array($entity->getRequisitionMode(),array('Expense'))) {
                return $this->redirectToRoute('procure_billentry_garment');
            }elseif(in_array($entity->getRequisitionMode(),array('Confidential'))){
                return $this->redirectToRoute('procure_confidential_garment');
            }else{
                return $this->redirectToRoute('procure_requisition_garment');
            }

        }
        $purchaseItem = new RequisitionItem();
        $approveUsers = $processRepository->getExistApprovalUsers($entity->getId(),$entity->getModule());
        $assignForm = $this->createForm(ProcurementProcessFormType::class, new ProcurementProcess() ,array('config' => $genricConfig,'users' => $approveUsers));
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity,$financialYear);
        $low = $this->getDoctrine()->getRepository(Requisition::class)->checkBudgetLowStatus($entity->getId());
        if(in_array($entity->getRequisitionMode(),array('CAPEX','INV'))) {
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/capex.html.twig', [
                'entity' => $entity,
                'branches' => '',
                'requisitionShares' => $requisitionShares,
                'form' => $form->createView(),
            ]);
        }elseif(in_array($entity->getRequisitionMode(),array('Expense','Confidential'))){
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/new-billentry.html.twig', [
                'entity'                => $entity,
                'branches'              => '',
                'requisitionShares'     => $requisitionShares,
                'low'                   => $low,
                'form'                  => $form->createView(),
            ]);
        }else{
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/edit.html.twig', [
                'entity'                => $entity,
                'branches'              => '',
                'requisitionShares'     => $requisitionShares,
                'low'                   => $low,
                'form'                  => $form->createView(),
            ]);
        }
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/admin-edit", methods={"GET", "POST"}, name="procure_requisition_garment_admin_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function editAdmin(Request $request, $id, EventDispatcherInterface $eventDispatcher , TranslatorInterface $translator, UserRepository $userRepository,ApprovalUserRepository $approvalUserRepository , ProcurementRepository $procurementRepository, RequisitionRepository $repository,GenericMasterRepository $genericMasterRepository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository,ProcurementProcessRepository $processRepository, StockRepository $stockRepository,BudgetRequisitionAmendmentRepository $requisitionAmendmentRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity Requisition */

        $em =  $this->getDoctrine()->getManager();
        $entity = $repository->findOneBy(array('config' => $config ,'id' => $id));
        if (empty($entity) and $this->getUser()->getId() != 129 and !in_array($entity->getWaitingProcess(),['Approved']) ) {
            $message = "Error message: You are not authorized to access this requisition";
            $this->addFlash('notice', $message);
            return $this->redirectToRoute('procure_requisition_garment');
        }
        $form = $this->createForm(GarmentRequisitionFormType::class , $entity,array('config'=>$config));
        $count = $this->getDoctrine()->getRepository(RequisitionItem::class)->count(array('requisition' => $id));
        $form->handleRequest($request);
        $purchaseItem = new RequisitionItem();
        $approveUsers = $processRepository->getExistApprovalUsers($entity->getId(),$entity->getModule());
        $assignForm = $this->createForm(ProcurementProcessFormType::class, new ProcurementProcess() ,array('config' => $genricConfig,'users' => $approveUsers));
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity,$financialYear);
        $low = $this->getDoctrine()->getRepository(Requisition::class)->checkBudgetLowStatus($entity->getId());
        if(in_array($entity->getRequisitionMode(),array('Expense','Confidential'))){
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/new-billentry.html.twig', [
                'entity'                => $entity,
                'branches'              => '',
                'requisitionShares'     => $requisitionShares,
                'low'                   => $low,
                'form'                  => $form->createView(),
            ]);
        }elseif(in_array($entity->getRequisitionMode(),array('CAPEX','INV'))){
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/capex.html.twig', [
                'entity' => $entity,
                'branches' => '',
                'requisitionShares' => $requisitionShares,
                'form' => $form->createView(),
            ]);
        }elseif(in_array($entity->getRequisitionMode(),array('OPEX'))){
            return $this->render('@TerminalbdProcurement/garments/purchase-requisition/edit.html.twig', [
                'entity'                => $entity,
                'branches'              => '',
                'requisitionShares'     => $requisitionShares,
                'low'                   => $low,
                'form'                  => $form->createView(),
            ]);
        }else{
            return $this->redirectToRoute('procure_billentry_garment');
        }
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_requisition_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config =  $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(GarmentRequisitionFormType::class , $entity,array('config' => $config));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        if($entity->getCompanyUnit()){
            $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->insertRequisitionShareSingle($entity);
        }
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
          //  $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/{branch}/share-update", methods={"GET"}, name="procure_requisition_garments_share_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function shareUpdate(Requisition $requisition,Branch $branch): Response
    {
        $amount = $_REQUEST['amount'];
        $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->updateShareRequisition($requisition,$branch,$amount);
        return new Response('success');
    }

     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/requisition_billmonth", methods={"GET"}, name="procure_garments_requisition_billmonth" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionBillmonth(Requisition $requisition): Response
    {
        $billMonth = $_REQUEST['billMonth'];
        $em = $this->getDoctrine()->getManager();
        $requisition->setBillMonth($billMonth);
        $requisition->getCreated()->format('F');
        if($requisition->getBillMonth() == 'Previous'){
            $lastmonth = date("Y-m-d H:i:s",strtotime("last day of previous month"));
            $date = new \DateTime($lastmonth);
            $requisition->setCreated($date);
            if ($requisition->getCreated()->format('F') == "June") {
                $requisition->setFinancialYear($_ENV['OLD_FINANCIAL_YEAR']);
            }
        }else{
            $date = new \DateTime("now");
            $requisition->setCreated($date);
            $requisition->setFinancialYear($_ENV['FINANCIAL_YEAR']);
        }
        foreach ($requisition->getBudgetItems() as $budget):
        if($budget){
            $entity = $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->find($budget->getId());
            $em->remove($entity);
        }
        endforeach;
        $em->persist($requisition);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/requisition-update", methods={"GET","POST"}, name="procure_requisition_garment_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionUpdate(Request $request , Requisition $requisition): Response
    {
        $data = $request->request->all();
        $form = $data['garment_requisition_form'];

        if($form['processDepartment']){
            $department = $this->getDoctrine()->getRepository(Setting::class)->find($form['processDepartment']);
            $requisition->setProcessDepartment($department);
        }
        if($form['section']){
            $section = $this->getDoctrine()->getRepository(Setting::class)->find($form['section']);
            $requisition->setSection($section);
        }
        if($form['priority']){
            $priority = $this->getDoctrine()->getRepository(Particular::class)->find($form['priority']);
            $requisition->setPriority($priority);
        }
        if($form['companyUnit']){
            $unit = $this->getDoctrine()->getRepository(Branch::class)->find($form['companyUnit']);
            $requisition->setCompanyUnit($unit);
        }
        if($form['expectedDate']){
            $date = new \DateTime($form['expectedDate']);
            $requisition->setExpectedDate($date);
        }
        if($form['content']){
            $requisition->setContent($form['content']);
        }
        if($form['isEmergency']){
            $requisition->setIsEmergency($form['isEmergency']);
        }
        $em =  $this->getDoctrine()->getManager();
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/budget-generate", methods={"GET"}, name="procure_requisition_garment_budget_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function budget(Requisition $requisition): Response
    {
        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition,$financialYear);
    //    $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($requisition);
        return $this->redirectToRoute('procure_requisition_garment_edit',array('id'=>$requisition->getId()));
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/budget-reload-generate", methods={"GET"}, name="procure_requisition_garment_budget_reload" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_BUDGET') or is_granted('ROLE_DOMAIN')")
     */
    public function budgetReload(Requisition $requisition): Response
    {

        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition, $_ENV['FINANCIAL_YEAR']);
       // $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($requisition);
        return new Response('success');

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{requisition}/{id}/budget-delete", methods={"GET"}, name="procure_requisition_garment_budget_delete" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_BUDGET') or is_granted('ROLE_DOMAIN')")
     */
    public function budgetDeleteReload(Request $request ,Requisition $requisition,  $id): Response
    {

        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->find($id);
        $em->remove($entity);
        $em->flush();
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition,$financialYear);
        return $this->redirect($request->server->get('HTTP_REFERER'));

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/budget-reload-generate-from-budget", methods={"GET"}, name="procure_requisition_garment_reload_from_budget" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_BUDGET') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReloadFromBudget(Request $request,Requisition $requisition): Response
    {

        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition,$_ENV['FINANCIAL_YEAR']);
        // $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($requisition);
        return $this->redirect($request->server->get('HTTP_REFERER'));
        //  return new Response('success');

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/budget-adjustment", methods={"GET"}, name="procure_requisition_garment_budget_adjustment" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function budgetAdjustment(RequisitionBudgetItem $item, RequisitionRepository $repository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $item->getGlBudgetMonth()->getGl()->getName();
        $entity = $item->getRequisition();
        $config = $this->getDoctrine()->getRepository(Budget::class)->config($terminal);
        $limit = empty($config->getBudgetMergeLimit()) ? 3 : $config->getBudgetMergeLimit();
        $budgets = $repository->requisitionBudgetGenerate($entity);
        $start = strtolower($entity->getCreated()->format('Y-01-01'));
        $end = strtolower($entity->getCreated()->format('Y-m-01'));
        $budgetTillMonth = array();
        $budgetadjustmentMonths = array();
        $start = $month = strtotime($start);
        $end = strtotime($end);
        while($month < $end)
        {
            $budgetTillMonth[] = date('F', $month);
            $month = strtotime("+1 month", $month);

        }
        $tillMonth = strtolower($entity->getCreated()->format('m'));
        $till = (int)$tillMonth;
        $limitMonth = ($till+$limit);
        if($limitMonth > 12 ){
           $monthCount = 12;
        }else{
            $monthCount = $limitMonth;
        }
        $nextMonth = ($till+1);
        $monthAdjust = strtolower($entity->getCreated()->format("Y-$nextMonth-01"));
        $endMonth = strtotime($entity->getCreated()->format("Y-$monthCount-01"));
        $start = $startmonth = strtotime($monthAdjust);
        while($startmonth <= $endMonth)
        {
            $budgetadjustmentMonths[] =  date('F', $startmonth);
            $startmonth = strtotime("+1 month", $startmonth);
        }
        $months = array_merge($budgetTillMonth,$budgetadjustmentMonths);
        $adjusments = $this->getDoctrine()->getRepository(BudgetMonth::class)->getTillAdjustmentMonths($item,$budgetadjustmentMonths);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/budget.html.twig', [
            'entity' => $entity,
            'budget'  => $item,
            'adjusments'  => $adjusments,
            'requisitionShares'  => $requisitionShares,
        ]);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/budget-adjustment-save", methods={"GET","POST"}, name="procure_requisition_garment_budget_adjustment_save" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function budgetAdjustmentSave(Request $request , RequisitionBudgetItem $item, RequisitionRepository $repository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $item->getGlBudgetMonth()->getGl()->getName();
        $entity = $item->getRequisition();
        $config = $this->getDoctrine()->getRepository(Budget::class)->config($terminal);
        $this->getDoctrine()->getRepository(BudgetMonthlyAdjusment::class)->insertAdjustment($item,$data);
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity, $_ENV['FINANCIAL_YEAR']);
      //  $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($entity);
        return $this->redirectToRoute('procure_requisition_garment_edit',array('id'=>$entity->getId()));

    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/budget-view", methods={"GET"}, name="procure_requisition_budget_view" , options={"expose"=true})
     */
    public function budgetView(Request $request,Requisition $requisition)
    {
        $mode = (isset($_REQUEST['mode']) and $_REQUEST['mode'] ) ? 'show' : '';
        $budgets = $this->getDoctrine()->getRepository(Requisition::class)->requisitionBudgetGenerate($requisition);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/budgetItemShow.html.twig',
            [
                'entity' => $requisition,
                'budgets' => $budgets,
                'mode' => $mode
            ]
        );

    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/{mode}/head-view", methods={"GET"}, name="procure_requisition_head_view" , options={"expose"=true})
     */
    public function purchaseRequisitionHead(Request $request,Requisition $requisition,$mode)
    {
        $requisitionShares = $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->requisitionShares($requisition);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/requisition-head.html.twig',
            [
                'entity' => $requisition,
                'mode' => $mode,
                'requisitionShares' => $requisitionShares
            ]
        );

    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_garment_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionRepository $repository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $budgets = $repository->requisitionBudgetGenerate($entity);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/purchase-requisition/show.html.twig', array(
                'entity' => $entity,
                'attchments'        => $attchments,
                'budgets'        => $budgets,
                'mode'        => 'show',
                'requisitionShares'  => $requisitionShares,
            )
        );
        return new Response($html);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_garment_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id ,EventDispatcherInterface $eventDispatcher , RequisitionRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity Requisition */
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
      //  $this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setSubTotal(0);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                if($entity->getJobRequisition()){
                    $this->getDoctrine()->getRepository(Requisition::class)->removeJobApproval($entity);
                }
                if($entity->getRequisitionMode() != "CAPEX"){
                    $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->deleteRequisitionBudgetHead($entity,$_ENV['FINANCIAL_YEAR']);
                }
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }elseif(isset($data['budget']) and $data['budget'] == "budget"){
                $assign =  $data['assignTo'];
                $assignTo  = empty($assign) ? $this->getUser()->getId() : $assign;
                $this->getDoctrine()->getRepository(BudgetRequisition::class)->insertPurchaseRequisition($entity,$this->getUser(),$assignTo,$comment);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_garment')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                if($entity->getWaitingProcess() == "Approved" and $entity->getRequisitionMode() == "Expense"){
                    $this->getDoctrine()->getRepository(Requisition::class)->monthlyRequisitionExpenseProcess($entity);
                }
                $this->getDoctrine()->getRepository(RequisitionItemHistory::class)->requisitionItemHistory($this->getUser(),$entity);
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_requisition_garment',['mode'=>'approve']);
        }
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $budgets = $repository->requisitionBudgetGenerate($entity);
        $budgetUsers = "";
        if($entity->getReportTo() and $entity->getReportTo()->getProfile()->getDepartment()->getSlug() == "budget"){
            $department = $entity->getReportTo()->getProfile()->getDepartment()->getId();
            $budgetUsers = $this->getDoctrine()->getRepository(Profile::class)->getDepartmentUsers($entity->getReportTo()->getId(),$department);
        }
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/process.html.twig', [
            'entity' => $entity,
            'requisitionShares'  => $requisitionShares,
            'budgetUsers'  => $budgetUsers,
            'budgets'  => $budgets,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/requisition-budget-assign", methods={"POST"}, name="procure_requisition_garment_budget_assign" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionBudgetAssign(Request $request): Response
    {
        $user = $this->getUser();
        $assign = $request->request->get('assignTo');
        $comment = $request->request->get('comment');
        $itemIds = $request->request->get('itemId');
        $assignTo  = empty($assign) ? $user->getId() : $assign;
        if($itemIds){
            $this->getDoctrine()->getRepository(BudgetRequisition::class)->processRequisitionBundle($itemIds,$user,$assignTo,$comment);
        }
        unset($_COOKIE['itemIds']);
        setcookie('itemIds', '', time() - 3600, '/');
        return $this->redirectToRoute('procure_requisition_garment_budget_process');

    }




    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/reverse", methods={"GET"}, name="procure_requisition_garment_reverse" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReverse(Requisition $entity, RequisitionRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $requisition = $requisitionRepository->insertReverseRequsition($entity);
        $this->getDoctrine()->getRepository(RequisitionItem::class)->getItemSummary($requisition);
        return $this->redirectToRoute('procure_requisition_garment_edit',array('id'=>$requisition->getId()));
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_garment_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/item-revise-history", methods={"GET"}, name="procure_requisition_garment_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemReviseHistory($id): Response
    {
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($id);
        return $this->render('@TerminalbdProcurement/garments/purchase-requisition/kanban.html.twig',
            [
                'entity' => $entity,
            ]
        );
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_garment_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function delete($id, RequisitionRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $response = "invalid";
        $em = $this->getDoctrine()->getManager();
        $entity = $repository->findOneBy(array('id' => $id,'createdBy' => $this->getUser()));
        if($entity){
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Particular entity.');
            }else{
                $attchments = $this->getDoctrine()->getRepository(Requisition::class)->getDmsAttchmentFile($entity);
                $requisitionShares = $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->requisitionShares($entity);
                $budgets = $repository->requisitionBudgetGenerate($entity);
                $html = $this->renderView(
                    '@TerminalbdProcurement/garments/purchase-requisition/show.html.twig', array(
                        'entity' => $entity,
                        'attchments'        => $attchments,
                        'budgets'        => $budgets,
                        'requisitionShares'  => $requisitionShares,
                    )
                );
                $entity->setProcess('Deleted');
                $entity->setWaitingProcess('Deleted');
                $entity->setDeleteContent($html);
                $entity->setIsDelete(1);
                $entity->setDeletedBy($this->getUser());
                $entity->setJobApprovalAmount(0);
                $em->persist($entity);
                $em->flush();
                if($entity->getRequisitionMode() != "CAPEX"){
                    $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->deleteRequisitionBudgetHead($entity,$_ENV['FINANCIAL_YEAR']);
                }
                if($entity->getJobRequisition()){
                    $this->getDoctrine()->getRepository(Requisition::class)->removeJobApproval($entity);
                }
                $response = 'valid';
            }
        }
        return new Response($response);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/administratior-delete", methods={"GET"}, name="procure_requisition_garment_administratior_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function administratiorDelete($id, RequisitionRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $response = "invalid";
        $em = $this->getDoctrine()->getManager();
        $entity = $repository->findOneBy(array('id' => $id));
        if($entity){
            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Particular entity.');
            }else{
                $attchments = $this->getDoctrine()->getRepository(Requisition::class)->getDmsAttchmentFile($entity);
                $requisitionShares = $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->requisitionShares($entity);
                $budgets = $repository->requisitionBudgetGenerate($entity);
                $html = $this->renderView(
                    '@TerminalbdProcurement/garments/purchase-requisition/show.html.twig', array(
                        'entity' => $entity,
                        'attchments'        => $attchments,
                        'budgets'        => $budgets,
                        'requisitionShares'  => $requisitionShares,
                    )
                );
                $entity->setWaitingProcess("Deleted");
                $entity->setProcess("Deleted");
                $entity->setDeleteContent($html);
                $entity->setIsDelete(1);
                $entity->setJobApprovalAmount(0);
                $entity->setDeletedBy($this->getUser());
                $em->persist($entity);
                $em->flush();
                if($entity->getRequisitionMode() != "CAPEX"){
                    $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->deleteRequisitionBudgetHead($entity,$_ENV['FINANCIAL_YEAR']);
                }
                if($entity->getJobRequisition()){
                    $this->getDoctrine()->getRepository(Requisition::class)->removeJobApproval($entity);
                }
                $response = 'valid';
            }
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_garment_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id, RequisitionRepository $repository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $requisitionShares = $comapnyRequisitionShareRepository->requisitionShares($entity);
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        $budgets = $repository->requisitionBudgetGenerate($entity);
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/purchase-requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'entity' => $requisition,
                    'approvals' => $approvals,
                    'requisitionShares'  => $requisitionShares,
                    'budgets' => $budgets,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/purchase-requisition/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'requisitionShares'  => $requisitionShares,
                'budgets' => $budgets,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/purchase-requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'requisitionShares'  => $requisitionShares,
                    'budgets' => $budgets,
                    'mode' => 'preview'
                )
            );
        }

    }

    /**
     * @Route("/approve-requisition-budget-checked", methods={"GET", "POST"}, name="approve_requisition_budget_checked", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function approveRequisitionBudgetChecked(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
       // $this->getDoctrine()->getRepository(Requisition::class)->approveRequisitionBudgetChecked($config->getId(),$this->getUser());
        $this->getDoctrine()->getRepository(Requisition::class)->approvedRequisitionMissingBudget($config, $_ENV['FINANCIAL_YEAR']);
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/excel", methods={"GET"}, name="procure_requisition_garment_excel")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_SBS_MANAGER')")
     */
    public function invoiceExcel(Request $request,Invoice $entity, InvoiceParticularRepository $invoiceParticularRepository,  CompanyInvoiceTemplateRepository $templateRepository): Response
    {

        $templateItems = $templateRepository->findBy(array('customer' => $entity->getCustomer(), 'printStatus' => 1), array('ordering' => 'asc'));
        $invoiceItems = $invoiceParticularRepository->getInvoicePrintItems($entity, $templateItems);

        $html = $this->renderView('@TerminalbdSecurityBilling/invoice/printPreviewExcel.html.twig',[
            'entity' => $entity,
            'templateItems' => $templateItems,
            'invoiceItems' => $invoiceItems
        ]);

        $fileName = $request->get('_route') . '-' . time() . '.xls';
        header("Content-Type: application/vnd.ms-excel; charset=utf-8");
        header("Content-Disposition: attachement; filename=$fileName");

        echo $html;
        die();
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/requisition-reset", methods={"GET"}, name="procure_requisition_requisition_reset" , options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReset(Request $request ,Requisition $entity, ProcurementProcessRepository $processRepository,ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $em = $this->getDoctrine()->getManager();
        $entity->setWaitingProcess("In-progress");
        $entity->setProcess("In-progress");
        $entity->setProcessOrdering(1);
        $em->flush();
        $processRepository->requisitionAssignHod($entity);
        if(in_array($entity->getRequisitionMode(),array('CAPEX','OPEX','Expense'))) {
            $assignUsers = $approvalUserRepository->getApprovalAssignMatrixUser($terminal, $entity);
            $processRepository->insertProcurementRequisitionProcessAssign($entity, $entity->getModule(), $assignUsers);
        }
        if(empty($entity->getCreatedBy()->getReportTo())){
            $processRepository->approvalAssign($entity);
        }
        $this->getDoctrine()->getRepository(ProcurementProcess::class)->revisedProcess($entity);
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->resetRequisitionBudgetItem($entity);
        return $this->redirect($request->server->get('HTTP_REFERER'));
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/budget-head-reset", methods={"GET"}, name="procure_requisition_budget_head_reset" , options={"expose"=true})
     */
    public function resetRequisitionBudgetItem(Request $request)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = $this->getUser()->getTerminal()->getId();
        $config =  $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $searches = $this->getDoctrine()->getRepository(Requisition::class)->findRequisitionInprogressSearchQuery($config);
        /* @var $row Requisition */

        dd($searches);
        foreach ($searches as $row):
            if(count($row->getBudgetItems()) == 0 and $row->getCompanyUnit()){
                $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->resetRequisitionBudgetItem($row);
            }
        endforeach;
        return $this->redirectToRoute('procure_requisition_garment_budget_process');
    }

}
