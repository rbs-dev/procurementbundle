<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueRepository;


/**
 * @Route("/procurement/garments/requisition-issue-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionIssueItemController extends AbstractController
{

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-item", methods={"GET","POST"}, name="procure_requisition_garments_issue_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, RequisitionIssue $entity, RequisitionIssueRepository $requisitionRepository , RequisitionIssueItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['requisition_issue_item_form'];
        $itemRepository->insertRequisitionItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($requisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }


    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/delete-item", methods={"GET"}, name="procure_requisition_garments_issue_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, ProcurementRepository $procurementRepository, RequisitionIssueRepository $requisitionRepository, RequisitionIssueItemRepository $itemRepository): Response
    {
        $requisition = $requisitionRepository->findOneBy(array('id' => "{$entity}"));
        $item = $itemRepository->findOneBy(array('requisition' => $requisition ,'id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getItemSummary($requisition);
        $return = $this->returnResultData($requisitionRepository,$requisition->getId());
        return new Response(json_encode($return));
    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/update-item", methods={"GET","POST"}, name="procure_requisition_garments_issue_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request,$id,RequisitionIssueItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionIssueItem */

        $data = $request->request->all();
        $item = $itemRepository->find($id);
        $item->setQuantity($data['quantity']);
        $item->setIssueQuantity($data['quantity']);
        $this->getDoctrine()->getManager()->flush();
        return  new Response('success');

    }

    public function returnResultData(RequisitionIssueRepository $requisitionRepository ,$requisition){

        $entity = $requisitionRepository->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-issue/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }



}
