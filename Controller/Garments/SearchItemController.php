<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;


use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Form\ItemFormType;
use Terminalbd\GenericBundle\Repository\CategoryRepository;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\CmpPrice;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Form\InventoryFilterFormType;
use Terminalbd\InventoryBundle\Form\PurchaseFilterFormType;
use Terminalbd\InventoryBundle\Form\StockFormType;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\InventoryBundle\Repository\StockWearhouseRepository;

class SearchItemController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            50  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_INVEROLE_INVENTORY') or is_granted('ROLE_GRAMENTS')")
     * @Route("/stock-book-item", methods={"GET", "POST"}, name="procure_item_search")
     */
    public function index(Request $request,StockBookRepository $repository, InventoryRepository $inventoryRepository, GenericMasterRepository $masterRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genericConfig = $masterRepository->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(InventoryFilterFormType::class , null,array('config'=>$genericConfig,'categoryRepo'=>$categoryRepo));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$data);
        } else {
            $search = $repository->findBySearchQuery($config,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/search/index.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

}