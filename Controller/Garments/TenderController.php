<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementConditionItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderCompare;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderItem;
use Terminalbd\ProcurementBundle\Entity\TenderItemDetails;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderCsWorkorderFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderCsFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderPreparetionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderVendorFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;



/**
 * @Route("/procure/garments/tender")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_garments")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $processMode = "tender";
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findSearchQuery($config,$this->getUser(),$processMode,$data);
        } else {
            $search = $repository->findSearchQuery($config,$this->getUser(),$processMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/tender/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

     /**
     * @Route("/direct-workorder", methods={"GET", "POST"}, name="procure_tender_direct_garments")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function indexDirectWorkorder(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , TenderRepository $repository): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        $processMode = "workorder";
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findSearchQuery($config,$this->getUser(),$processMode,$data);
        } else {
            $search = $repository->findSearchQuery($config,$this->getUser(),$processMode,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/tender/indexDirect.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'searchForm' => $searchForm->createView()
        ]);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     * @Route("/open-item", methods={"GET", "POST"}, name="procure_tender_garments_open_item")
     */
    public function openItem(Request $request, ProcurementRepository $procurementRepository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository, RequisitionRepository $requisitionRepository, TenderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = array('items'=> '');
        $data = array_merge($data,$mode);
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prOpenItem($config->getId(),$data);
        } else {
            $search = $this->getDoctrine()->getRepository(RequisitionItem::class)->prOpenItem($config->getId(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/tender/open-item.html.twig', [
            'pagination' => $pagination,
            'selected' => explode(',', $request->cookies->get('itemIds', '')),
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/open-item-selected", methods={"GET", "POST"}, name="procure_tender_garments_open_item_selected", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function openItemSelected(Request $request,ProcurementRepository $procurementRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $items = explode(',',$request->cookies->get('itemIds'));
        if(is_null($items)) {
            return $this->redirect($this->generateUrl('procure_tender_garments_open_item'));
        }
        $entity = new Tender();
        $form = $this->createForm(TenderFormType::class , $entity);
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setCreatedBy($this->getUser());
            $module = "tender";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
            }
            $entity->setModule($module);
            $date = new \DateTime();
            $date1->add(new DateInterval('P7D'));
            $date2->add(new DateInterval('P2D'));
            $deliveryDate = $date1->format('Y-m-d');
            $replyDate = $date2->format('Y-m-d');
            $entity->setDeliveryDate($deliveryDate);
            $entity->setTenderReplyDate($replyDate);
            $entity->setBusinessGroup("garment");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            if($entity->isTender() == 1){
                $entity->setProcessMode("tender");
            }else{
                $entity->setProcessMode("workorder");
            }
            $em->persist($entity);
            $em->flush();
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $this->getDoctrine()->getRepository(TenderItemDetails::class)->insertTenderItem($entity, $data);
            $this->getDoctrine()->getRepository(TenderItem::class)->insertTenderStockBookItem($entity);
            $directAmount = empty($entity->getConfig()->getDirectTenderAmount()) ? 25000 : $entity->getConfig()->getDirectTenderAmount();
            //if($entity->isTender() == 1 and $entity->getSubTotal() > $directAmount){
            unset($_COOKIE['itemIds']);
            setcookie('itemIds', '', time() - 3600, '/');
            if($entity->isTender() == 1 ){
                return $this->redirectToRoute('procure_tender_garments_preparetion',array('id' => $entity->getId()));
            }else{
                return $this->redirectToRoute('procure_tender_garments_preparetion_workorder',array('id' => $entity->getId()));
            }

        }
        $data = array('items'=> $items);
        $entities = $this->getDoctrine()->getRepository(RequisitionItem::class)->prOpenItem($config->getId(),$data);
            return $this->render('@TerminalbdProcurement/garments/tender/selected-item.html.twig', array(
                'entities'      => $entities,
                'form' => $form->createView(),
            ));
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/pr-to-tender", methods={"GET", "POST"}, name="procure_tender_topr_garments", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function prToTender(Request $request, Requisition $requisition , ProcurementRepository $procurementRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new Tender();
        $form = $this->createForm(TenderFormType::class , $entity);
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $entity->setCreatedBy($this->getUser());
            $module = "tender";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
            }
            $date1 = new \DateTime();
            $date2 = new \DateTime();
            $date1->add(new \DateInterval('P7D'));
            $date2->add(new \DateInterval('P2D'));
            $deliveryDate = $date1->format('Y-m-d');
            $replyDate = $date2->format('Y-m-d');
            $entity->setDeliveryDate($date1);
            $entity->setTenderReplyDate($date2);
            $entity->setModule($module);
            $entity->setBusinessGroup("garment");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            if($entity->isTender() == 1){
                $entity->setProcessMode("tender");
            }else{
                $entity->setProcessMode("workorder");
            }
            $entity->setRequisition($requisition);
            $em->persist($entity);
            $em->flush();
            if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }
            $this->getDoctrine()->getRepository(TenderItemDetails::class)->insertTenderItem($entity, $data);
            $this->getDoctrine()->getRepository(TenderItem::class)->insertTenderStockBookItem($entity, $data);
            //$directAmount = empty($entity->getConfig()->getDirectTenderAmount()) ? 25000 : $entity->getConfig()->getDirectTenderAmount();
            if($entity->isTender() == 1 ){
                return $this->redirectToRoute('procure_tender_garments_preparetion',array('id' => $entity->getId()));
            }else{
                return $this->redirectToRoute('procure_tender_garments_preparetion_workorder',array('id' => $entity->getId()));
            }
        }
        $items = array();
        foreach ($requisition->getRequisitionItems() as $row){
            $items[]= $row->getId();
        }

        $data = array('items'=> $items);
        $entities = $this->getDoctrine()->getRepository(RequisitionItem::class)->prOpenItem($config->getId(),$data);
        return $this->render('@TerminalbdProcurement/garments/tender/selected-item.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
        ));

    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/preparetion-workorder", methods={"GET", "POST"}, name="procure_tender_garments_preparetion_workorder")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function preparetionWorkorder(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, TenderVendorRepository $vendorRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity Tender */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(TenderCsWorkorderFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        $form->handleRequest($request);
        $data = $request->request->all();

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setProcess('Direct Workorder');
            $entity->setWaitingProcess('In-progress');
            $entity->setProcessMode('workorder');
            $this->getDoctrine()->getManager()->flush();
            $processRepository->approvalAssign($entity);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_comparative')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            $files = isset( $_FILES['files']) ?  $_FILES['files'] : "";
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            };
            $vendor = $entity->getDirectVendor();
            if($vendor){
                $this->getDoctrine()->getRepository(TenderVendor::class)->directTenderVendor($entity,$vendor);
            }
            //$this->getDoctrine()->getRepository(TenderItem::class)->updateDirectWorkorderPrItem($entity,$data);
            if($entity->getCondition()){
                $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertTenderConditionItem($entity,$data);
            }
            if($entity->getWaitingProcess() == "Approved" and $entity->getDirectVendor()){
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                $workorder = $this->getDoctrine()->getRepository(TenderComparative::class)->directCsWorkorderProcess($entity);
                return $this->redirectToRoute('procure_tender_garments_workorder_edit',array('id' => $workorder->getId()));
            }
            return $this->redirectToRoute('procure_tender_direct_garments',array('mode' => 'list'));
        }
        return $this->render('@TerminalbdProcurement/garments/tender/directWorkorder.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/preparetion-cs", methods={"GET", "POST"}, name="procure_tender_garments_preparetion")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function preparetionCs(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, TenderVendorRepository $vendorRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Tender */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(TenderPreparetionFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        $form->handleRequest($request);
        $data = $request->request->all();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setProcessMode('tender');
            $this->getDoctrine()->getManager()->flush();
            $processRepository->approvalAssign($entity);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_comparative')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                 $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
           // $this->getDoctrine()->getRepository(TenderItem::class)->updateTenderItem($entity,$data);
            $this->getDoctrine()->getRepository(TenderVendor::class)->insertTendorVendor($entity,$data);
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertTenderUpdateConditionItem($entity,$data);
            $files = isset( $_FILES['files']) ?  $_FILES['files'] : "";
            if(!empty($files)){
                $processRepository->insertAttachmentFile($entity,$data,$files);
            };
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                return $this->redirectToRoute('procure_tender_garments_comparative_create_vendor',array('id' => $entity->getId()));
            }
            return $this->redirectToRoute('procure_tender_garments',array('mode' => 'list'));
        }
        return $this->render('@TerminalbdProcurement/garments/tender/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_garments_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity Tender */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $this->getDoctrine()->getRepository(Tender::class)->vendorDeletes($entity);
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_comparative')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
                if($entity->getWaitingProcess() == "Approved" and $entity->getDirectVendor()){
                    $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                    $this->getDoctrine()->getRepository(TenderComparative::class)->directCsWorkorderProcess($entity);
                }elseif($entity->getWaitingProcess() == "Approved" ) {
                    $this->getDoctrine()->getRepository(Tender::class)->updateOpenItem($entity);
                }
            }
            if($entity->getProcessMode() =='workorder'){
                return $this->redirectToRoute('procure_tender_direct_garments',['mode'=>'approve']);
            }else{
                return $this->redirectToRoute('procure_tender_garments',['mode'=>'approve']);
            }
        }
        return $this->render('@TerminalbdProcurement/garments/tender/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }



    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_garments_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(Tender $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/tender/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-close", methods={"GET"}, name="procure_tender_garments_item_close" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemClose(RequisitionItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity->setIsClose(0);
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('procure_tender_garments_open_item');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_garments_tender_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, Tender $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        if($v){
            $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
            $tender->setCondition($entity);
            $tender->setContent($entity->getBody());
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->initialGarmentsTenderConditionItem($tender);
            $em->flush();
        }
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{invoice}/{id}/item-update", methods={"GET","POST"}, name="procure_tender_garments_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, Tender $invoice, TenderItemDetails $attribute): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $price = isset($data['price']) ? floatval($data['price']) : 0;
        $attribute->setIssueQuantity($data['quantity']);
        if($price > 0){
            $attribute->setPrice($price);
        }
        $attribute->setSubTotal($attribute->getIssueQuantity() * $attribute->getPrice());
        $em->persist($attribute);
        $em->flush($attribute);
        $this->getDoctrine()->getRepository(TenderItem::class)->getTenderItemSummary($invoice);
        $this->getDoctrine()->getRepository(TenderItem::class)->countTenderQuantity($attribute->getRequisitionItem());
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/tender/item.html.twig', array(
                'entity' => $invoice,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_garments_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(Tender $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($entity->getRequisition()){
            $entity->getRequisition()->setProcess('Approved');
            $entity->getRequisition()->setWaitingProcess('Approved');
            $em->flush();
        }
        $this->getDoctrine()->getRepository(Tender::class)->resetPurchaseWorder($entity);
        return new Response('valid');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/vendor-delete", methods={"GET"}, name="procure_tender_garments_vendor_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteVendor(TenderVendor $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($entity);
        $em->flush();
        return new Response('Success');
    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/item-delete", methods={"GET"}, name="procure_tender_garments_item_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function itemDelete(TenderItem $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_garments_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function ajaxUpdate(Request $request, ProcurementRepository $procurementRepository, Tender $entity): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        if($entity->getProcessMode() =="tender"){
            $form = $this->createForm(TenderPreparetionFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        }else{
            $form = $this->createForm(TenderCsWorkorderFormType::class, $entity, array('terminal'=>$this->getUser()->getTerminal(),'config'=>$config));
        }
        $form->handleRequest($request);
        $this->getDoctrine()->getManager()->flush();
        $reponse = array('msg'=>'success');
        return new Response(json_encode($reponse));

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_garments_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(Tender $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/tender/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/tender/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/tender/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/reset-direct-workorder", methods={"GET"}, name="procure_tender_garments_reset")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function resetDirectWorkorder(Tender $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository(Tender::class)->resetPurchaseWorder($entity);
        return $this->redirectToRoute('procure_tender_direct_garments',['mode'=>'list']);
    }


}
