<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderReceiveFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository;


/**
 * @Route("/procure/garments/goods-receive")
 * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderGoodsReceiveController extends AbstractController
{


    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/work-order", methods={"GET", "POST"}, name="procure_tender_garments_receive_workorder")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function goodReceive(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "inhouse-pending";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "inhouse-pending";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "inhouse-pending";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->goodsReceiveQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->goodsReceiveQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $grns = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->getReceiveOrders($pagination);

        $data = $_REQUEST;
        return $this->render('@TerminalbdProcurement/garments/goods-receive/workorder.html.twig', [
            'pagination' => $pagination,
            'grns' => $grns,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_garments_receive")
     * @Security("is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_PROCUREMENT_STORE')  or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderReceiveRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $data = $_REQUEST;
        $grns = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->getReceiveOrders($pagination);
        return $this->render('@TerminalbdProcurement/garments/goods-receive/index.html.twig', [
            'pagination' => $pagination,
            'mode' => $mode,
            'grns' => $grns,
            'searchForm' => $searchForm->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/receive", methods={"GET", "POST"}, name="procure_tender_garments_workorder_receive")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function receive(Request $request, TenderWorkorder $workorder ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $amount = $this->getDoctrine()->getRepository(TenderWorkorder::class)->currentAmount($workorder);
        if($amount > 0){
            $em =  $this->getDoctrine()->getManager();
            $entity = new TenderWorkorderReceive();
            $module = "goods-receive";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)){
                $entity->setModuleProcess($moduleProcess);
            }
            $entity->setConfig($config);
            $entity->setWorkorder($workorder);
            $entity->setModule($module);
            $entity->setVendorCommissionPercent($workorder->getVendorCommissionPercent());
            $entity->setBusinessGroup("garment");
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($this->getUser());
            $em->persist($entity);
            $em->flush();
            $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->insertWorkOrderReceiveItem($entity,$workorder);
            /*if($entity->getModuleProcess()->isStatus() == 1){
                $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
                $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            }*/
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_garments_receive_edit',['id'=>$entity->getId()]);
        }
        $message = "This is no work worder left";
        $this->addFlash('notice', $message);
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);

    }


     /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/new", methods={"GET", "POST"}, name="procure_tender_garments_workorder_new")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $amount = $this->getDoctrine()->getRepository(TenderWorkorder::class)->currentAmount($workorder);
        $em =  $this->getDoctrine()->getManager();
        $entity = new TenderWorkorderReceive();
        $module = "goods-receive";
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $entity->setConfig($config);
        $entity->setModule($module);
        $entity->setVendorCommissionPercent($workorder->getVendorCommissionPercent());
        $entity->setBusinessGroup("garment");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $entity->setMode("Direct");
        $entity->setCreatedBy($this->getUser());
        $em->persist($entity);
        $em->flush();
        $message = $translator->trans('data.updated_successfully');
        $this->addFlash('success', $message);
        return $this->redirectToRoute('procure_tender_garments_receive_direct',['id'=>$entity->getId()]);

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_tender_garments_receive_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request,EventDispatcherInterface $eventDispatcher , TenderWorkorderReceive $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $form = $this->createForm(TenderReceiveFormType::class , $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $processRepository->approvalAssign($entity);
            $this->getDoctrine()->getManager()->flush();
           // $processRepository->approvalAssign($entity);
           // $processRepository->storeReceiveAssignHod($entity);
           /* if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_receive')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                 $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }*/
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }

            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->removeEmptyQuantity($entity->getId());
                $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(StockBook::class)->insertWorkorderStockQuantity($entity);
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_garments_receive');
        }
       return $this->render('@TerminalbdProcurement/garments/goods-receive/new.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_garments_receive_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function ajaxUpdate(Request $request, ProcurementRepository $procurementRepository, TenderWorkorderReceive $entity): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $data = $request->request->all();
        $form = $this->createForm(TenderReceiveFormType::class , $entity);
        $form->handleRequest($request);
        $this->getDoctrine()->getManager()->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->getReceiveItemSummary($entity);
        $reponse = $this->returnResultData($entity->getId());
        return new Response(json_encode($reponse));

    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/item-update", methods={"GET","POST"}, name="procure_tender_garments_receive_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderWorkorderReceiveItem $item ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $quantity = ($data['quantity']) ? $data['quantity'] : 0;
        $item->setQuantity($quantity);
        $returnQuantity = ($data['returnQuantity']) ? $data['returnQuantity'] : 0;
        $item->setReturnQuantity($returnQuantity);
        $item->setPrice($data['price']);
        $item->setCasNo($data['casNo']);
        $item->setCasType($data['casType']);
        if($data['expireDate']){
            $date = new \DateTime($data['expireDate']);
            $item->setExpireDate($date);
        }
        if($data['msds'] == 1){
            $item->setMsds(true);
        }else{
            $item->setMsds(false);
        }
        if($data['price'] > 0 ){
            $item->setSubTotal($item->getQuantity() * $item->getPrice());
        }else{
            $price = $item->getTenderWorkorderItem()->getUnitPrice();
            $item->setPrice($price);
            $item->setSubTotal($item->getQuantity() * $price);
        }
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->getReceiveItemSummary($item->getWorkorderReceive());
        $reponse = $this->returnResultData($item->getWorkorderReceive()->getId());
        return new Response(json_encode($reponse));
    }

    public function returnResultData($id){

        $entity = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->find($id);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $percent = $entity->getVendorCommissionPercent() > 0 ? $entity->getVendorCommissionPercent() : 0;
        $commission = $entity->getVendorCommission() > 0 ? $entity->getVendorCommission() : 0;
        $discount = $entity->getDiscount() > 0 ? $entity->getDiscount() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $data = array(
            'subTotal' => $subTotal,
            'vat' => $vat,
            'percent' => $percent,
            'commission' => $commission,
            'discount' => $discount,
            'total' => $total
        );
        return $data;

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_garments_receive_process", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request, $id,EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator , ProcurementRepository $procurementRepository , TenderWorkorderReceiveRepository $repository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity TenderWorkorderReceive */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_receive')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                     $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->updateOpenItem($entity);
                $this->getDoctrine()->getRepository(StockBook::class)->insertWorkorderStockQuantity($entity);
                $this->getDoctrine()->getRepository(Stock::class)->insertGarmentWorkorderStockQuantity($entity);
            }
            return $this->redirectToRoute('procure_tender_garments_receive',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/goods-receive/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_garments_receive_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderWorkorderReceive $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/goods-receive/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_garments_receive_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderWorkorderReceive $entity): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/goods-receive/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/goods-receive/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/goods-receive/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_garments_receive_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderWorkorderReceive $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

}
