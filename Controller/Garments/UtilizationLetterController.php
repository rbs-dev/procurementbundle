<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\Budget;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\CommentCreatedEvent;
use App\Event\EmailEvent;
use App\EventListener\EmailListener;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\EasyMailer;
use App\Service\FileUploader;
use App\Service\GoogleMailer;
use App\Service\PHPMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetGlAdditional;
use Terminalbd\DmsBundle\Entity\ProcurementFile;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\DmsBundle\Repository\ProcurementFileRepository;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\UtilizationLetter;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\UtilizationLetterBudgetFormType;
use Terminalbd\ProcurementBundle\Form\Garments\UtilizationLetterFormType;
use Terminalbd\ProcurementBundle\Form\Garments\UtilizationLetterResoansFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\UtilizationLetterRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;



/**
 * @Route("/procure/utilization-letter")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class UtilizationLetterController extends AbstractController
{
    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_utilization_letter")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request,TranslatorInterface $translator, UtilizationLetterRepository $repository , ProcurementRepository $procurementRepository): Response
    {

        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $entity = new UtilizationLetter();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);

        return $this->render('@TerminalbdProcurement/garments/utilization-letter/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_utilization_letter_archived")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator, UtilizationLetterRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();

        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = "archived";
        $pageMode = array('mode' => $mode);
        $data = array_merge($data,$pageMode);
        $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/utilization-letter/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/process-index", methods={"GET", "POST"}, name="procure_utilization_letter_process_index")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function utilization(Request $request, TranslatorInterface $translator, UtilizationLetterRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();

        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = "archived";
        $pageMode = array('mode' => $mode);
        $data = array_merge($data,$pageMode);
        $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/utilization-letter/uitilization.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_utilization_letter_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = new UtilizationLetter();
        $entity->setConfig($config);
        $module = "utilization-letter";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and $user->getProfile() and $user->getProfile()->getDepartment()) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            $entity->setWaitingProcess("In-progress");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            $checkBy = $this->getDoctrine()->getRepository(User::class)->find( $entity::CHECKED_BY);
            $entity->setCheckedBy($checkBy);
            $approvedBy = $this->getDoctrine()->getRepository(User::class)->find( $entity::APPROVED_BY);
            $entity->setApprovedBy($approvedBy);

            $entity->setBillMonth("Previous");
            $lastmonth = date("Y-m-d H:i:s",strtotime("last day of previous month"));
            $date = new \DateTime($lastmonth);
            $entity->setCreated($date);
            if ($entity->getCreated()->format('F') == "June") {
                $entity->setFinancialYear($_ENV['OLD_FINANCIAL_YEAR']);
                $entity->setBillMonthName($entity->getCreated()->format('F'));
            }else{
                $entity->setFinancialYear($_ENV['FINANCIAL_YEAR']);
                $entity->setBillMonthName($entity->getCreated()->format('F'));
            }
            if ($user->getProfile() and $user->getProfile()->getDepartment()) {
                $entity->setDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
         //   $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
         //   $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_utilization_letter_edit', array('id' => $entity->getId()));
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_utilization_letter_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function edit(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator,UserRepository $userRepository, ProcurementRepository $procurementRepository, UtilizationLetterRepository $repository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, ApprovalUserRepository $approvalUserRepository ): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity UtilizationLetter */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(UtilizationLetterFormType::class , $entity,array('config'=>$config));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setProcess('Verify');
            $entity->setWaitingProcess("In-progress");
            $this->getDoctrine()->getManager()->flush();
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
           // $processRepository->approvalAssign($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_utilization_letter',['mode'=>'list']);
        }
        $formBudget = $this->createForm(UtilizationLetterBudgetFormType::class , $entity,array('config' => $entity->getConfig()));
        return $this->render('@TerminalbdProcurement/garments/utilization-letter/edit.html.twig', [
            'entity'                => $entity,
            'form'                  => $form->createView(),
            'formBudget'            => $formBudget->createView(),
        ]);
    }



    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/reason-update", methods={"GET", "POST"}, name="procure_utilization_letter_reason")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function reasonUpdate(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator,UserRepository $userRepository, ProcurementRepository $procurementRepository, UtilizationLetterRepository $repository, ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository, ApprovalUserRepository $approvalUserRepository ): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity UtilizationLetter */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(UtilizationLetterResoansFormType::class , $entity,array('config'=>$config));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setWaitingProcess('In-progress');
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            $processRepository->approvalAssign($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_utilization_letter')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
             //   $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            return $this->redirectToRoute('procure_utilization_letter',['mode'=>'list']);
        }
        $formBudget = $this->createForm(UtilizationLetterBudgetFormType::class , $entity,array('config' => $entity->getConfig()));
        return $this->render('@TerminalbdProcurement/garments/utilization-letter/reason.html.twig', [
            'entity'                => $entity,
            'form'                  => $form->createView(),
            'formBudget'            => $formBudget->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_utilization_letter_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $entity = $this->getDoctrine()->getRepository(UtilizationLetter::class)->find($id);
        $form = $this->createForm(UtilizationLetterFormType::class , $entity,array('config' => $entity->getConfig()));
        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $request->request->all();
            $company = ($data['utilization_letter_form']['company']);
            $em = $this->getDoctrine()->getManager();
            if($company){
                $entity->setCompany($this->getDoctrine()->getRepository(Branch::class)->find($company));
            }
            $em->flush();
            return new Response('success');
        }
        return new Response('false');
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-budget-update", methods={"GET", "POST"}, name="procure_utilization_letter_ajax_budget_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteBudget(Request $request ,UtilizationLetter $entity)
    {
        $formBudget = $this->createForm(UtilizationLetterBudgetFormType::class , $entity,array('config' => $entity->getConfig()));
        $formBudget->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        if(($entity->getExpenseYtdAmount() - $entity->getYearlyYtdAmount()) > 0){
            $totalAmount = (($entity->getExpenseYtdAmount() - $entity->getYearlyYtdAmount()) + $entity->getRequiredApprovalAmount());
        }else{
            $totalAmount = $entity->getRequiredApprovalAmount();
        }
        $entity->setTotalApprovalAmount($totalAmount);
        $em->persist($entity);
        $em->flush();
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/utilization-letter/budget-utilization.html.twig', array(
                'entity' => $entity,
                'formBudget'  => $formBudget->createView(),
            )
        );
        return new Response($html);
    }

     /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-budget-update-approver", methods={"GET", "POST"}, name="procure_utilization_letter_ajax_budget_update_approver")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteBudgetApprover(Request $request ,UtilizationLetter $entity)
    {
        $formBudget = $this->createForm(UtilizationLetterBudgetFormType::class , $entity,array('config' => $entity->getConfig()));
        $formBudget->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $amount =($_REQUEST['amount']);
        $entity->setRequiredApprovalAmount($amount);
        if($entity->getExpenseYtdAmount() > $entity->getYearlyYtdAmount()){
            $totalAmount = (($entity->getExpenseYtdAmount() - $entity->getYearlyYtdAmount()) + $entity->getRequiredApprovalAmount());
        }else{
            $totalAmount =  $amount;
        }
        $entity->setTotalApprovalAmount($totalAmount);
        $em->persist($entity);
        $em->flush();
        $updateEntity = $this->getDoctrine()->getRepository(UtilizationLetter::class)->find($entity->getId());
        $html =  $this->renderView('@TerminalbdProcurement/garments/utilization-letter/reason-budget-update.html.twig',
            [
                'entity' => $updateEntity,
            ]
        );
        return new Response($html);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/requisition_billmonth", methods={"GET"}, name="procure_utilization_letter_billmonth" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionBillmonth(UtilizationLetter $requisition): Response
    {

        $billMonth = $_REQUEST['billMonth'];
        $em = $this->getDoctrine()->getManager();
        $requisition->setBillMonth($billMonth);
        $requisition->getCreated()->format('F');
        if($requisition->getBillMonth() == 'Previous'){
            $lastmonth = date("Y-m-d H:i:s",strtotime("last day of previous month"));
            $date = new \DateTime($lastmonth);
            $requisition->setCreated($date);
            if ($requisition->getCreated()->format('F') == "June") {
                $requisition->setFinancialYear($_ENV['OLD_FINANCIAL_YEAR']);
            }
            $requisition->setBillMonthName($requisition->getCreated()->format('F'));
        }else{
            $date = new \DateTime("now");
            $requisition->setCreated($date);
            $requisition->setBillMonthName($requisition->getCreated()->format('F'));
            $requisition->setFinancialYear($_ENV['FINANCIAL_YEAR']);
        }
        $em->persist($requisition);
        $em->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/reverse", methods={"GET"}, name="procure_utilization_letter_garment_reverse" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReverse(UtilizationLetter $entity, UtilizationLetterRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $requisition = $requisitionRepository->insertReverseRequsition($entity,$this->getUser());
        return $this->redirectToRoute('procure_utilization_letter_edit',array('id'=>$requisition->getId()));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_utilization_letter_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, EventDispatcherInterface $eventDispatcher , UtilizationLetterRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessUtilizationComment($this->getUser(),$entity,$comment,$entity->getTotalApprovalAmount());
            }
            if(isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_utilization_letter')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                   // $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_utilization_letter',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/utilization-letter/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_utilization_letter_process_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, UtilizationLetter $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /* @var $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setTitle($entity->getBody());
        $tender->setContent($entity->getDescription());
        $em->flush();
        return new Response('success');
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_utilization_letter_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, UtilizationLetterRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/utilization-letter/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_utilization_letter_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(UtilizationLetter $entity, UtilizationLetterRepository $repository): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        return $this->render(
            '@TerminalbdProcurement/garments/utilization-letter/preview.html.twig', array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => 'preview',
            )
        );
    }

    /**
     * Status a ProcurementProcess entity.
     * @Route("/{id}/reason-budget-inline-update", methods={"GET"}, name="procure_utilization_letter_inline_update" , options={"expose"=true})
     */
    public function reasonBudgetUpdate(Request $request,$id,UtilizationLetterRepository $processRepository)
    {
        $entity = $processRepository->find($id);
        $html =  $this->renderView('@TerminalbdProcurement/garments/utilization-letter/reason-budget-update.html.twig',
            [
                'entity' => $entity,
            ]
        );
        return new Response($html);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/generated", methods={"GET"}, name="procure_utilization_letter_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function utilizationGenerated($id, UtilizationLetterRepository $repository, RequisitionRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = $repository->find($id);
        $remainAmount = ($entity->getRequiredApprovalAmount() - $entity->getProcessAmount() );
        if( $remainAmount > 0){
            $config = $this->getDoctrine()->getRepository(Budget::class)->config($terminal);
            $additional = $this->getDoctrine()->getRepository(BudgetGlAdditional::class)->insertAdditionalBudget($entity,$config,$this->getUser());
            return $this->redirectToRoute('budget_gladditional_edit',['id'=>$additional->getId()]);
        }else{
            $message = "This job approval is not sufficent amount of budget";
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_utilization_letter',['mode'=>'list']);
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_utilization_letter_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status(UtilizationLetter $entity): Response
    {
        if($entity->getProcess() == "Verify"){
            $entity->setProcess("Verified");
            $entity->setWaitingProcess("Verified");
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_utilization_letter_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id): Response
    {

        $em = $this->getDoctrine()->getManager();
        /* @var $entity UtilizationLetter */
        $entity = $this->getDoctrine()->getRepository(UtilizationLetter::class)->findOneBy(array('id' => "{$id}"));
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        $html = $this->renderView('@TerminalbdProcurement/garments/utilization-letter/show.html.twig',array(
            'entity' => $entity,
            'approvals' => $approvals,
            'mode' => "print"
        ));
        $entity->setDeleteContent($html);
        $entity->setIsDelete(1);
        $em->flush();
        $response = 'valid';
        return new Response($response);
    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_utilization_letter_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function downloadAttachFile(Request $request,$id)
    {

        $entity =  $this->getDoctrine()->getRepository(UtilizationLetter::class)->find($id);
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/procurement/utilization/{$entity->getPath()}";

        if (!empty($entity->getPath()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}
