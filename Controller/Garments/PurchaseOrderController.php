<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\Budget;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use App\Entity\Application\Procurement;
use App\Entity\Core\Profile;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\EmailEvent;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\GoogleMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetMonthlyAdjusment;
use Terminalbd\BudgetBundle\Entity\BudgetPurchaseRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisitionAmendment;
use Terminalbd\BudgetBundle\Repository\BudgetRequisitionAmendmentRepository;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Form\InventoryFilterFormType;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\GarmentRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\PurchaseRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\ReportFilterFormType;
use Terminalbd\ProcurementBundle\Form\GarmentsRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;


/**
 * @Route("/procure/garments/purchase-order")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseOrderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_purchase_order_garment")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findPurchaseOrderSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findPurchaseOrderSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        $tenders = $this->getDoctrine()->getRepository(Tender::class)->getRequisitionFromTender($pagination);
        $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorder($pagination);
        return $this->render('@TerminalbdProcurement/garments/purchase-order/index.html.twig',
            [
                'pagination' => $pagination,
                'tenders' => $tenders,
                'workOrders' => $workOrders,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/revised", methods={"GET"}, name="procure_purchase_order_garment_revised")
     * @Security("is_granted('ROLE_PROCUREMENT_ADMIN') or is_granted('ROLE_DOMAIN')")
     */

    public function revised($id, RequisitionRepository $repository, ProcurementRepository $procurementRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id,'waitingProcess'=>'Approved'));
        $em = $this->getDoctrine()->getManager();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find requisition entity.');
        }
        $entity = $repository->find($id);
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->requisitionShares($entity);
        $budgets = $repository->requisitionBudgetGenerate($entity);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/purchase-requisition/show.html.twig', array(
                'entity' => $entity,
                'attchments'        => $attchments,
                'budgets'        => $budgets,
                'requisitionShares'  => $requisitionShares,
            )
        );
        $entity->setIsRevised(true);
        $entity->setProcess("Reversed");
        $entity->setWaitingProcess("Reversed");
        $entity->setRevisedContent($html);
       // $this->getDoctrine()->getRepository(ProcurementProcess::class)->revisedProcess($entity);
        $em->persist($entity);
        $em->flush();
        return $this->redirectToRoute('procure_purchase_order_garment');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/closed", methods={"GET"}, name="procure_purchase_order_garment_closed", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function requisitionClose(Request $request,Requisition $entity): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity->setProcess('Closed');
        $em->persist($entity);
        $em->flush();
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->closeRequisitionItem($entity);
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     * @Route("/item-purchase-status", methods={"GET", "POST"}, name="procure_tender_garments_item_purchase_status")
     */
    public function itemPurchaseStatus(Request $request, ProcurementRepository $procurementRepository, ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository, RequisitionRepository $requisitionRepository, TenderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal->getId());
        $generic = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(ReportFilterFormType::class,null,array('terminal' => $terminal,'config' => $config,'generic' => $generic,'categoryRepo' => $categoryRepo));
        $searchForm -> handleRequest($request);
        return $this->render('@TerminalbdProcurement/garments/purchase-order/requisition-item.html.twig', [
            'pagination' => '',
            'searchForm' => $searchForm->createView(),
        ]);
    }


}
