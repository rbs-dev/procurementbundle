<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\Budget;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use App\Entity\Application\Procurement;
use App\Entity\Core\PercentLabel;
use App\Entity\Core\Profile;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\EmailEvent;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\GoogleMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\BudgetBundle\Entity\BudgetMonthlyAdjusment;
use Terminalbd\BudgetBundle\Entity\BudgetPurchaseRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisition;
use Terminalbd\BudgetBundle\Entity\BudgetRequisitionAmendment;
use Terminalbd\BudgetBundle\Repository\BudgetRequisitionAmendmentRepository;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Form\InventoryFilterFormType;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\InventoryBundle\Repository\StockWearhouseRepository;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\GarmentRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\Garments\PurchaseRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\GarmentsRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionApproveFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\SecurityBillingBundle\Entity\Invoice;
use Terminalbd\SecurityBillingBundle\Repository\CompanyInvoiceTemplateRepository;
use Terminalbd\SecurityBillingBundle\Repository\InvoiceParticularRepository;


/**
 * @Route("/procure/garments/kanban-requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class KanbanRequisitionController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/kanban", methods={"GET", "POST"}, name="procure_requisition_garment_kanban")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function kanban(Request $request, InventoryRepository $inventoryRepository ,GenericMasterRepository $masterRepository, StockWearhouseRepository $repository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genericConfig = $masterRepository->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(InventoryFilterFormType::class , null,array('config'=>$genericConfig,'categoryRepo'=>$categoryRepo));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
            $wearhouse = 0;
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
            $wearhouse = 0;
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            if(isset($data['inventory_filter_form']) and !empty($data['inventory_filter_form']['wearhouse'])){
                $wearhouse = $data['inventory_filter_form']['wearhouse'];
            }else{
                $wearhouse = 0;
            }
        }
        $searchData = array();
        if(isset($data['inventory_filter_form'])){
            $searchData = $data['inventory_filter_form'];
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findByKanbanQuery($config,$data,$mode);
        } else {
            $search = $repository->findByKanbanQuery($config,$data,$mode);
        }
        $pagination = $this->paginate($request,$search);
        $parentLabel = $this->getDoctrine()->getRepository(PercentLabel::class)->findBy(array('status'=>1));
        return $this->render('@TerminalbdProcurement/garments/kanban-requisition/kanban.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'wearhouse' => $wearhouse,
                'parentLabels' => $parentLabel,
                'searchData' => $searchData,
                'selected' => explode(',', $request->cookies->get('kanbanIds', '')),
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{store}/kanban-generate", methods={"GET","POST"}, name="procure_requisition_garment_kanban_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function kanbanGenerate(Request $request, $store , TranslatorInterface $translator,ProcurementRepository $procurementRepository, RequisitionRepository $repository,RequisitionItemRepository $itemRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $data = $request->request->all();
        $user = $this->getUser();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $items = explode(',',$request->cookies->get('kanbanIds'));
        if(is_null($items)) {
            return $this->redirect($this->generateUrl('procure_requisition_garment_kanban'));
        }
        $entity = new Requisition();
        $form = $this->createForm(PurchaseRequisitionFormType::class , $entity,array('config'=>$config));
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $files = $_FILES['files'];
            $entity->setConfig($config);
            $module = "purchase-requisition";
            $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
            if(!empty($moduleProcess)) {
                $entity->setModuleProcess($moduleProcess);
                $entity->setModule($module);
            }
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setRequisitionMode("INV");
            $user = $this->getUser();
            $entity->setCreatedBy($user);
            if($user->getProfile()->getDepartment()){
                $entity->setDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
            $processRepository->requisitionAssignHod($entity);
            $assignUsers = $approvalUserRepository->getApprovalAssignMatrixUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            if(empty($entity->getCreatedBy()->getReportTo())){
                $processRepository->approvalAssign($entity);
            }
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            $repository->inserCanbanItem($entity,$data);
            $itemRepository->getItemSummary($entity);
            $items  = 'kanbanIds';
            unset($items);
            setcookie('kanbanIds', null, -1, '/');
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_garment_edit',array('id'=>$entity->getId()));
        }
        $inventory = $this->getDoctrine()->getRepository(Inventory::class)->config($terminal);
        $items = $this->getDoctrine()->getRepository(StockWearhouse::class)->findByKanbanPurchaseRequisitionItem($inventory,$store,$items);
        return $this->render('@TerminalbdProcurement/garments/kanban-requisition/new.html.twig', [
            'entity'                => $entity,
            'items'                 => $items,
            'store'                 => $store,
            'itemMode'              => $data['itemMode'],
            'form'                  => $form->createView(),
        ]);
    }


}
