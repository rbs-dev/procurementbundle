<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\Procurement;
use App\Entity\Domain\ApprovalUser;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\Vendor;
use App\Event\EmailEvent;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementCondition;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\Tender;
use Terminalbd\ProcurementBundle\Entity\TenderComparative;
use Terminalbd\ProcurementBundle\Entity\TenderComparativeItem;
use Terminalbd\ProcurementBundle\Entity\TenderConditionItem;
use Terminalbd\ProcurementBundle\Entity\TenderVendor;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Form\TenderReceiveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderWorkorderFormType;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeItemAttributeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderComparativeRepository;
use Terminalbd\ProcurementBundle\Repository\TenderItemRepository;
use Terminalbd\ProcurementBundle\Repository\TenderRepository;
use Terminalbd\ProcurementBundle\Repository\TenderVendorRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderRepository;


/**
 * @Route("/procure/garments/work-order")
 * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_PROCUREMENT_TENDER') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class TenderWorkorderController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_tender_garments_workorder")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
      //  $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository))
            ->remove('createdBy')
            ->remove('generalLedger')
            ->remove('branch')
            ->remove('enlistedVendor');
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);

        $data = $_REQUEST;
     //   $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorderRequisition($pagination);
        $grns = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->getReceiveOrders($pagination);
        return $this->render('@TerminalbdProcurement/garments/workorder/index.html.twig', [
            'pagination' => $pagination,
            'workOrders' => '',
            'grns' => $grns,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * @Route("/update-requisition", methods={"GET", "POST"}, name="procure_tender_garments_workorder_requisition")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function upadteRequisition(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $data = $_REQUEST;
        $search = $repository->updateWorkorderQuery($config,$this->getUser(),$data);
        return $this->redirectToRoute('procure_tender_garments_workorder');
    }

    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_tender_garments_workorder_archived")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository, TenderWorkorderRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $data = $request->request->all();
        $entities = $this->getDoctrine()->getRepository(TenderWorkorder::class)->findBy(array('config' => $config),array('code'=>"DESC"));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , NULL,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository))
            ->remove('createdBy')
            ->remove('generalLedger')
            ->remove('branch')
            ->remove('enlistedVendor');
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "archived";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "archived";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "archived";
        }
        $search = $repository->findSearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        $workOrders = $this->getDoctrine()->getRepository(TenderWorkorder::class)->getTenderWorkorderRequisition($pagination);
        $grns = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->getReceiveOrders($pagination);

        return $this->render('@TerminalbdProcurement/garments/workorder/index.html.twig', [
            'pagination' => $pagination,
            'workOrders' => $workOrders,
            'grns' => $grns,
            'mode' => $mode,
            'searchForm' => $searchForm->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/new", methods={"GET"}, name="procure_tender_garments_workorder_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request, TenderComparative $comparative ,ProcurementRepository $procurementRepository,ApprovalUserRepository $approvalUserRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new TenderWorkorder();
        $em = $this->getDoctrine()->getManager();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,'work-order');
        if(!empty($moduleProcess)){
            $entity->setModuleProcess($moduleProcess);
        }
        $entity->setConfig($config);
        $entity->setTenderComparative($comparative);
        $entity->setModule("work-order");
        $entity->setBusinessGroup("garment");
        $entity->setWaitingProcess("New");
        $entity->setProcess("New");
        $date = new \DateTime();
        $date1 = new \DateTime();
        $date2 = new \DateTime();
        $date1->add(new \DateInterval('P15D'));
        $date2->add(new \DateInterval('P30D'));
        $deliveryDate = $date1->format('Y-m-d');
        $validateDate = $date2->format('Y-m-d');
        $entity->setWorkorderMode('PO-01');
        $entity->setWorkorderDate($date);
        $entity->setDeliveryDate($date1);
        $entity->setValidateDate($date2);
        $entity->setPaymentMode($comparative->getTender()->getPaymentMode());
        if($comparative->getTender()->getRequisition()){
            $entity->setRequisition($comparative->getTender()->getRequisition());
        }
        if($comparative->getProcess() == 'Direct Workorder'){
            $tenderVendor = $em->getRepository(TenderVendor::class)->findOneBy(array('tender' => $comparative->getTender(),'isDirect'=>1));
            $vendorItems = $em->getRepository(TenderComparativeItem::class)->existVendorItems( $comparative->getId(),$tenderVendor->getId());
            if($vendorItems > 0){
                $entity->setTenderVendor($tenderVendor);
                $entity->setIsDirect(true);
                $em->getRepository(TenderWorkorderItem::class)->insertGarmentWorkorderItem($entity);
            }else{
                $message = "This CS is not available items";
                $this->addFlash('notice', $message);
                return $this->redirectToRoute('procure_tender_garments_comparative');
            }
        }
        $entity->setBranch($comparative->getTender()->getRequisition()->getCompanyUnit());
        $entity->setShipTo($comparative->getTender()->getShipTo());
        $entity->setWearhouse($comparative->getTender()->getShipTo());
        $user = $this->getUser();
        $entity->setCreatedBy($user);
        $em->persist($entity);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($entity);
        if($entity->getModuleProcess()->isStatus() == 1){
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
        }
        return $this->redirectToRoute('procure_tender_garments_workorder_edit',array('id' => $entity->getId()));
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_tender_garments_workorder_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, EventDispatcherInterface $eventDispatcher , TenderWorkorder $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $data = $request->request->all();
        $form = $this->createForm(TenderWorkorderFormType::class , $entity,array('config' => $config,'tenderComparative' => $tenderComparative));
        $form->handleRequest($request);
        if ($entity and $this->getUser()->getId() != 129 and in_array($entity->getWaitingProcess(),['Approved']) ) {
            $message = "Error message: You are not authorized to access this requisition";
            $this->addFlash('notice', $message);
            return $this->redirectToRoute('procure_tender_garments_workorder');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $request->request->all();
            $entity->setWaitingProcess('In-progress');
            $em = $this->getDoctrine()->getManager();
            $em->flush();
            $processRepository->approvalAssign($entity);
            if($entity->getWorkorderMode() == "PO-01" and $entity->getWearhouse()){
                $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateWearhouse($entity);
            }
            if($entity->getWaitingProcess() == "Approved"){
                $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateTenderItemDetails($entity);
                $this->getDoctrine()->getRepository(StockBook::class)->updateItemPrice($entity);
            }
            $this->getDoctrine()->getRepository(TenderConditionItem::class)->insertWorkorderConditionItem($entity,$data);
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_workorder')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_tender_garments_workorder');
        }
        $vendorItems = "";
        if($entity->getTenderVendor()){
            $vendorItems = $this->getDoctrine()->getRepository(TenderComparativeItem::class)->getTenderVendorItems($entity->getTenderVendor());
        }
        return $this->render('@TerminalbdProcurement/garments/workorder/new.html.twig', [
            'entity'            => $entity,
            'vendorItems'       => $vendorItems,
            'form'              => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_tender_garments_workorder_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function ajaxUpdate(Request $request, ProcurementRepository $procurementRepository, TenderWorkorder $entity): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $data = $request->request->all();
        $form = $this->createForm(TenderWorkorderFormType::class , $entity,array('config' => $config,'tenderComparative'=>$tenderComparative));
        $form->handleRequest($request);
        $entity->setAdditionalDiscount($data['additionalDiscount']);
        $this->getDoctrine()->getManager()->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($entity);
        $reponse = $this->returnResultData($entity->getId());
        return new Response(json_encode($reponse));

    }



    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="procure_tender_garments_workorder_process")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request,EventDispatcherInterface $eventDispatcher , TenderWorkorder $entity ,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $tenderComparative = $entity->getTenderComparative();
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_tender_garments_workorder')}?requisition_filter_form[invoice]={$entity->getInvoice()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                //if($entity->getWaitingProcess() == "Approved" and $entity->getTenderComparative()->getTender()->getRequisition()->getRequisitionMode() != "CAPEX"){
                if($entity->getWaitingProcess() == "Approved"){
                    $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateTenderItemDetails($entity);
                    $this->getDoctrine()->getRepository(StockBook::class)->updateItemPrice($entity);
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_tender_garments_workorder',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/workorder/process.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView()
        ]);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/vendor", methods={"GET","POST"}, name="procure_tender_garments_workorder_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function vendorUpdate(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['vendor'];
        $vendor = $this->getDoctrine()->getRepository(TenderVendor::class)->find($v);
        $workorder->setTenderVendor($vendor);
        $em->flush();
        $em->getRepository(TenderWorkorderItem::class)->insertGarmentWorkorderItem($workorder);
      //  $em->getRepository(TenderWorkorderItem::class)->insertGarmentWorkorderPrItem($workorder);
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($workorder);
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/workorder-mode", methods={"GET","POST"}, name="procure_tender_garments_workorder_mode" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function workorderModeUpdate(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['mode'];
        $workorder->setWorkorderMode($v);
        $em->flush();
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/send-toVendor", methods={"GET","POST"}, name="procure_tender_garments_workorder_sendto_vendor" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function sendToWorkorderMail(Request $request, TenderWorkorder $workorder): Response
    {
        $em = $this->getDoctrine()->getManager();
        if($workorder->getProcess() == "Approved"){
            $workorder->setProcess('Send to Vendor');
            $workorder->setSendToVendor(1);
        }
        $em->flush();
        if($workorder->getRequisition() and $workorder->getRequisition()->getRequisitionMode() =="OPEX"){
           // $this->getDoctrine()->getRepository(Requisition::class)->workorderwithBudgetExpense($workorder);
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
        //return $this->redirectToRoute('procure_tender_garments_workorder_archived',['mode'=>'send-to-vendor']);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{invoice}/{id}/item-update", methods={"GET","POST"}, name="procure_tender_garments_workorder_item_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function itemUpdate(Request $request, TenderWorkorder $invoice , TenderWorkorderItem $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $quantity = floatval($data['quantity']);
        $price = floatval($data['price']);
        $discount = floatval($data['discount']) ;
        $vat = floatval($data['vat']);
        $shippingCharge = floatval($data['shippingCharge']);
        $commission = floatval($data['commission']);
        $description = isset($data['description']) ? $data['description']:'';
        $wearhouseId = (int)$data['wearhouse'];
        if($wearhouseId){
            $wearhouse = $this->getDoctrine()->getRepository(Branch::class)->find($wearhouseId);
            $attribute->setWearhouse($wearhouse);
        }
        $attribute->setPrice($price);
        $attribute->setUnitPrice($price);
        $attribute->setQuantity($quantity);
        $attribute->setRemaining($quantity);
        $attribute->setDiscount($discount);
        $attribute->setVendorCommissionPercent($invoice->getVendorCommissionPercent());
        $attribute->setShippingCharge($shippingCharge);
        $attribute->setVat($vat);
        $attribute->setDescription($description);
        $attribute->setSubTotal($attribute->getQuantity() * $attribute->getPrice());
        $attribute->setTotal($attribute->getSubTotal() - $attribute->getDiscount());
       /* if($invoice->getEnlistedVendor() and $attribute->getVendorCommissionPercent()) {
            $commission = ((floatval($attribute->getTotal()) * floatval($attribute->getVendorCommissionPercent())) / 100);
            $attribute->setVendorCommission($commission);
        }*/
        $attribute->setGrandTotal($attribute->getTotal() + $attribute->getVat() + $attribute->getShippingCharge());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($invoice);
        $entity = $this->getDoctrine()->getRepository(TenderWorkorder::class)->find($invoice);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/workorder/item.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/condition-update", methods={"GET","POST"}, name="procure_tender_garments_workorder_condition" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function conditionUpdate(Request $request, TenderWorkorder $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['condition'];
        /* @var $entity ProcurementCondition */
        $entity = $this->getDoctrine()->getRepository(ProcurementCondition::class)->find($v);
        $tender->setCondition($entity);
        $tender->setContent($entity->getBody());
        $em->flush();
        $this->getDoctrine()->getRepository(TenderConditionItem::class)->initialWorkorderConditionItem($tender);
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/enlisted-update", methods={"GET","POST"}, name="procure_tender_garments_workorder_enlisted" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function enlistedUpdate(Request $request, TenderWorkorder $tender): Response
    {
        $em = $this->getDoctrine()->getManager();
        $v = $_REQUEST['enlisted'];

        $entity = $this->getDoctrine()->getRepository(EnlistedVendor::class)->find($v);
        if($entity){
            $tender->setEnlistedVendor($entity);
            $tender->setVendorCommissionPercent($entity->getCommissionPercent());
            $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($tender);
        }else{
            $tender->setEnlistedVendor(null);
            $tender->setVendorCommissionPercent(0);
            $this->getDoctrine()->getRepository(TenderWorkorderItem::class)->updateGarmentsWorkOrderSummary($tender);
        }
        $em->flush();
        return new Response('success');
    }

    public function returnResultData($id){

        $entity = $this->getDoctrine()->getRepository(TenderWorkorder::class)->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/workorder/item.html.twig', array(
                'entity' => $entity,
            )
        );
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $percent = $entity->getVendorCommissionPercent() > 0 ? $entity->getVendorCommissionPercent() : 0;
        $commission = $entity->getVendorCommission() > 0 ? $entity->getVendorCommission() : 0;
        $discount = $entity->getDiscount() > 0 ? $entity->getDiscount() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $netTotal = $entity->getNetTotal() > 0 ? $entity->getNetTotal() : 0;
        $grandTotal = $entity->getGrandTotal() > 0 ? $entity->getGrandTotal() : 0;
        $data = array(
            'subTotal' => $subTotal,
            'vat' => $vat,
            'percent' => $percent,
            'commission' => $commission,
            'discount' => $discount,
            'total' => $total,
            'netTotal' => $netTotal,
            'grandTotal' => $grandTotal
        );
        return $data;

    }


     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/attribute-update", methods={"GET","POST"}, name="procure_tender_garments_workorder_attribute_update" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function attributeUpdate(Request $request, TenderComparativeItemAttribute $attribute ): Response
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $attribute->setMetaValue($data['description']);
        $em->flush();
        return new Response('success');
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_tender_garments_workorder_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show(TenderWorkorder $entity): Response
    {
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/workorder/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_tender_garments_workorder_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete(TenderWorkorder $entity): Response
    {
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find TenderWorkorder entity.');
        }
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/workorder/show.html.twig', array(
                'entity' => $entity,
            )
        );
        $em = $this->getDoctrine()->getManager();
        $this->getDoctrine()->getRepository(Tender::class)->resetPurchaseWorder($entity->getTenderComparative()->getTender());
        $response = 'valid';
        return new Response($response);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_tender_garments_workorder_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status(TenderWorkorder $entity): Response
    {
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setRecurring(false);
        }else{
            $entity->setRecurring(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_tender_garments_workorder_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(TenderWorkorder $entity): Response
    {

        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/workorder/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/workorder/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/workorder/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }

}
