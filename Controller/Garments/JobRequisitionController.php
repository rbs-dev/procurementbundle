<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\CommentCreatedEvent;
use App\Event\EmailEvent;
use App\EventListener\EmailListener;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use App\Service\EasyMailer;
use App\Service\FileUploader;
use App\Service\GoogleMailer;
use App\Service\PHPMailer;
use App\Service\SwiftMailer;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\DmsBundle\Entity\ProcurementFile;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Terminalbd\DmsBundle\Entity\DmsFile;
use Terminalbd\DmsBundle\Repository\ProcurementFileRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\BudgetRequisition;
use Terminalbd\ProcurementBundle\Entity\ComapnyRequisitionShare;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\RequisitionDistrubutionCompany;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\ApproveFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionAdditionalItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\BudgetRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ComapnyRequisitionShareRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;


/**
 * @Route("/procure/garments/job-requisition")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class JobRequisitionController extends AbstractController
{
    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_jobrequisition")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request,TranslatorInterface $translator, JobRequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/job-requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_jobrequisition_archived")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator, JobRequisitionRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $settingRepository = $this->getDoctrine()->getRepository(Setting::class);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = "archived";
        $pageMode = array('mode' => $mode);
        $data = array_merge($data,$pageMode);
        $search = $repository->findBySearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/job-requisition/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_jobrequisition_new")
     */
    public function new(Request $request, TranslatorInterface $translator, ProcurementRepository $repository, ProcurementProcessRepository $processRepository, ApprovalUserRepository $approvalUserRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $repository->config($terminal);
        $em = $this->getDoctrine()->getManager();
        $entity = new JobRequisition();
        $entity->setConfig($config);
        $module = "job-approval";
        $user = $this->getUser();
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess) and $user->getProfile() and $user->getProfile()->getDepartment()) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            if(isset($_REQUEST['requisitionMode'])){
                $mode = $_REQUEST['requisitionMode'];
                $entity->setRequisitionMode($mode);
            }
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $entity->setCreatedBy($user);
            if ($user->getProfile() and $user->getProfile()->getDepartment()) {
                $entity->setDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
            $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_jobrequisition_edit', array('id' => $entity->getId()));
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_jobrequisition_edit")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function edit(Request $request, $id, EventDispatcherInterface $eventDispatcher ,TranslatorInterface $translator,UserRepository $userRepository, ProcurementRepository $procurementRepository, JobRequisitionRepository $repository, StockRepository $stockRepository, InventoryRepository $inventoryRepository , ProcurementProcessRepository $processRepository, ModuleProcessItemRepository $processItemRepository,GenericMasterRepository $genericMasterRepository, ApprovalUserRepository $approvalUserRepository , ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $inventory = $inventoryRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity JobRequisition */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $form = $this->createForm(JobRequisitionFormType::class , $entity,array('config'=>$config,'particularRepo' => $particularRepository));
        $form->handleRequest($request);
        $count = $this->getDoctrine()->getRepository(RequisitionItem::class)->count(array('jobRequisition' => $id));
        $assignCount = $this->getDoctrine()->getRepository(ProcurementProcess::class)->count(array('module' => 'job-approval','entityId' => $id));
        $additionalCount = $this->getDoctrine()->getRepository(JobRequisitionAdditionalItem::class)->count(array('jobRequisition' => $id));
        if (($form->isSubmitted() && $form->isValid() and $count > 0 and $assignCount > 0) || ($form->isSubmitted() && $form->isValid() and $additionalCount > 0 and $assignCount > 0)) {
            $data = $request->request->all();
            $this->getDoctrine()->getManager()->flush();
           // $comapnyRequisitionShareRepository->insertJobRequisitionShare($entity,$data);
            $comapnyRequisitionShareRepository->insertJobShareSingle($entity,$data);
            $files = !empty($_FILES['files']) ? $_FILES['files']:'';
            if($files){
                $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
            }
            $entity->setTotal($entity->getSubTotal()+$entity->getAdditionalSubTotal());
            $processRepository->approvalAssign($entity);
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_jobrequisition')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            return $this->redirectToRoute('procure_jobrequisition',['mode'=>'list']);
        }
        $itemAdditionalForm = $this->createForm(JobRequisitionAdditionalItemFormType::class, new JobRequisitionAdditionalItem() ,array('config' => $genricConfig));
        $branches = $this->getDoctrine()->getRepository(Branch::class)->findBy(array('terminal' => $terminal,'branchType' => 'branch','status'=>1), array('name'=>'ASC'));
        $requisitionShares = $comapnyRequisitionShareRepository->jobRequisitionShares($entity);
        return $this->render('@TerminalbdProcurement/garments/job-requisition/edit.html.twig', [
            'entity'                => $entity,
            'branches'              => $branches,
            'requisitionShares'     => $requisitionShares,
            'form'                  => $form->createView(),
            'itemAdditionalForm'    => $itemAdditionalForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/ajax-update", methods={"GET", "POST"}, name="procure_jobrequisition_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config =  $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $this->getDoctrine()->getRepository(JobRequisition::class)->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(JobRequisitionFormType::class , $entity,array('config'=>$config,'particularRepo' => $particularRepository));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        if($entity->getCompanyUnit()){
            $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->insertJobRequisitionShareSingle($entity);
        }
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
          //  $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/reverse", methods={"GET"}, name="procure_jobrequisition_garment_reverse" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionReverse(JobRequisition $entity, JobRequisitionRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $requisition = $requisitionRepository->insertReverseRequsition($entity);
        $this->getDoctrine()->getRepository(RequisitionItem::class)->getJobItemSummary($requisition);
        $this->getDoctrine()->getRepository(JobRequisitionAdditionalItem::class)->getItemSummary($requisition);
        return $this->redirectToRoute('procure_jobrequisition_edit',array('id'=>$requisition->getId()));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_jobrequisition_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, EventDispatcherInterface $eventDispatcher , JobRequisitionRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            if(isset($data['approve']) and $data['approve'] == "approve") {
                $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            }
            if(isset($data['reject']) and $data['reject'] == "reject") {
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setComment($comment);
                $entity->setRejectedBy($this->getUser());
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                $this->getDoctrine()->getRepository(RequisitionItemHistory::class)->jobrequisitionItemHistory($this->getUser(),$entity);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_jobrequisition')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_jobrequisition',['mode'=>'approve']);
        }
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $comapnyRequisitionShareRepository->jobRequisitionShares($entity);
        return $this->render('@TerminalbdProcurement/garments/job-requisition/process.html.twig', [
            'entity' => $entity,
            'attchments'        => $attchments,
            'requisitionShares'  => $requisitionShares,
            'form' => $form->createView(),
        ]);
    }



    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_jobrequisition_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, JobRequisitionRepository $repository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $attchments = $repository->getDmsAttchmentFile($entity);
        $requisitionShares = $comapnyRequisitionShareRepository->jobRequisitionShares($entity);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/job-requisition/show.html.twig', array(
                'entity' => $entity,
                'attchments'        => $attchments,
                'requisitionShares'  => $requisitionShares,
            )
        );
        return new Response($html);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_jobrequisition_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview($id, JobRequisitionRepository $repository,ComapnyRequisitionShareRepository $comapnyRequisitionShareRepository): Response
    {
        $entity = $repository->find($id);
        $requisitionShares = $comapnyRequisitionShareRepository->jobRequisitionShares($entity);
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));

        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/job-requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'requisitionShares'  => $requisitionShares,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
            $mpdf = new \Mpdf\Mpdf(
            );
            $html = $this->renderView('@TerminalbdProcurement/garments/job-requisition/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'requisitionShares'  => $requisitionShares,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/job-requisition/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'requisitionShares'  => $requisitionShares,
                    'mode' => 'preview',
                )
            );
        }

    }
    

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/generated", methods={"GET"}, name="procure_jobrequisition_generate" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionGenerated($id, JobRequisitionRepository $repository, RequisitionRepository $requisitionRepository, ApprovalUserRepository $approvalUserRepository, ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $entity = $repository->find($id);
        $remainAmount = ($entity->getTotal() - $entity->getRequisitionAmount() );
        if( $remainAmount > 0){
            $requisition = $requisitionRepository->insertRequsitionFromJobRequisition($entity, $_ENV['FINANCIAL_YEAR']);
            $this->getDoctrine()->getRepository(RequisitionItem::class)->getItemSummary($requisition);
            // $approveUsers = $approvalUserRepository->getDepartmentApprovalAssignUser($terminal->getId(),$requisition);
            // $processRepository->insertProcurementProcessAssign($requisition,$requisition->getModule(),$approveUsers);
            // $processRepository->requisitionAssignHod($requisition);
            // $assignUsers = $approvalUserRepository->getApprovalAssignMatrixUser($terminal,$requisition);
            // $processRepository->insertProcurementRequisitionProcessAssign($requisition,$requisition->getModule(),$assignUsers);
            if(empty($requisition->getCreatedBy()->getReportTo())){
              //  $processRepository->approvalAssign($requisition);
            }
            $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->jobApprovalToRequisitionShares($entity,$requisition);
            return $this->redirectToRoute('procure_requisition_garment_edit',array('id'=>$requisition->getId()));
        }else{
            $message = "This job approval is not sufficent amount of budget";
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_jobrequisition',['mode'=>'list']);
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_jobrequisition_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(JobRequisition::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_jobrequisition_delete")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id): Response
    {

        $em = $this->getDoctrine()->getManager();
         /* @var $entity JobRequisition */
        $entity = $this->getDoctrine()->getRepository(JobRequisition::class)->findOneBy(array('id' => "{$id}"));
        $requisitionShares = $this->getDoctrine()->getRepository(ComapnyRequisitionShare::class)->jobRequisitionShares($entity);
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$id,'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        $html = $this->renderView('@TerminalbdProcurement/garments/job-requisition/show.html.twig',array(
            'entity' => $entity,
            'approvals' => $approvals,
            'requisitionShares'  => $requisitionShares,
            'mode' => "print"
        ));
        $entity->setDeleteContent($html);
        $entity->setIsDelete(1);
        $entity->setDeletedBy($this->getUser());
        $em->flush();
        $this->getDoctrine()->getRepository(JobRequisition::class)->itemDeletes($entity->getId());
        $response = 'valid';
        return new Response($response);
    }

}
