<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Admin\AppModule;
use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use App\Entity\Domain\EmailTemplate;
use App\Entity\Domain\ModuleProcess;
use App\Entity\Domain\ModuleProcessItem;
use App\Entity\User;
use App\Event\EmailEvent;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\ProcurementRepository;
use App\Repository\Domain\ApprovalUserRepository;
use App\Repository\Domain\BranchRepository;
use App\Repository\Domain\ModuleProcessItemRepository;
use App\Repository\UserRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssue;
use Terminalbd\ProcurementBundle\Entity\RequisitionIssueItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionIssueFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionIssueItemFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFilterFormType;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionIssueRepository;


/**
 * @Route("/procure/garments/requisition-issue")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionIssueController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_requisition_garments_issue")
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function index(Request $request, TranslatorInterface $translator, RequisitionIssueRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if(empty($data)) {
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $data = array('mode' => $mode);
        }elseif(!empty($data) and empty(isset($data['mode']))){
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
            $pageMode = array('mode'=> $mode);
            $data = array_merge($data,$pageMode);
        }else{
            $mode = !empty($data['mode']) ? $data['mode'] : "list";
        }
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        } else {
            $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/requisition-issue/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Route("/archived", methods={"GET", "POST"}, name="procure_requisition_garments_issue_archived")
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function archived(Request $request, TranslatorInterface $translator, RequisitionIssueRepository $repository , ProcurementRepository $procurementRepository): Response
    {
        $user = $this->getUser();
        $terminal = $user->getTerminal();
        $config = $procurementRepository->config($terminal->getId());
        $particularRepository = $this->getDoctrine()->getRepository(Particular::class);
        $searchForm = $this->createForm(RequisitionFilterFormType::class , null,array('terminal'=>$terminal,'config'=>$config,'particularRepo' => $particularRepository));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        $mode = "archived";
        $pageMode = array('mode' => $mode);
        $data = array_merge($data,$pageMode);
        $search = $repository->findRequisitionSearchQuery($config,$this->getUser(),$data);
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdProcurement/garments/requisition-issue/index.html.twig',
            [
                'pagination' => $pagination,
                'mode' => $mode,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/new", methods={"GET"}, name="procure_requisition_garments_issue_new" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function new(Request $request,TranslatorInterface $translator ,ProcurementRepository $procurementRepository,ApprovalUserRepository $approvalUserRepository ,ProcurementProcessRepository $processRepository): Response
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entity = new RequisitionIssue();
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $entity->setBusinessGroup("garment");
        $user = $this->getUser();
        $entity->setCreatedBy($user);
        $module = "requisition-issue";
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if(!empty($moduleProcess)) {
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            $user = $this->getUser();
            $entity->setCreatedBy($user);
            if ($user->getProfile()->getDepartment()) {
                $entity->setDepartment($user->getProfile()->getDepartment());
            }
            $em->persist($entity);
            $em->flush();
        //    $assignUsers = $approvalUserRepository->getApprovalAssignUser($terminal,$entity);
        //    $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('procure_requisition_garments_issue_edit', array('id' => $entity->getId()));
        }
        $referer = $request->headers->get('referer');
        return new RedirectResponse($referer);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_requisition_garments_issue_edit")
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id , EventDispatcherInterface $eventDispatcher , TranslatorInterface $translator, UserRepository $userRepository,ApprovalUserRepository $approvalUserRepository , ProcurementRepository $procurementRepository, RequisitionIssueRepository $repository,GenericMasterRepository $genericMasterRepository,ProcurementProcessRepository $processRepository, BranchRepository $branchRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();

        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $genricConfig = $genericMasterRepository->config($terminal);

        /* @var $entity RequisitionIssue */

        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(RequisitionIssueFormType::class , $entity,array('config' => $config));
        $form->handleRequest($request);
        $count = $this->getDoctrine()->getRepository(RequisitionIssueItem::class)->count(array('requisition' => $id));
        if ($form->isSubmitted() && $form->isValid() and $count > 0) {
            $data = $request->request->all();
            $processRepository->storeRequisitionAssignHod($entity);
            $processRepository->approvalAssign($entity);
            $assignUsers = $approvalUserRepository->getApprovalAssignUserRequisitionIssue($terminal,$entity);
            $processRepository->insertProcurementProcessAssign($entity,$entity->getModule(),$assignUsers);
            if($entity->getApproveProcess()){
                $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_garments_issue')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
            }
            $entity->setBranch($entity->getWearhouse()->getParent());
            $entity->upload();
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_requisition_garments_issue');
        }
        $itemForm = $this->createForm(RequisitionIssueItemFormType::class, new RequisitionIssueItem(),array('config' => $genricConfig,'issue' => $entity));
        return $this->render('@TerminalbdProcurement/garments/requisition-issue/edit.html.twig', [
            'entity'            => $entity,
            'form'              => $form->createView(),
            'itemForm'          => $itemForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/issue-ajax-update", methods={"GET", "POST"}, name="procure_requisition_issue_ajax_update")
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function ajaxUpadteRequsition(Request $request ,$id)
    {
        $terminal = $this->getUser()->getTerminal();
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config =  $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        /* @var $entity RequisitionIssue */
        $entity = $this->getDoctrine()->getRepository(RequisitionIssue::class)->findOneBy(array('config' => $config,'id' => $id));
        $form = $this->createForm(RequisitionIssueFormType::class , $entity,array('config' => $config));
        $form->handleRequest($request);
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        if($entity->getWearhouse()){
            $entity->setBranch($entity->getWearhouse()->getParent()->getParent());
            $entity->setCompany($entity->getWearhouse()->getParent()->getParent());
        }
        $em->persist($entity);
        $em->flush();
        $files = !empty($_FILES['files']) ? $_FILES['files']:'';
        if($files){
            //  $this->getDoctrine()->getRepository(ProcurementProcess::class)->insertAttachmentFile($entity,$data,$files);
        }
        return new Response('success');
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET","POST"}, name="procure_requisition_garments_issue_process" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function process(Request $request , $id, EventDispatcherInterface $eventDispatcher , RequisitionIssueRepository $repository,TranslatorInterface $translator, ProcurementRepository $procurementRepository, ProcurementProcessRepository $processRepository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity RequisitionIssue */
        $entity = $repository->find($id);
        $form = $this->createForm(ApproveCommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $data = $request->request->all();
            $comment = $form["comment"]->getData();
            $processRepository->insertApprovalProcessComment($this->getUser(),$entity->getId(),$comment);
            if(isset($data['reject']) and $data['reject'] == "reject"){
                $entity->setProcess('Rejected');
                $entity->setWaitingProcess('Rejected');
                $entity->setReportTo(null);
                $entity->setProcessOrdering(0);
                $em->persist($entity);
                $em->flush();
                $message = $translator->trans('data.reject_successfully');
                $this->addFlash('success', $message);
            }else{
                $count = $processRepository->count(array('entityId'=> $entity->getId(), 'module'=>$entity->getModule(),'close'=>1));
                $ordering = ($count == 0) ? 1 : $count+1;
                $processRepository->approvalAssign($entity,$ordering);
                if($entity->getApproveProcess()){
                    $emailTemplate = $this->getDoctrine()->getRepository(EmailTemplate::class)->findEmailTemplate($entity->getModuleProcess());
                    $link ="{$request->getHost()}{$this->generateUrl('procure_requisition_garments_issue')}?requisition_filter_form[requisitionNo]={$entity->getRequisitionNo()}&mode=approve";
                    $emailData = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEmailData($entity,$link);
                    $eventDispatcher->dispatch(new EmailEvent($emailTemplate,$emailData));
                }
                $message = $translator->trans('data.updated_successfully');
                $this->addFlash('success', $message);
            }
            return $this->redirectToRoute('procure_requisition_garments_issue',['mode'=>'approve']);
        }
        return $this->render('@TerminalbdProcurement/garments/requisition-issue/process.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_requisition_garments_issue_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, RequisitionIssueRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-issue/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/preview", methods={"GET"}, name="procure_requisition_garments_issue_preview" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function printPreview(RequisitionIssue $entity): Response
    {
        $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] :'';
        $approvals = $this->getDoctrine()->getRepository(ProcurementProcess::class)->findBy(array('entityId'=>$entity->getId(),'module'=>$entity->getModule()),array('ordering'=>'ASC'));
        if($mode == 'print'){
            return $this->render(
                '@TerminalbdProcurement/garments/requisition-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'print'
                )
            );
        }elseif ($mode == 'pdf'){
             $mpdf = new \Mpdf\Mpdf(['tempDir' => __DIR__ . '/../../../../public/temp']);
            $html = $this->renderView('@TerminalbdProcurement/garments/requisition-issue/pdf.html.twig',array(
                'entity' => $entity,
                'approvals' => $approvals,
                'mode' => "pdf"
            ));
            $mpdf->WriteHTML($html);
            $mpdf->Output();
        }else{
            return $this->render(
                '@TerminalbdProcurement/garments/requisition-issue/preview.html.twig', array(
                    'entity' => $entity,
                    'approvals' => $approvals,
                    'mode' => 'preview'
                )
            );
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_requisition_garments_issue_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_requisition_garments_issue_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, RequisitionIssueRepository $repository,ProcurementRepository $procurementRepository): Response
    {


        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        /* @var $entity RequisitionIssue */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));
        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $html = $this->renderView(
                '@TerminalbdProcurement/garments/requisition-issue/show.html.twig', array(
                    'entity' => $entity,
                )
            );
            $entity->setWaitingProcess("Deleted");
            $entity->setProcess("Deleted");
            $entity->setDeleteContent($html);
            $entity->setIsDelete(1);
            $entity->setDeletedBy($this->getUser());
            $this->getDoctrine()->getManager()->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/process-item-ordering", methods={"GET"}, name="procure_requisition_garments_ordering" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */
    public function invoiceItemOrdering(ProcurementProcessRepository $repository): Response
    {

        $data = $_REQUEST;
        $id = $data['columnId'];
        $sorting = ((integer)$data['sorting']);
        $entity = $repository->find($id);
        $entity->setOrdering($sorting);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * Update a RequisitionItem entity.
     * @Route("/{id}/download-attachment", methods={"GET","POST"}, name="procure_requisition_garments_issue_download_attachment", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT_REQUISITION') or is_granted('ROLE_PROCUREMENT_STORE_ISSUE') or is_granted('ROLE_DOMAIN')")
     */


    public function downloadAttachFileAction(Request $request , Requisition  $requisition)
    {


        $path = $request->headers->get('host')."/public/uploads/";
        $file = $path.$requisition->getFilename();
        if (!empty($requisition->getFilename()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit;
        }
        exit;
    }



}
