<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller\Garments;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Entity\User;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getRequisitionItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\BudgetBundle\Entity\BudgetMonth;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\ProcurementBundle\Entity\JobRequisition;
use Terminalbd\ProcurementBundle\Entity\JobRequisitionAdditionalItem;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionSlip;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;
use Terminalbd\ProcurementBundle\Form\ApproveCommentFormType;
use Terminalbd\ProcurementBundle\Form\Garments\JobRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\RequisitionSlipItemFormType;
use Terminalbd\ProcurementBundle\Form\Garments\TenderReceiveDirectItemFormType;
use Terminalbd\ProcurementBundle\Form\GarmentsRequisitionItemFormType;
use Terminalbd\ProcurementBundle\Form\ProcurementProcessFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionAdditionalItemRepository;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\ProcurementProcessRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionItemRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionSlipRepository;
use Terminalbd\ProcurementBundle\Repository\TenderWorkorderReceiveItemRepository;


/**
 * @Route("/procurement/garments/requisition-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class RequisitionItemController extends AbstractController
{

    /**
     * Show a ApprovalProcess entity.
     *
     * @Route("/{id}/{module}/item-create", methods={"GET","POST"}, name="procure_requisition_garments_garments_item_create" , options={"expose"=true})
     */
    public function itemCreate(Request $request, $id ,$module): Response
    {
        /* @var $config  Procurement */

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $this->getDoctrine()->getRepository(Procurement::class)->config($terminal);
        $genricConfig = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal);
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        if(in_array($entity->getModule() ,array('job-approval','job-requisition'))){
            $mode = $entity->getRequisitionMode();
            $form = $this->createForm(JobRequisitionItemFormType::class, null,array('config' => $genricConfig,'mode' => $mode,'entity' => $entity));
        }elseif(in_array($entity->getModule() ,array('purchase-requisition'))){
            $mode = $entity->getRequisitionMode();
            $form = $this->createForm(RequisitionItemFormType::class, null,array('config' => $genricConfig,'mode' => $mode,'entity' => $entity));
        }else{
            $form = $this->createForm(RequisitionItemFormType::class, null,array('config' => $genricConfig,'mode' => '','entity' => $entity));
        }
        $form->handleRequest($request);
        $budgets ="";
        if($entity->getModule()  == "purchase-requisition" and in_array($entity->getRequisitionMode(),array('OPEX',"Expense"))){
            $budgets = $this->getDoctrine()->getRepository(Requisition::class)->requisitionBudgetGenerate($entity);
        }
        $units = $this->getDoctrine()->getRepository(ItemUnit::class)->findAll();
        return $this->render('@TerminalbdProcurement/garments/requisition-item/item-form.html.twig', [
            'entity'                => $entity,
            'units'                 => $units,
            'budgets'               => $budgets,
            'itemForm'              => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{module}/save-item", methods={"GET","POST"}, name="procure_requisition_garments_save_item" , options={"expose"=true})
     */

    public function saveItem(Request $request, $id,$module): Response
    {
        $data = $request->request->all();
        $entity = $this->getDoctrine()->getRepository(ProcurementProcess::class)->getEntityCheck($id,$module);
        $itemRepository = $this->getDoctrine()->getRepository(RequisitionItem::class);
        if($entity->getModule() == 'job-approval'){
            $purchaseData = $data['job_requisition_item_form'];
            $itemRepository->insertJobRequisitionItem($entity,$purchaseData);
            $itemRepository->getJobItemSummary($entity);
            $reponse = $this->returnJobResultData($entity->getId());
        }elseif($entity->getModule() == 'purchase-requisition'){
            $purchaseData = $data['requisition_item_form'];
            $itemRepository->insertGarmentRequisitionItem($entity,$purchaseData);
            $itemRepository->getItemSummary($entity);
            $reponse = $this->returnPurchaseRequisitionResultData($entity->getId());
        }
        return new Response(json_encode($reponse));
    }



    /**
     * Update a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/update-item", methods={"GET","POST"}, name="procure_requisition_garments_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request ,$entity,$id,$module,RequisitionItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionItem */
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item = $itemRepository->find($id);
        $item->setApproveQuantity($item->getQuantity());
        $remark = (isset($data['remark']) and $data['remark']) ? $data['remark']: '';
        if($remark and $remark != 'undefined'){ $item->setRemark($remark); }
        $description = (isset($data['description']) and $data['description']) ? $data['description']: '';
        if($description and $description != 'undefined'){
            $item->setDescription($description);
        }
        $item->setQuantity(abs($data['quantity']));
        $item->setRemainigQuantity(abs($data['quantity']));
        if(isset($data['price']) and $data['price']){
            $item->setPrice(abs($data['price']));
        }
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $this->getDoctrine()->getManager()->flush();
        if($module == "job-approval"){
            $requisition = $em->getRepository(JobRequisition::class)->find($entity);
            $itemRepository->getJobItemSummary($requisition);
            $return = $this->returnJobResultData($requisition->getId());
        }elseif($module == "purchase-requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnPurchaseRequisitionResultData($requisition->getId());
            $em->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($requisition,$_ENV['FINANCIAL_YEAR']);
            //$this->getDoctrine()->getRepository(BudgetMonth::class)->processRequisitionItemExpense($requisition);

        }
        return new Response(json_encode($return));

    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/update-slip-item", methods={"GET","POST"}, name="procure_requisition_slip_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateSlipItem(Request $request ,$entity,$id,$module,JobRequisitionAdditionalItemRepository $itemRepository): Response
    {

        /* @var $item  RequisitionItem */
        $em = $this->getDoctrine()->getManager();
        $data = $request->request->all();
        $item = $itemRepository->find($id);
        $description = (isset($data['description']) and $data['description']) ? $data['description']: '';
        if($description and $description != 'undefined'){
            $item->setDescription($description);
        }
        if(isset($data['price']) and $data['price']){
            $item->setPrice(abs($data['price']));
        }
        $item->setSubTotal($item->getQuantity() * $item->getPrice());
        $this->getDoctrine()->getManager()->flush();
        $requisition = $em->getRepository(RequisitionSlip::class)->find($entity);
        $itemRepository->getSlipItemSummary($requisition);
        $subTotal = $requisition->getSubTotal() > 0 ? $requisition->getSubTotal() : 0;
        $total = $requisition->getTotal() > 0 ? $requisition->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-slip/requisitionSlipItemUpdate.html.twig', array(
                'entity' => $requisition,
            )
        );
        $return = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return new Response(json_encode($return));

    }



    /**
     * @Route("/{id}/item-inline-update", methods={"GET", "POST"}, name="procu_item_inline_update", options={"expose"=true})
     * @Security("is_granted('ROLE_USER') or is_granted('ROLE_DOMAIN')")
     */
    public function inlineItemUpdate(Request $request,RequisitionItem $item)
    {

        $data = $request->request->all();
        if($data['value']){
            $fieldName = $data['name'];
            $setValue = $data['value'];
            $item->$fieldName($setValue);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/{module}/delete-item", methods={"GET"}, name="procure_requisition_garments_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, $module ,RequisitionItemRepository $itemRepository): Response
    {
        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        if($item->getBudgetHead()){
            $_SESSION["budgethead"] = $item->getBudgetHead()->getId();
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        if($module == "job-approval"){
            $requisition = $em->getRepository(JobRequisition::class)->find($entity);
            $itemRepository->getJobItemSummary($requisition);
            $return = $this->returnJobResultData($requisition->getId());
        }elseif($module == "purchase-requisition"){
            $requisition = $em->getRepository(Requisition::class)->find($entity);
            $itemRepository->getItemSummary($requisition);
            $return = $this->returnPurchaseRequisitionResultData($requisition->getId());
            if(in_array($requisition->getRequisitionMode(),['OPEX','Expense']) and $_SESSION["budgethead"]){
                $financialYear =  $_ENV['FINANCIAL_YEAR'];
                $em->getRepository(RequisitionBudgetItem::class)->deleteRequisitionItemBudgetHead($requisition,$financialYear,$_SESSION["budgethead"]);
            }
        }
        return new Response(json_encode($return));
    }


    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-additional-job-item", methods={"GET","POST"}, name="procure_requisition_garments_jobitem_additional" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addJobItemAdditional(Request $request, JobRequisition $entity, JobRequisitionRepository $jobRequisitionRepository , JobRequisitionAdditionalItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['job_requisition_additional_item_form'];
        $itemRepository->insertJobRequisitionItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnJobAdditionalResultData($jobRequisitionRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }
    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/job-additional-delete-item", methods={"GET"}, name="procure_jobrequisition_garments_additional_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteJobAdditionalItem($entity , $id , JobRequisitionRepository $jobRequisitionRepository, JobRequisitionAdditionalItemRepository $itemRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $requisition = $jobRequisitionRepository->findOneBy(array('id' => "{$entity}"));
        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getItemSummary($requisition);
        $return = $this->returnJobAdditionalResultData($jobRequisitionRepository,$requisition->getId());
        return new Response(json_encode($return));
    }



     /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-requisition-slip-item", methods={"GET","POST"}, name="procure_requisition_slip_item" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function requisitionSlipItem(Request $request, RequisitionSlip $entity, RequisitionSlipRepository $jobRequisitionRepository , JobRequisitionAdditionalItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $item = new JobRequisitionAdditionalItem();
        $form = $this->createForm(RequisitionSlipItemFormType::class, $item);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $item->setRequisitionSlip($entity);
        $quantity = $item->getQuantity() > 0 ? $item->getQuantity():1;
        $item->setQuantity($quantity);
        if($item->getChargeLine() and $item->getChargeLine()->getGeneralLedger()){
            $item->setBudgetHead($item->getChargeLine()->getGeneralLedger());
        }
        $item->setQuantity($quantity);
        $item->setSubTotal($item->getPrice() * $item->getQuantity());
        $em->persist($item);
        $em->flush();
      //  $itemRepository->insertRequisitionSlipItem($entity,$purchaseData);
        $itemRepository->getSlipItemSummary($entity);
        $reponse = $this->returnRequisitionSlipResultData($entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-direct-receive-item", methods={"GET","POST"}, name="procure_direct_receive_item" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function directReceiveItem(Request $request, TenderWorkorderReceive $entity): Response
    {
        $data = $request->request->all();
        $itemData = $data['tender_receive_direct_item_form'];
        $item = new TenderWorkorderReceiveItem();
        $form = $this->createForm(TenderReceiveDirectItemFormType::class, $item);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $item->setWorkorderReceive($entity);
        $quantity = $item->getQuantity() > 0 ? $item->getQuantity():1;
        $item->setQuantity($quantity);
        $item->setSubTotal($item->getPrice() * $item->getQuantity());
        /* @var $stock Stock */
        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $itemData));
        $item->setStock($stock);
        /* @var $stockBook StockBook */
        $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $data);
        $exist = $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->findOneBy(array('workorderReceive' => $entity, 'stockBook' => $stockBook));
        if ($stockBook and empty($exist)) {
            $item->setStockBook($stockBook);
        }
        $em->persist($item);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->getReceiveItemSummary($item->getWorkorderReceive());
        $reponse = $this->returnResultData($item->getWorkorderReceive()->getId());
        return new Response(json_encode($reponse));
    }


    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/goods-receive-delete-item", methods={"GET"}, name="procure_tender_garments_receive_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteGoodsReceiveItem(TenderWorkorderReceive $entity , $id , TenderWorkorderReceiveItemRepository $itemRepository): Response
    {

        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $this->getDoctrine()->getRepository(TenderWorkorderReceiveItem::class)->getReceiveItemSummary($item->getWorkorderReceive());
        $reponse = $this->returnResultData($entity->getId());
        return new Response(json_encode($reponse));
    }

    public function returnResultData($id){

        $entity = $this->getDoctrine()->getRepository(TenderWorkorderReceive::class)->find($id);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $percent = $entity->getVendorCommissionPercent() > 0 ? $entity->getVendorCommissionPercent() : 0;
        $commission = $entity->getVendorCommission() > 0 ? $entity->getVendorCommission() : 0;
        $discount = $entity->getDiscount() > 0 ? $entity->getDiscount() : 0;
        $vat = $entity->getVat() > 0 ? $entity->getVat() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/goods-receive-direct/receive-item.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'invoiceItem' => $html,
            'vat' => $vat,
            'percent' => $percent,
            'commission' => $commission,
            'discount' => $discount,
            'total' => $total
        );
        return $data;

    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{entity}/{id}/requisition-slip-delete-item", methods={"GET"}, name="procure_requisition_slip_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteRequisitionSlipItem(RequisitionSlip $entity , $id , JobRequisitionAdditionalItemRepository $itemRepository): Response
    {

        $item = $itemRepository->findOneBy(array('id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getSlipItemSummary($entity);
        $reponse = $this->returnRequisitionSlipResultData($entity->getId());
        return new Response(json_encode($reponse));
    }






    /**
     * Deletes a RequisitionItem entity.
     * @Route("/search-select2-stock-book-item", methods={"GET"}, name="search_select2_stock_book_item", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function searchStockBookItem()
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $inventory = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal)->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(StockBook::class)->searchStockBookItemAutoComplete($inventory,$item);
        }
        return new JsonResponse($item);
    }

     /**
     * Deletes a RequisitionItem entity.
     * @Route("/search-select2-stock-item", methods={"GET"}, name="search_select2_stock_item", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function searchStockItem()
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $inventory = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal)->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(StockBook::class)->searchItemAutoComplete($inventory,$item);
        }
        return new JsonResponse($item);
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/{id}/{module}/search-select2-stock-item_entity", methods={"GET"}, name="search_select2_stock_item_entity", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function searchStockItemEntity($id,$module)
    {
        $terminal = $this->getUser()->getTerminal()->getId();
        if($module == "job-approval"){
            $entity = $this->getDoctrine()->getRepository(JobRequisition::class)->find($id);
        }else{
            $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($id);
        }
        $item = array('No record found, please select process department');
        $q = trim($_REQUEST['q']);
        if($entity->getProcessDepartment() and $q){
            $department = $entity->getProcessDepartment();
            $mode = $entity->getRequisitionMode();
            $inventory = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal)->getId();
            $item = $this->getDoctrine()->getRepository(StockBook::class)->searchDepartmentItemAutoComplete($inventory,$q,$department,$mode);
        }
        return new JsonResponse($item);
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/search-select2-opex-stock-item", methods={"GET"}, name="search_select2_opex_stock_item", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function searchOpexStockItem()
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $inventory = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal)->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(StockBook::class)->searchItemAutoComplete($inventory,$item,'OPEX');
        }
        return new JsonResponse($item);
    }

    /**
     * Deletes a RequisitionItem entity.
     * @Route("/search-select2-capex-stock-item", methods={"GET"}, name="search_select2_capex_stock_item", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function searchCapexStockItem()
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $inventory = $this->getDoctrine()->getRepository(GenericMaster::class)->config($terminal)->getId();
        $item = trim($_REQUEST['q']);
        if ($item) {
            $item = $this->getDoctrine()->getRepository(StockBook::class)->searchItemAutoComplete($inventory,$item,"CAPEX");
        }
        return new JsonResponse($item);
    }



    public function returnPurchaseRequisitionResultData($requisition){

        /* @var $entity Requisition */
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $budgetHtml = "";
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-item/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $low = $this->getDoctrine()->getRepository(Requisition::class)->checkBudgetLowStatus($entity->getId());
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html,
            'budgetItem' => $budgetHtml,
            'low' => $low
        );
        return $data;

    }

    public function returnJobResultData($requisition){

        $entity = $this->getDoctrine()->getRepository(JobRequisition::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/job-requisition/requisitionItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnJobAdditionalResultData(JobRequisitionRepository $requisitionRepository ,$requisition){

        $entity = $requisitionRepository->find($requisition);
        $subTotal = $entity->getAdditionalSubTotal() > 0 ? $entity->getAdditionalSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/job-requisition/requisitionAdditionalItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnRequisitionSlipResultData($requisition){

        $entity = $this->getDoctrine()->getRepository(RequisitionSlip::class)->find($requisition);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdProcurement/garments/requisition-slip/requisitionAdditionalItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }


    /**
     * Status a Setting entity.
     *
     * @Route("/category-item", methods={"GET","POST"}, name="procure_category_item" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryItem(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];

        /* @var $entity Category */

        $entity = $this->getDoctrine()->getRepository(Category::class)->find($id);
        $selectItem = "";
        $selectItem .= "<option value=''>--- Choose a item name ---</option>";
        if($entity){
            foreach ($entity->getItems() as $item){
                $selectItem .= "<option value='{$item->getId()}'>{$item->getName()}</option>";
            }
        }
        return new Response($selectItem);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/category-requisition-item", methods={"GET","POST"}, name="procure_category_requisition_item" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryRequisitionItem(Request $request , StockRepository $stockRepository): Response
    {
        $id         = $_REQUEST['id'];
        $itemMode   = $_REQUEST['itemMode'];

        $entity = $this->getDoctrine()->getRepository(Item::class)->getStockBookItems($itemMode,$id);
        $selectItem = "";
        $selectItem .= "<option value=''>--- Choose a item name ---</option>";
        if($entity){
            foreach ($entity as $item){
                $selectItem .= "<option value='{$item['id']}'>{$item['name']}</option>";
            }
        }
        return new Response($selectItem);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/department-category", methods={"GET","POST"}, name="procure_department_category_item" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function departmentCategory(Request $request , StockRepository $stockRepository): Response
    {
        $id         = $_REQUEST['id'];
        $entity = $this->getDoctrine()->getRepository(RequisitionItem::class)->getDepartmentCategory($itemMode,$id);
        $selectItem = "";
        $selectItem .= "<option value=''>--- Choose a item name ---</option>";
        if($entity){
            foreach ($entity as $item){
                $selectItem .= "<option value='{$item['id']}'>{$item['name']}</option>";
            }
        }
        return new Response($selectItem);
    }


    /**
     * Status a Setting entity.
     *
     * @Route("/category-attribute", methods={"GET","POST"}, name="procure_category_attribute" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryAttribute(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];

        /* @var $entity Stock */

        $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $selectBrand = "";
        $selectSize = "";
        $selectColor = "";
        if($entity and $entity->getCategory()){
            $category = $entity->getCategory();
            $brands = $category->getBrand();
            $sizes = $category->getSize();
            $colors = $category->getColor();
            $selectBrand = "";
            $selectBrand .= "<option value=''>---Select a Brand---</option>";
            foreach ($brands as $brand){
                $selectBrand .= "<option value='{$brand->getId()}'>{$brand->getName()}</option>";
            }

            $selectSize .= "<option value=''>---Select a Size/Parts ID---</option>";
            foreach ($sizes as $size){
                $selectSize .= "<option value='{$size->getId()}'>{$size->getName()}</option>";
            }

            $selectColor .= "<option value=''>---Select a Color/GG---</option>";
            foreach ($colors as $color){
                $selectColor .= "<option value='{$color->getId()}'>{$color->getName()}</option>";
            }
        }
        $data = array('brand'=>$selectBrand,'size'=>$selectSize,'color'=>$selectColor);
        return new Response(json_encode($data));
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/item-attribute", methods={"GET","POST"}, name="procure_item_attribute" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_GENERIC') or is_granted('ROLE_PROCUREMENT')")
     */
    public function itemAttribute(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];

        /* @var $entity Stock */

        $entity = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $selectCategory = "";
        $selectBrand = "";
        $selectSize = "";
        $selectColor = "";
        if($entity){
            $category = $entity->getCategory();
            $brands = $this->getDoctrine()->getRepository(StockBook::class)->getItemBrands($entity);
            $sizes = $this->getDoctrine()->getRepository(StockBook::class)->getItemSizes($entity);
            $colors = $this->getDoctrine()->getRepository(StockBook::class)->getItemColors($entity);
            $selectBrand .= "<option value=''>---Select a Brand---</option>";
            foreach ($brands as $brand){
                $selectBrand .= "<option value='{$brand['id']}'>{$brand['name']}</option>";
            }
            $selectSize .= "<option value=''>---Select a Size/Parts ID---</option>";
            foreach ($sizes as $size){
                $selectSize .= "<option value='{$size['id']}'>{$size['name']}</option>";
            }
            $selectColor .= "<option value=''>---Select a Color/GG---</option>";
            foreach ($colors as $color){
                $selectColor .= "<option value='{$color['id']}'>{$color['name']}</option>";
            }
        }
   
        $data = array('brand'=>$selectBrand,'size'=>$selectSize,'color'=>$selectColor);
        return new Response(json_encode($data));
    }

}
