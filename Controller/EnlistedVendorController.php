<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\ProcurementBundle\Controller;

use App\Entity\Application\Procurement;
use App\Repository\Application\ProcurementRepository;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\ProcurementBundle\Entity\EnlistedVendor;
use Terminalbd\ProcurementBundle\Entity\Particular;
use Terminalbd\ProcurementBundle\Form\EnlistedVendorFormType;
use Terminalbd\ProcurementBundle\Form\ParticularFomFormType;
use Terminalbd\ProcurementBundle\Form\ParticularFormType;
use Terminalbd\ProcurementBundle\Form\RequisitionFormType;
use Terminalbd\ProcurementBundle\Repository\EnlistedVendorRepository;
use Terminalbd\ProcurementBundle\Repository\ParticularRepository;


/**
 * @Route("/procure/enlisted-vendor")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class EnlistedVendorController extends AbstractController
{


    /**
     * @Route("/", methods={"GET", "POST"}, name="procure_enlisted_vendor")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_PROCUREMENT_ADMIN')")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository , EnlistedVendorRepository $repository): Response
    {
        /* @var $config  Procurement */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entities = $this->getDoctrine()->getRepository(EnlistedVendor::class)->findBy(['config'=>$config],['name'=>'ASC']);
        $entity = new EnlistedVendor();
        $data = $request->request->all();
        $form = $this->createForm(EnlistedVendorFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $message = $translator->trans('data.created_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_enlisted_vendor');
        }
        return $this->render('@TerminalbdProcurement/vendor/index.html.twig', [
            'entity' => $entity,
            'entities' => $entities,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     * @Route("/new", methods={"GET", "POST"}, name="procure_enlisted_vendor_new")
     */
    public function new(Request $request): Response
    {

        $entity = new EnlistedVendor();
        $data = $request->request->all();

        $form = $this->createForm(EnlistedVendorFormType::class , $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        return $this->render('@TerminalbdProcurement/vendor/index.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET"}, name="procure_enlisted_vendor_show" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id, EnlistedVendorRepository $repository): Response
    {
        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdProcurement/vendor/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="procure_enlisted_vendor_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_CORE') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id, EnlistedVendorRepository $repository): Response
    {
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit", methods={"GET", "POST"}, name="procure_enlisted_vendor_edit")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */
    public function edit(Request $request, $id,TranslatorInterface $translator , ProcurementRepository $procurementRepository , EnlistedVendorRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $data = $request->request->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entities = $this->getDoctrine()->getRepository(EnlistedVendor::class)->findBy(['config'=>$config],['name'=>'ASC']);

        /* @var $entity EnlistedVendor */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $form = $this->createForm(EnlistedVendorFormType::class, $entity)
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $message = $translator->trans('data.updated_successfully');
            $this->addFlash('success', $message);
            return $this->redirectToRoute('procure_enlisted_vendor');
        }
        return $this->render('@TerminalbdProcurement/vendor/index.html.twig', [
            'entity' => $entity,
            'entities' => $entities,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="procure_enlisted_vendor_delete")
     * @Security("is_granted('ROLE_ADMIN') or is_granted('ROLE_DOMAIN')")
     */

    public function delete($id, ProcurementRepository $procurementRepository , EnlistedVendorRepository $repository): Response
    {

        /* @var $config  SecurityBilling */
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);

        /* @var $entity Particular */
        $entity = $repository->findOneBy(array('config' => $config,'id' => $id));

        $em = $this->getDoctrine()->getManager();
        $response = "invalid";
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Particular entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            $response = 'valid';

        } catch (ForeignKeyConstraintViolationException $e) {
            $this->get('session')->getFlashBag()->add(
                'notice',"Data has been relation another Table"
            );
        }catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add(
                'notice', 'Please contact system administrator further notification.'
            );
        }
        return new Response($response);
    }



}
