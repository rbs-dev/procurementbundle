<?php
/**
 * Created by PhpStorm.
 * User: shafiq
 * Date: 9/29/19
 * Time: 9:09 PM
 */

namespace Terminalbd\ProcurementBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Terminalbd\DmsBundle\Repository\DmsFileRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Terminalbd\ProcurementBundle\Repository\JobRequisitionRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionOrderRepository;
use Terminalbd\ProcurementBundle\Repository\RequisitionRepository;

class DefaultController extends AbstractController
{


    /**
     * @Route("procurement", name="procure_index")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    function index(RequisitionRepository $requisitionRepository, RequisitionOrderRepository $requisitionOrderRepository, JobRequisitionRepository $repository) {
        $user = $this->getUser()->getId();
        $jobcount = $repository->count(['reportTo'=> $user]);
        $reqcount = $requisitionRepository->count(['reportTo'=> $user]);
        $ordercount = $requisitionOrderRepository->count(['reportTo'=> $user]);
        return $this->render('@TerminalbdProcurement/default/dashboard.html.twig', [
            'jobcount'=> $jobcount,
            'reqcount'=> $reqcount,
            'ordercount'=> $ordercount
        ]);
    }


    /**
     * Update a RequisitionItem entity.
     * @Route("/procurement/{id}/download-attachment", methods={"GET","POST"}, name="procurement_download_attachment", options={"expose"=true})
     */

    public function downloadAttachFile(Request $request,$id, DmsFileRepository $dmsFileRepository)
    {

        $entity =  $dmsFileRepository->find($id);
        $filepath = $this->get('kernel')->getProjectDir() . "/public/uploads/budget/{$entity->getFilename()}";
        if (!empty($entity->getFilename()))
        {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filepath));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            ob_clean();
            flush();
            readfile($filepath);
        }
        exit;
    }

}