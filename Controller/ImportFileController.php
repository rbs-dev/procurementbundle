<?php

namespace Terminalbd\ProcurementBundle\Controller;


use App\Entity\Core\Setting;
use App\Entity\Domain\ModuleProcess;
use App\Repository\Application\ProcurementRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\ExpenseImportFile;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionBudgetItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Form\ExpenseImportFileFormType;

/**
 * @Route("/procure/expense/import-file")
 * @Security("is_granted('ROLE_PROCUREMENT_BILL_ENTRY') or is_granted('ROLE_DOMAIN')")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */

class ImportFileController extends AbstractController
{
    /**
     * @Security("is_granted('ROLE_PROCUREMENT_BILL_ENTRY') or is_granted('ROLE_DOMAIN')")
     * @Route("/", methods={"GET", "POST"}, name="expense_importfile")
     */
    public function index(Request $request, TranslatorInterface $translator,ProcurementRepository $procurementRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $entities = $this->getDoctrine()->getRepository(Requisition::class)->findBy(['config' => NULL],['created'=>'DESC']);
        $entity = new Requisition();
        $data = $request->request->all();
        $form = $this->createForm(ExpenseImportFileFormType::class, $entity,['terminal' => $terminal])->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $module = "purchase-requisition";
        $moduleProcess = $this->getDoctrine()->getRepository(ModuleProcess::class)->existModuleProcess($terminal,$module);
        if ($moduleProcess && $form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $attachFile = $form->get('file')->getData();
            if ($attachFile and $entity->getPath()) {
                $entity->removeUpload();
            }
            $entity->setCreatedBy($this->getUser());
            $entity->setModuleProcess($moduleProcess);
            $entity->setModule($module);
            $entity->setWaitingProcess("New");
            $entity->setProcess("New");
            if($this->getUser()->getProfile()->getDepartment()){
                $entity->setDepartment($this->getUser()->getProfile()->getDepartment());
            }
            $entity->setFinancialYear($_ENV['FINANCIAL_YEAR']);
            $entity->setBillMonth("Previous");
            $lastmonth = date("Y-m-d H:i:s", strtotime("-1 month"));
            $date = new \DateTime($lastmonth);
            $entity->setCreated($date);
            if ($entity->getCreated()->format('F') == "June") {
                $entity->setFinancialYear($_ENV['OLD_FINANCIAL_YEAR']);
            }
            $entity->setStatus(false);
            $entity->upload();
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success', $translator->trans('data.created_successfully'));
            return $this->redirectToRoute('expense_importfile');
        }
        return $this->render('@TerminalbdProcurement/import-file/index.html.twig', [
            'entity' => $entity,
            'entities' => $entities,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/delete", methods={"GET"}, name="expense_importfile_delete")
     * @Security("is_granted('ROLE_PROCUREMENT_BILL_ENTRY') or is_granted('ROLE_DOMAIN')")
     */
    public function delete($id, TranslatorInterface $translator): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(Requisition::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find the file.');
        }
        try {
            $entity->removeUpload();
            $em->remove($entity);
            $em->flush();
            $this->addFlash('success', $translator->trans('data.deleted_successfully'));
            return $this->redirectToRoute('expense_importfile');

        } catch (ForeignKeyConstraintViolationException $e) {
            return new Response('Can not delete! Data has dependency.');
        }
        return $this->redirectToRoute('expense_importfile');
    }

    /**
     * @Security("is_granted('ROLE_PROCUREMENT_BILL_ENTRY') or is_granted('ROLE_DOMAIN')")
     * @Route("/{id}/import", methods={"GET", "POST"}, name="expense_importfile_process")
     */
    public function import(Requisition $entity,ProcurementRepository $procurementRepository,TranslatorInterface $translator)
    {
        set_time_limit(0);
        ignore_user_abort(true);
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $procurementRepository->config($terminal);
        $fileName = $entity->getPath();
        $uploadDir = $this->get('kernel')->getProjectDir() . '/public/uploads/procurement/';
        $reader = new Xlsx();
        $spreadSheet = $reader->load($uploadDir . $fileName);
        $excelSheet = $spreadSheet->getActiveSheet();
        $allData =($excelSheet->toArray());
        $firstRow = array_shift($allData);
        $keys = array_map('trim',array_filter($firstRow));
        $limit = (int) count($keys);
        $terminal = $this->getUser()->getTerminal()->getId();
        $this->expenseItemImport($entity,$allData,$keys,$limit);
        $amount = $this->getDoctrine()->getRepository(RequisitionItem::class)->getItemSummary($entity);
        $entity->setStatus(1);
        $entity->setConfig($config);
        $em = $this->getDoctrine()->getManager();
        $em->persist($entity);
        $em->flush();
        $financialYear =  $_ENV['FINANCIAL_YEAR'];
        $this->getDoctrine()->getRepository(RequisitionBudgetItem::class)->insertBudgetItem($entity,$financialYear);
        $this->addFlash('success', $translator->trans('data.migrated_successfully'));
        return $this->redirectToRoute('procure_requisition_garment_edit', array('id' => $entity->getId()));

    }

    public function expenseItemImport(Requisition $entity,$allData,$keys,$limit)
    {
        $em = $this->getDoctrine()->getManager();
        $terminal = $this->getUser()->getTerminal()->getId();
        $qb = $em->createQueryBuilder();
        $remove = $qb->delete(RequisitionItem::class, 'e')->where('e.requisition = ?1')->setParameter(1, $entity->getId())->getQuery();
        if($remove){ $remove->execute();}
        $remove1 = $qb->delete(RequisitionBudgetItem::class, 'e')->where('e.requisition = ?1')->setParameter(1, $entity->getId())->getQuery();
        if($remove1){ $remove1->execute();}
        foreach ($allData as $key => $row) {
            $data = array_combine($keys, array_slice($row,null,$limit));
            $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy([ 'name' => $data['Category']]);
            $productItem = $this->getDoctrine()->getRepository(Item::class)->findOneBy(['name' => $data['ItemName']]);
            if (empty($count) && $productItem ) {
                $item = new RequisitionItem();
                $item->setConfig($entity->getConfig());
                $item->setRequisition($entity);
                $item->setItem($productItem);
                $item->setOrdering($key);
                $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $productItem));
                if($stock){
                    $item->setStock($stock);
                }
                /* @var $stockBook StockBook */
                $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock, $array=[]);
                if($stockBook){
                  $item->setStockBook($stockBook);
                }
                if ($category) {
                    $item->setCategory($category);
                    if($category->getGeneralLedger()){
                        $item->setBudgetHead($category->getGeneralLedger());
                    }
                }
                if (isset($data['Section']) and $data['Section']) {
                    $section = $em->getRepository(Setting::class)->findOneBy(['name' => $data['Section']]);
                    if($section){
                        $item->setSection($section);
                    }
                }
                $item->setCreated($entity->getCreated());
                if (isset($data['Price']) and $data['Price']) {
                    $item->setPrice(abs($data['Price']));
                }
                if (isset($data['Quantity']) and $data['Quantity']) {
                    $item->setQuantity(abs($data['Quantity']));
                }
                $item->setSubTotal($item->getQuantity() * $item->getPrice());
                if (isset($data['Description']) and $data['Description']) {
                    $item->setDescription($data['Description']);
                }
                if (isset($data['Remark']) and $data['Remark']) {
                    $item->setRemark($data['Remark']);
                }
                if (isset($data['UOM']) and $data['UOM']) {
                    $item->setUom($data['UOM']);
                }
                $em->persist($item);
            }
        }
        $em->flush();
    }
}


